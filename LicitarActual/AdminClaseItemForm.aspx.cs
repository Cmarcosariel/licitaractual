﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminClaseItemForm : Cascara
    {

        string valortmp;

        protected void Page_Init(object sender, EventArgs e)
        {
            cargarData();
        }


        protected void Page_Load(object sender, EventArgs e)
        {


            validarAcceso("NEG004");
            btnGuardar.Text = Traducir("GUARDAR");
            btnagregarpropiedad123.Text = Traducir("AGREGAR");
            txtAreaDescripcion.Attributes.Add("PlaceHolder", Traducir("AGREGAR_SEPARADO_X_;"));
            PH_NombreProp.Visible = true;
            RequiredFieldValidator_iptDescripcion.ErrorMessage = Traducir("CAMPO_REQUERIDO");

            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarDropdownRubros();
                //cargarData();
                PH_NombreProp.Visible = true;
            }
        }

        protected void cargarData()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int idOrg = Convert.ToInt32(Request.QueryString["id"]);

                ddRubros.Enabled = false;

                BLL.Clase_ItemBLL Licbll = new BLL.Clase_ItemBLL();

                BE.Clase_ItemBE licitacionBE = new BE.Clase_ItemBE() { Identidad = idOrg };

                BE.Clase_ItemBE licitacion = Licbll.Select(licitacionBE);

                PH_Propiedades.Visible = true;

                if (licitacion != null)
                {
                    using (BLL.RubroBLL bLL = new BLL.RubroBLL())
                    {
                        BE.RubroBE rubro = bLL.GetRubrofromClaseItem(licitacion);
                        ddRubros.SelectedValue = rubro.Identidad.ToString();
                    }


                    CargarPHPropiedades(licitacion);
                    iptDescripcion.Text = licitacion.Descripcion;

                }
            }


        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            BE.Clase_ItemBE org = null;
            BLL.Clase_ItemBLL orgbll = new BLL.Clase_ItemBLL();

            BE.RubroBE rub = new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) };

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                org = orgbll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));
            }

            if (org == null)
            {
                org = new BE.Clase_ItemBE();
                org.Identidad = 0;
            }

            org.Descripcion = iptDescripcion.Text;


            if (org.Identidad == 0)
            {
                orgbll.Insert(org, rub);
                NuevaBitacora((String.Format("Se creo la clase {0}", org.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                BE.Clase_ItemBE clase_ItemBE = orgbll.Select(org, rub);

                Response.Redirect("AdminClaseItemForm.aspx?id=" + clase_ItemBE.Identidad);
            }
            else
            {
                orgbll.Update(org, rub);
                NuevaBitacora((String.Format("Se modifico la clase {0}", org.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
                //Response.Redirect("AdminClaseItemForm.aspx?id=" + org.Identidad);

                Response.Redirect("AdminClaseItem.aspx");


            }

            Response.Redirect("AdminClaseItem.aspx");

        }


        private void cargarDropdownRubros()
        {

            using (BLL.RubroBLL sec = new BLL.RubroBLL())
            {
                ddRubros.DataSource = null;
                ddRubros.DataMember = "Rubro";
                ddRubros.DataValueField = "Identidad";
                ddRubros.DataTextField = "Descripcion";

                ddRubros.DataSource = sec.SelectAll();

                ddRubros.DataBind();



            }

        }

        protected void btnAgregarPropiedad_Click(object sender, EventArgs e)
        {

            PH_CamposProp.Visible = true;
            PH_NombreProp.Visible = true;

        }


        protected void btnagregarpropiedad123_Click(object sender, EventArgs e)
        {
            PH_CamposProp.Visible = true;
            BE.PropiedadItemBE propiedadItemBE = new BE.PropiedadItemBE();

            propiedadItemBE.Nombre = iptPropNombbre.Text;
            iptPropNombbre.Text = "";

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                BE.Clase_ItemBE clase = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

                if (clase.Propiedades.Any(x => x.Nombre == propiedadItemBE.Nombre))
                {
                    return;
                }

                clase.Propiedades.Add(propiedadItemBE);
                bll.Update(clase);
                //CargarPHPropiedades(clase);
                Response.Redirect("AdminClaseItemForm.aspx?id=" + Request.QueryString["id"]);
            }


        }

        public void CargarPHPropiedades(BE.Clase_ItemBE clase)
        {
            PH_NombreProp.Controls.Clear();

            foreach (BE.PropiedadItemBE item in clase.Propiedades)
            {

                DropDownList ddprop = new DropDownList();
                ddprop.ID = "ddl" + item.Nombre;
                ddprop.CssClass = "form-control";

                ddprop.DataSource = item.valores;
                ddprop.DataBind();
                LiteralControl spacer = new LiteralControl("<br />");
                string milbl = string.Format("<label>{0}</label>", item.Nombre);
                LiteralControl literal = new LiteralControl(milbl);

                LinkButton btnAgregarValorAProp = new LinkButton();
                btnAgregarValorAProp.ID = "btnagregarvalor" + item.Nombre;
                btnAgregarValorAProp.Attributes.Add("Class", "glyphicon glyphicon-edit btn btn-info pull-left");

                //btnAgregarValorAProp.OnClientClick = string.Format("DesplegarModalEditProp({0})",item.Nombre);

                btnAgregarValorAProp.Click += new EventHandler(this.btnAgregarValorAProp_Click);

                //btnborrarprop.Click += new EventHandler(this.btnAgregarValorAProp_Click);

                //btnAgregarValorAProp.OnClientClick = "EditarddlProp(this)";


                HtmlGenericControl newControl = new HtmlGenericControl("div");
                newControl.Attributes.Add("class", "form-group col-md-4");

                HtmlGenericControl ddlyeditar = new HtmlGenericControl("div");
                ddlyeditar.Attributes.Add("class", "form-group row");

                HtmlGenericControl divboton = new HtmlGenericControl("div");
                divboton.Attributes.Add("class", "col-md-4");

                HtmlGenericControl divddl = new HtmlGenericControl("div");
                divddl.Attributes.Add("class", "col-md-8");

                divddl.Controls.Add(ddprop);

                divboton.Controls.Add(btnAgregarValorAProp);

                ddlyeditar.Controls.Add(divddl);
                ddlyeditar.Controls.Add(divboton);




                newControl.Controls.Add(spacer);
                newControl.Controls.Add(spacer);
                newControl.Controls.Add(literal);
                newControl.Controls.Add(ddlyeditar);


                PH_NombreProp.Controls.Add(newControl);
            }
        }

        private void btnAgregarValorAProp_Click(object sender, EventArgs e)
        {
            LinkButton botonqmellamo = (System.Web.UI.WebControls.LinkButton)sender;

            string propiedadacambiar = botonqmellamo.ClientID.Substring(27);

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                BE.Clase_ItemBE clase = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

                BE.PropiedadItemBE propiedad = clase.Propiedades.Find(x => x.Nombre == propiedadacambiar);

                miPopUpeditorprop(propiedad);

            }



        }


        public void miPopUpeditorprop(BE.PropiedadItemBE prop)
        {

            CargarDGVModal(prop);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"$('#myModalEditorProp').modal('show');");
            sb.Append(@"</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());
        }


        //protected void DgvPropiedadValores_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    validarAcceso("NEG004");


        //    using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
        //    {
        //        CargarDGVModal(bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) })).Propiedades.Single(x => x.Nombre == titmodalprop.Text));
        //    }


        //    DgvPropiedadValores.EditIndex = e.NewEditIndex;

        //    valortmp = ((GridView)sender).Rows[e.NewEditIndex].Cells[0].Text;
        //}
        //protected void DgvPropiedadValores_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    validarAcceso("NEG004");


        //    BE.Clase_ItemBE clase_ItemBE;

        //    using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
        //    {

        //        clase_ItemBE = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

        //        if (clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Count == DgvPropiedadValores.EditIndex+1)
        //        {

        //        }

        //        //clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.RemoveAll(x => x.ToString() == string.Empty);

        //        clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.RemoveAt(DgvPropiedadValores.EditIndex);

        //        //clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Remove((DgvPropiedadValores.Rows[DgvPropiedadValores.EditIndex].Cells[0].Text));

        //        bll.Update(clase_ItemBE);

        //        if (e.NewValues["test"] != null)
        //        {
        //            clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Insert(DgvPropiedadValores.EditIndex, (e.NewValues["test"].ToString()));

        //            //clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Add((e.NewValues["test"].ToString()));

        //            bll.Update(clase_ItemBE);

        //            NuevaBitacora((String.Format("el usuario {0} Actualizo la Propiedad {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, titmodalprop.Text)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

        //        }

        //        DgvPropiedadValores.EditIndex = -1;
                
        //        //Call ShowData method for displaying updated data  
        //        CargarDGVModal(clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text));
        //    }

        //}
        //protected void DgvPropiedadValores_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{

        //}
        //protected void DgvPropiedadValores_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    BE.Clase_ItemBE clase_ItemBE;

        //    using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
        //    {
               
        //        clase_ItemBE = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));
        //        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  

        //        if (((GridView)sender).Rows[e.RowIndex].Cells[0].Text == "")
        //        {

        //            if (valortmp == "")
        //            {

        //            }

        //            clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.RemoveAt(DgvPropiedadValores.EditIndex);

        //            bll.Update(clase_ItemBE);
        //        }

        //        DgvPropiedadValores.EditIndex = -1;
        //        //Call ShowData method for displaying updated data  
        //        CargarDGVModal(clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text));
        //    }


        //}
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    return;
        //}
        //protected void DgvPropiedadValores_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    validarAcceso("NEG004");
        //    //Finding the controls from Gridview for the row which is going to update  

        //    BE.Clase_ItemBE clase_ItemBE;

        //    using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
        //    {

        //        clase_ItemBE = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

        //        if ((clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Count >= e.RowIndex))
        //        {
        //            clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.RemoveAt(e.RowIndex);
        //        }

        //        //clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.RemoveAt((e.RowIndex));

        //        //clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Remove((DgvPropiedadValores.Rows[e.RowIndex].Cells[0].Text));

        //        bll.Update(clase_ItemBE);

        //        NuevaBitacora((String.Format("el usuario {0} modifico la propiedad {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, titmodalprop.Text)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

        //        DgvPropiedadValores.EditIndex = -1;
        //        CargarDGVModal(clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text));

        //    }

        //    //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
        //    //DgvPropiedadValores.EditIndex = -1;
        //    //Call ShowData method for displaying updated data  

        //}

        //protected void DgvPropiedadValores_RowDataBound(object sender, GridViewRowEventArgs e)
        //{



        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        for (int i = 0; i < e.Row.Cells.Count; i++)
        //        {
        //            e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
        //        }
        //    }
        //}


        public void CargarDGVModal(BE.PropiedadItemBE prop)
        {

            string valores123 = "";

            foreach (var item in prop.valores)
            {
                valores123 += item + ";";
            }

            txtAreaDescripcion.InnerText = valores123;

            //DgvPropiedadValores.DataSource = null;
            //DgvPropiedadValores.AutoGenerateColumns = false;
            //DgvPropiedadValores.DataSource = prop.valores.Select(l => new { test = l }).ToList();
            //DgvPropiedadValores.DataBind();
            titmodalprop.Text = string.Format("{0}", prop.Nombre);

        }

        //protected void btnAgregarRowValor_Click(object sender, EventArgs e)
        //{
        //    validarAcceso("NEG004");
        //    //Finding the controls from Gridview for the row which is going to update  

        //    BE.Clase_ItemBE clase_ItemBE;

        //    using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
        //    {

        //        clase_ItemBE = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

        //        BE.PropiedadItemBE miprop = clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text);

        //        clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text).valores.Add("");

        //        bll.Update(clase_ItemBE);

        //        CargarDGVModal(clase_ItemBE.Propiedades.Single(x => x.Nombre == titmodalprop.Text));

        //        DgvPropiedadValores.EditIndex = DgvPropiedadValores.Rows.Count -1;

        //        DgvPropiedadValores.DataBind();

        //    }

        //    //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  

        //    //Call ShowData method for displaying updated data  

        //}

        protected void btnEliminarProp_Click(object sender, EventArgs e)
        {
            BE.Clase_ItemBE clase_ItemBE;

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                clase_ItemBE = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

                clase_ItemBE.Propiedades.RemoveAll(x => x.Nombre == titmodalprop.Text);

                bll.Update(clase_ItemBE);

                NuevaBitacora((String.Format("el usuario {0} elimino la propiedad {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, titmodalprop.Text)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
            }

            Response.Redirect("AdminClaseItemForm.aspx?id=" + Request.QueryString["id"]);
        }

        protected void btnGuardarTodaLaProp_Click(object sender, EventArgs e)
        {
            string valores123 = "";

            valores123 = txtAreaDescripcion.InnerText;

            BE.Clase_ItemBE clase_ItemBE;

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {

                clase_ItemBE = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

                if (clase_ItemBE.Propiedades.Any(x => x.Nombre == titmodalprop.Text))
                {
                    BE.PropiedadItemBE propiedad = clase_ItemBE.Propiedades.Find(x => x.Nombre == titmodalprop.Text);

                    propiedad.valores.Clear();

                    string[] todaslasprops = valores123.Split(';');

                    foreach (string item in todaslasprops)
                    {
                        if (item != string.Empty)
                        {
                            propiedad.valores.Add(item);
                        }


                    }


                }

                bll.Update(clase_ItemBE);

            }


            Response.Redirect("AdminClaseItemForm.aspx?id=" + Request.QueryString["id"]);







        }


    }
}