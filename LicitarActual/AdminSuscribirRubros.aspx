﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminSuscribirRubros.aspx.cs" Inherits="LicitarActual.AdminSuscribirRubros" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        .hiddencol { display: none; }
    </style>


    <div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="litUsuarios" runat="server"></asp:Literal></strong>
                    <a href="AdminRubrosForm.aspx"  style="display: none" class="btn btn-primary pull-right">
                        <asp:Literal ID="Literal2" runat="server"></asp:Literal></a><br />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="row">
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default bg-panel">
                        <div class="panel-body">

                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon"><%= Traducir("RUBROS") %></div>
                                    <asp:TextBox ID="filtroUsername" runat="server" MaxLength="20" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <asp:Button ID="btnBuscar" runat="server" Text='Buscar' CssClass="btn btn-info" OnClick="btnBuscar_Click" />
                            <a href="/AdminSuscribirRubros.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;<%= Traducir("LIMPIAR") %></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong>  <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_15") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvUsuarios" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None"
                    CssClass="table table-striped table-hover" AllowPaging="True"
                    GridLines="None" PagerStyle-CssClass="pagination-dgv "
                    PagerStyle-HorizontalAlign="Right"
                    DataKeyNames="Identidad"
                    OnRowDataBound="DgvUsuarios_RowDataBound"
                    OnPageIndexChanging="DgvUsuarios_PageIndexChanging"
                    OnRowCancelingEdit="DgvUsuarios_RowCancelingEdit"
                    OnRowDeleting="DgvUsuarios_RowDeleting"
                    OnRowEditing="DgvUsuarios_RowEditing" OnRowUpdating="DgvUsuarios_RowUpdating"
                    PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="Identidad" HeaderText="CODIGO" ItemStyle-CssClass="hiddencol"  HeaderStyle-CssClass="hiddencol" />
                        <asp:BoundField DataField="Descripcion" HeaderText="RUBRO" />
                        <asp:TemplateField ControlStyle-CssClass="" HeaderText="SUSCRIPCION">
                            <ItemTemplate>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                    <label class="btn btn-default" onclick="Suscribir(this,true)">

                                        <input type="radio" name="option1" id="radiosuscribir" runat="server">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                        <%# Traducir("Suscribir") %>
                                    </label>
                                    <label class="btn btn-default" onclick="Suscribir(this,false)">

                                        <input type="radio" name="option2" id="radiodesuscribir" runat="server">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        <%# Traducir("Desuscribir") %>
                                    </label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Identidad" HeaderText="CODIGO" Visible="false" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">&nbsp;</div>
<%--        <div class="col-md-3 pull-right">
            <asp:Button ID="btnExportarExcel" runat="server" OnClick="btnExportarExcel_Click" Text='<%# Traducir("EXPORTAR_EXCEL") %>' CssClass="btn btn-sm btn-primary right" />
            &nbsp; &nbsp;
            <asp:Button ID="btnExportarPDF" runat="server" OnClick="btnExportarPDF_Click" Text='<%# Traducir("EXPORTAR_PDF") %>' CssClass="btn btn-sm btn-primary right" />
        </div>--%>
    </div>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div style="display:none">
        <asp:Button ID="btnSuscribir" Text="text" runat="server" OnClick="btnSuscribir_Click"/>
        <asp:Button ID="btnDesuscribir" Text="text" runat="server" OnClick="btnDesuscribir_Click"/>    
        <asp:HiddenField ID="hdnRubro" runat="server" />
    </div>

    <script type="text/javascript">
        function Suscribir(pepe, suscribe) {
            //obtengo el valor de la primer columna de la row IE:"construccion"
            var lblQueSeClickea = $(pepe);
            var rubroRow = lblQueSeClickea.parent().parent().parent();
            var rubroTextCell = rubroRow.find("td").first();
            var rubroText = rubroTextCell.text();
            console.log(rubroText);
            //obtengo el hidden de la linea 121
            var hdnRubro = document.getElementById('<%=hdnRubro.ClientID %>');
            //seteo el valor en el hidden
            hdnRubro.value = rubroText;

            var btnSuscribir = null;
            if (suscribe) {
                btnSuscribir = document.getElementById('<%=btnSuscribir.ClientID %>');
            } else {
                btnSuscribir = document.getElementById('<%=btnDesuscribir.ClientID %>');
            }
            btnSuscribir.click();
        }
        </script> 

</asp:Content>
