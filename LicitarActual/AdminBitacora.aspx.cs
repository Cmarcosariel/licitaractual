﻿using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminBitacora : Cascara
    {
        SEC.BitacoraSEC bitacoraBll;

        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM005");
            
            bitacoraBll = new SEC.BitacoraSEC();
            btnExportarExcel.Text = Traducir("EXPORTAR_EXCEL");
            btnExportarPDF.Text = Traducir("EXPORTAR_PDF");
            btnBuscar.Text =   Traducir("BUSCAR");
            txtBuscarUsuario.Text = Traducir("USUARIO");
            txtBuscarCategoria.Text = Traducir("CATEGORIA");
            txtBuscarTipoEvento.Text = Traducir("TIPOEVENTO");
            txtLimpiar.Text = Traducir("LIMPIAR");


            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                CargarDatos();
            }
                
        }

        private void CargarDatos()
        {
            IList<BitacoraBE> eventos = bitacoraBll.SelectAll();

            ddlTipoEvento.Items.Clear();
            ddlTipoEvento.Items.Add("");
            ddlTipoEvento.Items.Add(Traducir("EVENTO_INFO"));
            ddlTipoEvento.Items.Add(Traducir("EVENTO_WARN"));
            ddlTipoEvento.Items.Add(Traducir("EVENTO_ERR"));



            using (SEC.BitacoraSEC sec = new SEC.BitacoraSEC())
            {
                ddlCategoria.AppendDataBoundItems = true;
                ddlCategoria.Items.Clear();
                ddlCategoria.Items.Add("");
                ddlCategoria.DataSource = sec.GetCategoriasEventoBitacora();
                ddlCategoria.DataBind();
                
            }


            IList<UsuarioBE> usuarios = eventos.Select(x => x.Usuario).ToList();
            usuarios = usuarios.GroupBy(p => p.NombreUsuario).Select(g => g.First()).ToList();

            ddlUsuario.Items.Add("");
            foreach (UsuarioBE usuario in usuarios)
            {
                ddlUsuario.Items.Add(usuario.NombreUsuario);
            }

            dgvBitacora.AutoGenerateColumns = false;
            dgvBitacora.DataSource = eventos;
            dgvBitacora.DataBind();

            txtCantRegistros.Text = eventos.Count.ToString();
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            dgvBitacora.AllowPaging = false;
            CargarDatos();
            exportarExcel(sender, e, dgvBitacora, 0, 0, Traducir("BITACORA"));
            dgvBitacora.AllowPaging = true;
            CargarDatos();
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            dgvBitacora.AllowPaging = false;
            CargarDatos();
            exportarPDF(sender, e, dgvBitacora, 0, 0, Traducir("BITACORA"));
            dgvBitacora.AllowPaging = true;
            CargarDatos();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        protected void dgvBitacora_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvBitacora.PageIndex = e.NewPageIndex;
            CargarDatos();
        }

        protected void dgvBitacora_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }

            if (e.Row.Cells.Count > 2)
            {
                switch (e.Row.Cells[3].Text)
                {
                    case "1":
                        e.Row.Cells[3].Text = Traducir("EVENTO_INFO");
                        break;
                    case "2":
                        e.Row.Cells[3].Text = Traducir("EVENTO_WARN");
                        break;
                    case "3":
                        e.Row.Cells[3].Text = Traducir("EVENTO_ERR");
                        e.Row.BackColor = System.Drawing.Color.LightCoral;
                        break;
                    default:
                        break;
                }
            }



        }

        public string FormatTimeStamp(object _DateFromGrid)
        {
            long GridDate = long.Parse(_DateFromGrid.ToString());
            DateTime timeStamp = new DateTime(GridDate);

            return timeStamp.ToString();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            IList<BitacoraBE> registros = bitacoraBll.SelectAll();

            if (!String.IsNullOrEmpty(ddlUsuario.SelectedValue))
            {
                registros = registros.Where(x => x.Usuario.NombreUsuario == ddlUsuario.SelectedValue).ToList();
            }

            if (!String.IsNullOrEmpty(ddlTipoEvento.SelectedValue))
            {
                registros = registros.Where(x => x.Evento_Bitacora.Severidad == ddlTipoEvento.SelectedIndex).ToList();
            }

            if (!String.IsNullOrEmpty(ddlCategoria.SelectedValue))
            {
                registros = registros.Where(x => x.Evento_Bitacora.Descripcion == ddlCategoria.SelectedValue).ToList();
            }

            dgvBitacora.AutoGenerateColumns = false;
            dgvBitacora.DataSource = registros;
            if (registros != null)
            {
                txtCantRegistros.Text = registros.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }

            dgvBitacora.DataBind();
        }
    }
}