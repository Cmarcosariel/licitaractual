﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class ReporteLicitacionXRubro : Cascara
    {
       
        private string _connString = ConfigurationManager.ConnectionStrings["PRD"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

            validarAcceso("NEG007");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            CargarData();

            if (!IsPostBack)
            {
                Page.DataBind();
            }

        }

        protected void Page_Prerender(object sender, EventArgs e)
        {

        }


        private void CargarData()
        {

            Dictionary<BE.Clase_ItemBE, string> DiccionarioCosas;

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                DiccionarioCosas = bll.SP_reporteCatxCant();
            }

            IList<CategoriaCantidadMapping> ventasProductosList = new List<CategoriaCantidadMapping>();


            List<BE.RubroBE> rubroBEs;

            using (BLL.RubroBLL bll = new BLL.RubroBLL())
            {
                rubroBEs = bll.SelectAll();
            }

            foreach (BE.RubroBE rubro in rubroBEs)
            {

                CategoriaCantidadMapping asd = new CategoriaCantidadMapping();

                asd.Rubros = rubro.Descripcion;


                using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
                {
                    asd.cantidadLicitaciones = (licitacionBLL.SelectAllByID_Rubro(rubro)).Count();
                }

                ventasProductosList.Add(asd);

            }


            hdnJson.Value = Newtonsoft.Json.JsonConvert.SerializeObject(ventasProductosList);

        }


    }

    public class CategoriaCantidadMapping
    {
        public string Rubros { get; set; }
        public int cantidadLicitaciones { get; set; }
    }


}