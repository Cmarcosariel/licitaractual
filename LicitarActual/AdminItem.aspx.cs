﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace LicitarActual
{
    public partial class AdminItem : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("ITEMS");
            validarAcceso("NEG005");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarItems();
            Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG005");
            BLL.ItemBLL usuarioBll = new BLL.ItemBLL();
            BE.ItemBE usuarioBe = new BE.ItemBE();
            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());
            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino el item {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarItems();
        }
        protected void llenarItems()
        {
            BLL.ItemBLL ItemBLL = new BLL.ItemBLL();

            List<BE.ItemBE> ItemBEs = ItemBLL.SelectAll();

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = ItemBEs;

            if (ItemBEs != null)
            {
                txtCantRegistros.Text = ItemBEs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG005");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminItemForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarItems();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarItems();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.ItemBLL orgbll = new BLL.ItemBLL();
                List<BE.ItemBE> usuarios = orgbll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            using (BLL.ItemBLL bll = new BLL.ItemBLL())
            {


                Dictionary<string, string> midiccionario1 = bll.GetClasesStringForItems();


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string mistring1;

                    midiccionario1.TryGetValue((HttpUtility.HtmlDecode(e.Row.Cells[2].Text)), out mistring1);

                    e.Row.Cells[1].Text = mistring1;
                }

            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarItems();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("ITEMS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("ITEMS"));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}