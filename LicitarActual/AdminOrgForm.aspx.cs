﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminOrgForm : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("NEG002");
            btnGuardar.Text = Traducir("GUARDAR");

            RequiredFieldValidator_iptCUIT.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptNombreOrg.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptDomicilio.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptEMAIL.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptTELEFONO.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptRzn_Social.ErrorMessage = Traducir("CAMPO_REQUERIDO");

            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarData();
            }
        }

        protected void cargarData()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int idOrg = Convert.ToInt32(Request.QueryString["id"]);
                BLL.OrganizacionBLL Orgbll = new BLL.OrganizacionBLL();

                BE.OrganizacionBE orgtmp = new BE.OrganizacionBE() { Identidad = idOrg };

                BE.OrganizacionBE organizacion = Orgbll.Select(orgtmp);


                if (organizacion != null)
                {
                    iptNombreOrg.Text = organizacion.Descripcion;
                    iptDomicilio.Text = organizacion.Domicilio;
                    iptEMAIL.Text = organizacion.Email;
                    iptCUIT.Text = organizacion.CUIT;
                    iptRzn_Social.Text = organizacion.Razon_Social;
                    iptTELEFONO.Text = organizacion.Telefono;
                }
            }
            else
            {
                //asignarGruposPermisos.Visible = false;
                //iptPassword.Visible = true;
                //txtPwdGuardada.Visible = false;
                //RequiredFieldValidator_iptPassword.Visible = true;
            }
        }

          protected void btnGuardar_Click(object sender, EventArgs e)
        {


            BE.OrganizacionBE org = null;
            BLL.OrganizacionBLL orgbll = new BLL.OrganizacionBLL();

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                org = orgbll.Select((new BE.OrganizacionBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));
            }

            if (org == null)
            {
                org = new BE.OrganizacionBE();
                org.Identidad = 0;
            }

            org.Descripcion = iptNombreOrg.Text;
            org.Domicilio = iptDomicilio.Text;
            org.Email = iptEMAIL.Text;
            org.CUIT = iptCUIT.Text;
            org.Razon_Social = iptRzn_Social.Text;
            org.Telefono = iptTELEFONO.Text;

            using (BLL.OrganizacionBLL bll = new BLL.OrganizacionBLL())
            {
                List<BE.OrganizacionBE> organizacionBEs = bll.SelectAll();

                //si ya existe la razon social y es el mismo id permitimos
                if (organizacionBEs.Any(x => x.Razon_Social == org.Razon_Social && x.Identidad == org.Identidad))
                {

                }

                //si ya existe la razon social y no es el mismo id cancelamos
                if (organizacionBEs.Any(x => x.Razon_Social == org.Razon_Social && x.Identidad != org.Identidad))
                {
                    string mensaje = Traducir("ORG_RZN_SOC_EXIST");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }

                if (organizacionBEs.Any(x => x.CUIT == org.CUIT && x.Identidad != org.Identidad))
                {
                    string mensaje = Traducir("ORG_CUIT_EXIST");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }


            }



            if (org.Identidad == 0)
            {
                orgbll.Insert(org);
                NuevaBitacora((String.Format("Se creo la organizacion {0}", org.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            }
            else
            {
                orgbll.Update(org);
                NuevaBitacora((String.Format("Se modifico la organizacion {0}", org.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
            }

            Response.Redirect("AdminOrg.aspx");
        }

    }
}
