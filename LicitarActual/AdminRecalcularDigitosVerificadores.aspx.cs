﻿using System;
using System.Web.Services;



namespace LicitarActual
{
    public partial class AdminRecalcularDigitosVerificadores : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM016");

            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

            }

            btnRecalcular.Text = Traducir("RECALCULARDV");
            btnVerificar.Text = Traducir("VERIFICARINTEGRIDAD");

        }
        protected void btnRecalcular_Click(object sender, EventArgs e)
        {
            validarAcceso("ADM016");

            using (SEC.GestorDV sec = new SEC.GestorDV())
            {

                RecalcularTodosDvh();
                RecalcularTodosDvv();

                NuevaBitacora("Se Regeneraron todos los DVH y DVV", (BE.UsuarioBE)Session["UsuarioLogueado"], "BASE DE DATOS", 2);

                if (!sec.CalcularIntegridadDB())
                {
                    PanelMensajeRespuesta.Visible = true;
                    PanelMensajeSuccess.Visible = false;
                    NuevaBitacora("La base de datos se encuentra Corrupta", (BE.UsuarioBE)Session["UsuarioLogueado"], "BASE DE DATOS", 3);
                    lblError.Text = Traducir("ERRORDV");
                }
                else
                {
                    PanelMensajeSuccess.Visible = true;
                    PanelMensajeRespuesta.Visible = false;
                    NuevaBitacora("La base de datos se encuentra OK", (BE.UsuarioBE)Session["UsuarioLogueado"], "BASE DE DATOS", 1);
                    lblSuccess.Text = Traducir("OKDV");
                }

            }
        }

        protected void btnVerificar_Click(object sender, EventArgs e)
        {
            validarAcceso("ADM016");

            using (SEC.GestorDV sec = new SEC.GestorDV())
            {
                if (!sec.CalcularIntegridadDB())
                {
                    PanelMensajeRespuesta.Visible = true;
                    PanelMensajeSuccess.Visible = false;
                    NuevaBitacora("La base de datos se encuentra Corrupta", (BE.UsuarioBE)Session["UsuarioLogueado"], "BASE DE DATOS", 3);
                    lblError.Text = Traducir("ERRORDV");
                }
                else
                {
                    PanelMensajeSuccess.Visible = true;
                    PanelMensajeRespuesta.Visible = false;
                    NuevaBitacora("La base de datos se encuentra OK", (BE.UsuarioBE)Session["UsuarioLogueado"], "BASE DE DATOS", 1);
                    lblSuccess.Text = Traducir("OKDV");
                }
            }

        }

        [WebMethod]
        public static void VerificarDigitosVerificadores()
        {
            RecalcularTodosDvh();
            RecalcularTodosDvv();
        }
    }
}