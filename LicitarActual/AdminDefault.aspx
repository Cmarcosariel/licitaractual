﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminDefault.aspx.cs" Inherits="LicitarActual.AdminDefault" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1><%= Traducir("TitAdminBienvenida") %> <%= ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombrePersona %></h1>
        <p><%= Traducir("TitAdminDefault") %></p>
        
      </div>
    
</asp:Content>
