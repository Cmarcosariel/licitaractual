﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminBackup.aspx.cs" Inherits="LicitarActual.AdminBackup" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <div>
            <asp:Label ID="lblbkp" Text='<%# Traducir("CONFIRMAR_BKP") %>' runat="server" Visible="false" type="hidden"></asp:Label>
        </div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong><%# Traducir("BACKUPS") %></strong>
                    <asp:Button ID="nuevoBkp" runat="server" CssClass="btn btn-primary pull-right" UseSubmitBehavior="false" OnClientClick="return Confirmar();" />
                    <asp:Button ID="savebtn" runat="server" OnClick="nuevoBkp_Click" Style="display: none" />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_15") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="dgvBackup" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-striped table-hover"
                    AllowPaging="True" GridLines="None" PagerStyle-CssClass="pagination-dgv"
                    DataKeyNames="Filepath"
                    PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="dgvBackup_PageIndexChanging"
                    OnRowEditing="dgvBackup_RowEditing" OnRowDataBound="dgvBackup_RowDataBound"
                    PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="Identidad" HeaderText="Codigo" Visible="false" />
                        <asp:BoundField DataField="Filepath" HeaderText="RUTA" Visible="false" />
                        <asp:TemplateField>
                            <HeaderTemplate>NOMBRE</HeaderTemplate>
                            <%--                            <ItemTemplate>
                                <%# Eval("Filepath").ToString().Substring(Eval("Filepath").ToString().LastIndexOf('/')+1,23) %>
                            </ItemTemplate>--%>
                            <ItemTemplate>
                                <%# Eval("Filepath").ToString().Substring(1)%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Timestamp" HeaderText="FECHA" />
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-wrench'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="RESTAURAR" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">&nbsp;</div>
<%--            <div class="col-md-3 pull-right">
                <asp:Button ID="btnExportarExcel" runat="server" OnClick="btnExportarExcel_Click" Text='<%# Traducir("EXPORTAR_EXCEL") %>' CssClass="btn btn-sm btn-primary right" />
                &nbsp; &nbsp;
                <asp:Button ID="btnExportarPDF" runat="server" OnClick="btnExportarPDF_Click" Text='<%# Traducir("EXPORTAR_PDF") %>' CssClass="btn btn-sm btn-primary right" />
            </div>--%>
        </div>

    </div>

        <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        function Confirmar(variable) {

            if (confirm('<%# Traducir("CONFIRMAR_BKP") %>'))

                document.getElementById('<%= savebtn.ClientID %>').click();

            else

                return false;
        }

        function ConfirmarRestore() {

            if (confirm('<%# Traducir("CONFIRMAR_RESTORE") %>'))

                return true;

            else

                return false;
        }

    </script>
</asp:Content>
