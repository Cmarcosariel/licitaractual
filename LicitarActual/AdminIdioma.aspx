﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminIdioma.aspx.cs" Inherits="LicitarActual.AdminIdioma" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="Literal1" runat="server" Text='<%# Traducir("IDIOMAS") %>'></asp:Literal></strong>
                        <a href="AdminNuevoIdioma.aspx" class="btn btn-primary pull-right">
                        <asp:Literal ID="Literal2" runat="server" Text='<%# Traducir("NUEVO") %>'></asp:Literal></a><br />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong>  <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_10") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvIdiomas" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None" 
                    CssClass="table table-striped table-hover" AllowPaging="True" 
                    GridLines="None" PagerStyle-CssClass="pagination-dgv " 
                    PagerStyle-HorizontalAlign="Right" 
                    DataKeyNames="Identidad"
                    OnPageIndexChanging="DgvIdiomas_PageIndexChanging" 
                    OnRowCancelingEdit="DgvIdiomas_RowCancelingEdit" 
                    OnRowDeleting="DgvIdiomas_RowDeleting" 
                    OnRowDataBound="DgvIdiomas_RowDataBound"
                    OnRowEditing="DgvIdiomas_RowEditing" OnRowUpdating="DgvIdiomas_RowUpdating"
                     PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="Identidad" HeaderText="Id" Visible="false"/>
                        <asp:BoundField DataField="descripcion" HeaderText='DESCRIPCION' />
<%--                        <asp:BoundField DataField="clave" HeaderText='CLAVE' />--%>
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText='EDITAR' />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger"  HeaderText='ELIMINAR' />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">&nbsp;</div>
<%--            <div class="col-md-3 pull-right">
                <asp:Button ID="btnExportarExcel" runat="server" OnClick="btnExportarExcel_Click" Text='<%# Traducir("EXPORTAR_EXCEL") %>' CssClass="btn btn-sm btn-primary right" />
                &nbsp; &nbsp;
                <asp:Button ID="btnExportarPDF" runat="server" OnClick="btnExportarPDF_Click" Text='<%# Traducir("EXPORTAR_PDF") %>' CssClass="btn btn-sm btn-primary right" />
            </div>--%>
        </div>
    </div>
    <%--<a href="javascript:void(0);" data-href="getContent.php?id=1" class="openPopup">About Us</a>--%>

     <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bootstrap Modal with Dynamic Content</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
      
        </div>
    </div>
    <script type="text/javascript" >
        $('.openPopup').on('click', function () {
            var dataURL = $(this).attr('data-href');
            $('.modal-body').load(dataURL, function () {
                $('#myModal').modal({ show: true });
            });
        });

    </script>
</asp:Content>