﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminPermisosGrupos : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                litGrupos.Text = Traducir("GRUPOS");
                litNuevo.Text = Traducir("NUEVO");
                validarAcceso("ADM002");
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                Page.DataBind();
                llenarGrupos();
            }
        }

        protected void llenarGrupos()
        {

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                List<BE.PermisoBE> grupos = sec.GetAllRoles();

                DgvGrupos.AutoGenerateColumns = false;
                DgvGrupos.DataSource = grupos;
                DgvGrupos.DataBind();
                if (grupos != null)
                {
                    txtCantRegistros.Text = grupos.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }

            }

        }

        protected void DgvGrupos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("ADM002");

            BE.PermisoBE permisoBE = new BE.PermisoBE();
            permisoBE.Codigo_Perfil = (DgvGrupos.DataKeys[e.RowIndex].Values[0].ToString());

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                sec.Delete(permisoBE);

                NuevaBitacora((String.Format("el usuario {0} Elimino el Rol {1} ", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, permisoBE.Codigo_Perfil)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);

                llenarGrupos();

            }


        }
        protected void DgvGrupos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("ADM002");
            string id = (DgvGrupos.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminPermisosGruposForm.aspx?id=" + id);
        }
        protected void DgvGrupos_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvGrupos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvGrupos.PageIndex = e.NewPageIndex;
            llenarGrupos();
        }
        protected void DgvGrupos_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvGrupos.EditIndex = -1;
            llenarGrupos();
        }

        protected void DgvGrupos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                    }
                }
            }
        }


        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            this.exportarExcel(sender, e, DgvGrupos, 2, 3, Traducir("GRUPOS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvGrupos, 2, 3, Traducir("GRUPOS"));
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

    }
}