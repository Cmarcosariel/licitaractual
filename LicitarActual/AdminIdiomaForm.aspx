﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminIdiomaForm.aspx.cs" Inherits="LicitarActual.AdminIdiomaForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="Literal1" runat="server" Text="Idiomas"></asp:Literal></strong>

                </h3>
            </div>
        </div>
        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong>  <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_15") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="dgvAdminIdioma" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None"
                    CssClass="table table-striped table-hover" AllowPaging="True"
                    GridLines="None" PagerStyle-CssClass="pagination-dgv "
                    PagerStyle-HorizontalAlign="Right"
                    OnPageIndexChanging="dgvAdminIdioma_PageIndexChanging"
                    OnRowEditing="GridView1_RowEditing"
                    DataKeyNames="Identidad"
                    OnRowDataBound="dgvAdminIdioma_RowDataBound"
                    OnRowUpdating="GridView1_RowUpdating"
                    OnRowCancelingEdit="GridView1_RowCancelingEdit"
                    PageSize="15">
                    <Columns>
                        <asp:TemplateField HeaderText="EDITAR">
                            <ItemTemplate>
                                <asp:LinkButton ID="btn_Edit" runat="server" Text="" CommandName="Edit">
                                 <span class="glyphicon glyphicon-edit"></span>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="btn_Update" runat="server" Text='<%# Traducir("ACTUALIZAR") %>' CommandName="Update" />
                                <span class="glyphicon glyphicon-ok"></span>
                                &nbsp;
                                <asp:LinkButton ID="btn_Cancel" runat="server" Text='<%# Traducir("CANCELAR") %>' CommandName="Cancel" />
                                <span class="glyphicon glyphicon-remove"></span>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CODIGO">
                            <ItemTemplate>
                                <asp:Label ID="lbl_id" runat="server" Text='<%#Eval("Identidad") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ETIQUETA">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Clave" runat="server" Text='<%#Eval("Mensaje.Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NOMBRE">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Valor" runat="server" Text='<%#Eval("Traduccion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_Valor" runat="server" Text='<%#Eval("Traduccion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="form-group clearfix">
            <a href="AdminIdioma.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        </div>
    </div>
</asp:Content>

