﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminItemForm : Cascara
    {


        protected void Page_Load(object sender, EventArgs e)
        {


            validarAcceso("NEG005");
            btnGuardar.Text = Traducir("GUARDAR");
            Button1.Text = Traducir("Guardar_Y_otro");
            PH_NombreProp.Visible = true;
            RequiredFieldValidator_ddRubros.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_ddCategorias.ErrorMessage = Traducir("CAMPO_REQUERIDO");

            if (ddCategorias.SelectedIndex > 0)
            {
                using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
                {
                    CargarPHPropiedades(bll.Select(new BE.Clase_ItemBE { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) }));
                }
            }

            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarDropdownRubros();
                cargarData();
            }
        }

        protected void cargarData()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int idOrg = Convert.ToInt32(Request.QueryString["id"]);

                BLL.ItemBLL Orgbll = new BLL.ItemBLL();

                BE.ItemBE orgtmp = new BE.ItemBE() { Identidad = idOrg };

                BE.ItemBE item = Orgbll.Select(orgtmp);

                //PH_Propiedades.Visible = true;

                if (item != null)
                {
                    using (BLL.Clase_ItemBLL bLL = new BLL.Clase_ItemBLL())
                    {

                        BE.Clase_ItemBE clase_ItemBE = bLL.GetClaseforItem(item);

                        using (BLL.RubroBLL bllrubro = new BLL.RubroBLL())
                        {
                            BE.RubroBE rubroBE = bllrubro.GetRubrofromClaseItem(clase_ItemBE);
                            ddRubros.SelectedValue = rubroBE.Identidad.ToString();

                            cargarDropdownCategorias(rubroBE);
                        }

                        ddCategorias.SelectedValue = clase_ItemBE.Identidad.ToString();

                        txtAreaNotas.Value = item.Observaciones;

                        CargarPHPropiedades(clase_ItemBE);

                        ddRubros.Enabled = false;
                        ddCategorias.Enabled = false;
                        PH_NombreProp.Visible = false;

                    }



                }
            }


        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            BE.ItemBE nuevoitem = null;
            BLL.ItemBLL itemBLL = new BLL.ItemBLL();

            if (ddCategorias.SelectedIndex ==0 || ddRubros.SelectedIndex ==0)
            {
                string mensaje = Traducir("ITM_SEL_RUB_CAT");
                lblMessage.Text = mensaje;
                miPopUp();
                return;
            }

            BE.RubroBE rub = new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) };
            BE.Clase_ItemBE clase_Item = new BE.Clase_ItemBE { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) };


            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                nuevoitem = itemBLL.Select((new BE.ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));
            }

            if (nuevoitem == null)
            {
                nuevoitem = new BE.ItemBE();
                nuevoitem.Identidad = 0;
            }

            nuevoitem.Observaciones = txtAreaNotas.InnerText;

            nuevoitem.Descripcion = (ddCategorias.SelectedItem.Text + ";");

            foreach (DropDownList item in getDropdowns(PH_NombreProp))
            {
                nuevoitem.Descripcion += (item.ID.Substring(3) + ": " + item.SelectedItem.Text + ";");
            }

            using (BLL.ItemBLL asd = new BLL.ItemBLL())
            {
                if (asd.SelectByDesc(nuevoitem) != null)
                {
                    string mensaje = Traducir("ITM_YA_EXISTE");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                } 
            }
                    

            if (nuevoitem.Identidad == 0)
            {
                itemBLL.Insert(nuevoitem, clase_Item);
                NuevaBitacora((String.Format("Se creo el item {0}", nuevoitem.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                BE.ItemBE itemBE = itemBLL.SelectbyDescyClase(nuevoitem,clase_Item);

                //Response.Redirect("AdminItemForm.aspx?id=" + itemBE.Identidad);
                Response.Redirect("AdminItem.aspx");
            }
            else
            {
                itemBLL.Update(nuevoitem);
                NuevaBitacora((String.Format("Se modifico el item {0}", nuevoitem.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
                //Response.Redirect("AdminItemForm.aspx?id=" + nuevoitem.Identidad);
                Response.Redirect("AdminItem.aspx");
            }

            Response.Redirect("AdminItem.aspx");

        }


        private void cargarDropdownRubros()
        {

            using (BLL.RubroBLL sec = new BLL.RubroBLL())
            {
                ddRubros.DataSource = null;
                ddRubros.DataMember = "Rubro";
                ddRubros.DataValueField = "Identidad";
                ddRubros.DataTextField = "Descripcion";

                ddRubros.DataSource = sec.SelectAll();



                ddRubros.DataBind();

                ddRubros.Items.Insert(0, "Seleccionar");
                ddRubros.SelectedIndex = 0;

            }

        }



        //protected void btnAgregarPropiedad_Click(object sender, EventArgs e)
        //{

        //    PH_CamposProp.Visible = true;
        //    PH_NombreProp.Visible = true;

        //}


        //protected void btnagregarpropiedad123_Click(object sender, EventArgs e)
        //{
        //    PH_CamposProp.Visible = true;
        //    BE.PropiedadItemBE propiedadItemBE = new BE.PropiedadItemBE();

        //    propiedadItemBE.Nombre = iptPropNombbre.Text;
        //    iptPropNombbre.Text = "";

        //    using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
        //    {
        //        BE.Clase_ItemBE clase = bll.Select((new BE.Clase_ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));

        //        if (clase.Propiedades.Any(x => x.Nombre == propiedadItemBE.Nombre))
        //        {
        //            return;
        //        }

        //        clase.Propiedades.Add(propiedadItemBE);
        //        bll.Update(clase);
        //        //CargarPHPropiedades(clase);
        //        Response.Redirect("AdminClaseItemForm.aspx?id=" + Request.QueryString["id"]);
        //    }


        //}

        public void CargarPHPropiedades(BE.Clase_ItemBE clase)
        {
            PH_NombreProp.Controls.Clear();

            if (clase.Propiedades.Count() == 0)
            {
                return;
            }

            foreach (BE.PropiedadItemBE item in clase.Propiedades)
            {

                DropDownList ddprop = new DropDownList();
                ddprop.ID = "ddl" + item.Nombre;
                ddprop.CssClass = "form-control";


                ddprop.DataSource = item.valores;
                ddprop.DataBind();
                LiteralControl spacer = new LiteralControl("<br />");
                string milbl = string.Format("<label>{0}</label>", item.Nombre);
                LiteralControl literal = new LiteralControl(milbl);

                HtmlGenericControl newControl = new HtmlGenericControl("div");
                newControl.Attributes.Add("class", "form-group col-md-4");

                HtmlGenericControl ddlyeditar = new HtmlGenericControl("div");
                ddlyeditar.Attributes.Add("class", "form-group row");

                HtmlGenericControl divboton = new HtmlGenericControl("div");
                divboton.Attributes.Add("class", "col-md-4");

                HtmlGenericControl divddl = new HtmlGenericControl("div");
                divddl.Attributes.Add("class", "col-md-8");

                divddl.Controls.Add(ddprop);

                //divboton.Controls.Add(btnAgregarValorAProp);

                ddlyeditar.Controls.Add(divddl);
                ddlyeditar.Controls.Add(divboton);




                //newControl.Controls.Add(spacer);
                //newControl.Controls.Add(spacer);
                newControl.Controls.Add(literal);
                newControl.Controls.Add(ddlyeditar);
                PH_NombreProp.Controls.Add(newControl);



            }
        }



        protected void ddCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                if (ddCategorias.SelectedIndex == 0)
                {
                    return;
                }

                CargarPHPropiedades(bll.Select(new BE.Clase_ItemBE { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) }));
            }

        }

        private void cargarDropdownCategorias(BE.RubroBE rubro)
        {
            using (BLL.Clase_ItemBLL sec = new BLL.Clase_ItemBLL())
            {
                ddCategorias.DataSource = null;
                ddCategorias.DataMember = "Clase_Item";
                ddCategorias.DataValueField = "Identidad";
                ddCategorias.DataTextField = "Descripcion";

                ddCategorias.DataSource = sec.SelectAllByID_Rubro(rubro);

                ddCategorias.DataBind();

                if (ddCategorias.Items.Count > 0)
                {
                    ddCategorias.Items.Insert(0, "Seleccionar");
                    ddCategorias.SelectedIndex = 0;
                }



            }
        }

        protected void ddRubros_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddRubros.SelectedIndex == 0)
            {
                return;
            }

            cargarDropdownCategorias(new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) });
        }

        public List<DropDownList> getDropdowns(PlaceHolder miph)
        {
            List<DropDownList> milista = new List<DropDownList>();

            foreach (Control item in miph.Controls)
            {
                RecorrerControl(milista, item);
            }
            return milista;
            
        }

        public List<DropDownList> RecorrerControl(List<DropDownList> listaddl, Control control)
        {
            foreach (Control item in control.Controls)
            {
                if (item.GetType() == typeof(DropDownList))
                {
                    listaddl.Add((DropDownList)item);
                }
                else
                {
                    RecorrerControl(listaddl, item);
                }
            }

            return listaddl;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BE.ItemBE nuevoitem = null;
            BLL.ItemBLL itemBLL = new BLL.ItemBLL();

            if (ddCategorias.SelectedIndex == 0 || ddRubros.SelectedIndex == 0)
            {
                string mensaje = Traducir("ITM_SEL_RUB_CAT");
                lblMessage.Text = mensaje;
                miPopUp();
                return;
            }

            BE.RubroBE rub = new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) };
            BE.Clase_ItemBE clase_Item = new BE.Clase_ItemBE { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) };


            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                nuevoitem = itemBLL.Select((new BE.ItemBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));
            }

            if (nuevoitem == null)
            {
                nuevoitem = new BE.ItemBE();
                nuevoitem.Identidad = 0;
            }

            nuevoitem.Observaciones = txtAreaNotas.InnerText;

            nuevoitem.Descripcion = (ddCategorias.SelectedItem.Text + ";");

            foreach (DropDownList item in getDropdowns(PH_NombreProp))
            {
                nuevoitem.Descripcion += (item.ID.Substring(3) + ": " + item.SelectedItem.Text + ";");
            }

            using (BLL.ItemBLL asd = new BLL.ItemBLL())
            {
                if (asd.SelectByDesc(nuevoitem) != null)
                {
                    string mensaje = Traducir("ITM_YA_EXISTE");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }
            }


            if (nuevoitem.Identidad == 0)
            {
                itemBLL.Insert(nuevoitem, clase_Item);
                NuevaBitacora((String.Format("Se creo el item {0}", nuevoitem.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                BE.ItemBE itemBE = itemBLL.SelectbyDescyClase(nuevoitem, clase_Item);

                Response.Redirect("AdminItemForm.aspx?id=" + itemBE.Identidad);
            }
            else
            {
                itemBLL.Update(nuevoitem);
                NuevaBitacora((String.Format("Se modifico el item {0}", nuevoitem.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
                Response.Redirect("AdminItemForm.aspx?id=" + nuevoitem.Identidad);
            }

            Response.Redirect("AdminItemForm.aspx");

        }



    }
}