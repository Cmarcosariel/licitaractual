﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LicitarActual.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


<%--    <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">--%>
    <br /><br />
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="form-group text-left" >
                    <img  src="Images/Logo.png" />
                </div>
                <br />
                
                <div class="form-group text-left">
                    <asp:TextBox class="form-control" placeholder='<%# /*"ASD"*/ Traducir("txtUserFrmLogin") %>' ClientIDMode="Static" ID="username" runat="server"></asp:TextBox>
                    <%--<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="username" MinimumValue="1" MaximumValue="10"  Type="Integer" ErrorMessage="RangeValidator"></asp:RangeValidator>--%>
                   
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="username" ForeColor="Red"  runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>

                </div>
                
                <div class="form-group text-left">
                    <asp:TextBox CssClass="form-control" placeholder='<%# /*"ASD"*/ Traducir("txtPasswordFrmLogin") %>' ClientIDMode="Static" ID="password" runat="server" TextMode="Password" OnTextChanged="password_TextChanged"></asp:TextBox>
               <%--       <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="password" ForeColor="Red"  runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                </div>
                <asp:PlaceHolder ID="rtaError" Visible="false" runat="server">
                    <div class="alert alert-danger" role="alert">
                        <asp:Label ID="error" runat="server" Text=""></asp:Label>
                    </div>
                </asp:PlaceHolder>
                <asp:Button ID="sendlogin" runat="server" Text='<%# /*"ASD"*/ Traducir("btnLoginFrmLogin")%>' TabIndex="4" class="form-control btn btn-success" OnClick="sendlogin_Click" />
             </div>
        </div>
    </div>

    <div id="myModal" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">LicitAR</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="body">
					<asp:Label ID="lblMessage" runat="server" />
				</div>
				<div class="modal-footer">							
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div> 
</asp:Content>

<%--</asp:Content>--%>
