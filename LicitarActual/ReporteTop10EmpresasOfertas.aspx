﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="ReporteTop10EmpresasOfertas.aspx.cs" Inherits="LicitarActual.ReporteTop10EmpresasOfertas" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

    <script>
        window.onload = function () {

            var dataPoints = [];

            var options = {
                animationEnabled: true,

                theme: "light2",
                title: {
                    text: "Cantidad de Ofertas por Empresa (TOP 10)"
                },
                axisX: {
                    title: "Empresa",
                    //valueFormatString: "DD MMM YYYY",
                },
                axisY: {
                    title: "Cantidad de Ofertas",
                    titleFontSize: 24
                },
                data: [{
                    type: "bar",
                    //yValueFormatString: "$#,###.##",
                    dataPoints: dataPoints
                }]
            };

            function addData(data) {
                for (var i = 0; i < data.length; i++) {
                    dataPoints.push({

                        label: data[i].Empresa,
                        y: data[i].cantidadOfertas
                    });
                }
                $("[id$=chartContainer]").CanvasJSChart(options);

            }
            var values = $('[id$=hdnJson12345]').val();
            addData(jQuery.parseJSON(values));
            //https://canvasjs.com/jquery-charts/json-data-api-ajax-chart/

        }
    </script>

                <asp:HiddenField ID="hdnJson12345" runat="server" />

        <div class="row">
        <h1 class="centrar-texto-Ok"><%= Traducir("REPORTES_LICITACION") %></h1>
        <br />
        <br />
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("RUBRO") %>:</label>
            </div>  
            <div class="col-md-9">
                <asp:DropDownList ID="ddRubros" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddRubros_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>

            </div>
        </div>
        <br />
        <br />
        <div class="row ">
            <div class="col-md-11">
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            </div>
<%--            <div class="col-md-1">
                <asp:Button ID="btnExportarPdf2" runat="server" OnClick="btnExportarPdf1_Click" Text='<%# Traducir("EXPORTAR_PDF") %>' CssClass="btn btn-primary btn-outline-white btn-misProyectos" />
            </div>--%>
        </div>
        <br />
        <br />


    </div>

</asp:Content>

