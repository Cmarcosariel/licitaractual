﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminIdioma : Cascara

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM015");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            Page.DataBind();
            llenarIdiomas();
        }

        protected void llenarIdiomas()
        {

            using (BLL.IdiomaBLL bll = new BLL.IdiomaBLL())
            {
                List<BE.IdiomaBE> idiomas = bll.SelectAll();

                DgvIdiomas.AutoGenerateColumns = false;
                DgvIdiomas.DataSource = idiomas;

                if (idiomas != null)
                {
                    txtCantRegistros.Text = idiomas.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }

                DgvIdiomas.DataBind();
            }



        }

        protected void DgvIdiomas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("ADM015");

            using (BLL.IdiomaBLL bll = new BLL.IdiomaBLL())
            {

                BE.IdiomaBE idioma = new BE.IdiomaBE() { Identidad = Int32.Parse(DgvIdiomas.DataKeys[e.RowIndex].Values[0].ToString()) };

                if (idioma.Identidad == 1 || idioma.Identidad == 2)
                {
                    string mensaje = Traducir("IDIOMA_DELETE_CORE");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }

                GridViewRow row = (GridViewRow)DgvIdiomas.Rows[e.RowIndex];
                bll.Delete(idioma);
                NuevaBitacora((String.Format("el usuario {0} borro el idioma {1} con ID {2}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, idioma.Descripcion, idioma.Identidad)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
                llenarIdiomas();

            }
            

        }
        protected void DgvIdiomas_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("ADM015");
            GridViewRow row = (GridViewRow)DgvIdiomas.Rows[e.NewEditIndex];
            int id = Int32.Parse(DgvIdiomas.DataKeys[e.NewEditIndex].Values[0].ToString());
            String clave = row.Cells[2].Text;

            Response.Redirect("/AdminIdiomaForm.aspx?id=" + id);
        }
        protected void DgvIdiomas_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvIdiomas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvIdiomas.PageIndex = e.NewPageIndex;
            llenarIdiomas();
        }
        protected void DgvIdiomas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvIdiomas.EditIndex = -1;
            llenarIdiomas();
        }

        protected void DgvIdiomas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            this.exportarExcel(sender, e, DgvIdiomas, 3, 4, Traducir("IDIOMAS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvIdiomas, 3, 4, Traducir("IDIOMAS"));
        }

    }
}