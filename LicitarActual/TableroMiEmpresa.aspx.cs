﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class TableroMiEmpresa : Cascara
    {
        BE.LicitacionBE MiLicitacionEdit;
        BE.OfertaBE MiOfertaEdit;
        DataTable MiDTEdit;

        protected void Page_Init(object sender,EventArgs e)
        {

            //llenarLicitacionesconOfertas();

        }



        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("MIS_LICITACIONES");
            litUsuariosOfertas.Text = Traducir("MIS_OFERTAS");

            btnPublicarOfertaLicitacion.Text = Traducir("PUBLICAR_OFERTA");
            btnCancelarOfertaLicitacion.Text = Traducir("CANCELAR_OFERTA");
            btnAcept12Oferta_.Text = Traducir("ACEPTAR_OFERTA");
            btnRechaz13Oferta_.Text = Traducir("RECHAZAR_OFERTA");

            validarAcceso("NEG009");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            //llenarLicitaciones();
            //llenarLicitacionesconOfertas();

            if (!IsPostBack)
            {
                llenarLicitaciones();
                llenarLicitacionesconOfertas();
            }


            //Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG009");
            BLL.LicitacionBLL usuarioBll = new BLL.LicitacionBLL();
            BE.LicitacionBE usuarioBe = new BE.LicitacionBE();



            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());
            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino la Licitacion {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarLicitaciones();
        }
        protected void llenarLicitaciones()
        {
            BLL.LicitacionBLL LicitacionBLL = new BLL.LicitacionBLL();

            List<BE.LicitacionBE> orgs = LicitacionBLL.SelectAllByID_Organizacion(((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion);

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = orgs;

            if (orgs != null)
            {
                txtCantRegistros.Text = orgs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG009");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminLicitacionForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarLicitaciones();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarLicitaciones();
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltrosOferta().Values != null)
            {
                BLL.LicitacionBLL licbll = new BLL.LicitacionBLL();
                List<BE.LicitacionBE> usuarios = licbll.Select(getFiltrosOferta());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistrosOfertas.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistrosOfertas.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ofertasxlicitacion;

                Int64 id = Int32.Parse(DgvUsuarios.DataKeys[Convert.ToInt16(e.Row.RowIndex)].Values[0].ToString());

                using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
                {
                    ofertasxlicitacion = bll.SelectCountOfertasXlicitacion(new LicitacionBE { Identidad = id});
                }

                ((Button)(e.Row.Cells[10].Controls[0])).Text = (Traducir("OFERTAS") + ": " + ofertasxlicitacion);
                ((Button)(e.Row.Cells[9].Controls[0])).Text = Traducir("CANCELAR");


                // es un borrador?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Borrador.ToString())
                {
                    // editar = si  / borrar = si / cancelar = no / ofertas = no
                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = true;
                    e.Row.Cells[9].Controls[0].Visible = false;
                    e.Row.Cells[10].Controls[0].Visible = false;
                }


                // esta cerrada?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Cerrada.ToString())
                {
                    // editar = si (solo visible)  / borrar = no / cancelar = no / ofertas = si

                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = false;
                    e.Row.Cells[9].Controls[0].Visible = false;
                    e.Row.Cells[10].Controls[0].Visible = true;
                }


                // esta abierta?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Abierta.ToString())
                {
                    // editar = si (solo visible)  / borrar = no / cancelar = si / ofertas = si
                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = false;
                    e.Row.Cells[9].Controls[0].Visible = true;
                    e.Row.Cells[10].Controls[0].Visible = true;
                }

                // esta cancelada?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Cancelada.ToString())
                {
                    // editar = si  / borrar = si / cancelar = no / ofertas = si
                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = true;
                    e.Row.Cells[9].Controls[0].Visible = false;
                    e.Row.Cells[10].Controls[0].Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarLicitaciones();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("MIS_OFERTAS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("MIS_OFERTAS"));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }


        protected void DgvUsuariosOfertas_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            validarAcceso("NEG009");

            if (e.CommandName == "CancelarLicitacion")
            {
                using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
                {
                    BE.LicitacionBE licitacionBE = new BE.LicitacionBE();

                    Int64 id = Int32.Parse(DgvUsuarios.DataKeys[Convert.ToInt16(e.CommandArgument)].Values[0].ToString());

                    licitacionBE.Identidad = id;

                    bll.CancelarLicitacion(licitacionBE);

                    NuevaBitacora((String.Format("Se cancela la licitacion {0} de la empresa {1}", licitacionBE.Identidad, ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                }
            }

            llenarLicitaciones();
        }

        protected void DgvUsuariosOfertas_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void DgvUsuariosOfertas_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuariosOfertas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarLicitaciones();
        }
        protected void DgvUsuariosOfertas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
        }

        public IDictionary<String, String> getFiltrosOferta()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void DgvUsuariosOfertas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        


            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }


        protected void llenarLicitacionesconOfertas()
        {
            BLL.LicitacionBLL LicitacionBLL = new BLL.LicitacionBLL();

            List<BE.LicitacionBE> orgs = LicitacionBLL.SelectAllVigenteConOfertaDeMiOrg(((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion);

            DgvUsuariosOfertas.AutoGenerateColumns = false;
            DgvUsuariosOfertas.DataSource = orgs;

            if (orgs != null)
            {
                txtCantRegistrosOfertas.Text = orgs.Count.ToString();
            }
            else
            {
                txtCantRegistrosOfertas.Text = "0";
            }
            DgvUsuariosOfertas.DataBind();
        }

        protected void DgvUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            validarAcceso("NEG009");

            if (e.CommandName == "VerOfertasdeMiLicitacion")
            {

                using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
                {



                    CargarControlesModalVerOfertasMiLicitacion(licitacionBLL.Select(new LicitacionBE { Identidad = Convert.ToInt64(((GridView)sender).DataKeys[Convert.ToInt16(e.CommandArgument.ToString())].Value) }));

                }

                divErrorOfertarLicitacion.Visible = false;
                lblErrorOfertarLicitacion.Text = string.Empty;



                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script language='javascript'>");
                sb.Append(@"$('#myModalVerOfertasMiLicitacion').modal('show');");
                sb.Append(@"</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript3", sb.ToString());



            }

            if (e.CommandName == "CancelarLicitacion")
            {

                using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
                {

                    LicitacionBE asd = new LicitacionBE { Identidad = Convert.ToInt64(((GridView)sender).DataKeys[Convert.ToInt16(e.CommandArgument.ToString())].Value) };

                    licitacionBLL.CancelarLicitacion(asd);
                }

                llenarLicitaciones();
            }


        }

        private void CargarControlesModalVerOfertasMiLicitacion(BE.LicitacionBE licitacionBE)
        {
            List<OfertaBE> ofertaBEs;

            divAlertaAceptarOferta.Visible = true;
            lblAlertaAceptarOferta.Text = Traducir("MSJ_WARN_OK_OFERTA");

            div6.Visible = false;


            int contador = 0;

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                ofertaBEs = bll.GetOfertasParaLicitacion(licitacionBE);
            }


            if (ofertaBEs.Count == 0)
            {
                lblAlertaAceptarOferta.Text = Traducir("MSJ_NO_OFERTAS");
            }


            foreach (BE.OfertaBE item in ofertaBEs)
            {

                HtmlGenericControl newpanel = new HtmlGenericControl("div");
                newpanel.Attributes.Add("class", "panel panel-default");


                HtmlGenericControl newControlhdr = new HtmlGenericControl("div");
                newControlhdr.Attributes.Add("class", "panel-heading");
                newControlhdr.Attributes.Add("role", "tab");
                newControlhdr.ID = String.Format("AcordeonItem" + contador);

                HtmlGenericControl htmlGenericControlh4 = new HtmlGenericControl("h4");
                htmlGenericControlh4.Attributes.Add("class", "panel-title");


                HtmlGenericControl newControlcollapse = new HtmlGenericControl("div");
                newControlcollapse.Attributes.Add("class", "panel-collapse collapse");
                newControlcollapse.Attributes.Add("role", "tabpanel");
                newControlcollapse.Attributes.Add("aria-labelledby", "MainContent_" + newControlhdr.ID);
                newControlcollapse.ID = String.Format("AcordeonItemcollapse" + contador);


                HtmlGenericControl htmlGenericControla = new HtmlGenericControl("a");
                htmlGenericControla.Attributes.Add("role", "button");
                htmlGenericControla.Attributes.Add("data-toggle", "collapse");
                htmlGenericControla.Attributes.Add("data-parent", "#accordion");
                htmlGenericControla.Attributes.Add("href", "#MainContent_" + newControlcollapse.ID);
                htmlGenericControla.Attributes.Add("aria-expanded", "false");
                htmlGenericControla.Attributes.Add("aria-controls", "MainContent_" + newControlcollapse.ID);

                string milbl;

                milbl = string.Format("<label>{0} {1}: -- {2} {3} </label>", Traducir("OFERTA"), contador + 1, Traducir("EMPRESA"), item.Organizacion.Razon_Social);

                if (item.Estado_Oferta.ToString() == Estado_OfertaBE.Aceptada.ToString())
                {
                    milbl = string.Format("<label>{0} {1}: -- {2} {3} </label> // {4}", Traducir("OFERTA"), contador + 1, Traducir("EMPRESA"), item.Organizacion.Razon_Social, Traducir("OFERTA_ACEPTADA"));

                }

                if (item.Estado_Oferta.ToString() == Estado_OfertaBE.Rechazada.ToString())
                {
                    milbl = string.Format("<label>{0} {1}: -- {2} {3} </label> // {4}", Traducir("OFERTA"), contador + 1, Traducir("EMPRESA"), item.Organizacion.Razon_Social, Traducir("OFERTA_RECHAZADA"));
                }

                if (item.Estado_Oferta.ToString() == Estado_OfertaBE.Cancelada.ToString())
                {
                    milbl = string.Format("<label>{0} {1}: -- {2} {3} </label> // {4}", Traducir("OFERTA"), contador + 1, Traducir("EMPRESA"), item.Organizacion.Razon_Social, Traducir("OFERTA_CANCELADA"));
                }

                if (item.Estado_Oferta.ToString() == Estado_OfertaBE.Vencida.ToString())
                {
                    milbl = string.Format("<label>{0} {1}: -- {2} {3} </label> // {4}", Traducir("OFERTA"), contador + 1, Traducir("EMPRESA"), item.Organizacion.Razon_Social, Traducir("OFERTA_VENCIDA"));
                }


                LiteralControl literal = new LiteralControl(milbl);
                literal.ID = String.Format("AcordeonItemlbl" + contador);

                htmlGenericControla.Controls.Add(literal);

                htmlGenericControlh4.Controls.Add(htmlGenericControla);

                newControlhdr.Controls.Add(htmlGenericControlh4);

                newpanel.Controls.Add(newControlhdr);


                HtmlGenericControl newControlbody = new HtmlGenericControl("div");
                newControlbody.Attributes.Add("class", "panel-body");


                //Comienzo controles internos del acordeon

                HtmlGenericControl divrowdatosOferta = new HtmlGenericControl("div");
                divrowdatosOferta.Attributes.Add("class", "row");


                HtmlGenericControl divnewlblestado = new HtmlGenericControl("div");
                divnewlblestado.Attributes.Add("class", "col-md-1");

                string milblestado = string.Format("<label>{0}</label>", Traducir("Estado"));
                LiteralControl literalestado = new LiteralControl(milblestado);

                divnewlblestado.Controls.Add(literalestado);


                HtmlGenericControl divnewtxtestadooferta = new HtmlGenericControl("div");
                divnewtxtestadooferta.Attributes.Add("class", "col-md-3");


                TextBox txtestadoOferta = new TextBox();
                txtestadoOferta.Attributes.Add("Class", "form-control pull-right");
                txtestadoOferta.ReadOnly = true;
                txtestadoOferta.ID = "txtestadoOferta_" + contador;

                txtestadoOferta.Text = item.Estado_Oferta.ToString();


                divnewtxtestadooferta.Controls.Add(txtestadoOferta);

                HtmlGenericControl divnewvaciocolmd4 = new HtmlGenericControl("div");
                divnewvaciocolmd4.Attributes.Add("class", "col-md-4");

                HtmlGenericControl divnewlblsubtotal = new HtmlGenericControl("div");
                divnewlblsubtotal.Attributes.Add("class", "col-md-1");

                string milblsubtotal = string.Format("<label>{0}</label>", Traducir("Subtotal"));
                LiteralControl literalsubtotal = new LiteralControl(milblsubtotal);

                divnewlblsubtotal.Controls.Add(literalsubtotal);


                HtmlGenericControl divnewtxtsubtotal = new HtmlGenericControl("div");
                divnewtxtsubtotal.Attributes.Add("class", "col-md-3");

                LiteralControl spacer = new LiteralControl("<br />");

                TextBox txtsubtotal = new TextBox();
                txtsubtotal.Attributes.Add("Class", "form-control pull-right");
                txtsubtotal.ReadOnly = true;
                txtsubtotal.ID = "txtSubtotal_" + contador;

                txtsubtotal.Text = item.Total.ToString("C");

                divnewtxtsubtotal.Controls.Add(txtsubtotal);

                divrowdatosOferta.Controls.Add(spacer);
                divrowdatosOferta.Controls.Add(divnewlblestado);
                divrowdatosOferta.Controls.Add(divnewtxtestadooferta);
                divrowdatosOferta.Controls.Add(divnewvaciocolmd4);
                divrowdatosOferta.Controls.Add(divnewlblsubtotal);
                divrowdatosOferta.Controls.Add(divnewtxtsubtotal);
                divrowdatosOferta.Controls.Add(spacer);

                GridView migridviewofertaitems = new GridView();
                migridviewofertaitems.ID = "migridviewofertaitems_" + contador;
                migridviewofertaitems.Attributes.Add("class", "table table-striped table-hover");
                migridviewofertaitems.Attributes.Add("CssClass", "table table-striped table-hover");
                migridviewofertaitems.Attributes.Add("Css-Class", "table table-striped table-hover");
                migridviewofertaitems.AllowSorting = true;
                migridviewofertaitems.CssClass = "table table-striped table-hover";
                migridviewofertaitems.AutoGenerateColumns = false;
                migridviewofertaitems.BorderStyle = BorderStyle.None;
                migridviewofertaitems.AllowPaging = true;
                migridviewofertaitems.PagerStyle.HorizontalAlign = HorizontalAlign.Right;
                migridviewofertaitems.PageSize = 15;
                migridviewofertaitems.GridLines = GridLines.None;
                migridviewofertaitems.PagerStyle.CssClass = "pagination-dgv";

                BoundField ColumnNumero= new BoundField();
                ColumnNumero.ReadOnly = true;
                ColumnNumero.DataField = "ContadorNumero";
                ColumnNumero.HeaderText = "No.";

                BoundField ColumnIdentidad = new BoundField();
                ColumnIdentidad.ReadOnly = true;
                ColumnIdentidad.DataField = "Identidad";
                ColumnIdentidad.HeaderText = "Identidad";

                BoundField ColumnItemDescripcion = new BoundField();
                ColumnItemDescripcion.ReadOnly = true;
                ColumnItemDescripcion.DataField = "ItemDescripcion";
                ColumnItemDescripcion.HeaderText = Traducir("ITEMS");

                BoundField ColumnCantidad = new BoundField();
                ColumnCantidad.ReadOnly = true;
                ColumnCantidad.DataField = "Cantidad";
                ColumnCantidad.HeaderText = Traducir("CANTIDAD");

                BoundField ColumnPrecioUnitario = new BoundField();
                ColumnPrecioUnitario.ReadOnly = true;
                ColumnPrecioUnitario.DataFormatString = "{0:c}";
                ColumnPrecioUnitario.DataField = "PrecioUnitario";
                ColumnPrecioUnitario.HeaderText = Traducir("PRECIO_UNIT_OFFER");


                //migridviewofertaitems.Columns.Add(templateField);
                //migridviewofertaitems.Columns.Add(ColumnIdentidad);
                migridviewofertaitems.Columns.Add(ColumnNumero);
                migridviewofertaitems.Columns.Add(ColumnItemDescripcion);
                migridviewofertaitems.Columns.Add(ColumnCantidad);
                migridviewofertaitems.Columns.Add(ColumnPrecioUnitario);


                DataTable dt = new DataTable();

                dt.Columns.Add("Identidad", typeof(Int64));
                dt.Columns.Add("ItemDescripcion", typeof(string));
                dt.Columns.Add("Cantidad", typeof(Decimal));
                dt.Columns.Add("PrecioUnitario", typeof(Decimal));
                dt.Columns.Add("ContadorNumero", typeof(Int64));


                foreach (BE.ItemsLicitacionBE itemlicitacion in licitacionBE.Pliego.ItemsLicitacion)
                {
                    DataRow dr = dt.NewRow();
                    //dr[0] = item.Identidad;
                    dr[0] = (item.items.Any(x => x.Item.Identidad == itemlicitacion.Item.Identidad)) ? (item.items.FirstOrDefault(x => x.Item.Identidad == itemlicitacion.Item.Identidad)).Identidad : -1;
                    dr[1] = itemlicitacion.Item.Descripcion;
                    dr[2] = itemlicitacion.Cantidad;

                    dr[3] = (item.items.Any(x => x.Item.Identidad == itemlicitacion.Item.Identidad)) ? (item.items.FirstOrDefault(x => x.Item.Identidad == itemlicitacion.Item.Identidad)).PrecioUnitario : 0;

                    dr[4] = contador + 1;

                    dt.Rows.Add(dr);
                }

                migridviewofertaitems.DataSource = dt;
                migridviewofertaitems.DataBind();



                UpdatePanel updatePanel = new UpdatePanel();
                updatePanel.ContentTemplateContainer.Controls.Add(spacer);
                updatePanel.ContentTemplateContainer.Controls.Add(migridviewofertaitems);




                HtmlGenericControl divbtnsAceptarRechazarOferta = new HtmlGenericControl("div");
                divbtnsAceptarRechazarOferta.Attributes.Add("class", "form-group clearfix");

                HtmlGenericControl divrowbtnsAceptarRechazarOferta = new HtmlGenericControl("div");
                divrowbtnsAceptarRechazarOferta.Attributes.Add("class", "row");




                HtmlGenericControl divbtnAceptarOferta = new HtmlGenericControl("div");
                divbtnAceptarOferta.Attributes.Add("class", "col-md-6");

                Button btnAceptarOferta = new Button();
                btnAceptarOferta.ID = "btnAceptarOferta_" + item.Identidad + "_" + licitacionBE.Identidad;
                btnAceptarOferta.CssClass = "btn btn-success btn-lg btn-block";
                btnAceptarOferta.Text = Traducir("ACEPTAR_OFERTA");


                //btnAceptarOferta.OnClientClick = "fncaceptar(this);";
                btnAceptarOferta.OnClientClick = "fncaceptarmodal(this);";
                //btnAceptarOferta.Click += new EventHandler(this.btnAceptarOferta_Click);

                divbtnAceptarOferta.Controls.Add(btnAceptarOferta);

                HtmlGenericControl divbtnRechazarOferta = new HtmlGenericControl("div");
                divbtnRechazarOferta.Attributes.Add("class", "col-md-6");

                Button btnRechazarOferta = new Button();
                btnRechazarOferta.ID = "btnRechazarOferta_" + item.Identidad + "_" + licitacionBE.Identidad;
                btnRechazarOferta.CssClass = "btn btn-danger btn-lg btn-block";
                btnRechazarOferta.Text = Traducir("RECHAZAR_OFERTA");

                //btnRechazarOferta.OnClientClick = "fncrechazar(this);";
                btnRechazarOferta.OnClientClick = "fncrechazarmodal(this);";


                HtmlGenericControl divnewvaciocolmd1 = new HtmlGenericControl("div");
                divnewvaciocolmd1.Attributes.Add("class", "col-md-1");


                divbtnRechazarOferta.Controls.Add(btnRechazarOferta);

                divrowbtnsAceptarRechazarOferta.Controls.Add(divbtnAceptarOferta);

                divrowbtnsAceptarRechazarOferta.Controls.Add(divbtnRechazarOferta);


                divbtnsAceptarRechazarOferta.Controls.Add(divrowbtnsAceptarRechazarOferta);

                LiteralControl spacertest = new LiteralControl("<br>");

                newControlbody.Controls.Add(divrowdatosOferta);
                newControlbody.Controls.Add(spacertest);

                if (item.Estado_Oferta == Estado_OfertaBE.Presentada)
                {
                    updatePanel.ContentTemplateContainer.Controls.Add(spacertest);
                    updatePanel.ContentTemplateContainer.Controls.Add(divbtnsAceptarRechazarOferta);
                }


                newControlbody.Controls.Add(updatePanel);
                newControlbody.Controls.Add(spacertest);

                //if (item.Estado_Oferta == Estado_OfertaBE.Presentada)
                //{
                //    newControlbody.Controls.Add(divbtnsAceptarRechazarOferta);
                //}

                newControlcollapse.Controls.Add(newControlbody);
                newpanel.Controls.Add(newControlcollapse);
                accordion.Controls.Add(newpanel);
                contador++;
            }



        }

        protected void DgvUsuariosOfertas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG009");

            BE.LicitacionBE licitacionBE = new LicitacionBE();

            int indicelicitacion = Convert.ToInt16(((GridView)sender).DataKeys[e.RowIndex].Value);

            licitacionBE.Identidad = indicelicitacion;


            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {
                licitacionBE = licitacionBLL.Select(licitacionBE);


            }



            BE.OfertaBE nueva = new OfertaBE();

            nueva.Organizacion = ((UsuarioBE)Session["UsuarioLogueado"]).Organizacion;

            //existe una oferta en estado borrador para esa empresa y licitacion?

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                if (bll.SelectByLicitacionOrgBorrador(licitacionBE, nueva) != null)
                {
                    // Si, existe

                    nueva = bll.SelectByLicitacionOrgBorrador(licitacionBE, nueva);


                }
                else
                {
                    // No existe Borrador

                    // Existe en algun otro estado que no sea BORRADOR ni CANCELADA? 

                    if (bll.SelectByLicitacionOrg(licitacionBE, nueva) != null)
                    {
                        //Si existe, NO PUEDE OFERTAR, podemos mostrar status o algo asi


                        nueva = bll.SelectByLicitacionOrg(licitacionBE, nueva);


                    }
                    else
                    {

                        //No existe borrador, podemos crear uno.

                        foreach (ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
                        {
                            BE.ItemsOfertaBE itemsOfertaBE = new ItemsOfertaBE();
                            itemsOfertaBE.Item = item.Item;
                            itemsOfertaBE.PrecioUnitario = 0;
                            nueva.items.Add(itemsOfertaBE);

                        }

                        //Insertamos 

                        using (BLL.OfertaBLL blloferta = new BLL.OfertaBLL())
                        {
                            blloferta.Insert(nueva, licitacionBE);
                        }

                        //calculamos valores target 
                        using (BLL.LicitacionBLL licitacionBLL1 = new BLL.LicitacionBLL())
                        {
                            licitacionBLL1.CalcularPreciosTarget(licitacionBE, nueva);
                        }

                        //asignamos el borrador recien creado
                        using (BLL.OfertaBLL blloferta2 = new BLL.OfertaBLL())
                        {
                            blloferta2.SelectByLicitacionOrgBorrador(licitacionBE, nueva);
                        }



                    }

                }
            }





            ViewState["MiLicitacionEdit"] = licitacionBE.Identidad.ToString();
            MiLicitacionEdit = licitacionBE;
            ViewState["MiOfertaEdit"] = nueva.Identidad.ToString();
            MiOfertaEdit = nueva;


            VerOfertaModal(licitacionBE, nueva);
        }


        private void VerOfertaModal(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {

                CargarControlesModalOferta(licitacionBE, nueva);

            }

            divErrorOfertarLicitacion.Visible = false;
            lblErrorOfertarLicitacion.Text = string.Empty;



            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"$('#myModalOfertarLicitacion').modal('show');");
            sb.Append(@"</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript2", sb.ToString());

        }

        private void CargarControlesModalOferta(LicitacionBE licitacionBE, OfertaBE nueva)
        {


            CargarPanelLicitacionModalOferta(licitacionBE);

            CargarDGVPliegoLicitacionModalOferta(licitacionBE, nueva);

            CargarPanelDatosOferta(nueva);

        }

        private void CargarPanelDatosOferta(OfertaBE nueva)
        {
            txtestadoOferta.Text = nueva.Estado_Oferta.ToString();
            txtSubtotalOferta.Text = nueva.Total.ToString("C");
            //txtSubtotalOferta.Text = (Convert.ToDecimal(nueva.Total)).ToString();
        }

        private void CargarDGVPliegoLicitacionModalOferta(DataTable dataTable)
        {
            DgvPliegoItemsOferta.DataSource = null;
            DgvPliegoItemsOferta.AutoGenerateColumns = false;

            DgvPliegoItemsOferta.DataSource = dataTable;
            DgvPliegoItemsOferta.DataBind();

            MiDTEdit = dataTable;

            DgvPliegoItemsOferta.Columns[1].Visible = false;
        }

        private void CargarDGVPliegoLicitacionModalOferta(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            Dictionary<BE.ItemBE, decimal> dicPrecioSugerido;

            DgvPliegoItemsOferta.DataSource = null;
            DgvPliegoItemsOferta.AutoGenerateColumns = false;

            DataTable dt = new DataTable();



            dt.Columns.Add("ItemDescripcion", typeof(string));
            dt.Columns.Add("Cantidad", typeof(Decimal));
            dt.Columns.Add("PrecioUnitario", typeof(Decimal));
            dt.Columns.Add("Identidad", typeof(Int64));
            dt.Columns.Add("PrecioSugerido", typeof(Decimal));

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                dicPrecioSugerido = bll.LlenarOfertaRecomendada(licitacionBE, nueva);
            }

            divnodatasugerido.Visible = false;


            foreach (BE.ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
            {
                DataRow dr = dt.NewRow();
                //dr[0] = item.Identidad;
                dr[0] = item.Item.Descripcion;
                dr[1] = item.Cantidad;

                dr[2] = (nueva.items.FirstOrDefault(x => x.Item != null && x.Item.Descripcion == item.Item.Descripcion)) != null ? (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).PrecioUnitario : 0;
                dr[3] = (nueva.items.FirstOrDefault(x => x.Item != null && x.Item.Descripcion == item.Item.Descripcion)) != null ? (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).Identidad : 0;



                //dr[2] = (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).PrecioUnitario;
                //dr[3] = (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).Identidad;

                decimal preciosugerido = (dicPrecioSugerido.Keys.Any(x => x.Identidad == item.Item.Identidad)) ? dicPrecioSugerido[dicPrecioSugerido.Keys.FirstOrDefault(x => x.Identidad == item.Item.Identidad)] : 0;



                //dr[4] = preciosugerido;

                if (preciosugerido != 0)
                {
                    dr[4] = preciosugerido;
                }
                else
                {
                    dr[0] = (dr[0].ToString() + "(*)");
                    dr[4] = preciosugerido;
                    divnodatasugerido.Visible = true;
                }



                dt.Rows.Add(dr);
            }

            DgvPliegoItemsOferta.DataSource = dt;
            DgvPliegoItemsOferta.DataBind();

            if (MiDTEdit == null)
            {
                MiDTEdit = dt;

                ViewState["MiDTEdit"] = dt;
            }

            if (nueva.Estado_Oferta != Estado_OfertaBE.Borrador)
            {
                DgvPliegoItemsOferta.Columns[6].Visible = false;
                btnPublicarOfertaLicitacion.Visible = false;


                lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC_PRES");


                btnCancelarOfertaLicitacion.Visible = true;

                if (nueva.Estado_Oferta == Estado_OfertaBE.Cancelada)
                {
                    btnCancelarOfertaLicitacion.Visible = false;
                }



            }
            else
            {
                DgvPliegoItemsOferta.Columns[6].Visible = true;
                btnPublicarOfertaLicitacion.Visible = true;
                btnCancelarOfertaLicitacion.Visible = false;
                lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC_OFERT");
            }


            DgvPliegoItemsOferta.Columns[1].Visible = false;

            if (nueva.Estado_Oferta != BE.Estado_OfertaBE.Borrador)
            {
                DgvPliegoItemsOferta.Columns[6].Visible = false;
            }

            //DgvPliegoItemsOferta.DataSource = null;
            //DgvPliegoItemsOferta.AutoGenerateColumns = false;

            //DataTable dt = new DataTable();


            //dt.Columns.Add("ItemDescripcion", typeof(string));
            //dt.Columns.Add("Cantidad", typeof(Decimal));
            //dt.Columns.Add("PrecioUnitario", typeof(Decimal));
            //dt.Columns.Add("Identidad", typeof(Int64));

            //foreach (BE.ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
            //{
            //    DataRow dr = dt.NewRow();
            //    //dr[0] = item.Identidad;
            //    dr[0] = item.Item.Descripcion;
            //    dr[1] = item.Cantidad;

            //    dr[2] = (nueva.items.FirstOrDefault(x => x.Item != null && x.Item.Descripcion == item.Item.Descripcion)) != null ? (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).PrecioUnitario : 0;
            //    dr[3] = (nueva.items.FirstOrDefault(x => x.Item != null && x.Item.Descripcion == item.Item.Descripcion)) != null ? (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).Identidad : 0;


            //    dt.Rows.Add(dr);
            //}

            //DgvPliegoItemsOferta.DataSource = dt;
            //DgvPliegoItemsOferta.DataBind();

            //if (MiDTEdit == null)
            //{
            //    MiDTEdit = dt;

            //    ViewState["MiDTEdit"] = dt;
            //}

            //if (nueva.Estado_Oferta != Estado_OfertaBE.Borrador)
            //{
            //    DgvPliegoItemsOferta.Columns[5].Visible = false;
            //    btnPublicarOfertaLicitacion.Visible = false;


            //    lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC_PRES");


            //    btnCancelarOfertaLicitacion.Visible = true;

            if (nueva.Estado_Oferta == Estado_OfertaBE.Cancelada || nueva.Estado_Oferta == Estado_OfertaBE.Vencida || nueva.Estado_Oferta == Estado_OfertaBE.Rechazada || nueva.Estado_Oferta == Estado_OfertaBE.Aceptada)
            {
                btnCancelarOfertaLicitacion.Visible = false;
            }



            //}
            //else
            //{
            //    DgvPliegoItemsOferta.Columns[5].Visible = true;
            //    btnPublicarOfertaLicitacion.Visible = true;
            //    btnCancelarOfertaLicitacion.Visible = false;
            //    lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC_OFERT");
            //}


            //DgvPliegoItemsOferta.Columns[1].Visible = false;

        }

        private void CargarPanelLicitacionModalOferta(LicitacionBE licitacionBE)
        {
            titmodalpropOferta.Text = Traducir("Detalle_Licitacion");

            txtRznSocialOferta.Text = licitacionBE.Organizacion.Razon_Social;

            txtEstadoLicOferta.Text = licitacionBE.Estado_Licitacion.ToString();

            txtRubroOferta.Text = licitacionBE.Rubro.Descripcion;


            txtAreaDescripcionOferta.InnerText = licitacionBE.Descripcion;

            txtAreaNotasOferta.InnerText = licitacionBE.Observaciones;

            txtAreaEntregaOferta.InnerText = licitacionBE.Pliego.DetallesEntrega;

            milabelfechahastaOferta.Text = licitacionBE.FechaCierre.ToString("dd'/'MM'/'yyyy");
            milabelfechadesdeOferta.Text = licitacionBE.FechaApertura.ToString("dd'/'MM'/'yyyy");
        }


        protected void DgvPliegoItemsOferta_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }

        }

        protected void DgvPliegoItemsOferta_RowEditing(object sender, GridViewEditEventArgs e)
        {

            OfertaBE asdasda;

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                asdasda = bll.Select((new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) }));
            }

            LicitacionBE asdasda123;

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                asdasda123 = bll.Select((new BE.LicitacionBE { Identidad = Convert.ToInt64(ViewState["MiLicitacionEdit"]) }));
            }

            DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];

            divErrorOfertarLicitacion.Visible = false;

            DgvPliegoItemsOferta.EditIndex = e.NewEditIndex;
            CargarDGVPliegoLicitacionModalOferta(MidataTable);


        }

        protected void DgvPliegoItemsOferta_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvPliegoItemsOferta.EditIndex = -1;
            DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];
            CargarDGVPliegoLicitacionModalOferta(MidataTable);
        }

        protected void DgvPliegoItemsOferta_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            BE.ItemsOfertaBE itemsOfertaBE = new BE.ItemsOfertaBE() { Identidad = (Convert.ToInt64(((GridView)sender).DataKeys[e.RowIndex].Value)) };

            using (BLL.ItemsOfertaBLL bll = new BLL.ItemsOfertaBLL())
            {
                itemsOfertaBE = bll.Select(itemsOfertaBE);

                if (!(decimal.TryParse(e.NewValues[0].ToString(), out decimal asdasd123)))
                {
                    return;
                }

                itemsOfertaBE.PrecioUnitario = Convert.ToDecimal(e.NewValues[0]);

                BE.OfertaBE miofertaparaItem = new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) };

                bll.Update(itemsOfertaBE, miofertaparaItem);

                ((DataTable)ViewState["MiDTEdit"]).Rows[e.RowIndex]["PrecioUnitario"] = itemsOfertaBE.PrecioUnitario;

                using (BLL.OfertaBLL bll123 = new BLL.OfertaBLL())
                {
                    bll123.CalcularTarget(((DataTable)ViewState["MiDTEdit"]), bll123.Select(miofertaparaItem));

                    CargarPanelDatosOferta(bll123.Select(miofertaparaItem));

                }


                DgvPliegoItemsOferta.EditIndex = -1;
                DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];
                CargarDGVPliegoLicitacionModalOferta(MidataTable);

            }

        }

        protected void DgvPliegoItemsOferta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "GuardarPrecio")
            {

            }
        }

        protected void DgvPliegoItemsOferta_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
        }

        protected void btnPublicarOfertaLicitacion_Click(object sender, EventArgs e)
        {

            DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];

            foreach (DataRow item in MidataTable.Rows)
            {
                if (Convert.ToDecimal(item["PrecioUnitario"].ToString()) == 0)
                {
                    lblErrorOfertarLicitacion.Text = Traducir("MSJ_ERR_VER_PRECIO");
                    divErrorOfertarLicitacion.Visible = true;
                    return;
                }
            }

            BE.OfertaBE miofertaparaItem = new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) };

            miofertaparaItem.FechaPresentacion = DateTime.Now.Date;

            miofertaparaItem.Estado_Oferta = BE.Estado_OfertaBE.Presentada;


            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                bll.Update(miofertaparaItem);   

                NuevaBitacora((String.Format("Nueva Oferta de {0} para la licitacion {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social, Convert.ToInt64(ViewState["MiLicitacionEdit"]))), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);


            }

            Response.Redirect(Request.RawUrl);
        }

        protected void btnCancelarOfertaLicitacion_Click(object sender, EventArgs e)
        {
            BE.OfertaBE miofertaparaItem = new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) };

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {

                BE.OfertaBE ofertatmp = bll.Select(miofertaparaItem);

                ofertatmp.Estado_Oferta = BE.Estado_OfertaBE.Cancelada;

                bll.Update(ofertatmp);

                NuevaBitacora((String.Format("Se Retiro la Oferta de {0} para la licitacion {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social, Convert.ToInt64(ViewState["MiLicitacionEdit"]))), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);


            }

            Response.Redirect(Request.RawUrl);

        }

        protected void btnaceptaroferta_js_Click(object sender, EventArgs e)
        {





            string OfertaBtn = hdnOferta.Value;
            //MainContent_btnAceptarOferta_
            string idofertaylicitacion = OfertaBtn.Substring(29);

            string[] ids = idofertaylicitacion.Split('_');

            string idoferta = ids[0];
            string idlicitacion = ids[1];

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                BE.LicitacionBE licitacionBE = new LicitacionBE() { Identidad = Convert.ToInt64(idlicitacion) };

                licitacionBE = bll.Select(licitacionBE);

                using (BLL.OfertaBLL blloferta = new BLL.OfertaBLL())
                {

                    BE.OfertaBE mioferta = new OfertaBE() { Identidad = Convert.ToInt64(idoferta) };

                    mioferta = blloferta.Select(mioferta);

                    bll.AceptarOferta(licitacionBE, mioferta);

                    NuevaBitacora((String.Format("Se Acepto la Oferta  {0} para la licitacion {1}", idoferta, idlicitacion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);


                }

            }

            llenarLicitaciones();
            llenarLicitacionesconOfertas();

        }

        protected void btnaceptarofertamodal_js_Click(object sender, EventArgs e)
        {

            string OfertaBtn = hdnOferta.Value;
            //MainContent_btnAceptarOferta_
            string idofertaylicitacion = OfertaBtn.Substring(29);

            string[] ids = idofertaylicitacion.Split('_');

            string idoferta = ids[0];
            string idlicitacion = ids[1];


            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                BE.LicitacionBE licitacionBE = new LicitacionBE() { Identidad = Convert.ToInt64(idlicitacion) };

                licitacionBE = bll.Select(licitacionBE);

                using (BLL.OfertaBLL blloferta = new BLL.OfertaBLL())
                {

                    BE.OfertaBE mioferta = new OfertaBE() { Identidad = Convert.ToInt64(idoferta) };

                    mioferta = blloferta.Select(mioferta);


                    CargarModalConfirmarAceptarOferta(mioferta);

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script language='javascript'>");
                    sb.Append(@"$('#myModalAceptarOferta').modal('show');");
                    sb.Append(@"</script>");
                    ClientScript.RegisterStartupScript(this.GetType(), "JSScript5", sb.ToString());
                }

            }


        }

        private void CargarModalConfirmarAceptarOferta(OfertaBE ofertaBE)
        {
            lblmessageconfirmaceptaroferta.Text = String.Format(Traducir("ConfirmOfertaModalTexto"), ofertaBE.Organizacion.Razon_Social);

        }

        protected void btnrechazaroferta_js_Click(object sender, EventArgs e)
        {
            string OfertaBtn = hdnOferta.Value;
            //MainContent_btnRechazarOferta_
            string idofertaylicitacion = OfertaBtn.Substring(30);

            string[] ids = idofertaylicitacion.Split('_');

            string idoferta = ids[0];
            string idlicitacion = ids[1];

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                BE.LicitacionBE licitacionBE = new LicitacionBE() { Identidad = Convert.ToInt64(idlicitacion) };

                licitacionBE = bll.Select(licitacionBE);

                using (BLL.OfertaBLL blloferta = new BLL.OfertaBLL())
                {

                    BE.OfertaBE mioferta = new OfertaBE() { Identidad = Convert.ToInt64(idoferta) };

                    mioferta = blloferta.Select(mioferta);

                    bll.RechazarOferta(licitacionBE, mioferta);


                    NuevaBitacora((String.Format("Se Rechazo la Oferta {0} para la licitacion {1}", idoferta, idlicitacion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);


                }

                CargarControlesModalVerOfertasMiLicitacion(licitacionBE);

            }


        }

        protected void btnrechazarofertamodal_js_Click(object sender, EventArgs e)
        {

            string OfertaBtn = hdnOferta.Value;
            //MainContent_btnAceptarOferta_
            string idofertaylicitacion = OfertaBtn.Substring(29);

            string[] ids = idofertaylicitacion.Split('_');

            string idoferta = ids[1];
            string idlicitacion = ids[2];


            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                BE.LicitacionBE licitacionBE = new LicitacionBE() { Identidad = Convert.ToInt64(idlicitacion) };

                licitacionBE = bll.Select(licitacionBE);

                using (BLL.OfertaBLL blloferta = new BLL.OfertaBLL())
                {

                    BE.OfertaBE mioferta = new OfertaBE() { Identidad = Convert.ToInt64(idoferta) };

                    mioferta = blloferta.Select(mioferta);


                    CargarModalConfirmarRechazarOferta(mioferta);

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script language='javascript'>");
                    sb.Append(@"$('#myModalRechazarOferta').modal('show');");
                    sb.Append(@"</script>");
                    ClientScript.RegisterStartupScript(this.GetType(), "JSScript555", sb.ToString());
                }

            }




        }

        private void CargarModalConfirmarRechazarOferta(OfertaBE mioferta)
        {
            lblmessageconfirmarrechazaroferta.Text = String.Format(Traducir("RechazarOfertaModalTexto"), mioferta.Organizacion.Razon_Social);
        }
    }


}

//public class MyTemplate : ITemplate
//{
//    #region ITemplate Members

//    public void InstantiateIn(Control container)
//    {

//        Label asd = new Label();
//        asd.Text = "<%# Container.DataItemIndex + 1 %>";





//        container.Controls.Add(asd);
//    }

//    #endregion
//}




