﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminRecalcularDigitosVerificadores.aspx.cs" Inherits="LicitarActual.AdminRecalcularDigitosVerificadores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br /><br />
    
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span><asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelMensajeSuccess" Visible="false" runat="server">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Mensaje:</span><asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label></div>
    </asp:Panel>
    <div class="form-group clearfix">
        <asp:Button ID="btnRecalcular" runat="server" OnClick="btnRecalcular_Click"  CssClass="btn btn-warning" Text="Recalcular Digitos Verificadores"  />
        <asp:Button ID="btnVerificar" runat="server" OnClick="btnVerificar_Click" CssClass="btn btn-danger" Text="Verificar Integridad"  />
    </div>
        <script>
            function ShowCurrentTime() {
                waitingDialog.show();
                $.ajax({
                    type: "POST",
                    url: '<%= ResolveUrl("AdminRecalcularDigitosVerificadores.aspx/VerificarDigitosVerificadores") %>',
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        if (data.d == true) {
                            var v = document.getElementById('<%=PanelMensajeSuccess.ClientID %>');
                        } else {
                            var v = document.getElementById('<%=PanelMensajeRespuesta.ClientID %>');
                        }
                    }
                });
            }

    </script>
</asp:Content>
