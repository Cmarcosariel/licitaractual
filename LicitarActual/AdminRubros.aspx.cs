﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminRubros : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("RUBROS");
            validarAcceso("NEG003");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarRubros();
            Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG003");
            BLL.RubroBLL usuarioBll = new BLL.RubroBLL();
            BE.RubroBE usuarioBe = new BE.RubroBE();
            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {

                if (bll.SelectAllByID_Rubro(usuarioBe).Count > 0)
                {
                    string mensaje = Traducir("RUB_ERR_LIC_ENCURSO");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }

            }

            using (BLL.Clase_ItemBLL bll2 = new BLL.Clase_ItemBLL())
            {

                if (bll2.SelectAllByID_Rubro(usuarioBe).Count > 0)
                {
                    string mensaje = Traducir("RUB_ERR_CAT_EXIST");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }

            }




            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino el Rubro {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarRubros();
        }
        protected void llenarRubros()
        {
            BLL.RubroBLL RubroBLL = new BLL.RubroBLL();

            List<BE.RubroBE> orgs = RubroBLL.SelectAll();

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = orgs;

            if (orgs != null)
            {
                txtCantRegistros.Text = orgs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG003");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminRubrosForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarRubros();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarRubros();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.RubroBLL orgbll = new BLL.RubroBLL();
                List<BE.RubroBE> usuarios = orgbll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    TableCell statusCell = e.Row.Cells[4];
            //    if (statusCell.Text == "False")
            //    {
            //        statusCell.Text = "No";
            //    }
            //    if (statusCell.Text == "True")
            //    {
            //        statusCell.Text = "Si";
            //    }
            //}

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarRubros();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("RUBROS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("RUBROS"));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}