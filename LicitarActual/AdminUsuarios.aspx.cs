﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminUsuarios : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("USUARIOS");
            validarAcceso("ADM014");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarUsuarios();
            Page.DataBind();
        }

        protected void llenarGrupos()
        {

        }

        protected void llenarUsuarios()
        {
            BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();
            List<BE.UsuarioBE> usuarios = usuarioBll.SelectAll();
            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = usuarios;
            if (usuarios != null)
            {
                txtCantRegistros.Text = usuarios.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }

        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {



            validarAcceso("ADM014");
            BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();
            BE.UsuarioBE usuarioBe = new BE.UsuarioBE();
            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());

            if (usuarioBe.Identidad == 6)
            {
                string mensaje = Traducir("USU_ADM_NOT_DEL");
                lblMessage.Text = mensaje;
                miPopUp();
                return;
            }



            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino al usuario {0}", usuarioBe.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarUsuarios();

            if (usuarioBe.Identidad == ((BE.UsuarioBE)Session["UsuarioLogueado"]).Identidad)
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }

        }
        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("ADM014");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminUsuarioForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarUsuarios();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarUsuarios();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();
                List<BE.UsuarioBE> usuarios = usuarioBll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TableCell statusCell = e.Row.Cells[4];
                if (statusCell.Text == "False")
                {
                    statusCell.Text = "No";
                }
                if (statusCell.Text == "True")
                {
                    statusCell.Text = "Si";
                }
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarUsuarios();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("USUARIOS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("USUARIOS"));
        }
    }
}