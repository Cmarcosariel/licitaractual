﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminNuevoIdioma : Cascara

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM015");
            
            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarDropdownIdiomas();
            }

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            using (BLL.IdiomaBLL bll = new BLL.IdiomaBLL())
            {

                BE.IdiomaBE idioma = new BE.IdiomaBE() { Descripcion = iptIdioma.Text, CreateDate = DateTime.Now.Ticks, ChangeDate = DateTime.Now.Ticks };
                BE.IdiomaBE idiomaComplemento = new BE.IdiomaBE() { Identidad = Convert.ToInt64(ddIdiomas123.SelectedValue.ToString()) };
                
                if (bll.GetByDescripcion(idioma) == null)
                {
                    bll.InsertIdiomaNuevo(idioma, idiomaComplemento);
                    NuevaBitacora((String.Format("el usuario {0} Creo el idioma {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, idioma.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                    Application.Add(idioma.Descripcion, bll.ObtenerMensajesPorIdioma(bll.GetByDescripcion(idioma)));

                    Response.Redirect("/AdminIdiomaForm.aspx?id=" + bll.GetByDescripcion(idioma).Identidad);
                }
                else
                {
                    PanelMensajeRespuesta.Visible = true;
                    lblError.Text = Traducir("IDIOMA_CLAVE_EXISTE");
                }

            }

        }
        protected void cargarDropdownIdiomas()
        {
                using (BLL.IdiomaBLL bll = new BLL.IdiomaBLL())
                {
                    ddIdiomas123.DataMember = "Idioma";
                    ddIdiomas123.DataValueField = "Identidad";
                    ddIdiomas123.DataTextField = "Descripcion";
                    ddIdiomas123.DataSource = bll.SelectAll();
                    ddIdiomas123.Items.Insert(0, new ListItem("Seleccione", ""));
                    ddIdiomas123.DataBind();
                }
        }


    }
}