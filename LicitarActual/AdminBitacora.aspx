﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminBitacora.aspx.cs" Inherits="LicitarActual.AdminBitacora" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <style>
            <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>

    </style>








    <div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="litBitacora" runat="server" Text='<%# Traducir("BITACORA") %>'></asp:Literal></strong>
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="row">
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default bg-panel">
                        <div class="panel-body">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon"><asp:Label ID="txtBuscarUsuario" runat="server" Text=""></asp:Label></div>
                                    <%--TODO: Translate!--%>
                                    <asp:DropDownList ID="ddlUsuario" runat="server" AutoPostBack="false" CssClass="form-control input-sm"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon"><asp:Label ID="txtBuscarCategoria" runat="server" Text=""></asp:Label></div>
                                    <%--TODO: Translate!--%>
                                    <asp:DropDownList ID="ddlCategoria" runat="server" AutoPostBack="false" CssClass="form-control input-sm"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon"><asp:Label ID="txtBuscarTipoEvento" runat="server" Text=""></asp:Label></div>
                                    <%--TODO: Translate!--%>
                                    <asp:DropDownList ID="ddlTipoEvento" runat="server" AutoPostBack="false" CssClass="form-control input-sm"></asp:DropDownList>
                                </div>
                            </div>
                            <div style="text-align: right" class="col-md-3">
                                <asp:Button ID="btnBuscar" runat="server" Text="asdasdasdssdsdsdsd" CssClass="btn btn-info" OnClick="btnBuscar_Click" />
                                <a href="/AdminBitacora.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;<asp:Literal ID="txtLimpiar" runat="server" Text=""></asp:Literal></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %><strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_10") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="dgvBitacora" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None"
                    CssClass="table table-striped table-hover" AllowPaging="True"
                    GridLines="None" PagerStyle-CssClass="pagination-ys "
                    PagerStyle-HorizontalAlign="Right"
                    OnPageIndexChanging="dgvBitacora_PageIndexChanging"
                    OnRowDataBound="dgvBitacora_RowDataBound"
                    PageSize="10">
                    <Columns>
                        <asp:TemplateField HeaderText="FECHA">
                            <ItemTemplate><%# FormatTimeStamp(Eval("Timestamp")) %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" HeaderText="DESCRIPCION" />
                        <asp:BoundField DataField="Evento_Bitacora.Descripcion" HeaderText="EVENTO_DESCRIPCION" />
                        <asp:BoundField DataField="Evento_Bitacora.Severidad" HeaderText="EVENTO_SEVERIDAD" />
                        <asp:BoundField DataField="Usuario.NombreUsuario" HeaderText="USUARIO" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">&nbsp;</div>
            <div class="col-md-3 pull-right">
                <asp:Button ID="btnExportarExcel" runat="server" OnClick="btnExportarExcel_Click" Text="asdasdasdqwe" CssClass="btn btn-sm btn-primary right" />
                &nbsp; &nbsp;
                <asp:Button ID="btnExportarPDF" runat="server" OnClick="btnExportarPDF_Click" Text="asdasdasd" CssClass="btn btn-sm btn-primary right" />
            </div>
        </div>

    </div>
</asp:Content>
