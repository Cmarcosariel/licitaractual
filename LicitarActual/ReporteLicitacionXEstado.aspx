﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="ReporteLicitacionXEstado.aspx.cs" Inherits="LicitarActual.ReporteLicitacionXEstado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

    <script>
        window.onload = function () {

            var dataPoints = [];

            var options = {
                animationEnabled: true,

                theme: "light2",
                title: {
                    text: "Desglose de Licitaciones por Estado"
                },
                axisX: {
                    title: "Empresa",
                    //valueFormatString: "DD MMM YYYY",
                },
                axisY: {
                    title: "Cantidad de Ofertas",
                    titleFontSize: 24
                },
                data: [{
                    type: "pie",
                    startAngle: 45,
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabel: "{label} ({y})",
                    yValueFormatString: "#,##0.#" % "",
                    dataPoints: dataPoints

                }]

            };

            function addData(data) {
                for (var i = 0; i < data.length; i++) {
                    dataPoints.push({

                        label: data[i].Estado,
                        y: data[i].cantidadLicitaciones
                    });
                }
                $("[id$=chartContainer]").CanvasJSChart(options);

            }
            var values = $('[id$=hdnJson123456]').val();
            addData(jQuery.parseJSON(values));
            //https://canvasjs.com/jquery-charts/json-data-api-ajax-chart/

        }
    </script>

    <asp:HiddenField ID="hdnJson123456" runat="server" />

    <div class="row">
        <h1 class="centrar-texto-Ok"><%= Traducir("REPORTES_LICITACION") %></h1>
        <br />
        <br />
        <div class="row ">
            <div class="col-md-11">
                <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            </div>
            <%--            <div class="col-md-1">
                <asp:Button ID="btnExportarPdf2" runat="server" OnClick="btnExportarPdf1_Click" Text='<%# Traducir("EXPORTAR_PDF") %>' CssClass="btn btn-primary btn-outline-white btn-misProyectos" />
            </div>--%>
        </div>
        <br />
        <br />


    </div>

</asp:Content>
