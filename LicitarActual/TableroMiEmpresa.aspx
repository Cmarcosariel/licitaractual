﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="TableroMiEmpresa.aspx.cs" Inherits="LicitarActual.TableroMiEmpresa" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <style>
        .lblinvisible {
            opacity: 0;
        }

        @media screen {
            #printSection {
                display: none;
            }
        }

        @media print {
            body * {
                visibility: hidden;
            }

            #printSection, #printSection * {
                visibility: visible;
            }

            #printSection {
                position: absolute;
                left: 0;
                top: 0;
            }
        }

        .ChkBoxconPadding input[type="checkbox"] {
            margin-right: 5px;
        }
    </style>


    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>


    <br />

    <div style="display: none">
        <asp:Button ID="btnaceptaroferta_js" Text="text" runat="server" OnClick="btnaceptaroferta_js_Click" />
        <asp:Button ID="btnrechazaroferta_js" Text="text" runat="server" OnClick="btnrechazaroferta_js_Click" />
        <asp:Button ID="btnaceptarofertamodal_js" Text="text" runat="server" OnClick="btnaceptarofertamodal_js_Click" />
        <asp:Button ID="btnrechazarofertamodal_js" Text="text" runat="server" OnClick="btnrechazarofertamodal_js_Click" />
        <asp:HiddenField ID="hdnOferta" runat="server" />
    </div>



    <div class="panel panel-default" id="DivMisLicitaciones" runat="server">
        <div class="panel-heading">
            <h3 class="panel-title"><%= Traducir("LICITACION") %></h3>
        </div>
        <div class="panel-body">

            <div class="content-header">
                <div class="header-section">
                    <div class="row">
                        <div class="col-md-2">
                            <h4>
                                <strong>
                                    <asp:Literal ID="litUsuarios" runat="server"></asp:Literal>
                                </strong>
                            </h4>
                        </div>
                        <div class="col-md-10">
                            <a href="AdminLicitacionForm.aspx" class="btn btn-success pull-left">
                                <span class="glyphicon glyphicon-plus"></span>
                                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                            </a>
                        </div>
                    </div>


                    <br />
                </div>
            </div>

            <div class="block full" style="display: none">
                <div class="row">
                    <div id="filter-panel" class="filter-panel">
                        <div class="panel panel-default bg-panel">
                            <div class="panel-body">

                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon"><%= Traducir("Titulo") %></div>
                                        <asp:TextBox ID="filtroUsername" runat="server" MaxLength="20" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:Button ID="btnBuscar" runat="server" Text='Buscar' CssClass="btn btn-info" OnClick="btnBuscar_Click" />
                                <a href="/AdminLicitacion.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;<%= Traducir("LIMPIAR") %></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block full">
                <div class="table-responsive">
                    <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                        <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong>  <%= Traducir("REGISTROS") %>
                    </span>
                    <span class="help-block pull-right"><%= Traducir("MOSTRANDO_15") %></span>
                    <div style="border-top: 1px solid black;"></div>
                    <asp:GridView ID="DgvUsuarios" runat="server" AllowSorting="True"
                        AutoGenerateColumns="False" BorderStyle="None"
                        CssClass="table table-striped table-hover" AllowPaging="True"
                        GridLines="None" PagerStyle-CssClass="pagination-ys "
                        PagerStyle-HorizontalAlign="Right"
                        DataKeyNames="Identidad"
                        OnRowDataBound="DgvUsuarios_RowDataBound"
                        OnPageIndexChanging="DgvUsuarios_PageIndexChanging"
                        OnRowCancelingEdit="DgvUsuarios_RowCancelingEdit"
                        OnRowDeleting="DgvUsuarios_RowDeleting"
                        OnRowEditing="DgvUsuarios_RowEditing"
                        OnRowUpdating="DgvUsuarios_RowUpdating"
                        OnRowCommand="DgvUsuarios_RowCommand"
                        PageSize="30">
                        <Columns>
                            <asp:BoundField DataField="Identidad" HeaderText="CODIGO" Visible="false" />

                            <asp:TemplateField HeaderText="RUBRO">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblrubro"
                                        runat="server"
                                        Text='<%# Bind("Rubro.Descripcion") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="EMPRESA">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblempresa"
                                        runat="server"
                                        Text='<%# Bind("Organizacion.Razon_Social") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apertura">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblApertura"
                                        runat="server"
                                        Text='<%# Bind("FechaApertura.Date","{0:dd-MM-yyyy}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Cierre">
                                <ItemTemplate>
                                    <asp:Label
                                        runat="server" ID="GVLicit_lblCierre"
                                        Text='<%# Bind("FechaCierre.Date", "{0:dd-MM-yyyy}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="Descripcion" HeaderText="DESCRIPCION" />

                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblEstado"
                                        runat="server"
                                        Text='<%# Bind("Estado_Licitacion") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="EDITAR" />
                            <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="ELIMINAR" />

                            <asp:ButtonField ButtonType="Button" CommandName="CancelarLicitacion" ControlStyle-CssClass="btn btn-xs btn-warning" Text="Cancelar" HeaderText="CANCELAR" />

                            <asp:ButtonField ButtonType="Button" CommandName="VerOfertasdeMiLicitacion" ControlStyle-CssClass="btn btn-xs btn-primary" Text="Ver Ofertas" HeaderText="OFERTAS" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>


        </div>
    </div>

    <div class="panel panel-default" id="DivMisOfertas" runat="server">
        <div class="panel-heading">
            <h3 class="panel-title"><%= Traducir("MIS_OFERTAS") %></h3>
        </div>
        <div class="panel-body">

            <div class="content-header">
                <div class="header-section">
                    <div class="row">
                        <div class="col-md-2">
                            <h4>
                                <strong>
                                    <asp:Literal ID="litUsuariosOfertas" runat="server"></asp:Literal>
                                </strong>
                            </h4>
                        </div>
                        <div class="col-md-10">
                        </div>
                    </div>


                    <br />
                </div>
            </div>

            <div class="block full" style="display: none">
                <div class="row">
                    <div id="filter-panelOfertas" class="filter-panel">
                        <div class="panel panel-default bg-panel">
                            <div class="panel-body">

                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon"><%= Traducir("Titulo") %></div>
                                        <asp:TextBox ID="filtroUsernameOfertas" runat="server" MaxLength="20" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:Button ID="btnBuscarOfertas" runat="server" Text='Buscar' CssClass="btn btn-info" OnClick="btnBuscar_Click" />
                                <a href="/AdminLicitacion.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;<%= Traducir("LIMPIAR") %></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block full">
                <div class="table-responsive">
                    <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                        <asp:Literal ID="txtCantRegistrosOfertas" runat="server"></asp:Literal></strong>  <%= Traducir("REGISTROS") %>
                    </span>
                    <span class="help-block pull-right"><%= Traducir("MOSTRANDO_15") %></span>
                    <div style="border-top: 1px solid black;"></div>
                    <asp:GridView ID="DgvUsuariosOfertas" runat="server" AllowSorting="True"
                        AutoGenerateColumns="False" BorderStyle="None"
                        CssClass="table table-striped table-hover" AllowPaging="True"
                        GridLines="None" PagerStyle-CssClass="pagination-ys "
                        PagerStyle-HorizontalAlign="Right"
                        DataKeyNames="Identidad"
                        OnRowDataBound="DgvUsuariosOfertas_RowDataBound"
                        OnPageIndexChanging="DgvUsuariosOfertas_PageIndexChanging"
                        OnRowCancelingEdit="DgvUsuariosOfertas_RowCancelingEdit"
                        OnRowDeleting="DgvUsuariosOfertas_RowDeleting"
                        OnRowEditing="DgvUsuariosOfertas_RowEditing"
                        OnRowUpdating="DgvUsuariosOfertas_RowUpdating"
                        OnRowCommand="DgvUsuariosOfertas_RowCommand"
                        PageSize="30">
                        <Columns>
                            <asp:BoundField DataField="Identidad" HeaderText="CODIGO" Visible="false" />

                            <asp:TemplateField HeaderText="RUBRO">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblrubro"
                                        runat="server"
                                        Text='<%# Bind("Rubro.Descripcion") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="EMPRESA">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblempresa"
                                        runat="server"
                                        Text='<%# Bind("Organizacion.Razon_Social") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apertura">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblApertura"
                                        runat="server"
                                        Text='<%# Bind("FechaApertura.Date","{0:dd-MM-yyyy}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Cierre">
                                <ItemTemplate>
                                    <asp:Label
                                        runat="server" ID="GVLicit_lblCierre"
                                        Text='<%# Bind("FechaCierre.Date", "{0:dd-MM-yyyy}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="Descripcion" HeaderText="DESCRIPCION" />

                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="GVLicit_lblEstado"
                                        runat="server"
                                        Text='<%# Bind("Estado_Licitacion") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-search'></i>" ControlStyle-CssClass="btn btn-xs btn-success" HeaderText="DETALLE" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>


        </div>
    </div>

    <div id="myModalOfertarLicitacion" class="modal fade" role="dialog" tabindex="-1" data-backdrop="static">

        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4>
                        <asp:Label class="modal-title" ID="titmodalpropOferta" runat="server"></asp:Label>
                    </h4>

                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel_PliegoItemsOferta" runat="server">
                        <ContentTemplate>

                            <div class="panel panel-default" id="PanelDatosLicitacionOferta" runat="server">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><%= Traducir("LICITACION") %></h3>
                                </div>

                                <div class="panel-body">

                                    <div class='row'>
                                        <div class='col-md-1'>
                                            <label><%= Traducir("Rubro") %>:</label>

                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtRubroOferta" CssClass="form-control" runat="server" ValidationGroup="vgtxtRubro" ReadOnly="true">
                                            </asp:TextBox>
                                        </div>

                                        <div class='col-md-1'>
                                            <label><%= Traducir("Empresa") %>:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtRznSocialOferta" CssClass="form-control" runat="server" ValidationGroup="vgRznSocial" ReadOnly="true">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtRznSocialOferta" runat="server" ControlToValidate="txtRznSocialOferta" ValidationGroup="vgRznSocial"></asp:RequiredFieldValidator>
                                        </div>


                                    </div>
                                    <br />
                                    <br />
                                    <div class='form-group clearfix'>
                                        <div class='row'>
                                            <div class='col-md-1'>
                                                <label><%= Traducir("Titulo") %>:</label>
                                            </div>
                                            <div class='col-md-9'>
                                                <textarea id="txtAreaDescripcionOferta" style="width: 100%; max-width: 100%" readonly="readonly" runat="server" class="form-control boxsizingBorder" rows="1" maxlength="300" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class='form-group clearfix'>
                                        <div class="row">
                                            <div class='col-md-1'>
                                                <label><%= Traducir("Apertura") %>:</label>
                                            </div>
                                            <div class='col-md-3'>
                                                <div id="datepickerAperturaOFerta" class="input-group date" data-date-format="dd-mm-yyyy">
                                                    <input class="form-control" type="text" id="fechaDesdeOferta" value="<%= this.milabelfechadesdeOferta.Text %>" name="datepickdesdeOferta" readonly="readonly" />
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Label ID="milabelfechadesdeOferta" runat="server" Text="" CssClass="lblinvisible"></asp:Label>
                                            </div>
                                            <div class='col-md-8'>&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class='form-group clearfix'>
                                        <div class="row">
                                            <div class='col-md-1'>
                                                <label><%= Traducir("Cierre") %>:</label>
                                            </div>
                                            <div class='col-md-3'>
                                                <div id="datepickerCierreOferta" class="input-group date" data-date-format="dd-mm-yyyy">
                                                    <input class="form-control" type="text" id="fechaHastaOferta" value="<%= this.milabelfechahastaOferta.Text %>" name="datepickhastaOferta" readonly="readonly" />
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>

                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Label ID="milabelfechahastaOferta" runat="server" Text="asdasd" CssClass="lblinvisible"></asp:Label>
                                            </div>
                                            <div class='col-md-8'>&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class='form-group clearfix'>
                                        <div class="row">
                                            <div class='col-md-1'>
                                                <label><%= Traducir("Estado") %>:</label>
                                            </div>
                                            <div class='col-md-3'>
                                                <asp:TextBox ID="txtEstadoLicOferta" CssClass="form-control" runat="server" ValidationGroup="vgtxtEstadoLic" ReadOnly="true">
                                                </asp:TextBox>
                                            </div>
                                            <div class='col-md-9'>&nbsp;</div>
                                        </div>
                                    </div>

                                    <div class='form-group clearfix'>
                                        <div class="row">
                                            <div class='col-md-1'>
                                                <label><%= Traducir("Notas") %>:</label>
                                            </div>
                                            <div class='col-md-9'>
                                                <textarea id="txtAreaNotasOferta" style="width: 100%; max-width: 100%" runat="server" class="form-control boxsizingBorder" rows="3" readonly="readonly" maxlength="300"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default" id="PanelPliegoOferta" runat="server">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title pull-left" style="padding-top: 7.5px;"><%= Traducir("PLIEGO") %></h4>

                                </div>

                                <div class="panel-body">



                                    <br />

                                    <asp:GridView ID="DgvPliegoItemsOferta" runat="server" AllowSorting="True"
                                        AutoGenerateColumns="False" BorderStyle="None"
                                        OnRowCommand="DgvPliegoItemsOferta_RowCommand"
                                        OnRowEditing="DgvPliegoItemsOferta_RowEditing"
                                        OnRowDataBound="DgvPliegoItemsOferta_RowDataBound"
                                        OnRowCancelingEdit="DgvPliegoItemsOferta_RowCancelingEdit"
                                        OnRowUpdating="DgvPliegoItemsOferta_RowUpdating"
                                        OnRowUpdated="DgvPliegoItemsOferta_RowUpdated"
                                        CssClass="table table-striped table-hover" AllowPaging="True"
                                        GridLines="None" PagerStyle-CssClass="pagination-dgv "
                                        DataKeyNames="Identidad"
                                        PagerStyle-HorizontalAlign="Right"
                                        PageSize="15">

                                        <Columns>

                                            <asp:TemplateField ControlStyle-CssClass="" HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Identidad" HeaderText="Identidad" ReadOnly="true" />
                                            <asp:BoundField DataField="ItemDescripcion" HeaderText="ITEMS" ReadOnly="true" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="CANTIDAD" ReadOnly="true" />
                                            <asp:BoundField DataField="PrecioSugerido" HeaderText="PRECIO_SUGERIDO" DataFormatString="{0:c}" ReadOnly="true" />
                                            <asp:BoundField DataField="PrecioUnitario" HeaderText="PRECIO_UNITARIO" DataFormatString="{0:c}" />

                                            <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-warning" HeaderText="Guardar" />


                                        </Columns>
                                    </asp:GridView>

                                    <br />
                                    <div runat="server" id="divnodatasugerido" visible="false" class="col-md-4 text-muted pull-right">
                                        <asp:Label ID="lblNoDataSugerido" CssClass="pull-right" runat="server"><%= Traducir("NoDataSugerido") %></asp:Label>
                                    </div>
                                    <br />
                                    <br />




                                    <br />

                                    <div runat="server" id="diventregaOferta" class='form-group clearfix'>

                                        <div class="row">
                                            <div class='col-md-2'>
                                                <label><%= Traducir("Entrega_Recepcion") %>:</label>
                                            </div>
                                            <div class='col-md-9'>
                                                <style>
                                                    .boxsizingBorder {
                                                        -webkit-box-sizing: border-box;
                                                        -moz-box-sizing: border-box;
                                                        box-sizing: border-box;
                                                    }
                                                </style>

                                                <textarea id="txtAreaEntregaOferta" style="width: 100%; max-width: 100%" readonly="readonly" runat="server" class="form-control boxsizingBorder" rows="3" maxlength="300"></textarea>
                                            </div>
                                            <div class='col-md-1'>&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default" id="PanelDatosOferta" runat="server">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><%= Traducir("DATOS_OFERTA") %></h3>
                                </div>

                                <div class="panel-body">

                                    <div class='row'>
                                        <div class='col-md-1'>
                                            <label><%= Traducir("Estado") %>:</label>

                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtestadoOferta" CssClass="form-control pull-right" runat="server" ReadOnly="true">
                                            </asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class='col-md-1'>
                                            <label><%= Traducir("Subtotal") %>:</label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtSubtotalOferta" CssClass="form-control pull-right" runat="server" DataFormatString="{0:C2}" ReadOnly="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>

                            <br />

                            <div id="divAlertaOfertarLicitacion" class="alert alert-warning" visible="true" role="alert" runat="server">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only"><%= Traducir("ATENCION") %>:</span>
                                <asp:Label ID="lblMensajeAlerta" runat="server"></asp:Label>
                            </div>

                            <div id="divErrorOfertarLicitacion" class="alert alert-danger" visible="true" role="alert" runat="server">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only"><%= Traducir("ATENCION") %>:</span>
                                <asp:Label ID="lblErrorOfertarLicitacion" runat="server"></asp:Label>
                            </div>


                            <div class="form-group clearfix">
                                <asp:Button ID="btnPublicarOfertaLicitacion" runat="server" Style="width: 100%; max-width: 100%" CssClass="btn btn-success btn-lg btn-block boxsizingBorder" OnClick="btnPublicarOfertaLicitacion_Click" Text='Presentar Oferta' />
                            </div>
                            <div class="form-group clearfix">
                                <asp:Button ID="btnCancelarOfertaLicitacion" runat="server" Style="width: 100%; max-width: 100%" CssClass="btn btn-danger btn-lg btn-block boxsizingBorder" OnClick="btnCancelarOfertaLicitacion_Click" Text='Cancelar Oferta' />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCloseOferta" CssClass="btn btn-default" runat="server" Text="Cancelar" data-dismiss="modal" aria-hidden="true" UseSubmitBehavior="false" />
                </div>
            </div>
        </div>
    </div>



    <div id="myModalAceptarOferta" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="bodymodalaceptaroferta">
                    <asp:HiddenField ID="lblidoferta" runat="server" />
                    <h4>
                        <asp:Label ID="lblmessageconfirmaceptaroferta" runat="server" />
                        <br />
                        <br />
                    </h4>


                    <asp:Button runat="server" ID="btnAcept12Oferta_" CssClass="btn btn-success btn-lg btn-block" Text='<%# Traducir("ACEPTAR_OFERTA") %>' OnClick="btnaceptaroferta_js_Click" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModalRechazarOferta" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="bodymodalrechazaroferta">

                    <h4>
                        <asp:Label ID="lblmessageconfirmarrechazaroferta" runat="server" />
                        <br />
                        <br />
                    </h4>


                    <br />
                    <asp:Button runat="server" ID="btnRechaz13Oferta_" CssClass="btn btn-danger btn-lg btn-block" Text='<%# Traducir("RECHAZAR_OFERTA") %>' OnClick="btnrechazaroferta_js_Click" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>









    <div id="myModalVerOfertasMiLicitacion" class="modal fade" role="dialog" tabindex="-1" data-backdrop="static">

        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4>
                        <asp:Label class="modal-title" ID="Label1" runat="server"></asp:Label>
                    </h4>

                </div>
                <div class="modal-body">


                    <div class="panel panel-default" id="Div1" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title"><%= Traducir("LICITACION") %></h3>
                        </div>

                        <div class="panel-body">

                            <div id="divAlertaAceptarOferta" class="alert alert-warning" visible="true" role="alert" runat="server">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only"><%= Traducir("ATENCION") %>:</span>
                                <asp:Label ID="lblAlertaAceptarOferta" runat="server"></asp:Label>
                            </div>


                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" runat="server">
                            </div>




                            <div id="div6" class="alert alert-danger" visible="true" role="alert" runat="server">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only"><%= Traducir("ATENCION") %>:</span>
                                <asp:Label ID="Label5" runat="server"></asp:Label>
                            </div>

                        </div>

                        <div class="modal-footer">
                            <asp:Button ID="Button3" CssClass="btn btn-default" runat="server" Text="Cancelar" data-dismiss="modal" aria-hidden="true" UseSubmitBehavior="false" />



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        function fncaceptar(pepe) {
            var asd = $(pepe);
            var htmltag = $(pepe).parent().html();
            var idoferta = $(htmltag).attr("id");

            var hdnoferta = document.getElementById('<%= hdnOferta.ClientID %>');
            hdnoferta.value = idoferta;

            document.getElementById('<%= btnaceptaroferta_js.ClientID %>').click();
        }

        function fncrechazar(pepe) {
            var asd1 = $(pepe);
            var htmltag1 = $(pepe).parent().html();
            var idoferta = $(htmltag1).attr("id");

            var hdnoferta = document.getElementById('<%= hdnOferta.ClientID %>');
            hdnoferta.value = idoferta;

            document.getElementById('<%= btnrechazaroferta_js.ClientID %>').click();
        }

        function fncaceptarmodal(pepe) {
            var asd = $(pepe);
            var htmltag = $(pepe).parent().html();
            var idoferta = $(htmltag).attr("id");

            var hdnoferta = document.getElementById('<%= hdnOferta.ClientID %>');
            hdnoferta.value = idoferta;

            document.getElementById('<%= btnaceptarofertamodal_js.ClientID %>').click();

        }

        function fncrechazarmodal(pepe) {
            var asd1 = $(pepe);
            var htmltag1 = $(pepe).parent().html();
            var idoferta = $(htmltag1).attr("id");

            var hdnoferta = document.getElementById('<%= hdnOferta.ClientID %>');
            hdnoferta.value = idoferta;

            document.getElementById('<%= btnrechazarofertamodal_js.ClientID %>').click();
        }


    </script>


</asp:Content>
