﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminRubrosForm : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("NEG003");
            btnGuardar.Text = Traducir("GUARDAR");

            RequiredFieldValidator_iptDescripcion.ErrorMessage = Traducir("CAMPO_REQUERIDO");

            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarData();
            }
        }

        protected void cargarData()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int idOrg = Convert.ToInt32(Request.QueryString["id"]);

                BLL.RubroBLL Orgbll = new BLL.RubroBLL();

                BE.RubroBE orgtmp = new BE.RubroBE() { Identidad = idOrg };

                BE.RubroBE organizacion = Orgbll.Select(orgtmp);


                if (organizacion != null)
                {
                    iptDescripcion.Text = organizacion.Descripcion;

                }
            }

            else
            {

            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            BE.RubroBE org = null;
            BLL.RubroBLL orgbll = new BLL.RubroBLL();

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                org = orgbll.Select((new BE.RubroBE() { Identidad = Convert.ToInt32(Request.QueryString["id"]) }));
            }

            if (org == null)
            {
                org = new BE.RubroBE();
                org.Identidad = 0;
            }

            org.Descripcion = iptDescripcion.Text;


            if (org.Identidad == 0)
            {
                orgbll.Insert(org);
                NuevaBitacora((String.Format("Se creo el Rubro {0}", org.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            }
            else
            {
                orgbll.Update(org);
                NuevaBitacora((String.Format("Se modifico el Rubro {0}", org.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
            }

            Response.Redirect("AdminRubros.aspx");
        }

    }
}