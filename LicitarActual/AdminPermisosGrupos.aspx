﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminPermisosGrupos.aspx.cs" Inherits="LicitarActual.AdminPermisosGrupos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="litGrupos" runat="server" Text="Grupos"></asp:Literal></strong>
                    <a href="AdminPermisosGruposForm.aspx" class="btn btn-primary pull-right">
                        <asp:Literal ID="litNuevo" runat="server" ></asp:Literal></a><br />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %><strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_10") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvGrupos" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None" 
                    CssClass="table table-striped table-hover" AllowPaging="True" 
                    GridLines="None" PagerStyle-CssClass="pagination-dgv " 
                    PagerStyle-HorizontalAlign="Right" 
                    DataKeyNames="Codigo_Perfil"
                    OnPageIndexChanging="DgvGrupos_PageIndexChanging" 
                    OnRowCancelingEdit="DgvGrupos_RowCancelingEdit" 
                    OnRowDeleting="DgvGrupos_RowDeleting" 
                    OnRowDataBound="DgvGrupos_RowDataBound"
                    OnRowEditing="DgvGrupos_RowEditing" OnRowUpdating="DgvGrupos_RowUpdating"
                     PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="Codigo_Perfil" HeaderText="CODIGO" visible="true"/>
                        <asp:BoundField DataField="descripcion" HeaderText="NOMBRE" />
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="EDITAR" />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger"  HeaderText="ELIMINAR" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">&nbsp;</div>
            <div class="col-md-3 pull-right">
                <asp:Button ID="btnExportarExcel" runat="server" OnClick="btnExportarExcel_Click" Text='<%# Traducir("EXPORTAR_EXCEL") %>' CssClass="btn btn-sm btn-primary right" />
                &nbsp; &nbsp;
                <asp:Button ID="btnExportarPDF" runat="server" OnClick="btnExportarPDF_Click" Text='<%# Traducir("EXPORTAR_PDF") %>' CssClass="btn btn-sm btn-primary right" />
            </div>
        </div>
        
    </div>
</asp:Content>
