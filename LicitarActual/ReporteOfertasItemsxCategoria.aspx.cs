﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;


namespace LicitarActual
{
    public partial class ReporteOfertasItemsxCategoria : Cascara
    {
        private string _connString = ConfigurationManager.ConnectionStrings["PRD"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

            validarAcceso("NEG007");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            //cargarDropdownRubros();
            //CargarData();

            if (!IsPostBack)
            {
                cargarDropdownRubros();
                CargarData();
                //Page.DataBind();
            }

        }

        private void cargarDropdownRubros()
        {

            using (BLL.RubroBLL sec = new BLL.RubroBLL())
            {
                ddRubros.DataSource = null;
                ddRubros.DataMember = "Rubro";
                ddRubros.DataValueField = "Identidad";
                ddRubros.DataTextField = "Descripcion";

                ddRubros.DataSource = sec.SelectAll();



                ddRubros.DataBind();

                ddRubros.Items.Insert(0, "Todos");
                ddRubros.SelectedIndex = 0;

            }

        }

        private void CargarData()
        {

            Dictionary<BE.Clase_ItemBE, string> DiccionarioCosas;

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                DiccionarioCosas = bll.SP_reporteCatxCant();
            }

            IList<CategoriaOfertasMapping> ventasProductosList = new List<CategoriaOfertasMapping>();


            foreach (KeyValuePair<BE.Clase_ItemBE, string> keyValuePair in DiccionarioCosas)
            {

                CategoriaOfertasMapping asd = new CategoriaOfertasMapping();



                asd.Categoria = keyValuePair.Key.Descripcion;

                asd.cantidadOfertas = Convert.ToInt32(keyValuePair.Value);

                ventasProductosList.Add(asd);

            }


            hdnJson123.Value = Newtonsoft.Json.JsonConvert.SerializeObject(ventasProductosList);

        }

        protected void ddRubros_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddRubros.SelectedIndex == 0)
            {
                CargarData();
                return;
            }

            CargarData(new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) });

        }

        private void CargarData(BE.RubroBE rubroBE)
        {

            Dictionary<BE.Clase_ItemBE, string> DiccionarioCosas;

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                DiccionarioCosas = bll.SP_reporteCatxCantFiltrarRubro(rubroBE);
            }

            IList<CategoriaOfertasMapping> ventasProductosList = new List<CategoriaOfertasMapping>();

            if (DiccionarioCosas != null)
            {


                foreach (KeyValuePair<BE.Clase_ItemBE, string> keyValuePair in DiccionarioCosas)
                {

                    CategoriaOfertasMapping asd = new CategoriaOfertasMapping();



                    asd.Categoria = keyValuePair.Key.Descripcion;

                    asd.cantidadOfertas = Convert.ToInt32(keyValuePair.Value);

                    ventasProductosList.Add(asd);

                }

            }

            hdnJson123.Value = Newtonsoft.Json.JsonConvert.SerializeObject(ventasProductosList);

        }



    }



    public class CategoriaOfertasMapping
    {
        public string Categoria { get; set; }
        public int cantidadOfertas { get; set; }
    }
}
