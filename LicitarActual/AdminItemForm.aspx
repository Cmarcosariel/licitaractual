﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminItemForm.aspx.cs" Inherits="LicitarActual.AdminItemForm" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <style>
        #popup {
            position: fixed;
            background: rgba(0,0,0,.8);
            display: none;
            top: 20px;
            left: 50px;
            width: 300px;
            height: 200px;
            border: 1px solid #000;
            border-radius: 5px;
            padding: 5px;
            color: #fff;
        }
    </style>

    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>



        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("RUBRO") %>:</label>
            </div>
            <div class="col-md-5">
                <asp:DropDownList ID="ddRubros" CssClass="form-control" runat="server" ValidationGroup="vgRubros" OnSelectedIndexChanged="ddRubros_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddRubros" runat="server" ControlToValidate="ddRubros" ValidationGroup="vgRubros"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label><%= Traducir("Notas") %>:</label>
            </div>
            <div class="col-md-5">
                <textarea id="txtAreaNotas" runat="server" class="form-control" rows="5" maxlength="300"></textarea>
            </div>

            <%--            <div class='col-md-1'>
                <label><%= /*Traducir*/("Imagen") %>:</label>
            </div>
            <div class="form-group col-md-5">
                <input type="file" id="FileImgItem" accept=".jpg,.png,.jpeg" onchange="ValidateSize(this)">
                <p class="help-block">Tamaño maximo de archivo 1MB</p>
            </div>--%>
            <br />
        </div>
        <br />
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("CLASEITEM") %>:</label>
            </div>
            <div class="col-md-5">
                <asp:DropDownList ID="ddCategorias" CssClass="form-control" runat="server" ValidationGroup="vgCategorias" OnSelectedIndexChanged="ddCategorias_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddCategorias" runat="server" ControlToValidate="ddCategorias" ValidationGroup="vgCategorias"></asp:RequiredFieldValidator>
            </div>

            <br />
            <br />
        </div>
        <br />
        <div class="col-md-12 form group">
            <div id="divPropiedades" class="row">
                <asp:UpdatePanel runat="server" ID="myUpdPanel">
                    <ContentTemplate>
                        <asp:PlaceHolder ID="PH_NombreProp" runat="server">

                        </asp:PlaceHolder>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
        <br />
        <br />
    </div>

    <div class="form-group clearfix">
        <a href="AdminItem.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" OnClick="btnGuardar_Click" Text='Guardar_Y_Volver' />
        <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" OnClick="Button1_Click" Text='Guardar_Y_otro' />
    </div>




    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <%--    <div id="myModalEditorProp" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <asp:Label class="modal-title" ID="titmodalprop" runat="server"></asp:Label>


                </div>
                <div class="modal-body">


                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="DgvPropiedadValores" runat="server" AllowSorting="True"
                                AutoGenerateColumns="False" BorderStyle="None"
                                CssClass="table table-striped table-hover" AllowPaging="True"
                                GridLines="None" PagerStyle-CssClass="pagination-dgv "
                                PagerStyle-HorizontalAlign="Right"
                                OnRowDataBound="DgvPropiedadValores_RowDataBound"
                                OnPageIndexChanging="DgvPropiedadValores_PageIndexChanging"
                                OnRowCancelingEdit="DgvPropiedadValores_RowCancelingEdit"
                                OnRowDeleting="DgvPropiedadValores_RowDeleting"
                                OnRowEditing="DgvPropiedadValores_RowEditing"
                                OnRowUpdating="DgvPropiedadValores_RowUpdating"
                                PageSize="15">
                                <Columns>
                                    <asp:BoundField DataField="test" HeaderText="Text" />
                                    <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="EDITAR" />
                                    <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="ELIMINAR" />
                                </Columns>
                            </asp:GridView>

                            <asp:Button ID="btnAgregarRowValor" runat="server" CssClass="btn btn-success" Text="+" OnClick="btnAgregarRowValor_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>



                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnEliminarProp" Text="Eliminar Propiedad" CssClass="btn btn-danger pull-left" runat="server" OnClick="btnEliminarProp_Click"></asp:Button>
                    <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" data-dismiss="modal" aria-hidden="true" UseSubmitBehavior="false"/>
                </div>
            </div>

        </div>
    </div>--%>





    <script>

        function ShowCurrentTime() {
            waitingDialog.show();
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("AdminItemForm.aspx/HelloWorld") %>',
                data: '',
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    waitingDialog.hide();
                }
            });
        }
    </script>

    <script type="text/javascript">
        $("#open_popup").click(function () {
            $("#popup").css("display", "block");
            $('body').css('overflow', 'hidden');
        });



        $("#close_popup").click(function () {
            $("#popup").css("display", "none");
            $('body').css('overflow', 'scroll');
        });

    </script>

    <script type="text/javascript">
        $('.openPopup').on('click', function () {
            var dataURL = $(this).attr('data-href');
            $('.modal-body').load(dataURL, function () {
                $('#myModal').modal({ show: true });
            });
        });

    </script>

    <script>
        function AgregarProp() {
            document.getElementById('MainContent_btnagregarpropiedad123').onclick();
        }

        function MostrarAltaProp() {
            document.getElementById('MainContent_btnAgregarPropiedad').onclick();
        }

        function Editarddlprop(midivdinamico) {
            alert("asd")
            console.log(midivdinamico);
            document.getElementById('MainContent_btnAgregarValorAProp').onclick();
        }

        function ActualizarPHProps() {
            document.getElementById('MainContent_btnAgregarValorAProp').onclick();
        }

        function ValidateSize(file) {
            var FileSize = file.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 1) {
                alert('File size exceeds 1 MB');
                // $(file).val(''); //for clearing with Jquery
            } else {

            }
        }


    </script>


</asp:Content>


