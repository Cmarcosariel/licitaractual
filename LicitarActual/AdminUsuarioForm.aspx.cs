﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;

namespace LicitarActual
{
    public partial class AdminUsuarioForm : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM005");
            gvPermisos.DataBound += GvPermisos_DataBound;
            btnGuardar.Text = Traducir("GUARDAR");
            RequiredFieldValidator_iptPassword.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptNombre.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptApellido.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_iptUsername.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_ddEmpresas.ErrorMessage = Traducir("CAMPO_REQUERIDO");

            rfv_ddGrupos.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            rfv_ddPermisos.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            gvGrupos.EmptyDataText = Traducir("NO_GRUPOS");
            gvGrupos.EmptyDataText = Traducir("NO_PERMISOS");

            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarData();
            }
        }

        private void GvPermisos_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvPermisos.Rows)
            {
                if (row.Cells[2].Text.CompareTo("Si") == 0)
                {
                    row.Cells[3].Controls.Clear();
                }
            }

        }

        protected void cargarData()
        {

            cargarDropdownEmpresas();

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                iptPassword.Visible = false;
                txtPwdGuardada.Visible = true;
                RequiredFieldValidator_iptPassword.Visible = false;
                int idUsuario = Convert.ToInt32(Request.QueryString["id"]);
                BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();

                BE.UsuarioBE usuariotmp = new BE.UsuarioBE() { Identidad = idUsuario };

                BE.UsuarioBE usuario = usuarioBll.Select(usuariotmp);

                llenarGrupos(usuario);
                cargarDropdownGrupos(usuario);
                cargarDropdownPermisos(usuario);
                llenarPermisos(usuario);
                llenarDropdownEmpresas(usuario);

                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {
                    usuario.permisos = sec.SelectAllByID_Usuario(usuario);
                }
                if (usuario != null)
                {
                    iptApellido.Text = usuario.ApellidoPersona;
                    iptCodigo.Value = usuario.Identidad.ToString();
                    iptNombre.Text = usuario.NombrePersona;
                    iptUsername.Text = usuario.NombreUsuario;
                    iptCII.Text = usuario.LoginFails.ToString();
                    ddBloqueado.SelectedValue = usuario.IsBlocked ? "1" : "0";
                }
            }
            else
            {
                asignarGruposPermisos.Visible = false;
                iptPassword.Visible = true;
                txtPwdGuardada.Visible = false;
                RequiredFieldValidator_iptPassword.Visible = true;
            }
        }

        private void llenarDropdownEmpresas(UsuarioBE usuario)
        {
            ddEmpresas.SelectedValue = usuario.Organizacion.Identidad.ToString();
        }

        private void cargarDropdownEmpresas()
        {
            using (BLL.OrganizacionBLL BLL = new BLL.OrganizacionBLL())
            {
                ddEmpresas.DataMember = "Empresa";
                ddEmpresas.DataValueField = "Identidad";
                ddEmpresas.DataTextField = "Razon_Social";
                ddEmpresas.DataSource = BLL.SelectAll();
                
                ddEmpresas.DataBind();

                ddEmpresas.Items.Insert(0, new ListItem("Seleccione", ""));
            }

        }

        protected void llenarGrupos(BE.UsuarioBE usuario)
        {

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                List<BE.PermisoBE> permisos = sec.GetRolesByUserID(usuario);
                gvGrupos.AutoGenerateColumns = false;
                gvGrupos.DataSource = permisos;
                gvGrupos.DataBind();

            }
        }

        protected void llenarPermisos(BE.UsuarioBE usuario)
        {
            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                List<BE.PermisoBE> permisos = new List<BE.PermisoBE>();


                foreach (BE.PermisoBE item in sec.GetPermisoBaseByUserID(usuario))
                {
                    permisos.Add(item);
                    permisos.AddRange(sec.GenerarListaPlanaRol(sec.SelectIndexadoByCodigo_Perfil(item)));
                }

                gvPermisos.AutoGenerateColumns = false;
                gvPermisos.DataSource = permisos;
                gvPermisos.DataBind();
            }


        }


        protected void cargarDropdownGrupos(BE.UsuarioBE usuario)
        {
            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                ddGrupos.DataMember = "Grupo";
                ddGrupos.DataValueField = "Identidad";
                ddGrupos.DataTextField = "Descripcion";
                ddGrupos.DataSource = sec.obtenerSoloGruposSinUsuario(usuario);
                ddGrupos.Items.Insert(0, new ListItem("Seleccione", ""));
                ddGrupos.DataBind();
            }


        }

        protected void cargarDropdownPermisos(BE.UsuarioBE usuario)
        {
            SEC.PermisoSEC permisoBll = new SEC.PermisoSEC();
            ddPermisos.DataMember = "Permiso";
            ddPermisos.DataValueField = "Identidad";
            ddPermisos.DataTextField = "Descripcion";
            ddPermisos.DataSource = permisoBll.obtenerSoloPermisosSinUsuario(usuario);
            ddPermisos.Items.Insert(0, new ListItem("Seleccione", ""));
            ddPermisos.DataBind();
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            BE.UsuarioBE usuario = null;
            BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                usuario = usuarioBll.Select((new BE.UsuarioBE() { Identidad = Convert.ToInt32(Request.QueryString["id"] )}));
            }

            if (usuario == null)
            {
                usuario = new BE.UsuarioBE();
                usuario.Identidad = 0;

                usuario.Idioma = new BE.IdiomaBE { Identidad = 1 };
            }

            if (ddEmpresas.SelectedIndex == 0)
            {
                return;
            }

            usuario.Organizacion = new BE.OrganizacionBE { Identidad = Convert.ToInt64(ddEmpresas.SelectedValue) };

            usuario.ApellidoPersona = iptApellido.Text;
            usuario.NombrePersona = iptNombre.Text;
            usuario.NombreUsuario = iptUsername.Text;
            usuario.IsBlocked = ddBloqueado.SelectedValue == "1" ? true : false;

            if (usuario.Identidad == 0)
            {
                using (BLL.UsuarioBLL bll = new BLL.UsuarioBLL())
                {
                    if (bll.SelectByNombre(usuario) != null)
                    {
                        string mensaje = Traducir("USU_USUNOM_EXIST");
                        lblMessage.Text = mensaje;
                        miPopUp();
                        return;
                    }
                }


                usuario.Clave = iptPassword.Text;
                usuarioBll.Insert(usuario);
                NuevaBitacora((String.Format("Se creo el usuario {0}", usuario.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                PermisoBE permisoBE = new PermisoBE() {Identidad = 10051, Codigo_Perfil = "NEG008" };
 
                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {
                    sec.AgregarPermisoAUsuario(permisoBE, usuario);
                    NuevaBitacora((String.Format("Se asigno el permiso {0} al usuario {1}", permisoBE.Codigo_Perfil, usuario.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
                }



            }
            else
            {
                usuarioBll.Update(usuario);
                NuevaBitacora((String.Format("Se modifico el usuario {0}", usuario.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

                if (usuario.Identidad == ((UsuarioBE)Session["UsuarioLogueado"]).Identidad)
                {
                    Session["UsuarioLogueado"] = usuarioBll.SelectUsuarioyPermisos(usuario);
                }

            }

            Response.Redirect("AdminUsuarios.aspx");
        }


        protected void btnAgregarGrupo_Click(object sender, EventArgs e)
        {
            BE.UsuarioBE usuarioBe = new BE.UsuarioBE();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                usuarioBe.Identidad = Convert.ToInt32(Request.QueryString["id"]);
            }
            BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();
            BE.PermisoBE grupoBe = new BE.PermisoBE();
            grupoBe.Identidad = Int32.Parse(ddGrupos.SelectedValue);


            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                sec.AddRecursiveGrant(grupoBe, usuarioBe);
                NuevaBitacora((String.Format("Se asigno el grupo {0} al usuario {1}", grupoBe.Codigo_Perfil, usuarioBe.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
            }

            llenarGrupos(usuarioBe);
            cargarDropdownGrupos(usuarioBe);
            cargarDropdownPermisos(usuarioBe);
            llenarPermisos(usuarioBe);

            rfv_ddPermisos.Enabled = true;
        }

        protected void gvGrupos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            BE.UsuarioBE usuarioBe = new BE.UsuarioBE();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                usuarioBe.Identidad = Convert.ToInt32(Request.QueryString["id"]);
            }
            BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL();
            BE.PermisoBE grupoBe = new BE.PermisoBE();
            GridViewRow row = (GridViewRow)gvGrupos.Rows[e.RowIndex];
            grupoBe.Codigo_Perfil = (row.Cells[0].Text).ToString();

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                sec.DeleteRecursiveGrant(grupoBe, usuarioBe);
                NuevaBitacora((String.Format("Se quito el grupo {0} al usuario {1}", grupoBe.Codigo_Perfil, usuarioBe.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
            }

            llenarPermisos(usuarioBe);
            cargarDropdownPermisos(usuarioBe);
            llenarGrupos(usuarioBe);
            cargarDropdownGrupos(usuarioBe);
        }

        protected void btnAgregarPermiso_Click(object sender, EventArgs e)
        {
           

            BE.UsuarioBE usuarioBe = new BE.UsuarioBE();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                usuarioBe.Identidad = Convert.ToInt32(Request.QueryString["id"]);
            }

            BE.PermisoBE permisoBe = new BE.PermisoBE();
            permisoBe.Identidad = Int32.Parse(ddPermisos.SelectedValue);


            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                sec.AgregarPermisoAUsuario(permisoBe, usuarioBe);
                NuevaBitacora((String.Format("Se asigno el permiso {0} al usuario {1}", permisoBe.Codigo_Perfil, usuarioBe.NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
            }

            if (GetUsuarioLogueado().Identidad == usuarioBe.Identidad)
            {
                using (BLL.UsuarioBLL BLL = new BLL.UsuarioBLL())
                {
                    BE.UsuarioBE tmp = GetUsuarioLogueado();
                    tmp.permisos.Clear();
                    tmp = BLL.CargarPerfil(usuarioBe);
                    Session.Remove("UsuarioLogueado");
                    Session.Add("UsuarioLogueado", tmp);
                }
            }


            llenarPermisos(usuarioBe);
            cargarDropdownPermisos(usuarioBe);
            llenarGrupos(usuarioBe);
            cargarDropdownGrupos(usuarioBe);

            rfv_ddGrupos.Enabled = true;

        }

        protected void gvPermisos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            BE.UsuarioBE usuarioBe = new BE.UsuarioBE();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                usuarioBe.Identidad = Convert.ToInt32(Request.QueryString["id"]);
            }
           
            BE.PermisoBE permisoBe = new BE.PermisoBE();
            GridViewRow row = (GridViewRow)gvPermisos.Rows[e.RowIndex];
            permisoBe.Codigo_Perfil = (row.Cells[0].Text).ToString();

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                List<BE.PermisoBE> permisos = sec.SelectAllByID_Usuario(usuarioBe);
                bool flagsepuedeborrar = false;
                foreach (BE.PermisoBE item in permisos)
                {
                    if (item.Codigo_Perfil == permisoBe.Codigo_Perfil)
                    {
                        flagsepuedeborrar = true;
                    }
                }

                if (flagsepuedeborrar)
                {
                    using (SEC.PermisoSEC sec2 = new SEC.PermisoSEC())
                    {
                        sec2.QuitarPermisoAUsuario(permisoBe, usuarioBe);
                        NuevaBitacora((String.Format("Se quito el permiso {0} al usuario ID {1}", permisoBe.Codigo_Perfil, usuarioBe.Identidad)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
                    }

                    llenarPermisos(usuarioBe);
                    cargarDropdownPermisos(usuarioBe);

                    llenarGrupos(usuarioBe);
                    cargarDropdownGrupos(usuarioBe);
                }
                else
                {
                    string mensaje = String.Format(Traducir("PERMISO_DELETE_HEREDADO"), permisoBe.Descripcion);
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }

            }

        }

        protected void gvPermisos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TableCell statusCell = e.Row.Cells[2];
                if (statusCell.Text == "False")
                {
                    statusCell.Text = "No";
                }
                if (statusCell.Text == "True")
                {
                    statusCell.Text = "Si";
                }
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        protected void gvGrupos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

    }
}