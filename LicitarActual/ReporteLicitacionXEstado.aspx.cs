﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class ReporteLicitacionXEstado : Cascara
    {
        private string _connString = ConfigurationManager.ConnectionStrings["PRD"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

            validarAcceso("NEG007");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            CargarData();

            if (!IsPostBack)
            {
                Page.DataBind();
            }
        }

        private void CargarData()
        {

            string query = @"EXEC sp_LicitacionesXestado";

            SqlConnection cs = new SqlConnection(_connString);
            SqlCommand cmd = new SqlCommand(query, cs);
            cs.Open();

            var dt = new DataTable();
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
            }


            IList<LicitacionEstadoMapping> ventasProductosList = new List<LicitacionEstadoMapping>();


            foreach (DataRow dr in dt.Rows)
            {

                LicitacionEstadoMapping asd = new LicitacionEstadoMapping();

                asd.Estado = ((BE.EstadoLicitacionBE)Enum.Parse(typeof(BE.EstadoLicitacionBE), dr["Estado_Licitacion"].ToString())).ToString();

                asd.cantidadLicitaciones = Convert.ToInt32(dr["CantLicitaciones"]);

                ventasProductosList.Add(asd);

            }


            hdnJson123456.Value = Newtonsoft.Json.JsonConvert.SerializeObject(ventasProductosList);

        }


    }

    public class LicitacionEstadoMapping
    {
        public string Estado { get; set; }
        public int cantidadLicitaciones { get; set; }
    }
}
