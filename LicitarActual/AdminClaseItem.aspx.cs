﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace LicitarActual
{
    public partial class AdminClaseItem : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("CLASE_ITEMS");
            validarAcceso("NEG004");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarClaseItem();
            Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG004");
            BLL.Clase_ItemBLL usuarioBll = new BLL.Clase_ItemBLL();
            BE.Clase_ItemBE usuarioBe = new BE.Clase_ItemBE();
            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());


            using (BLL.ItemBLL bll2 = new BLL.ItemBLL())
            {

                if (bll2.SelectAllByID_Clase(usuarioBe).Count > 0)
                {
                    string mensaje = Traducir("CAT_ERR_ITM_EXIST");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }

            }


            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino la clase {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarClaseItem();
        }
        protected void llenarClaseItem()
        {
            BLL.Clase_ItemBLL clase_ItemBLL = new BLL.Clase_ItemBLL();

            List<BE.Clase_ItemBE> clase_ItemBEs = clase_ItemBLL.SelectAll();

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = clase_ItemBEs;

            if (clase_ItemBEs != null)
            {
                txtCantRegistros.Text = clase_ItemBEs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG004");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminClaseItemForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarClaseItem();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarClaseItem();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.Clase_ItemBLL orgbll = new BLL.Clase_ItemBLL();
                List<BE.Clase_ItemBE> usuarios = orgbll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            using (BLL.Clase_ItemBLL bll = new BLL.Clase_ItemBLL())
            {
                Dictionary<string, string> midiccionario = bll.GetRubrosStringForClases();


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string mistring;

                    midiccionario.TryGetValue(e.Row.Cells[2].Text,out mistring);

                    e.Row.Cells[1].Text = mistring;
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                    }
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarClaseItem();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("CLASE_ITEM"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("CLASE_ITEM"));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}
