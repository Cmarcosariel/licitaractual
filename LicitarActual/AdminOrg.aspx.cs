﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminOrg : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("EMPRESAS");
            validarAcceso("NEG002");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarOrganizaciones();
            Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG002");
            BLL.OrganizacionBLL usuarioBll = new BLL.OrganizacionBLL();
            BE.OrganizacionBE usuarioBe = new BE.OrganizacionBE();
            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());

            if (usuarioBe.Identidad == 1)
            {
                string mensaje = Traducir("ORG_ADM_NOT_DEL");
                lblMessage.Text = mensaje;
                miPopUp();
                return;
            }

            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {
                licitacionBLL.SelectAllByID_Organizacion(usuarioBe);

                if (licitacionBLL.SelectAllByID_Organizacion(usuarioBe).Count() > 0)
                {
                    string mensaje = Traducir("ORG_ADM_LIC_AB_NOT_DEL");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }
            }



            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino la organizacion {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarOrganizaciones();
        }
        protected void llenarOrganizaciones()
        {
            BLL.OrganizacionBLL organizacionbll = new BLL.OrganizacionBLL();
            List<BE.OrganizacionBE> orgs = organizacionbll.SelectAll();
            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = orgs;

            if (orgs != null)
            {
                txtCantRegistros.Text = orgs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG002");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminOrgForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarOrganizaciones();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarOrganizaciones();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.OrganizacionBLL orgbll = new BLL.OrganizacionBLL();
                List<BE.OrganizacionBE> usuarios = orgbll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TableCell statusCell = e.Row.Cells[4];
                if (statusCell.Text == "False")
                {
                    statusCell.Text = "No";
                }
                if (statusCell.Text == "True")
                {
                    statusCell.Text = "Si";
                }
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarOrganizaciones();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("USUARIOS"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("USUARIOS"));
        }
    }
}
