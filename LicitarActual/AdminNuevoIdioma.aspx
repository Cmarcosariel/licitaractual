﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminNuevoIdioma.aspx.cs" Inherits="LicitarActual.AdminNuevoIdioma" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br /><br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span><asp:Label ID="lblError" runat="server" Text=""></asp:Label></div>
    </asp:Panel>
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Nombre Idioma:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptIdioma" runat="server" ClientIDMode="Static" required="required" MaxLength="10" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptIdioma" runat="server" ErrorMessage="Campo requerido" ControlToValidate="iptIdioma"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>idioma Complemento:</label>
            </div>
            <div class='col-md-5'>
                    <asp:DropDownList ID="ddIdiomas123" CssClass="form-control" runat="server" ValidationGroup="vgAgregarIdioma" />
                    <asp:RequiredFieldValidator ID="rfv_ddIdiomas123" runat="server" ErrorMessage="Campo requerido" ControlToValidate="ddIdiomas123"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="form-group clearfix">
        <a href="AdminIdioma.aspx" class="btn btn-info pull-left">Volver al listado</a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" OnClick="btnGuardar_Click" CssClass="btn btn-success" Text="Guardar"  />
    </div>

</asp:Content>
