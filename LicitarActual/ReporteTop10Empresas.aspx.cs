﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;

namespace LicitarActual
{
    public partial class ReporteTop10Empresas : Cascara
    {
        private string _connString = ConfigurationManager.ConnectionStrings["PRD"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

            validarAcceso("NEG007");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            CargarData();

            if (!IsPostBack)
            {
                cargarDropdownRubros();
                CargarData();
            }
        }

        private void cargarDropdownRubros()
        {

            using (BLL.RubroBLL sec = new BLL.RubroBLL())
            {
                ddRubros.DataSource = null;
                ddRubros.DataMember = "Rubro";
                ddRubros.DataValueField = "Identidad";
                ddRubros.DataTextField = "Descripcion";

                ddRubros.DataSource = sec.SelectAll();



                ddRubros.DataBind();

                ddRubros.Items.Insert(0, "Todos");
                ddRubros.SelectedIndex = 0;

            }

        }
        private void CargarData()
        {

            string query = @"EXEC sp_LicXOrg";

            SqlConnection cs = new SqlConnection(_connString);
            SqlCommand cmd = new SqlCommand(query, cs);
            cs.Open();

            var dt = new DataTable();
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
            }


            IList<LicitacionEmpresaMapping> ventasProductosList = new List<LicitacionEmpresaMapping>();


            foreach (DataRow dr in dt.Rows)
            {

                LicitacionEmpresaMapping asd = new LicitacionEmpresaMapping();



                asd.Empresa = dr["Descripcion"].ToString();

                asd.cantidadLicitaciones = Convert.ToInt32(dr["CantLicitaciones"]);

                ventasProductosList.Add(asd);

            }


            hdnJson1234.Value = Newtonsoft.Json.JsonConvert.SerializeObject(ventasProductosList);

        }

        protected void ddRubros_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddRubros.SelectedIndex == 0)
            {
                CargarData();
                return;
            }

            CargarData(new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) });

        }

        private void CargarData(RubroBE rubroBE)
        {
            string query = String.Format(@"EXEC sp_LicXOrgFiltroRubro {0}",rubroBE.Identidad);

            SqlConnection cs = new SqlConnection(_connString);
            SqlCommand cmd = new SqlCommand(query, cs);
            cs.Open();

            var dt = new DataTable();
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
            }


            IList<LicitacionEmpresaMapping> ventasProductosList = new List<LicitacionEmpresaMapping>();


            foreach (DataRow dr in dt.Rows)
            {

                LicitacionEmpresaMapping asd = new LicitacionEmpresaMapping();



                asd.Empresa = dr["Descripcion"].ToString();

                asd.cantidadLicitaciones = Convert.ToInt32(dr["CantLicitaciones"]);

                ventasProductosList.Add(asd);

            }


            hdnJson1234.Value = Newtonsoft.Json.JsonConvert.SerializeObject(ventasProductosList);

        }
    }

    public class LicitacionEmpresaMapping
    {
        public string Empresa { get; set; }
        public int cantidadLicitaciones { get; set; }
    }
}
