﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminOrgForm.aspx.cs" Inherits="LicitarActual.AdminOrgForm" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("Nombre") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptNombreOrg" runat="server" ClientIDMode="Static" required="required" MaxLength="30" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptNombreOrg" runat="server" ControlToValidate="iptNombreOrg"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label><%= Traducir("CUIT") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptCUIT" runat="server" ClientIDMode="Static" required="required" MaxLength="11" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptCUIT" runat="server" ControlToValidate="iptCUIT"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("Razon_Social") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptRzn_Social" runat="server" MaxLength="30" ClientIDMode="Static" required="required" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptRzn_Social" runat="server" ControlToValidate="iptRzn_Social"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label><%= Traducir("Domicilio") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptDomicilio" runat="server" MaxLength="30" ClientIDMode="Static" required="required" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptDomicilio" runat="server" ControlToValidate="iptDomicilio"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("Email") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptEMAIL" runat="server" MaxLength="50" CssClass="form-control"  Enabled="true" TextMode="Email"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptEMAIL" runat="server" ControlToValidate="iptEMAIL"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label><%= Traducir("TELEFONO") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptTELEFONO" runat="server" MaxLength="50" CssClass="form-control" Enabled="true" TextMode="Phone"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptTELEFONO" runat="server" ControlToValidate="iptTELEFONO"></asp:RequiredFieldValidator>
            
            </div>
        </div>
    </div>

    <div class="form-group clearfix">
        <a href="AdminOrg.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" OnClick="btnGuardar_Click" Text='Guardar' />

    </div>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        function ShowCurrentTime() {
            waitingDialog.show();
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("AdminOrgForm.aspx/HelloWorld") %>',
                data: '',
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    waitingDialog.hide();
                }
            });
        }
    </script>

</asp:Content>
