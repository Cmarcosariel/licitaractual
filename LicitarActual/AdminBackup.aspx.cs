﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Services;

namespace LicitarActual
{
    public partial class AdminBackup : Cascara
    {

        private String RUTA = ConfigurationManager.AppSettings["backupRuta"];

        protected void Page_Load(object sender, EventArgs e)
        {
            
            validarAcceso("ADM003");
           
            nuevoBkp.Text = Traducir("NUEVO");
            if (!Page.IsPostBack)
            {
                Page.DataBind();
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                llenarBackups();
            }
        }

        protected void llenarBackups()
        {

            using (SEC.GestorBackup sec = new SEC.GestorBackup())
            {
                List<BE.BackupBE> backups = sec.SelectAll();
                   

                dgvBackup.AutoGenerateColumns = false;
                dgvBackup.DataSource = backups;
                if (backups != null)
                {
                    txtCantRegistros.Text = backups.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }




                dgvBackup.DataBind();

                foreach (GridViewRow item in dgvBackup.Rows)
                {
                    item.Cells[3].Text = (new DateTime(Convert.ToInt64(item.Cells[3].Text))).ToString();
                }
            }


        }


        protected void dgvBackup_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvBackup.PageIndex = e.NewPageIndex;
            llenarBackups();
        }
        protected void dgvBackup_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            dgvBackup.EditIndex = -1;
            llenarBackups();
        }

        protected void dgvBackup_PageIndexChanged(object sender, GridViewCancelEditEventArgs e)
        {
            llenarBackups();
        }

        protected void dgvBackup_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("ADM003");

            String ruta = dgvBackup.DataKeys[e.NewEditIndex].Values[0].ToString();

            using (SEC.GestorBackup sec = new SEC.GestorBackup())
            {
                BE.BackupBE bkp = new BE.BackupBE() { Filepath = ruta};

                sec.RestaurarBackup(bkp);

                NuevaBitacora((string.Format("Se restauro la BD con el archivo {0}",bkp.Filepath)), GetUsuarioLogueado(), "BASE DE DATOS", 2);

                Session.Clear();
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }


        }

        protected void nuevoBkp_Click(object sender, EventArgs e)
        {
            validarAcceso("ADM003");
            DateTime hoy = DateTime.Now;
            string rutaRel = RUTA + "/" + hoy.ToString("yyyy-MM-dd-HH-mm-ss") + ".bak";
            BE.BackupBE bkp = new BE.BackupBE() { Filepath = "/", Timestamp = hoy.Ticks };

            try
            {
                using (SEC.GestorDV sec123 = new SEC.GestorDV())
                {
                    if (sec123.CalcularIntegridadDB())
                    {

                        using (SEC.GestorBackup sec = new SEC.GestorBackup())
                        {
                            sec.TomarBackup(bkp);
                        }

                        NuevaBitacora("Nuevo Snapshot de base de datos", GetUsuarioLogueado(), "BASE DE DATOS", 1);

                    }
                    else
                    {
                        string mensaje = Traducir("MSJ_ERR_INT_DB");
                        lblMessage.Text = mensaje;
                        miPopUp();
                        return;

                    }

                }


                llenarBackups();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static String GetDataTime()
        {
            return DateTime.Now.ToString();
        }

        protected void dgvBackup_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }


        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            this.exportarExcel(sender, e, dgvBackup, 0, 4, "BACKUPS");
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, dgvBackup, 0, 4, "BACKUPS");

        }


    }
}