﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public class Cascara : System.Web.UI.Page
    {
        protected BE.UsuarioBE usuarioLogueado;

        public static void RecalcularTodosDvh()
        {
            new BLL.UsuarioBLL().RecalcularDvhTotal();
            new SEC.PermisoSEC().RecalcularDvhTotal();
            //new SEC.BitacoraSEC().RecalcularDvhTotal();

        }

        public static void RecalcularTodosDvv()
        {
            new BLL.UsuarioBLL().GenerarDvv();
            new SEC.PermisoSEC().GenerarDvv();
            //new SEC.BitacoraSEC().GenerarDvv();
        }

        public bool EstaLogueado()
        {
            usuarioLogueado = (BE.UsuarioBE)Session["UsuarioLogueado"];
            if (usuarioLogueado == null)
                return false;
            return true;
        }

        public bool validarAcceso(string permiso)
        {

            if (EstaLogueado() == false)
            {
                Response.Redirect("Login.aspx");
                return false;
            }

            if (permiso == "LIN001")
            {
                if (!this.tienePermiso(permiso))
                {
                    return false;
                }
            }

            if (permiso != "")
            {
                if (!this.tienePermiso(permiso))
                {
                    Response.Redirect("PermisoDenegado.aspx");
                    return false;
                }
            }

            return true;
        }

        public bool tienePermiso(string CodPermisoValidar)
        {
            if (HttpContext.Current.Session["UsuarioLogueado"] == null)
                return false;
            BE.UsuarioBE UsuarioLogueado = (BE.UsuarioBE)HttpContext.Current.Session["UsuarioLogueado"];
            if (UsuarioLogueado.permisos == null)
            {
                return false;
            }

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                foreach (BE.PermisoBE permiso in sec.GenerarListaPlana(UsuarioLogueado))
                {
                    if (CodPermisoValidar == permiso.Codigo_Perfil)
                        return true;
                }
                
            }

            return false;
        }
        public BE.UsuarioBE GetUsuarioLogueado()
        {
            if (EstaLogueado())
                return usuarioLogueado;
            return null;
        }

        public long GetIdUsuario()
        {
            BE.UsuarioBE dataUsuario = this.usuarioLogueado;
            return dataUsuario.Identidad;
        }

        public string Traducir(string texto)
        {
            string traduccion = texto;
            IList<BE.TraduccionBE> traducciones = null;

            if (Session["Lang"] == null)
            {
                traducciones = (List<BE.TraduccionBE>)Application["Español"];
            }
            else
            {
                traducciones = (List<BE.TraduccionBE>)Application[Session["Lang"].ToString()];
            }

            try
            {
                BE.TraduccionBE mensaje = traducciones.FirstOrDefault(x => x.Mensaje.Descripcion.Equals(texto));
                if (mensaje != null) traduccion = mensaje.Traduccion;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return traduccion;
        }
        protected void exportarExcel(object sender, EventArgs e, GridView gridView, int editButton, int deleteButton, String nombre)

        {
            Response.Clear();

            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment; filename=" + nombre + ".xls");

            Response.Charset = "";

            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();

            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gridView.AllowPaging = false;

            //Change the Header Row back to white color
            gridView.HeaderRow.Style.Add("background-color", "#FFFFFF");

            //Apply style to Individual Cells

            gridView.HeaderRow.Cells[0].Style.Add("background-color", "blue");

            gridView.HeaderRow.Cells[1].Style.Add("background-color", "blue");

            gridView.HeaderRow.Cells[2].Style.Add("background-color", "blue");

            gridView.HeaderRow.Cells[3].Style.Add("background-color", "blue");


            for (int i = 0; i < gridView.Rows.Count; i++)

            {

                GridViewRow row = gridView.Rows[i];



                //Change Color back to white

                row.BackColor = System.Drawing.Color.White;



                //Apply text style to each Row

                row.Attributes.Add("class", "textmode");



                //Apply style to Individual Cells of Alternating Row

                if (i % 2 != 0)

                {

                    row.Cells[0].Style.Add("background-color", "#9BB6D6");

                    row.Cells[1].Style.Add("background-color", "#9BB6D6");

                    row.Cells[2].Style.Add("background-color", "#9BB6D6");

                    row.Cells[3].Style.Add("background-color", "#9BB6D6");

                }

            }
            if (editButton > 0)
            {
                gridView.Columns[editButton].Visible = false;
            }
            if (deleteButton > 0)
            {
                gridView.Columns[deleteButton].Visible = false;
            }
            gridView.RenderControl(hw);



            //style to format numbers to string

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";

            Response.Write(style);

            Response.Output.Write(sw.ToString());

            Response.Flush();

            Response.End();
        }

        protected void exportarPDF(object sender, EventArgs e, GridView gridView, int editButton, int deleteButton, String nombre)
        {

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + nombre + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            if (editButton > 0)
            {
                gridView.Columns[editButton].Visible = false;
            }
            if (deleteButton > 0)
            {
                gridView.Columns[deleteButton].Visible = false;
            }
            gridView.AllowPaging = false;
            gridView.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


        }

        public void miPopUp()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"$('#myModal').modal('show');");
            sb.Append(@"</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());
        }

        public void NuevaBitacora(string registro, BE.UsuarioBE _usuario, string Categoria, int Severidad)
        {
            BE.Evento_BitacoraBE eventonuevo = new BE.Evento_BitacoraBE();
            eventonuevo.Descripcion = Categoria;
            eventonuevo.Severidad = Severidad;

            using (SEC.BitacoraSEC sec = new SEC.BitacoraSEC())
            {
                BE.BitacoraBE nuevoregistro = new BE.BitacoraBE();
                nuevoregistro.Descripcion = registro;

                nuevoregistro.Evento_Bitacora = sec.GetSeveridadEvento(eventonuevo);

                nuevoregistro.Usuario = _usuario;

                nuevoregistro.Timestamp = DateTime.Now.Ticks;

                sec.GrabarBitacora(nuevoregistro);
            }

        }

    }
}