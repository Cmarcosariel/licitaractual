﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;


namespace LicitarActual
{
    public partial class AdminPermisosGruposForm : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM002");
            
            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarData();
                cargarDropdownPermisos();
                RegExValidator_TextBox1.ValidationExpression = @"[a-zA-Z]{3}\d{3}";
                RegExValidator_TextBox1.ErrorMessage = Traducir("FORMATO_PERMISO");
                RequiredFieldValidator_iptNombre.ErrorMessage = Traducir("CAMPO_REQUERIDO");
                RequiredFieldValidator_ddPermisos.ErrorMessage = Traducir("CAMPO_REQUERIDO");
                btnGuardar.Text = Traducir("GUARDAR");
            }
        }

        protected void cargarData()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                TextBox1.Enabled = false;
                TextBox1.Text = Request.QueryString["id"];
                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {

                    BE.PermisoBE Rol = sec.SelectIndexadoByCodigo_Perfil(new BE.PermisoBE { Codigo_Perfil = Request.QueryString["id"].ToString() });

                    if (Rol != null)
                    {
                        iptCodigo.Value = Rol.Codigo_Perfil.ToString();
                        iptNombre.Text = Rol.Descripcion;
                        this.cargarDgvPermisos(Rol.Components);
                        phAsignarGruposPermisos.Visible = true;
                    }

                }
                cargarDropdownPermisos();
            }
            else
            {
                phAsignarGruposPermisos.Visible = false;
            }

        }
        private void cargarDgvPermisos(List<BE.PermisoBE> permisos)
        {
            if (permisos != null)
            {
                gdvPermisos.DataSource = null;
                gdvPermisos.DataSource = permisos;
                gdvPermisos.DataBind();
            }
        }

        private void cargarDropdownPermisos()
        {

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                ddPermisos.DataSource = null;
                ddPermisos.DataMember = "Permiso";
                ddPermisos.DataValueField = "Codigo_Perfil";
                ddPermisos.DataTextField = "Descripcion";

                if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    string Codigo_Perfil = (Request.QueryString["id"]).ToString();
                    BE.PermisoBE permiso = new BE.PermisoBE() { Codigo_Perfil = Codigo_Perfil };

                    List<BE.PermisoBE> listapermisosposibles = sec.SelectAll();

                    foreach (BE.PermisoBE item in sec.SelectIndexadoByCodigo_Perfil(permiso).Components)
                    {
                        listapermisosposibles.Remove(item);
                    }

                    ddPermisos.DataSource = listapermisosposibles;
                }
                else
                {
                    ddPermisos.DataSource = sec.SelectAll();
                }

                ddPermisos.DataBind();



            }

        }

        protected void btnAgregarPermiso_Click(object sender, EventArgs e)
        {
            BE.PermisoBE hijo = new BE.PermisoBE() { Codigo_Perfil = (ddPermisos.SelectedValue).ToString() };
            BE.PermisoBE padre = new BE.PermisoBE() { Codigo_Perfil = (Request.QueryString["id"]).ToString() };



            if (hijo.Codigo_Perfil != "" && padre.Codigo_Perfil != "")
            {

                if (hijo.Codigo_Perfil == padre.Codigo_Perfil)
                {
                    string mensaje = Traducir("PERMISO_RECURSIVO");
                    lblMessage.Text = mensaje;
                    miPopUp();
                    return;
                }


                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {
                    List<BE.PermisoBE> Listahijos = sec.GenerarListaPlanaRol(sec.SelectIndexadoByCodigo_Perfil(hijo));


                    if (Listahijos.Any(x => x.Codigo_Perfil == padre.Codigo_Perfil))
                    {
                        string mensaje = Traducir("PERMISO_MULTINIVEL");
                        lblMessage.Text = mensaje;
                        miPopUp();
                        return;
                    }

                    sec.AssingPermisoARol(padre, hijo);

                    NuevaBitacora((String.Format("Se asigno el permiso {0} al Rol {1} ", hijo.Codigo_Perfil, padre.Codigo_Perfil)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 1);

                    using (SEC.PermisoSEC sec2 = new SEC.PermisoSEC())
                    {
                        if (sec2.GenerarListaPlana(GetUsuarioLogueado()).Exists(x => x.Codigo_Perfil == padre.Codigo_Perfil))
                        {
                            using (BLL.UsuarioBLL BLL = new BLL.UsuarioBLL())
                            {
                                BE.UsuarioBE tmp = GetUsuarioLogueado();
                                tmp.permisos.Clear();
                                tmp = BLL.CargarPerfil(tmp);
                                Session.Remove("UsuarioLogueado");
                                Session.Add("UsuarioLogueado", tmp);
                            }
                        }
                    }


                }
                cargarData();
            }

        }


        protected void gdvPermisos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = gdvPermisos.Rows[e.RowIndex];

            BE.PermisoBE hijo = new BE.PermisoBE() { Codigo_Perfil = (row.Cells[0].Text) };
            BE.PermisoBE padre = new BE.PermisoBE() { Codigo_Perfil = (Request.QueryString["id"]).ToString() };

            if (hijo.Codigo_Perfil != "" && padre.Codigo_Perfil != "")
            {
                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {
                    sec.RevokePermisoARol(padre, hijo);
                    NuevaBitacora((String.Format("Se quito el permiso {0} al Rol {1} ", hijo.Codigo_Perfil, padre.Codigo_Perfil)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
                }

                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {
                    if (sec.GenerarListaPlana(GetUsuarioLogueado()).Exists(x => x.Codigo_Perfil == padre.Codigo_Perfil))
                    {
                        using (BLL.UsuarioBLL BLL = new BLL.UsuarioBLL())
                        {
                            BE.UsuarioBE tmp = GetUsuarioLogueado();
                            tmp.permisos.Clear();
                            tmp = BLL.CargarPerfil(tmp);
                            Session.Remove("UsuarioLogueado");
                            Session.Add("UsuarioLogueado", tmp);
                        }
                    }
                }



                cargarData();
            }





        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string Codigo_Perfil = "0";

            if ((Request.QueryString.Keys.Count == 0))
            {

            }
            else
            {
                Codigo_Perfil = (Request.QueryString["id"]).ToString();
            }


            if (Codigo_Perfil == "0")
            {


                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {

                    if (sec.SelectPlanoByCodigo_Perfil(new BE.PermisoBE() { Codigo_Perfil = TextBox1.Text.ToUpper() }) != null)
                    {
                        string mensaje = Traducir("PERMISO_CODIGO_EXIST");
                        lblMessage.Text = mensaje;
                        miPopUp();
                        return;
                    }

                    sec.InsertRolNuevo(new BE.PermisoBE() { PermisoBase = false, Codigo_Perfil = TextBox1.Text.ToUpper(), Descripcion = iptNombre.Text });

                    NuevaBitacora((String.Format("Se creo el Rol {0}", TextBox1.Text.ToUpper())), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 1);
                }

                Response.Redirect("/AdminPermisosGruposForm.aspx?id=" + TextBox1.Text.ToUpper());
            }
            else
            {
                using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
                {
                    sec.UpdateRolExist(new BE.PermisoBE() { Codigo_Perfil = TextBox1.Text.ToUpper(), Descripcion = iptNombre.Text });
                    NuevaBitacora((String.Format("Se modifico el Rol {0}", TextBox1.Text.ToUpper())), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 2);
                }

                Response.Redirect("/AdminPermisosGruposForm.aspx?id=" + Codigo_Perfil);
            }

        }

        protected void gdvPermisos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }


    }
}