﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminLicitacionForm.aspx.cs" Inherits="LicitarActual.AdminLicitacionForm" %>




<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>



    <style>
        .txtareafull {
            width: 100%;
        }
    </style>

    <style>
        .boxsizingBorder {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
    </style>

    <style>
        .lblinvisible {
            opacity: 0;
        }
    </style>

    <br />
    <br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div id="divMensajesErrorAltaLicitacion" class="alert alert-danger" visible="False" role="alert" runat="server">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only"><%= Traducir("ATENCION") %>:</span>
        <asp:Label ID="labelMensajesErrorAltaLicitacion" runat="server"></asp:Label>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><%= Traducir("LICITACION") %></h3>
        </div>

        <div class="panel-body">

            <div class='row'>
                <div class='col-md-1'>
                    <label><%= Traducir("RUBRO") %>:</label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddRubros" CssClass="form-control" runat="server" ValidationGroup="vgRubros">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddRubros" runat="server" ControlToValidate="ddRubros" ValidationGroup="vgRubros"></asp:RequiredFieldValidator>
                </div>

                <div class='col-md-1'>
                    <label><%= Traducir("EMPRESA") %>:</label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtRznSocial" CssClass="form-control" runat="server" ValidationGroup="vgRznSocial" ReadOnly="true">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtRznSocial" runat="server" ControlToValidate="txtRznSocial" ValidationGroup="vgRznSocial"></asp:RequiredFieldValidator>
                </div>


            </div>
            <br />
            <br />
            <div class='form-group clearfix'>
                <div class='row'>
                    <div class='col-md-1'>
                        <label><%= Traducir("Titulo") %>:</label>
                    </div>
                    <div class='col-md-9'>
                        <textarea id="txtAreaDescripcion" style="width: 100%; max-width: 100%" runat="server" class="form-control boxsizingBorder" rows="1" maxlength="300" required></textarea>
                    </div>
                </div>
            </div>
            <br />
            <div class='form-group clearfix'>
                <div class="row">
                    <div class='col-md-1'>
                        <label><%= Traducir("Apertura") %>:</label>
                    </div>
                    <div class='col-md-3'>
                        <div id="datepickerApertura" class="input-group date" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="fechaDesde" value="<%= this.milabelfechadesde.Text %>" name="datepickdesde" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="milabelfechadesde" runat="server" Text="" CssClass="lblinvisible"></asp:Label>
                    </div>
                    <div class='col-md-2'>&nbsp;</div>
                    <div class='col-md-6'>&nbsp;</div>
                </div>
            </div>
            <div class='form-group clearfix'>
                <div class="row">
                    <div class='col-md-1'>
                        <label><%= Traducir("Cierre") %>:</label>
                    </div>
                    <div class='col-md-3'>
                        <div id="datepickerCierre" class="input-group date" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="fechaHasta" value="<%= this.milabelfechahasta.Text %>" name="datepickhasta" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>

                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="milabelfechahasta" runat="server" Text="asdasd" CssClass="lblinvisible"></asp:Label>
                    </div>
                    <div class='col-md-8'>&nbsp;</div>
                </div>
            </div>
            <div class='form-group clearfix'>
                <div class="row">
                    <div class='col-md-1'>
                        <label><%= Traducir("Estado") %>:</label>
                    </div>
                    <div class='col-md-3'>
                        <asp:TextBox ID="txtEstadoLic" CssClass="form-control" runat="server" ValidationGroup="vgtxtEstadoLic" ReadOnly="true">
                        </asp:TextBox>
                    </div>
                    <div class='col-md-9'>&nbsp;</div>
                </div>
            </div>

            <div class='form-group clearfix'>
                <div class="row">
                    <div class='col-md-1'>
                        <label><%= Traducir("Notas") %>:</label>
                    </div>
                    <div class='col-md-9'>
                        <textarea id="txtAreaNotas" style="width: 100%; max-width: 100%" runat="server" class="form-control boxsizingBorder" rows="3" maxlength="300"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="PanelPliego">
        <div class="panel-heading">
            <h3 class="panel-title"><%= Traducir("PLIEGO") %></h3>
        </div>
        <div class="panel-body">

            <asp:UpdatePanel ID="UpdatePanel_PliegoItems" runat="server">
                <ContentTemplate>

                    <a data-toggle="modal" href="#myModalAgregarItem" class="btn btn-success" onclick="hidepanelalerta()"><%= Traducir("AGREGAR_ITEMS") %>
                    </a>



                    <br />
                    <br />

                    <asp:GridView ID="DgvPliegoItems" runat="server" AllowSorting="True"
                        AutoGenerateColumns="False" BorderStyle="None"
                        CssClass="table table-striped table-hover" AllowPaging="True"
                        GridLines="None" PagerStyle-CssClass="pagination-dgv "
                        PagerStyle-HorizontalAlign="Right"
                        OnRowDataBound="DgvPliegoItems_RowDataBound"
                        OnPageIndexChanging="DgvPliegoItems_PageIndexChanging"
                        OnRowCancelingEdit="DgvPliegoItems_RowCancelingEdit"
                        OnRowDeleting="DgvPliegoItems_RowDeleting"
                        OnRowEditing="DgvPliegoItems_RowEditing"
                        OnRowUpdating="DgvPliegoItems_RowUpdating"
                        PageSize="15">
                        <Columns>

                            <asp:TemplateField ControlStyle-CssClass="" HeaderText="No.">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ItemDescripcion" HeaderText="ITEMS" />
                            <asp:BoundField DataField="Cantidad" HeaderText="CANTIDAD" />

                            <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="ELIMINAR" />


                        </Columns>
                    </asp:GridView>




                </ContentTemplate>

            </asp:UpdatePanel>

            <br />
            <div runat="server" id="diventrega" class='form-group clearfix'>

                <div class="row">
                    <div class='col-md-2'>
                        <label><%= Traducir("Entrega_Recepcion") %>:</label>
                    </div>
                    <div class='col-md-9'>
                        <style>
                            .boxsizingBorder {
                                -webkit-box-sizing: border-box;
                                -moz-box-sizing: border-box;
                                box-sizing: border-box;
                            }
                        </style>

                        <textarea id="txtAreaEntrega" style="width: 100%; max-width: 100%" runat="server" class="form-control boxsizingBorder" rows="3" maxlength="300"></textarea>
                    </div>
                    <div class='col-md-1'>&nbsp;</div>
                </div>
            </div>



        </div>
    </div>

    <div id="divAlertaPublicarLicitacion" class="alert alert-warning" visible="true" role="alert" runat="server">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only"><%= Traducir("ATENCION") %>:</span>
        <asp:Label ID="lblMensajeAlerta" runat="server"><%= Traducir("MSJ_WARN_LIC") %></asp:Label>
    </div>

    <div class="form-group clearfix">
        <asp:Button ID="btnPublicarLicitacion" runat="server" Style="width: 100%; max-width: 100%" CssClass="btn btn-success btn-lg btn-block boxsizingBorder" Text='' OnClick="btnPublicarLicitacion_Click" />

    </div>


    <div class="form-group clearfix">
        <a href="TableroMiEmpresa.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text='Guardar' OnClick="btnGuardar_Click" />

    </div>

    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModalAgregarItem" class="modal fade" role="dialog" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <asp:Label class="modal-title" ID="titmodalprop" runat="server"></asp:Label>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanelModalItems" runat="server">
                        <ContentTemplate>
                            <div id="divalerta" class="alert alert-danger" visible="false" role="alert" runat="server">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only"><%= Traducir("ERROR") %>:</span><asp:Label ID="lblErrorPliego" runat="server" Text=""></asp:Label>
                            </div>
                            <div id="divsuccess" class="alert alert-success" visible="false" runat="server" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">><%= Traducir("MENSAJE") %>:</span><asp:Label ID="lblSuccessPliego" runat="server" Text=""></asp:Label>
                            </div>
                            <br />
                            <div class='row'>
                                <div class='col-md-2'>
                                    <label><%= Traducir("CATEGORIA") %>:</label>
                                </div>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="ddCategorias" CssClass="form-control" runat="server" ValidationGroup="vgCategorias" AutoPostBack="true" OnSelectedIndexChanged="ddCategorias_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <asp:GridView ID="dgvItemsdeClaseItem" runat="server" AllowSorting="True"
                                AutoGenerateColumns="False" BorderStyle="None"
                                CssClass="table table-striped table-hover" AllowPaging="True"
                                GridLines="None" PagerStyle-CssClass="pagination-dgv "
                                PagerStyle-HorizontalAlign="Right"
                                OnRowDataBound="dgvItemsdeClaseItem_RowDataBound"
                                OnPageIndexChanging="dgvItemsdeClaseItem_PageIndexChanging"
                                OnRowCancelingEdit="dgvItemsdeClaseItem_RowCancelingEdit"
                                OnSelectedIndexChanging="dgvItemsdeClaseItem_SelectedIndexChanging"
                                OnRowDeleting="dgvItemsdeClaseItem_RowDeleting"
                                OnRowEditing="dgvItemsdeClaseItem_RowEditing"
                                OnRowUpdating="dgvItemsdeClaseItem_RowUpdating"
                                OnRowCreated="dgvItemsdeClaseItem_RowCreated"
                                PageSize="15">
                                <Columns>
                                    <asp:TemplateField ControlStyle-CssClass="" HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descripcion" HeaderText="Item" />
                                    <asp:CommandField ShowSelectButton="True" SelectText="<i class='glyphicon glyphicon-plus-sign'></i>" ControlStyle-CssClass="btn btn-sm btn-success" HeaderText="AGREGAR" />
                                    <asp:TemplateField ControlStyle-CssClass="" HeaderText="CANTIDAD">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCantidad" ClientIDMode="Predictable" runat="server" ValidationGroup="vgtxtCantidad" CssClass="form-group"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator_txtCantidad" ClientIDMode="Predictable" runat="server" ControlToValidate="txtCantidad" ValidationGroup="vgtxtCantidad" ValidationExpression="((\d+)+(\.\d+))$"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtCantidad" ClientIDMode="Predictable" runat="server" ControlToValidate="txtCantidad" ValidationGroup="vgtxtCantidad"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" EditText="<i class='glyphicon glyphicon-save'></i>" CausesValidation="true" ValidationGroup="vgtxtCantidad" DeleteText="<i class='glyphicon glyphicon-save'></i>" ControlStyle-CssClass="btn btn-sm btn-success" HeaderText="Agregar" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" data-dismiss="modal" aria-hidden="true" UseSubmitBehavior="false" />
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#open_popup").click(function () {
            $("#popup").css("display", "block");
            $('body').css('overflow', 'hidden');
            hidepanelalerta();
        });



        $("#close_popup").click(function () {
            $("#popup").css("display", "none");
            $('body').css('overflow', 'scroll');
        });

    </script>

    <script type="text/javascript">
        $('.openPopup').on('click', function () {
            var dataURL = $(this).attr('data-href');
            $('.modal-body').load(dataURL, function () {
                $('#myModal').modal({ show: true });
            });
        });

    </script>

    <script>

        function ShowCurrentTime() {
            waitingDialog.show();
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("AdminOrgForm.aspx/HelloWorld") %>',
                data: '',
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    waitingDialog.hide();
                }
            });
        }

        function hidepanelalerta() {

            var x = document.getElementById('<%=divsuccess.ClientID %>');
            if (x != null) {
                x.style.display = 'none';
            }

            var y = document.getElementById('<%=divalerta.ClientID %>');
            if (y != null) {
                y.style.display = 'none';
            }



        }
    </script>



    <script type="text/javascript">
        $(function () {
            $('#fechaDesde').datetimepicker({

                minDate: moment().add(3, 'days'),

                format: 'DD/MM/YYYY'
            });

            $('#fechaDesde').data("DateTimePicker").date(document.getElementById('<%=milabelfechadesde.ClientID %>').textContent);

        });
    </script>

    <script type="text/javascript">
        $(function () {

            $('#fechaHasta').datetimepicker({
                minDate: moment().add(6, 'days'),
                format: 'DD/MM/YYYY'



            });
            $('#fechaHasta').data("DateTimePicker").date(document.getElementById('<%=milabelfechahasta.ClientID %>').textContent);


        });



    </script>


</asp:Content>

