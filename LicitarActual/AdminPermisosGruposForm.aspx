﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminPermisosGruposForm.aspx.cs" Inherits="LicitarActual.AdminPermisosGruposForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:HiddenField ID="iptCodigo" runat="server" />
    <br />
    <br />
    <br />
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-6'>
                <label><%=Traducir("CODIGO_PERFIL") %>:</label>
                <asp:TextBox ID="TextBox1" runat="server" MaxLength="6" CssClass="form-control"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegExValidator_TextBox1" runat="server" ControlToValidate="TextBox1"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
            </div>
        </div>    <br />
        <div class='row'>
            <div class='col-md-4'>
                <label><%=Traducir("NOMBRE") %>:</label>
                <asp:TextBox ID="iptNombre" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_iptNombre" runat="server" ControlToValidate="iptNombre"></asp:RequiredFieldValidator>
            </div>
            <asp:PlaceHolder ID="phAsignarGruposPermisos" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <label><%=Traducir("ASIGNAR_PERMISOS") %></label></div>
                        <div class="col-md-9">
                            <asp:DropDownList ID="ddPermisos" CssClass="form-control" runat="server" ValidationGroup="vgAgregarPermiso">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddPermisos" runat="server" ControlToValidate="ddPermisos" ValidationGroup="vgAgregarPermiso"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-3">
                            <asp:Button ID="btnAgregarPermiso" OnClick="btnAgregarPermiso_Click" runat="server" CssClass="btn btn-sm btn-warning" Text="+" ValidationGroup="vgAgregarPermiso" />
                        </div>
                        <div class="col-md-12">
                            <asp:GridView ID="gdvPermisos" OnRowDeleting="gdvPermisos_RowDeleting" OnRowDataBound="gdvPermisos_RowDataBound" CssClass="table table-hover table-bordered" BorderStyle="None" ItemType="BE_Permiso" runat="server" AutoGenerateColumns="False">

                                <Columns>
                                    <asp:BoundField DataField="Codigo_Perfil" HeaderText="CODIGO" ReadOnly="True" SortExpression="id"></asp:BoundField>
                                    <asp:BoundField DataField="Descripcion" HeaderText="NOMBRE" ReadOnly="True" SortExpression="Descripcion"></asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="ELIMINAR" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>

    <hr />

    <div class="form-group clearfix">
        <a href="AdminPermisosGrupos.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER_LISTADO") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" OnClick="btnGuardar_Click" runat="server" CssClass="btn btn-warning" Text="Guardar" />
    </div>
        <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
