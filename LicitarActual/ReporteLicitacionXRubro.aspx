﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="ReporteLicitacionXRubro.aspx.cs" Inherits="LicitarActual.ReporteLicitacionXRubro" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

    <script>
        window.onload = function () {

            var dataPoints = [];

            var options = {
                animationEnabled: true,
                theme: "light2",
                title: {
                    text: "Cantidad de Licitaciones por Rubro"
                },
                axisX: {
                    title: "Rubros",
                    //valueFormatString: "DD MMM YYYY",
                },
                axisY: {
                    title: "Licitaciones",
                    titleFontSize: 24
                },
                data: [{
                    type: "column",
                    //yValueFormatString: "$#,###.##",
                    dataPoints: dataPoints
                }]
            };

            function addData(data) {
                for (var i = 0; i < data.length; i++) {
                    dataPoints.push({



                        label: data[i].Rubros,
                        y: data[i].cantidadLicitaciones
                    });
                }
                $("[id$=chartContainer]").CanvasJSChart(options);

            }
            var values = $('[id$=hdnJson]').val();
            addData(jQuery.parseJSON(values));
            //https://canvasjs.com/jquery-charts/json-data-api-ajax-chart/

        }
    </script>

    <asp:HiddenField ID="hdnJson" runat="server" />

    <div class="row">
        <h1 class="centrar-texto-Ok"><%= Traducir("REPORTES_LICITACION") %></h1>
        <br />
        <br />
        <div class="row ">
            <div class="col-md-11">
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
        <br />
        <br />

    </div>

</asp:Content>

