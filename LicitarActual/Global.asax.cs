﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using BLL;
using BE;

namespace LicitarActual
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {



            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            try
            {
                IdiomaBLL _idiomaBll = new IdiomaBLL();
                List<IdiomaBE> idiomas = _idiomaBll.SelectAll();
                foreach (IdiomaBE idioma in idiomas)
                {
                    idioma.mensajes = _idiomaBll.ObtenerMensajesPorIdioma(idioma);

                    Application.Add(idioma.Descripcion, idioma.mensajes);
                }
            }
            catch (Exception ex)
            {
                //aca tengo q loguear la excepcion;
            }



        }
    }
}