﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminLicitacion : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)    
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("LICITACIONES");
            validarAcceso("NEG006");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarLicitaciones();
            Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG006");
            BLL.LicitacionBLL usuarioBll = new BLL.LicitacionBLL();
            BE.LicitacionBE usuarioBe = new BE.LicitacionBE();

            

            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());
            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino la Licitacion {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarLicitaciones();
        }
        protected void llenarLicitaciones()
        {
            BLL.LicitacionBLL LicitacionBLL = new BLL.LicitacionBLL();

            List<BE.LicitacionBE> orgs = LicitacionBLL.SelectAll();

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = orgs;

            if (orgs != null)
            {
                txtCantRegistros.Text = orgs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG006");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminLicitacionForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarLicitaciones();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarLicitaciones();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.LicitacionBLL licbll = new BLL.LicitacionBLL();
                List<BE.LicitacionBE> usuarios = licbll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                ((Button)(e.Row.Cells[9].Controls[0])).Text = Traducir("CANCELAR");

                // es un borrador?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Borrador.ToString())
                {
                    // editar = si  / borrar = si / cancelar = no
                    e.Row.Cells[7].Controls[0].Visible = true;  
                    e.Row.Cells[8].Controls[0].Visible = true;
                    e.Row.Cells[9].Controls[0].Visible = false;
                }


                // esta cerrada?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Cerrada.ToString())
                {
                    // editar = si (solo visible)  / borrar = no / cancelar = no
                   
                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = false;
                    e.Row.Cells[9].Controls[0].Visible = false;
                }


                // esta abierta?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Abierta.ToString())
                {
                    // editar = si (solo visible)  / borrar = no / cancelar = si
                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = false;
                    e.Row.Cells[9].Controls[0].Visible = true;
                }

                // esta cancelada?
                if (((Label)(e.Row.Cells[6].Controls[1])).Text == BE.EstadoLicitacionBE.Cancelada.ToString())
                {
                    // editar = si  / borrar = si / cancelar = no
                    e.Row.Cells[7].Controls[0].Visible = true;
                    e.Row.Cells[8].Controls[0].Visible = true;
                    e.Row.Cells[9].Controls[0].Visible = false;
                }
            }


            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarLicitaciones();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("LICITACIONES"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("LICITACIONES"));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }


        protected void DgvUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            validarAcceso("NEG006");

            if (e.CommandName == "CancelarLicitacion")
            {
                using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
                {
                    BE.LicitacionBE licitacionBE = new BE.LicitacionBE();

                    Int64 id = Int32.Parse(DgvUsuarios.DataKeys[Convert.ToInt16(e.CommandArgument)].Values[0].ToString());

                    licitacionBE.Identidad = id;

                    bll.CancelarLicitacion(licitacionBE);

                    NuevaBitacora((String.Format("Se canceló la Licitacion {0}", licitacionBE.Identidad)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);

                }
            }

            llenarLicitaciones();
        }
    }
}