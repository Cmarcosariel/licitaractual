﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true"  CodeBehind="AdminClaseItemForm.aspx.cs" Inherits="LicitarActual.AdminClaseItemForm" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <style>
        #popup {
            position: fixed;
            background: rgba(0,0,0,.8);
            display: none;
            top: 20px;
            left: 50px;
            width: 300px;
            height: 200px;
            border: 1px solid #000;
            border-radius: 5px;
            padding: 5px;
            color: #fff;
        }
    </style>

    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>



        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("RUBRO") %>:</label>
            </div>
            <div class="col-md-9">
                <asp:DropDownList ID="ddRubros" CssClass="form-control" runat="server" ValidationGroup="vgRubros">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddRubros" runat="server" ControlToValidate="ddRubros" ValidationGroup="vgRubros"></asp:RequiredFieldValidator>
            </div>
        </div>
        <br />
        <br />
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("DESCRIPCION") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptDescripcion" runat="server" ClientIDMode="Static" required="required" MaxLength="50" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptDescripcion" runat="server" ControlToValidate="iptDescripcion"></asp:RequiredFieldValidator>
            </div>
            <br />
        </div>
        <br />
        <asp:PlaceHolder ID="PH_Propiedades" Visible="false" runat="server">
            <div id="divAgregarPropiedades" class="row">
                <div class='col-md-1'>
                    <label><%= Traducir("PROPIEDADES") %>:</label>
                </div>
                <div class='col-md-3'>
                    <asp:Button ID="btnAgregarPropiedad" runat="server" Text="+" CssClass="btn btn-sm btn-warning" OnClientClick="MostrarAltaProp(); return false;" OnClick="btnAgregarPropiedad_Click" />
                </div>
            </div>
        </asp:PlaceHolder>
        <br />
        <br />
        <asp:PlaceHolder ID="PH_CamposProp" runat="server" Visible="false">
            <div id="divNombreProp" class="row">
                <div class='col-md-1'>
                    <label><%= Traducir("PROPIEDAD") %>:</label>
                </div>
                <div class='col-md-3'>
                    <asp:TextBox ID="iptPropNombbre" runat="server" ClientIDMode="Static" required="required" MaxLength="50" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="red" ID="rfv_iptPropNombbre" runat="server" ControlToValidate="iptPropNombbre"></asp:RequiredFieldValidator>
                </div>
                <div class='col-md-1 pull-left'>
                    <asp:Button ID="btnagregarpropiedad123" CssClass="btn btn-info pull-left" runat="server" Text="Agregar" OnClientClick="AgregarProp();" OnClick="btnagregarpropiedad123_Click" />
                </div>
            </div>
        </asp:PlaceHolder>






        <div class="col-md-12 form group">
            <div id="divPropiedades" class="row">
                <asp:PlaceHolder ID="PH_NombreProp" runat="server"></asp:PlaceHolder>
            </div>
        </div>
    </div>





    <div class="form-group clearfix">
        <a href="AdminClaseItem.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" OnClick="btnGuardar_Click" Text='Guardar' />

    </div>




    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="myModalEditorProp" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <asp:Label class="modal-title" ID="titmodalprop" runat="server"></asp:Label>


                </div>
                <div class="modal-body">





                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>


                            <div class='form-group clearfix'>
                                <div class='row'>
                                    <div class='col-md-2'>
                                        <label><%= Traducir("Valores") %>:</label>
                                    </div>
                                    <div class='col-md-8'>
                                        <textarea id="txtAreaDescripcion" style="width: 100%; max-width: 100%" runat="server" class="form-control boxsizingBorder" rows="3" maxlength="300"></textarea>
                                    </div>
                                    <div class='col-md-1'>
                                        <asp:Button ID="btnGuardarTodaLaProp" runat="server" CssClass="btn btn-sm btn-success" OnClick="btnGuardarTodaLaProp_Click" Text='Guardar' />
                                    </div>
                                </div>
                            </div>




                            <%--                                <asp:GridView ID="DgvPropiedadValores" runat="server" AllowSorting="True"
                                    AutoGenerateColumns="False" BorderStyle="None"
                                    CssClass="table table-striped table-hover" AllowPaging="True"
                                    GridLines="None" PagerStyle-CssClass="pagination-dgv "
                                    PagerStyle-HorizontalAlign="Right"
                                    OnRowDataBound="DgvPropiedadValores_RowDataBound"
                                    OnPageIndexChanging="DgvPropiedadValores_PageIndexChanging"
                                    OnRowCancelingEdit="DgvPropiedadValores_RowCancelingEdit"
                                    OnRowDeleting="DgvPropiedadValores_RowDeleting"
                                    OnRowEditing="DgvPropiedadValores_RowEditing"
                                    OnRowUpdating="DgvPropiedadValores_RowUpdating"
                                    PageSize="15">
                                    <Columns>
                                        <asp:BoundField DataField="test" HeaderText="Text" />
                                        <asp:CommandField ShowEditButton="True" ShowCancelButton="false" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="EDITAR" />
                                        <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="ELIMINAR" />
                                    </Columns>
                                </asp:GridView>

                                <asp:Button ID="btnAgregarRowValor" runat="server" CssClass="btn btn-success" Text="+" OnClick="btnAgregarRowValor_Click" UseSubmitBehavior="False" />
                            --%>
                        </ContentTemplate>
                    </asp:UpdatePanel>



                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnEliminarProp" Text="Eliminar Propiedad" CssClass="btn btn-danger pull-left" runat="server" OnClick="btnEliminarProp_Click"></asp:Button>
                    <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" data-dismiss="modal" aria-hidden="true" UseSubmitBehavior="false" />
                </div>
            </div>

        </div>
    </div>

    <script>

        function ShowCurrentTime() {
            waitingDialog.show();
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("AdminClaseItemForm.aspx/HelloWorld") %>',
                data: '',
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    waitingDialog.hide();
                }
            });
        }
    </script>

    <script type="text/javascript">
        $("#open_popup").click(function () {
            $("#popup").css("display", "block");
            $('body').css('overflow', 'hidden');
        });



        $("#close_popup").click(function () {
            $("#popup").css("display", "none");
            $('body').css('overflow', 'scroll');
        });

    </script>

    <script type="text/javascript">
        $('.openPopup').on('click', function () {
            var dataURL = $(this).attr('data-href');
            $('.modal-body').load(dataURL, function () {
                $('#myModal').modal({ show: true });
            });
        });

    </script>

    <script>
        function AgregarProp() {
            document.getElementById('MainContent_btnagregarpropiedad123').onclick();
        }

        function MostrarAltaProp() {
            document.getElementById('MainContent_btnAgregarPropiedad').onclick();
        }

        function Editarddlprop(midivdinamico) {
            alert("asd")
            console.log(midivdinamico);
            document.getElementById('MainContent_btnAgregarValorAProp').onclick();
        }

        function ActualizarPHProps() {
            document.getElementById('MainContent_btnAgregarValorAProp').onclick();
        }



    </script>
</asp:Content>
