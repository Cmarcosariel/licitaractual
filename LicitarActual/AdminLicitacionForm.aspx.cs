﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;

namespace LicitarActual
{
    public partial class AdminLicitacionForm : Cascara
    {

        public string FechaDesde_CB;
        public string FechaHasta_CB;





        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("NEG012");
            btnGuardar.Text = Traducir("GUARDAR");

            RequiredFieldValidator_ddRubros.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            RequiredFieldValidator_txtRznSocial.ErrorMessage = Traducir("CAMPO_REQUERIDO");
            txtAreaEntrega.Attributes.Add("PlaceHolder", Traducir("PH_AREAENTREGA"));
            txtAreaDescripcion.Attributes.Add("PlaceHolder", Traducir("PH_TITLICITACION"));
            txtAreaNotas.Attributes.Add("PlaceHolder", Traducir("PH_NOTAS"));
            btnPublicarLicitacion.Text = Traducir("PUBLICAR_LICITACION");



            PanelVisible(false);


            if (Request.QueryString["id"] != null)
            {
                PanelVisible(true);
                SegurizarLicitacion();
            }


            if (!IsPostBack)
            {
                NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
                cargarDropdownRubros();
                cargarData();
                SegurizarLicitacion();
            }
        }
        private void PanelVisible(bool visible)
        {
            divAlertaPublicarLicitacion.Visible = visible;
            UpdatePanel_PliegoItems.Visible = visible;
            btnPublicarLicitacion.Visible = visible;
            diventrega.Visible = visible;
            btnGuardar.Visible = !visible;
            
        }

        private void cargarDropdownRubros()
        {

            using (BLL.RubroBLL sec = new BLL.RubroBLL())
            {
                ddRubros.DataSource = null;
                ddRubros.DataMember = "Rubro";
                ddRubros.DataValueField = "Identidad";
                ddRubros.DataTextField = "Descripcion";

                ddRubros.DataSource = sec.SelectAll();

                ddRubros.DataBind();

            }

        }

        protected void cargarData()
        {
            txtRznSocial.Text = ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social;

            txtEstadoLic.Text = ((BE.EstadoLicitacionBE)1).ToString();

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int idOrg = Convert.ToInt32(Request.QueryString["id"]);

                ddRubros.Enabled = false;

                BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL();

                BE.LicitacionBE orgtmp = new BE.LicitacionBE() { Identidad = idOrg };

                BE.LicitacionBE licitacionBE = licitacionBLL.Select(orgtmp);

                if (licitacionBE != null)
                {
                    BE.RubroBE rubro = licitacionBE.Rubro;
                    ddRubros.SelectedValue = rubro.Identidad.ToString();

                    txtRznSocial.Text = licitacionBE.Organizacion.Razon_Social;

                    txtEstadoLic.Text = licitacionBE.Estado_Licitacion.ToString();



                    txtAreaDescripcion.InnerText = licitacionBE.Descripcion;

                    txtAreaNotas.InnerText = licitacionBE.Observaciones;

                    txtAreaEntrega.InnerText = licitacionBE.Pliego.DetallesEntrega;

                    milabelfechahasta.Text = licitacionBE.FechaCierre.ToString("dd'/'MM'/'yyyy");
                    milabelfechadesde.Text = licitacionBE.FechaApertura.ToString("dd'/'MM'/'yyyy");

                    using (BLL.RubroBLL bll = new BLL.RubroBLL())
                    {
                        rubro = bll.SelectRubroCompleto(rubro);
                    }

                    CargarDDCategorias(rubro);
                }


                CargarDGVPliego(licitacionBE);
            }

        }

        private void CargarDGVPliego(LicitacionBE licitacionBE)
        {
            DgvPliegoItems.DataSource = null;
            DgvPliegoItems.AutoGenerateColumns = false;

            DataTable dt = new DataTable();

            //dt.Columns.Add("Identidad", typeof(Int64));
            dt.Columns.Add("ItemDescripcion", typeof(string));
            dt.Columns.Add("Cantidad", typeof(Decimal));

            foreach (BE.ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
            {
                DataRow dr = dt.NewRow();
                //dr[0] = item.Identidad;
                dr[0] = item.Item.Descripcion;
                dr[1] = item.Cantidad;
                dt.Rows.Add(dr);
            }

            


            DgvPliegoItems.DataSource = dt;
            DgvPliegoItems.DataBind();



        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social != txtRznSocial.Text)
            {
                validarAcceso("NEG006");
            }
            else
            {
                validarAcceso("NEG012");
            }



            BE.LicitacionBE licitacionBE = null;
            BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL();



            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                licitacionBE = licitacionBLL.Select((new BE.LicitacionBE() { Identidad = Convert.ToInt64(Request.QueryString["id"]) }));
            }

            if (licitacionBE == null)
            {
                licitacionBE = new BE.LicitacionBE();
                licitacionBE.Identidad = 0;
            }

            BE.RubroBE rub = new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) };

            licitacionBE.Descripcion = txtAreaDescripcion.InnerText;
            licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)Enum.Parse(typeof(EstadoLicitacionBE), txtEstadoLic.Text);

            licitacionBE.FechaApertura = Convert.ToDateTime(Request.Form["datepickdesde"]);
            licitacionBE.FechaCierre = Convert.ToDateTime(Request.Form["datepickhasta"]);

            licitacionBE.Organizacion = ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion;

            licitacionBE.Rubro = rub;

            licitacionBE.Observaciones = txtAreaNotas.InnerText;

            if (licitacionBE.Identidad == 0)
            {
                licitacionBLL.Insert(licitacionBE);
                NuevaBitacora((String.Format("Nueva Licitacion en rubro {0} por empresa {1}", licitacionBE.Rubro.Descripcion, licitacionBE.Organizacion.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                BE.LicitacionBE licitacionBE1 = licitacionBLL.LicitacionSelectLastInsertbyOrgyRub(licitacionBE);

                Response.Redirect("AdminLicitacionForm.aspx?id=" + licitacionBE1.Identidad);
            }
            else
            {
                licitacionBLL.Update(licitacionBE);
                NuevaBitacora((String.Format("Se modifico la Licitacion {0} de empresa {1}", licitacionBE.Identidad, licitacionBE.Organizacion.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
                Response.Redirect("AdminLicitacionForm.aspx?id=" + licitacionBE.Identidad);
            }

        }

        protected void DgvPliegoItems_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        protected void DgvPliegoItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void DgvPliegoItems_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void DgvPliegoItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LicitacionBE licitacionBE = new LicitacionBE();

            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {
                licitacionBE = licitacionBLL.Select((new BE.LicitacionBE() { Identidad = Convert.ToInt64(Request.QueryString["id"]) }));

                licitacionBE.Pliego.ItemsLicitacion.RemoveAll(x => x.Item.Descripcion == ((GridView)sender).Rows[e.RowIndex].Cells[1].Text);
                
                licitacionBLL.Update(licitacionBE);

                NuevaBitacora((String.Format("Se Actualizo el pliego de  la Licitacion {0} de empresa {1}", licitacionBE.Identidad, licitacionBE.Organizacion.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

            }

            CargarDGVPliego(licitacionBE);
        }

        protected void DgvPliegoItems_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void DgvPliegoItems_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }



        public void miPopUpCargaItem(BE.LicitacionBE licitacion)
        {

            CargarDDCategorias(licitacion.Rubro);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"$('#myModalAgregarProp').modal('show');");
            sb.Append(@"</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());
        }

        public void CargarDGVModal(BE.Clase_ItemBE clase_ItemBE)
        {
            using (BLL.ItemBLL bll = new BLL.ItemBLL())
            {

                dgvItemsdeClaseItem.DataSource = null;
                dgvItemsdeClaseItem.AutoGenerateColumns = false;
                dgvItemsdeClaseItem.DataSource = bll.SelectAllByID_Clase(clase_ItemBE);
                dgvItemsdeClaseItem.DataBind();

            }



        }

        public void CargarDDCategorias(RubroBE rubro)
        {
            ddCategorias.DataSource = null;
            ddCategorias.DataMember = "Clases";
            ddCategorias.DataValueField = "Identidad";
            ddCategorias.DataTextField = "Descripcion";

            ddCategorias.DataSource = rubro.Clases;

            ddCategorias.DataBind();

            ddCategorias.Items.Insert(0, Traducir("ELEGIR_1"));
            ddCategorias.SelectedIndex = 0;

        }

        protected void dgvItemsdeClaseItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }

        }

        protected void dgvItemsdeClaseItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void dgvItemsdeClaseItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void dgvItemsdeClaseItem_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            TextBox textBox = (TextBox)((GridView)sender).Rows[e.RowIndex].Cells[3].Controls[1];

            if (!Decimal.TryParse(textBox.Text, out decimal asd))
            {
                // No es un nro decimal

                ViewState["lblErrorPliego"] = Traducir("MSJ_ERR_CANT_MAL");
                divalerta.Visible = true;
                divsuccess.Visible = false;
                lblErrorPliego.Text = Traducir("MSJ_ERR_CANT_MAL");
                lblSuccessPliego.Text = "";
                lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC");

                using (BLL.Clase_ItemBLL clase_ItemBLL = new BLL.Clase_ItemBLL())
                {
                    dgvItemsdeClaseItem.EditIndex = -1;
                    CargarDGVModal(new Clase_ItemBE() { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) });
                }

                return;
            }

            // es decimal
            
            //es menor a 0?
            if (Convert.ToDecimal(textBox.Text) < 0)
            {
                ViewState["lblErrorPliego"] = Traducir("MSJ_ERR_CANT_MAL");
                divalerta.Visible = true;
                divsuccess.Visible = false;
                lblErrorPliego.Text = Traducir("MSJ_ERR_CANT_MAL");
                lblSuccessPliego.Text = "";
                lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC");

                using (BLL.Clase_ItemBLL clase_ItemBLL = new BLL.Clase_ItemBLL())
                {
                    dgvItemsdeClaseItem.EditIndex = -1;
                    CargarDGVModal(new Clase_ItemBE() { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) });
                }

                return;
            }

            //es mayor a 0 
            BE.ItemBE NuevoItem = new ItemBE();
            NuevoItem.Descripcion = ((GridView)sender).Rows[e.RowIndex].Cells[1].Text;
            BE.Clase_ItemBE clase = new BE.Clase_ItemBE() { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) };


            using (BLL.ItemBLL bllitem = new BLL.ItemBLL())
            {
                using (BLL.LicitacionBLL blllic = new BLL.LicitacionBLL())
                {
                    BE.LicitacionBE licitaciontmp = new LicitacionBE();

                    licitaciontmp.Identidad = Convert.ToInt64(Request.QueryString["id"]);

                    licitaciontmp = blllic.Select(licitaciontmp);

                    if (licitaciontmp.Estado_Licitacion != EstadoLicitacionBE.Borrador)
                    {
                        ViewState["lblErrorPliego"] = Traducir("MSJ_ERR_LIC_EST_FINAL");
                        divalerta.Visible = true;
                        divsuccess.Visible = false;
                        lblErrorPliego.Text = Traducir("MSJ_ERR_LIC_EST_FINAL");
                        lblSuccessPliego.Text = "";
                        lblMensajeAlerta.Text = Traducir("MSJ_ERR_LIC_EST_FINAL");

                        using (BLL.Clase_ItemBLL clase_ItemBLL = new BLL.Clase_ItemBLL())
                        {
                            dgvItemsdeClaseItem.EditIndex = -1;
                            CargarDGVModal(new Clase_ItemBE() { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) });
                        }

                        return;
                    }




                    BE.ItemsLicitacionBE itemsLicitacionBE = new BE.ItemsLicitacionBE();

                    itemsLicitacionBE.Item = bllitem.SelectbyDescyClase(NuevoItem, clase);

                    itemsLicitacionBE.Cantidad = Convert.ToDecimal(textBox.Text);

                    if (licitaciontmp.Pliego.ItemsLicitacion.Any(x => x.Item.Descripcion == itemsLicitacionBE.Item.Descripcion))
                    {


                        ViewState["lblErrorPliego"] = Traducir("MSJ_ERR_ITEM_EXIST");
                        divalerta.Visible = true;
                        divsuccess.Visible = false;
                        lblErrorPliego.Text = Traducir("MSJ_ERR_ITEM_EXIST");
                        lblSuccessPliego.Text = "";
                        using (BLL.Clase_ItemBLL clase_ItemBLL = new BLL.Clase_ItemBLL())
                        {
                            dgvItemsdeClaseItem.EditIndex = -1;
                            CargarDGVModal(new Clase_ItemBE() { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) });
                        }

                    }
                    else
                    {
                        licitaciontmp.Pliego.ItemsLicitacion.Add(itemsLicitacionBE);

                        blllic.Update(licitaciontmp);

                        NuevaBitacora((String.Format("Se Actualizo el pliego de  la Licitacion {0} de empresa {1}", licitaciontmp.Identidad, licitaciontmp.Organizacion.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

                        CargarDGVPliego(licitaciontmp);


                        ViewState["lblSuccessPliego"] = Traducir("MSJ_OK_ITEM_CARGA");
                        divsuccess.Visible = true;
                        divalerta.Visible = false;
                        lblSuccessPliego.Text = Traducir("MSJ_OK_ITEM_CARGA");
                        lblErrorPliego.Text = "";

                        using (BLL.Clase_ItemBLL clase_ItemBLL = new BLL.Clase_ItemBLL())
                        {
                            dgvItemsdeClaseItem.EditIndex = -1;
                            CargarDGVModal(new Clase_ItemBE() { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) });
                        }
                    }


                }
            }



        }
        protected void dgvItemsdeClaseItem_RowEditing(object sender, GridViewEditEventArgs e)
        {


        }

        protected void dgvItemsdeClaseItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void ddCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt64(ddCategorias.SelectedIndex) > 0)
            {
                BE.Clase_ItemBE clase_ItemBE = new BE.Clase_ItemBE { Identidad = Convert.ToInt64(ddCategorias.SelectedValue) };

                CargarDGVModal(clase_ItemBE);
            }
            else
            {
                dgvItemsdeClaseItem.DataSource = null;
                dgvItemsdeClaseItem.DataBind();
            }


        }

        protected void dgvItemsdeClaseItem_RowCreated(object sender, GridViewRowEventArgs e)
        {
            {
                if (e.Row.Cells.Count >= 2)
                {
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[4].Visible = false;
                }


            }
        }

        protected void dgvItemsdeClaseItem_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

            divsuccess.Visible = false;
            divalerta.Visible = false;  

            if (((GridView)sender).Rows[e.NewSelectedIndex].Cells.Count >= 2)
            {
                ((GridView)sender).Rows[e.NewSelectedIndex].Cells[3].Visible = true;
                ((GridView)sender).HeaderRow.Cells[3].Visible = true;
                ((GridView)sender).Rows[e.NewSelectedIndex].Cells[4].Visible = true;
                ((GridView)sender).HeaderRow.Cells[4].Visible = true;
            }
        }

        protected void btnPublicarLicitacion_Click(object sender, EventArgs e)
        {
            BE.LicitacionBE licitacionBE;

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                licitacionBE = bll.Select((new BE.LicitacionBE() { Identidad = Convert.ToInt64(Request.QueryString["id"]) }));
            }

            BE.RubroBE rub = new BE.RubroBE { Identidad = Convert.ToInt64(ddRubros.SelectedValue) };

            licitacionBE.Descripcion = txtAreaDescripcion.InnerText;
            licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)Enum.Parse(typeof(EstadoLicitacionBE), txtEstadoLic.Text);

            licitacionBE.FechaApertura = Convert.ToDateTime(Request.Form["datepickdesde"]);
            licitacionBE.FechaCierre = Convert.ToDateTime(Request.Form["datepickhasta"]);

            licitacionBE.Organizacion = ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion;

            licitacionBE.Rubro = rub;

            licitacionBE.Pliego.DetallesEntrega = txtAreaEntrega.InnerText;

            licitacionBE.Observaciones = txtAreaNotas.InnerText;


            if (ValidarLicitacion(licitacionBE))
            {
                licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)Enum.Parse(typeof(EstadoLicitacionBE), "Programada");

                using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
                {
                    bll.Update(licitacionBE);

                    NuevaBitacora((String.Format("Se publico la Licitacion {0} de empresa {1}", licitacionBE.Descripcion, licitacionBE.Organizacion.Razon_Social)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

                    bll.EnviarAlerta(licitacionBE, ((UsuarioBE)Session["UsuarioLogueado"]).Organizacion);


                }

                Response.Redirect("TableroMiEmpresa.aspx");


            }

        }

        private bool ValidarLicitacion(LicitacionBE licitacionBE)
        {
            if (licitacionBE.Estado_Licitacion != EstadoLicitacionBE.Borrador)
            {
                labelMensajesErrorAltaLicitacion.Text = Traducir("MSJ_ERR_LIC_EST_FINAL");
                divMensajesErrorAltaLicitacion.Visible = true;
                return false;
            }


            if (licitacionBE.Descripcion == null || licitacionBE.Descripcion.ToString() == "" || licitacionBE.Descripcion.Length < 10)
            {
                labelMensajesErrorAltaLicitacion.Text = Traducir("MSJ_ERR_LIC_EST_FINAL");
                divMensajesErrorAltaLicitacion.Visible = true;
                return false;
            }

            if (licitacionBE.Pliego.ItemsLicitacion.Count() == 0)
            {
                labelMensajesErrorAltaLicitacion.Text = Traducir("MSJ_ERR_PLIEGO_VACIO");
                divMensajesErrorAltaLicitacion.Visible = true;
                return false;
            }

            if (licitacionBE.FechaCierre <= licitacionBE.FechaApertura)
            {
                labelMensajesErrorAltaLicitacion.Text = Traducir("MSJ_ERR_FECHA_MAL");
                divMensajesErrorAltaLicitacion.Visible = true;
                return false;
            }

            if (licitacionBE.FechaApertura < (DateTime.Today.AddDays(3)))
            {
                labelMensajesErrorAltaLicitacion.Text = Traducir("MSJ_ERR_FECHA_AP3D");
                divMensajesErrorAltaLicitacion.Visible = true;
                return false;
            }

            if (licitacionBE.FechaCierre <= licitacionBE.FechaApertura.AddDays(2))
            {
                labelMensajesErrorAltaLicitacion.Text = Traducir("MSJ_ERR_FECHA_3DAB");
                divMensajesErrorAltaLicitacion.Visible = true;
                return false;
            }

            return true;


        }

        public void SegurizarLicitacion()
        {
            if (txtEstadoLic.Text != EstadoLicitacionBE.Borrador.ToString())
            {

                DgvPliegoItems.Columns[3].Visible = false;

                btnPublicarLicitacion.Visible = false;
                divAlertaPublicarLicitacion.Visible = false;
                
            }


        }

    }
}