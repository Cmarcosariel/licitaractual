﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class Login : Cascara
    {
        bool todook;

        protected void Page_Load(object sender, EventArgs e)
        {



            try
            {

                Page.DataBind();

                //RecalcularTodosDvh();
                //RecalcularTodosDvv();


                using (SEC.GestorDV sec = new SEC.GestorDV())
                {
                    if (!sec.CalcularIntegridadDB())
                    {
                        lblMessage.Text = "Error en BD contactar al Administrador";
                        todook = false;
                        miPopUp();
                    }
                    else
                    {
                         todook = true;
                    }

                    sec.CambioDia();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                miPopUp();
            }




        }







        protected void sendlogin_Click(object sender, EventArgs e)
        {
            BLL.UsuarioBLL UnUsuarioBLL = new BLL.UsuarioBLL();

            try
            {

                if (todook == false && username.Text != "Administrador")
                {
                    lblMessage.Text = "Error en BD contactar al Administrador";
                    todook = false;
                    miPopUp();
                    return;
                }
                               

                validarUsuarioPassword();

                BE.UsuarioBE usuarioLogueado = UnUsuarioBLL.IntentarLogin((new BE.UsuarioBE(username.Text, password.Text)));

                Session.Add("UsuarioLogueado", usuarioLogueado);

                Session.Add("Lang", usuarioLogueado.Idioma.Descripcion);

                if (!validarAcceso("LIN001"))
                {
                    rtaError.Visible = true;
                    error.Text = Traducir("LOGIN_ROLE_MISSING");
                }
                else
                {
                    Response.Redirect("TableroMiEmpresa.aspx");
                }
            }
            catch (Exception ex)
            {
                rtaError.Visible = true;
                error.Text = Traducir(ex.Message);
            }

        }

        private void validarUsuarioPassword()
        {
            if (username.Text == null || "".Equals(username.Text) || password.Text == null || "".Equals(password.Text))
            {
                throw new Exception("ExcFaltanCamposLogin");
            }
        }
        protected void password_TextChanged(object sender, EventArgs e)
        {

        }


    }
}