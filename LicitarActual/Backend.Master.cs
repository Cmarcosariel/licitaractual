﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class Backend : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    BLL.IdiomaBLL idiomaBll = new BLL.IdiomaBLL();
                    ddIdiomas.DataMember = "Idioma";
                    ddIdiomas.DataValueField = "Identidad";
                    ddIdiomas.DataTextField = "Descripcion";
                    List<BE.IdiomaBE> idiomas = idiomaBll.SelectAll();
                    BE.IdiomaBE idiomaSeleccionado = (BE.IdiomaBE)idiomas.FirstOrDefault(idioma => Session["Lang"].ToString().Equals(idioma.Descripcion));
                    ddIdiomas.DataSource = idiomas;
                    ddIdiomas.DataBind();
                    ddIdiomas.Items.FindByValue(idiomaSeleccionado.Identidad.ToString()).Selected = true;
                }
                catch (Exception ex)
                {
                    Response.Redirect("Login.aspx");
                }

            }


            if (TienePermisoMenu("ADM014") || TienePermisoMenu("ADM002") || TienePermisoMenu("ADM005") || TienePermisoMenu("ADM003") || TienePermisoMenu("ADM015") || TienePermisoMenu("ADM016"))
            {
                ItemSeguridad.Visible = true;
            }
            else
            {
                ItemSeguridad.Visible = false;
            }


            liAdminUsuario.Visible = TienePermisoMenu("ADM014");
            liAdminPermisosGrupos.Visible = TienePermisoMenu("ADM002");
            liAdminBitacora.Visible = TienePermisoMenu("ADM005");
            liAdminBackup.Visible = TienePermisoMenu("ADM003");
            liAdminIdioma.Visible = TienePermisoMenu("ADM015");
            liAdminRecalcularDV.Visible = TienePermisoMenu("ADM016");


            if (TienePermisoMenu("NEG002") || TienePermisoMenu("NEG003") || TienePermisoMenu("NEG005") || TienePermisoMenu("NEG004") || TienePermisoMenu("NEG006") || TienePermisoMenu("NEG010") || TienePermisoMenu("NEG011") || TienePermisoMenu("NEG009"))
            {
                Li1.Visible = true;
            }
            else
            {
                Li1.Visible = false;
            }

            liAdminOrg.Visible = TienePermisoMenu("NEG002");
            liAdminRubros.Visible = TienePermisoMenu("NEG003");
            liAdminClaseitem.Visible = TienePermisoMenu("NEG004");
            liAdminitem.Visible = TienePermisoMenu("NEG005");
            liAdminLicitacion.Visible = TienePermisoMenu("NEG006");
            liAdminSuscribirRubros.Visible = TienePermisoMenu("NEG011");
            liTableroLicitacion.Visible = TienePermisoMenu("NEG010");
            liTableroMiEmpresa.Visible = TienePermisoMenu("NEG009");

            if (TienePermisoMenu("NEG007"))
            {
                Li2.Visible = true;
            }
            else {
                Li2.Visible = false;
            }

            liReporteLicitacionXRubro.Visible = TienePermisoMenu("NEG007");

        }


        protected void ddIdiomas_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (BLL.IdiomaBLL idiomaBll = new BLL.IdiomaBLL())
            {
                BE.IdiomaBE Unidioma = new BE.IdiomaBE() { Identidad = Int32.Parse(ddIdiomas.SelectedValue) };

                BE.IdiomaBE idioma = idiomaBll.Select(Unidioma);

                using (BLL.UsuarioBLL usuarioBll = new BLL.UsuarioBLL())
                {
                    usuarioBll.cambiarIdioma((BE.UsuarioBE)Session["UsuarioLogueado"], idioma);
                    ((BE.UsuarioBE)Session["UsuarioLogueado"]).Idioma = idioma;

                    NuevaBitacora((String.Format("Se cambio el idioma asignado del usuario a {0}", idioma.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

                    Session.Add("Lang", idioma.Descripcion);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                
            }

        }

        public void NuevaBitacora(string registro, BE.UsuarioBE _usuario, string Categoria, int Severidad)
        {
            BE.Evento_BitacoraBE eventonuevo = new BE.Evento_BitacoraBE();
            eventonuevo.Descripcion = Categoria;
            eventonuevo.Severidad = Severidad;

            using (SEC.BitacoraSEC sec = new SEC.BitacoraSEC())
            {
                BE.BitacoraBE nuevoregistro = new BE.BitacoraBE();
                nuevoregistro.Descripcion = registro;

                nuevoregistro.Evento_Bitacora = sec.GetSeveridadEvento(eventonuevo);

                nuevoregistro.Usuario = _usuario;

                nuevoregistro.Timestamp = DateTime.Now.Ticks;

                sec.GrabarBitacora(nuevoregistro);
            }

        }

        public string traducir(string texto)
        {
            string traduccion = texto;
            IList<BE.TraduccionBE> traducciones = null;

            if (Session["Lang"] == null)
            {
                traducciones = (List<BE.TraduccionBE>)Application["Español"];
            }
            else
            {
                traducciones = (List<BE.TraduccionBE>)Application[Session["Lang"].ToString()];
            }

            try
            {
                BE.TraduccionBE mensaje = traducciones.FirstOrDefault(x => x.Mensaje.Descripcion.Equals(texto));
                if (mensaje != null) traduccion = mensaje.Traduccion;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return traduccion;
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Login.aspx");

        }

        protected void GrabarBitacoraNavegacion(string pagina)
        {
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario , pagina)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
        }


        public bool TienePermisoMenu(string permiso123)
        {

            if (HttpContext.Current.Session["UsuarioLogueado"] == null)
                return false;

            BE.UsuarioBE UsuarioLogueado = (BE.UsuarioBE)HttpContext.Current.Session["UsuarioLogueado"];

            if (UsuarioLogueado.permisos == null)
            {
                return false;
            }

            using (SEC.PermisoSEC sec = new SEC.PermisoSEC())
            {
                foreach (BE.PermisoBE permiso in sec.GenerarListaPlana(UsuarioLogueado))
                {
                    if (permiso123 == permiso.Codigo_Perfil)
                        return true;
                }

            }

            return false;
        }

    }
}