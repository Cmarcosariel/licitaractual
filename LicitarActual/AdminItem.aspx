﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminItem.aspx.cs" Inherits="LicitarActual.AdminItem" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>


    <div>
        <div class="content-header">
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="litUsuarios" runat="server"></asp:Literal></strong>
                    <a href="AdminItemForm.aspx" class="btn btn-primary pull-right">
                        <asp:Literal ID="Literal2" runat="server"></asp:Literal></a><br />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="row">
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default bg-panel">
                        <div class="panel-body">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon"><%= Traducir("ITEMS") %></div>
                                    <asp:TextBox ID="filtroUsername" runat="server" MaxLength="20" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <asp:Button ID="btnBuscar" runat="server" Text='Buscar' CssClass="btn btn-info" OnClick="btnBuscar_Click" />
                            <a href="/AdminItem.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;<%= Traducir("LIMPIAR") %></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead"><%= Traducir("ENCONTRADO") %> <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong>  <%= Traducir("REGISTROS") %>
                </span>
                <span class="help-block pull-right"><%= Traducir("MOSTRANDO_15") %></span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvUsuarios" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" BorderStyle="None"
                    CssClass="table table-striped table-hover" AllowPaging="True"
                    GridLines="None" PagerStyle-CssClass="pagination-ys"
                    PagerStyle-HorizontalAlign="Right"
                    DataKeyNames="Identidad"
                    OnRowDataBound="DgvUsuarios_RowDataBound"
                    OnPageIndexChanging="DgvUsuarios_PageIndexChanging"
                    OnRowCancelingEdit="DgvUsuarios_RowCancelingEdit"
                    OnRowDeleting="DgvUsuarios_RowDeleting"
                    OnRowEditing="DgvUsuarios_RowEditing" OnRowUpdating="DgvUsuarios_RowUpdating"
                    PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="Identidad" HeaderText="CODIGO" Visible="false" />
                        <asp:TemplateField ControlStyle-CssClass="" HeaderText="CLASEITEM">
                            <ItemTemplate>
                                <asp:Label ID="lblClaseItem" runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Item" />
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="EDITAR" />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="ELIMINAR" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">&nbsp;</div>

    </div>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
