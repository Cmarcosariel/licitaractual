﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class PermisoDenegado : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NuevaBitacora((String.Format("el usuario {0} intento realizar una accion no permitida", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 3);
            lbldenegado.Text = Traducir("DENEGADO");

        }
    }
}