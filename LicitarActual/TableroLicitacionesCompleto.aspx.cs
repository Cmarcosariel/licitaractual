﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;

namespace LicitarActual
{
    public partial class TableroLicitacionesCompleto : Cascara
    {
        BE.LicitacionBE MiLicitacionEdit;
        BE.OfertaBE MiOfertaEdit;
        DataTable MiDTEdit;


        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("LICITACIONES");


            validarAcceso("NEG010");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

            btnPublicarOfertaLicitacion.Text = Traducir("PUBLICAR_OFERTA");

            btnCancelarOfertaLicitacion.Text = Traducir("CANCELAR_OFERTA");

            if (!IsPostBack)
            {
                llenarLicitaciones();
                Page.DataBind();
            }



        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG010");
            BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL();
            BE.LicitacionBE licitacionBE = new BE.LicitacionBE();


            licitacionBE.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());

            VerDisplayModal(licitacionBE);

        }

        private void VerDisplayModal(LicitacionBE licitacionBE)
        {
            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {
                licitacionBE = licitacionBLL.Select(licitacionBE);

                CargarControlesModal(licitacionBE);

            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"$('#myModalVerLicitacion').modal('show');");
            sb.Append(@"</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());

        }


        private void VerOfertaModal(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {

                CargarControlesModalOferta(licitacionBE, nueva);

            }

            divErrorOfertarLicitacion.Visible = false;
            lblErrorOfertarLicitacion.Text = string.Empty;



            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"$('#myModalOfertarLicitacion').modal('show');");
            sb.Append(@"</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript2", sb.ToString());

        }

        private void CargarControlesModal(LicitacionBE licitacionBE)
        {
            titmodalprop.Text = Traducir("DETALLE_LICITACION");

            txtRznSocial.Text = licitacionBE.Organizacion.Razon_Social;

            txtEstadoLic.Text = licitacionBE.Estado_Licitacion.ToString();

            txtRubro.Text = licitacionBE.Rubro.Descripcion;


            txtAreaDescripcion.InnerText = licitacionBE.Descripcion;

            txtAreaNotas.InnerText = licitacionBE.Observaciones;

            txtAreaEntrega.InnerText = licitacionBE.Pliego.DetallesEntrega;

            milabelfechahasta.Text = licitacionBE.FechaCierre.ToString("dd'/'MM'/'yyyy");
            milabelfechadesde.Text = licitacionBE.FechaApertura.ToString("dd'/'MM'/'yyyy");

            DgvPliegoItems.DataSource = null;
            DgvPliegoItems.AutoGenerateColumns = false;

            DataTable dt = new DataTable();

            //dt.Columns.Add("Identidad", typeof(Int64));
            dt.Columns.Add("ItemDescripcion", typeof(string));
            dt.Columns.Add("Cantidad", typeof(Decimal));

            foreach (BE.ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
            {
                DataRow dr = dt.NewRow();
                //dr[0] = item.Identidad;
                dr[0] = item.Item.Descripcion;
                dr[1] = item.Cantidad;
                dt.Rows.Add(dr);
            }

            DgvPliegoItems.DataSource = dt;
            DgvPliegoItems.DataBind();
        }

        private void CargarControlesModalOferta(LicitacionBE licitacionBE, OfertaBE nueva)
        {


            CargarPanelLicitacionModalOferta(licitacionBE);

            CargarDGVPliegoLicitacionModalOferta(licitacionBE, nueva);

            CargarPanelDatosOferta(nueva);

        }

        private void CargarPanelDatosOferta(OfertaBE nueva)
        {
            txtestadoOferta.Text = nueva.Estado_Oferta.ToString();
            txtSubtotalOferta.Text = nueva.Total.ToString("C");
            //txtSubtotalOferta.Text = (Convert.ToDecimal(nueva.Total)).ToString();
        }

        private void CargarDGVPliegoLicitacionModalOferta(DataTable dataTable)
        {
            DgvPliegoItemsOferta.DataSource = null;
            DgvPliegoItemsOferta.AutoGenerateColumns = false;

            DgvPliegoItemsOferta.DataSource = dataTable;
            DgvPliegoItemsOferta.DataBind();

            MiDTEdit = dataTable;

            DgvPliegoItemsOferta.Columns[1].Visible = false;
        }

        private void CargarDGVPliegoLicitacionModalOferta(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            Dictionary<BE.ItemBE, decimal> dicPrecioSugerido;

            DgvPliegoItemsOferta.DataSource = null;
            DgvPliegoItemsOferta.AutoGenerateColumns = false;

            DataTable dt = new DataTable();



            dt.Columns.Add("ItemDescripcion", typeof(string));
            dt.Columns.Add("Cantidad", typeof(Decimal));
            dt.Columns.Add("PrecioUnitario", typeof(Decimal));
            dt.Columns.Add("Identidad", typeof(Int64));
            dt.Columns.Add("PrecioSugerido", typeof(Decimal));

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                dicPrecioSugerido =  bll.LlenarOfertaRecomendada(licitacionBE, nueva);
            }

            divnodatasugerido.Visible = false;  


            foreach (BE.ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
            {
                DataRow dr = dt.NewRow();
                //dr[0] = item.Identidad;
                dr[0] = item.Item.Descripcion;
                dr[1] = item.Cantidad;

                dr[2] = (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).PrecioUnitario;
                dr[3] = (nueva.items.First(x => x.Item.Descripcion == item.Item.Descripcion)).Identidad;

                decimal preciosugerido = (dicPrecioSugerido.Keys.Any(x => x.Identidad == item.Item.Identidad)) ? dicPrecioSugerido[dicPrecioSugerido.Keys.FirstOrDefault(x => x.Identidad == item.Item.Identidad)] : 0;



                //dr[4] = preciosugerido;

                if (preciosugerido != 0)
                {
                    dr[4] = preciosugerido;
                }
                else
                {
                    dr[0] = (dr[0].ToString() + "(*)");
                    dr[4] = preciosugerido;
                    divnodatasugerido.Visible = true;
                }



                dt.Rows.Add(dr);
            }

            DgvPliegoItemsOferta.DataSource = dt;
            DgvPliegoItemsOferta.DataBind();

            if (MiDTEdit == null)
            {
                MiDTEdit = dt;

                ViewState["MiDTEdit"] = dt;
            }

            if (nueva.Estado_Oferta != Estado_OfertaBE.Borrador)
            {
                DgvPliegoItemsOferta.Columns[6].Visible = false;
                btnPublicarOfertaLicitacion.Visible = false;


                lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC_PRES");


                btnCancelarOfertaLicitacion.Visible = true;

                if (nueva.Estado_Oferta == Estado_OfertaBE.Cancelada)
                {
                    btnCancelarOfertaLicitacion.Visible = false;
                }



            }
            else
            {
                DgvPliegoItemsOferta.Columns[6].Visible = true;
                btnPublicarOfertaLicitacion.Visible = true;
                btnCancelarOfertaLicitacion.Visible = false;
                lblMensajeAlerta.Text = Traducir("MSJ_WARN_LIC_OFERT");
            }


            DgvPliegoItemsOferta.Columns[1].Visible = false;

            if (nueva.Estado_Oferta != BE.Estado_OfertaBE.Borrador)
            {
                DgvPliegoItemsOferta.Columns[6].Visible = false;
            }

        }

        private void CargarPanelLicitacionModalOferta(LicitacionBE licitacionBE)
        {
            titmodalpropOferta.Text = Traducir("DETALLE_LICITACION");

            txtRznSocialOferta.Text = licitacionBE.Organizacion.Razon_Social;

            txtEstadoLicOferta.Text = licitacionBE.Estado_Licitacion.ToString();

            txtRubroOferta.Text = licitacionBE.Rubro.Descripcion;


            txtAreaDescripcionOferta.InnerText = licitacionBE.Descripcion;

            txtAreaNotasOferta.InnerText = licitacionBE.Observaciones;

            txtAreaEntregaOferta.InnerText = licitacionBE.Pliego.DetallesEntrega;

            milabelfechahastaOferta.Text = licitacionBE.FechaCierre.ToString("dd'/'MM'/'yyyy");
            milabelfechadesdeOferta.Text = licitacionBE.FechaApertura.ToString("dd'/'MM'/'yyyy");
        }

        protected void llenarLicitaciones()
        {
            BLL.LicitacionBLL LicitacionBLL = new BLL.LicitacionBLL();

            List<BE.LicitacionBE> orgs = LicitacionBLL.SelectAllVigente(((UsuarioBE)Session["UsuarioLogueado"]).Organizacion);

            //List<BE.LicitacionBE> orgs = LicitacionBLL.SelectAllVigenteParaSuscripciones(((UsuarioBE)Session["UsuarioLogueado"]).Organizacion);

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = orgs;

            if (orgs != null)
            {
                txtCantRegistros.Text = orgs.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }
            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarLicitaciones();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.LicitacionBLL licbll = new BLL.LicitacionBLL();
                List<BE.LicitacionBE> usuarios = licbll.SelectActivosParaUsuario(getFiltros(),((UsuarioBE)Session["UsuarioLogueado"]).Organizacion);
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {



            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarLicitaciones();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("LICITACIONES"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("LICITACIONES"));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }


        protected void DgvUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            validarAcceso("NEG010");

            if (e.CommandName == "OfertarLicitacion")
            {

            }
        }

        protected void btnExportarPliego_Click(object sender, EventArgs e)
        {

        }

        protected bool LicitacionAbierta(EstadoLicitacionBE asd)
        {
            if (asd == EstadoLicitacionBE.Abierta)
            {
                return true;
            }
            return false;
        }

        protected void BtnOfertarLicitacion_Click(object sender, EventArgs e)
        {
            validarAcceso("NEG010");

            BE.LicitacionBE licitacionBE = new LicitacionBE();

            int indicelicitacion = ((System.Web.UI.WebControls.GridViewRow)((Button)sender).NamingContainer).DataItemIndex;

            licitacionBE.Identidad = Int32.Parse(DgvUsuarios.DataKeys[indicelicitacion].Values[0].ToString());


            using (BLL.LicitacionBLL licitacionBLL = new BLL.LicitacionBLL())
            {
                licitacionBE = licitacionBLL.Select(licitacionBE);


            }



            BE.OfertaBE nueva = new OfertaBE();

            nueva.Organizacion = ((UsuarioBE)Session["UsuarioLogueado"]).Organizacion;

            //existe una oferta en estado borrador para esa empresa y licitacion?

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                if (bll.SelectByLicitacionOrgBorrador(licitacionBE, nueva) != null)
                {
                    // Si, existe

                    nueva = bll.SelectByLicitacionOrgBorrador(licitacionBE, nueva);


                }
                else
                {
                    // No existe Borrador

                    // Existe en algun otro estado que no sea BORRADOR ni CANCELADA? 

                    if (bll.SelectByLicitacionOrg(licitacionBE, nueva) != null)
                    {
                        //Si existe, NO PUEDE OFERTAR, podemos mostrar status o algo asi


                        nueva = bll.SelectByLicitacionOrg(licitacionBE, nueva);


                    }
                    else
                    {

                        //No existe borrador, podemos crear uno.

                        foreach (ItemsLicitacionBE item in licitacionBE.Pliego.ItemsLicitacion)
                        {
                            BE.ItemsOfertaBE itemsOfertaBE = new ItemsOfertaBE();
                            itemsOfertaBE.Item = item.Item;
                            itemsOfertaBE.PrecioUnitario = 0;
                            nueva.items.Add(itemsOfertaBE);

                        }

                        //Insertamos 

                        using (BLL.OfertaBLL blloferta = new BLL.OfertaBLL())
                        {
                            blloferta.Insert(nueva, licitacionBE);
                        }

                        //calculamos valores target 
                        using (BLL.LicitacionBLL licitacionBLL1 = new BLL.LicitacionBLL())
                        {
                            licitacionBLL1.CalcularPreciosTarget(licitacionBE, nueva);
                        }

                        //asignamos el borrador recien creado
                        using (BLL.OfertaBLL blloferta2 = new BLL.OfertaBLL())
                        {
                            blloferta2.SelectByLicitacionOrgBorrador(licitacionBE, nueva);
                        }




                    }

                }
            }





            ViewState["MiLicitacionEdit"] = licitacionBE.Identidad.ToString();
            MiLicitacionEdit = licitacionBE;
            ViewState["MiOfertaEdit"] = nueva.Identidad.ToString();
            MiOfertaEdit = nueva;


            VerOfertaModal(licitacionBE, nueva);

        }

        protected void DgvPliegoItemsOferta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "GuardarPrecio")
            {

            }
        }

        protected void btnGuardarPrecioUnitario_Click(object sender, EventArgs e)
        {

        }

        public void GridButtons_Command(object sender, CommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);

            if (e.CommandName == "GuardarPrecio")
            {
                BE.ItemsOfertaBE itemsOfertaBE = new BE.ItemsOfertaBE();

                itemsOfertaBE.Identidad = Convert.ToInt64(((GridViewRow)(((DataControlFieldCell)(((Button)sender).Parent)).Parent)).Cells[1].Text);

                using (BLL.ItemsOfertaBLL BLL = new BLL.ItemsOfertaBLL())
                {
                    itemsOfertaBE = BLL.Select(itemsOfertaBE);
                }

                itemsOfertaBE.PrecioUnitario = Convert.ToInt64(((GridViewRow)(((DataControlFieldCell)(((Button)sender).Parent)).Parent)).Cells[4].Text);


            }


        }

        protected void DgvPliegoItemsOferta_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }

        }

        protected void DgvPliegoItemsOferta_RowEditing(object sender, GridViewEditEventArgs e)
        {

            OfertaBE asdasda;

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                asdasda = bll.Select((new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) }));
            }

            LicitacionBE asdasda123;

            using (BLL.LicitacionBLL bll = new BLL.LicitacionBLL())
            {
                asdasda123 = bll.Select((new BE.LicitacionBE { Identidad = Convert.ToInt64(ViewState["MiLicitacionEdit"]) }));
            }

            DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];

            divErrorOfertarLicitacion.Visible = false;

            DgvPliegoItemsOferta.EditIndex = e.NewEditIndex;
            CargarDGVPliegoLicitacionModalOferta(MidataTable);


        }

        protected void DgvPliegoItemsOferta_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvPliegoItemsOferta.EditIndex = -1;
            DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];
            CargarDGVPliegoLicitacionModalOferta(MidataTable);
        }

        protected void DgvPliegoItemsOferta_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            BE.ItemsOfertaBE itemsOfertaBE = new BE.ItemsOfertaBE() { Identidad = (Convert.ToInt64(((GridView)sender).DataKeys[e.RowIndex].Value)) };

            using (BLL.ItemsOfertaBLL bll = new BLL.ItemsOfertaBLL())
            {
                itemsOfertaBE = bll.Select(itemsOfertaBE);

                if (!(decimal.TryParse(e.NewValues[0].ToString(), out decimal asdasd123)))
                {
                    return;
                }

                itemsOfertaBE.PrecioUnitario = Convert.ToDecimal(e.NewValues[0]);

                BE.OfertaBE miofertaparaItem = new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) };

                bll.Update(itemsOfertaBE, miofertaparaItem);

                ((DataTable)ViewState["MiDTEdit"]).Rows[e.RowIndex]["PrecioUnitario"] = itemsOfertaBE.PrecioUnitario;

                using (BLL.OfertaBLL bll123 = new BLL.OfertaBLL())
                {
                    bll123.CalcularTarget(((DataTable)ViewState["MiDTEdit"]), bll123.Select(miofertaparaItem));

                    CargarPanelDatosOferta(bll123.Select(miofertaparaItem));

                }


                DgvPliegoItemsOferta.EditIndex = -1;
                DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];
                CargarDGVPliegoLicitacionModalOferta(MidataTable);

            }

        }

        protected void DgvPliegoItemsOferta_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
        }

        protected void btnPublicarOfertaLicitacion_Click(object sender, EventArgs e)
        {

            DataTable MidataTable = (DataTable)ViewState["MiDTEdit"];

            foreach (DataRow item in MidataTable.Rows)
            {
                if (Convert.ToDecimal(item["PrecioUnitario"].ToString()) == 0)
                {
                    lblErrorOfertarLicitacion.Text = "Verificar Precios";
                    divErrorOfertarLicitacion.Visible = true;
                    return;
                }
            }

            BE.OfertaBE miofertaparaItem = new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) };

            miofertaparaItem.FechaPresentacion = DateTime.Now.Date;

            miofertaparaItem.Estado_Oferta = BE.Estado_OfertaBE.Presentada;

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {
                bll.UpdateEstadoOferta(miofertaparaItem);

                NuevaBitacora((String.Format("Nueva Oferta de {0} para la licitacion {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social, Convert.ToInt64(ViewState["MiLicitacionEdit"]))), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

            }

            Response.Redirect(Request.RawUrl);
        }

        protected void btnCancelarOfertaLicitacion_Click(object sender, EventArgs e)
        {
            BE.OfertaBE miofertaparaItem = new BE.OfertaBE { Identidad = Convert.ToInt64(ViewState["MiOfertaEdit"]) };

            using (BLL.OfertaBLL bll = new BLL.OfertaBLL())
            {

                BE.OfertaBE ofertatmp = bll.Select(miofertaparaItem);

                ofertatmp.Estado_Oferta = BE.Estado_OfertaBE.Cancelada;

                bll.UpdateEstadoOferta(ofertatmp);

                NuevaBitacora((String.Format("Se Retiro la Oferta de {0} para la licitacion {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).Organizacion.Razon_Social, Convert.ToInt64(ViewState["MiLicitacionEdit"]))), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);

            }

            Response.Redirect(Request.RawUrl);

        }

        protected void DgvPliegoItems_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }
    }




}