﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true" CodeBehind="AdminRubrosForm.aspx.cs" Inherits="LicitarActual.AdminRubrosForm" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("DESCRIPCION") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptDescripcion" runat="server" ClientIDMode="Static" required="required" MaxLength="50" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptDescripcion" runat="server" ControlToValidate="iptDescripcion"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>

    <div class="form-group clearfix">
        <a href="AdminRubros.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" OnClick="btnGuardar_Click" Text='Guardar' />

    </div>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        function ShowCurrentTime() {
            waitingDialog.show();
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("AdminOrgForm.aspx/HelloWorld") %>',
                data: '',
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    waitingDialog.hide();
                }
            });
        }

    </script>

    
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>


</asp:Content>
