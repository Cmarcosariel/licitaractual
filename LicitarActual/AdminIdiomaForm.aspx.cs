﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LicitarActual
{
    public partial class AdminIdiomaForm : Cascara

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validarAcceso("ADM015");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            if (!IsPostBack)
            {
                Literal1.Text = Traducir("IDIOMAS");
                cargarData();
            }
        }

        public void cargarData()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                using (BLL.IdiomaBLL bll = new BLL.IdiomaBLL())
                {
                    List<BE.TraduccionBE> traduccionBEs = bll.ObtenerMensajesPorIdioma(new BE.IdiomaBE { Identidad = Convert.ToInt32(Request.QueryString["id"]) });

                    dgvAdminIdioma.DataSource = traduccionBEs;
                    dgvAdminIdioma.AutoGenerateColumns = false;
                    dgvAdminIdioma.DataBind();


                    if (dgvAdminIdioma.Columns.Count > 0)
                        dgvAdminIdioma.Columns[1].Visible = false;
                    else
                    {
                        dgvAdminIdioma.HeaderRow.Cells[1].Visible = false;
                        foreach (GridViewRow gvr in dgvAdminIdioma.Rows)
                        {
                            gvr.Cells[1].Visible = false;
                        }
                    }


                    if (traduccionBEs != null)
                    {
                        txtCantRegistros.Text = traduccionBEs.Count.ToString();
                    }
                    else
                    {
                        txtCantRegistros.Text = "0";
                    }
                }


            }
        }

        protected void GridView1_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            validarAcceso("ADM015");
            //NewEditIndex property used to determine the index of the row being edited.  
            dgvAdminIdioma.EditIndex = e.NewEditIndex;
            cargarData();
        }
        protected void GridView1_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            validarAcceso("ADM015");
            //Finding the controls from Gridview for the row which is going to update  
            Label clave = dgvAdminIdioma.Rows[e.RowIndex].FindControl("lbl_Clave") as Label;
            Label id = dgvAdminIdioma.Rows[e.RowIndex].FindControl("lbl_id") as Label;
            TextBox name = dgvAdminIdioma.Rows[e.RowIndex].FindControl("txt_Valor") as TextBox;
            String claveIdioma; 

            using (BLL.IdiomaBLL bll = new BLL.IdiomaBLL())
            {

                 claveIdioma = bll.Select(new BE.IdiomaBE() { Identidad = Int64.Parse(Request.QueryString["id"]) } ).Descripcion ;

                BE.TraduccionBE Trad = new BE.TraduccionBE() { Identidad = Int64.Parse(id.Text), Traduccion = name.Text, ChangeDate = DateTime.Now.Ticks};

                bll.UpdateTraduccion(Trad);

                NuevaBitacora((String.Format("el usuario {0} Actualizo la Traduccion con ID {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, Trad.Identidad)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);
            }

            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            dgvAdminIdioma.EditIndex = -1;

            IList<BE.TraduccionBE> traducciones = (List<BE.TraduccionBE>)Application[claveIdioma];

            traducciones.FirstOrDefault(x => x.Mensaje.Descripcion.Equals(clave.Text)).Traduccion = name.Text;

            //Call ShowData method for displaying updated data  
            cargarData();
        }
        protected void GridView1_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            dgvAdminIdioma.EditIndex = -1;
            cargarData();
        }

        protected void dgvAdminIdioma_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvAdminIdioma.PageIndex = e.NewPageIndex;
            cargarData();
        }

        protected void dgvAdminIdioma_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

    }
}