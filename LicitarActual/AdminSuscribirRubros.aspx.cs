﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace LicitarActual
{
    public partial class AdminSuscribirRubros : Cascara
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal2.Text = Traducir("NUEVO");
            btnBuscar.Text = Traducir("BUSCAR");
            litUsuarios.Text = Traducir("SUSCRIPCIONES");
            validarAcceso("NEG011");
            NuevaBitacora((String.Format("el usuario {0} navego a la pagina {1}", ((BE.UsuarioBE)Session["UsuarioLogueado"]).NombreUsuario, this.AppRelativeVirtualPath.ToString())), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);
            llenarRubros();
            Page.DataBind();
        }


        protected void DgvUsuarios_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            validarAcceso("NEG011");
            BLL.RubroBLL usuarioBll = new BLL.RubroBLL();
            BE.RubroBE usuarioBe = new BE.RubroBE();
            usuarioBe.Identidad = Int32.Parse(DgvUsuarios.DataKeys[e.RowIndex].Values[0].ToString());
            usuarioBll.Delete(usuarioBe);
            NuevaBitacora((String.Format("Se elimino la suscripcion Rubro {0}", usuarioBe.Descripcion)), (BE.UsuarioBE)Session["UsuarioLogueado"], "SEGURIDAD", 3);
            llenarRubros();
        }
        protected void llenarRubros()
        {
            BLL.RubroBLL RubroBLL = new BLL.RubroBLL();

            List<BE.RubroBE> rubros = RubroBLL.SelectAll();

            DgvUsuarios.AutoGenerateColumns = false;
            DgvUsuarios.DataSource = rubros;

            if (rubros != null)
            {
                txtCantRegistros.Text = rubros.Count.ToString();
            }
            else
            {
                txtCantRegistros.Text = "0";
            }



            DgvUsuarios.DataBind();
        }


        protected void DgvUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            validarAcceso("NEG011");
            int id = Int32.Parse(DgvUsuarios.DataKeys[e.NewEditIndex].Values[0].ToString());

            Response.Redirect("/AdminRubrosForm.aspx?id=" + id);
        }
        protected void DgvUsuarios_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void DgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DgvUsuarios.PageIndex = e.NewPageIndex;
            llenarRubros();
        }
        protected void DgvUsuarios_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DgvUsuarios.EditIndex = -1;
            llenarRubros();
        }

        public IDictionary<String, String> getFiltros()
        {
            IDictionary<String, String> filtros = new Dictionary<String, String>();


            if (!String.IsNullOrEmpty(filtroUsername.Text))
            {
                string str = filtroUsername.Text;

                StringBuilder strB = new StringBuilder(str);

                for (int i = 0; i < strB.Length; i++)
                {
                    if (strB[i] == '*')
                    {
                        strB[i] = '%';
                    }
                }


                filtros.Add("@Username", strB.ToString());
            }

            return filtros;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (getFiltros().Values != null)
            {
                BLL.RubroBLL orgbll = new BLL.RubroBLL();
                List<BE.RubroBE> usuarios = orgbll.Select(getFiltros());
                DgvUsuarios.AutoGenerateColumns = false;
                DgvUsuarios.DataSource = usuarios;
                if (usuarios != null)
                {
                    txtCantRegistros.Text = usuarios.Count.ToString();
                }
                else
                {
                    txtCantRegistros.Text = "0";
                }
                DgvUsuarios.DataBind();
            }
        }

        protected void DgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                BE.UsuarioBE usuariotmp = (BE.UsuarioBE)Session["UsuarioLogueado"];

                bool mibool = usuariotmp.Organizacion.RubrosSuscriptos.Any(x => x.Identidad == ((BE.RubroBE)e.Row.DataItem).Identidad);

                System.Web.UI.HtmlControls.HtmlInputRadioButton suscribir = (System.Web.UI.HtmlControls.HtmlInputRadioButton)e.Row.Cells[2].Controls[1];

                suscribir.Checked = mibool;

                System.Web.UI.HtmlControls.HtmlInputRadioButton desuscribir = (System.Web.UI.HtmlControls.HtmlInputRadioButton)e.Row.Cells[2].Controls[3];
                desuscribir.Checked = !mibool;

            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Traducir(e.Row.Cells[i].Text);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            llenarRubros();
            this.exportarExcel(sender, e, DgvUsuarios, 5, 6, Traducir("SUSCRIPCIONES"));
        }

        protected void btnExportarPDF_Click(object sender, EventArgs e)
        {
            this.exportarPDF(sender, e, DgvUsuarios, 5, 6, Traducir("SUSCRIPCIONES"));
        }


        protected void Button1_Click(object sender, EventArgs e)
        {

        }


        protected void radiosuscribir_CheckedChanged(object sender, EventArgs e)
        {
            bool asd = true;
        }

        protected void btnSuscribir_Click(object sender, EventArgs e)
        {
            string blabla = hdnRubro.Value;

            using (BLL.RubroBLL rub1 = new BLL.RubroBLL())
            {
                BE.UsuarioBE usuariotmp1 = (BE.UsuarioBE)Session["UsuarioLogueado"];

                if (usuariotmp1.Organizacion.RubrosSuscriptos.Any(x => x.Descripcion == (rub1.Select(new BE.RubroBE { Identidad = Convert.ToInt64(blabla) })).Descripcion))
                {
                    return;
                }
            }

            using (BLL.OrganizacionBLL bll = new BLL.OrganizacionBLL())
            {
                BE.UsuarioBE usuariotmp = (BE.UsuarioBE)Session["UsuarioLogueado"];

                bll.NuevaSuscripcion(usuariotmp.Organizacion, new BE.RubroBE { Identidad = Convert.ToInt64(blabla) });

                NuevaBitacora((String.Format("{0} Suscribio el Rubro {0}", usuariotmp.Organizacion.Descripcion, blabla)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 1);


                using (BLL.RubroBLL rub = new BLL.RubroBLL())
                {
                    usuariotmp.Organizacion.RubrosSuscriptos.Add(rub.Select(new BE.RubroBE { Identidad = Convert.ToInt64(blabla) }));
                    Session["UsuarioLogueado"] = usuariotmp;
                }

            }

            llenarRubros();

        }

        protected void btnDesuscribir_Click(object sender, EventArgs e)
        {
            string blabla = hdnRubro.Value;

            using (BLL.OrganizacionBLL bll = new BLL.OrganizacionBLL())
            {
                BE.UsuarioBE usuariotmp = (BE.UsuarioBE)Session["UsuarioLogueado"];

                bll.BajaSuscripcion(usuariotmp.Organizacion, new BE.RubroBE { Identidad = Convert.ToInt64(blabla) });

                NuevaBitacora((String.Format("{0} Desuscribio el Rubro {0}", usuariotmp.Organizacion.Descripcion, blabla)), (BE.UsuarioBE)Session["UsuarioLogueado"], "DIALOGO", 2);

                using (BLL.RubroBLL rub = new BLL.RubroBLL())
                {
                    usuariotmp.Organizacion.RubrosSuscriptos.RemoveAll(x => x.Identidad == Convert.ToInt64(blabla));
                    Session["UsuarioLogueado"] = usuariotmp;
                }

            }

            llenarRubros();

        }
    }
}