﻿<%@ Page Language="C#" MasterPageFile="~/Backend.Master" AutoEventWireup="true"
    CodeBehind="AdminUsuarioForm.aspx.cs" Inherits="LicitarActual.AdminUsuarioForm" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <br />
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("USUARIO") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptUsername" runat="server" ClientIDMode="Static" required="required" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptUsername" runat="server" ControlToValidate="iptUsername"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label><%= Traducir("PASSWORD") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptPassword" runat="server" ClientIDMode="Static" required="required" MaxLength="10" CssClass="form-control" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptPassword" runat="server" ControlToValidate="iptPassword"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPwdGuardada" Visible="false" Enabled="false" CssClass="form-control" ReadOnly="true" value="**********" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("NOMBRE") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptNombre" runat="server" MaxLength="20" ClientIDMode="Static" required="required" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptNombre" runat="server" ControlToValidate="iptNombre"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label><%= Traducir("APELLIDO") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptApellido" runat="server" MaxLength="30" ClientIDMode="Static" required="required" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptApellido" runat="server" ControlToValidate="iptApellido"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("ACTIVO") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:DropDownList ID="ddBloqueado" runat="server" CssClass="form-control">
                    <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                    <asp:ListItem Selected="True" Value="0" Text="No"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class='col-md-1'>
                <label>C.I.I.:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptCII" runat="server" MaxLength="50" CssClass="form-control" ReadOnly="true" Enabled="false" TextMode="Number"></asp:TextBox>
                <p class="help-block"><%= Traducir("CII") %></p>
            </div>
        </div>
    </div>

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label><%= Traducir("EMPRESA") %>:</label>
            </div>
            <div class='col-md-5'>
                <asp:DropDownList ID="ddEmpresas" CssClass="form-control" runat="server" ValidationGroup="vgddEmpresas">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddEmpresas" runat="server" ControlToValidate="ddEmpresas" ValidationGroup="vgddEmpresas"></asp:RequiredFieldValidator>

            </div>

        </div>
    </div>

    <asp:PlaceHolder ID="asignarGruposPermisos" Visible="true" runat="server">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12"><span class="lead"><%= Traducir("ASIGNAR_GRUPO") %></span></div>
                <div class="col-md-10">
                    <asp:DropDownList ID="ddGrupos" CssClass="form-control" runat="server" ValidationGroup="vgAgregarGrupo" />
                    <asp:RequiredFieldValidator ID="rfv_ddGrupos" runat="server" ControlToValidate="ddGrupos" ValidationGroup="vgAgregarGrupo"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                    <asp:Button ID="btnAgregarGrupo" runat="server" Text="+" OnClick="btnAgregarGrupo_Click" CssClass="btn btn-sm btn-warning" ValidationGroup="vgAgregarGrupo" />
                </div>
                <div class="col-md-12">

                    <asp:GridView ID="gvGrupos" runat="server" CssClass="table table-hover table-bordered"
                        OnRowDeleting="gvGrupos_RowDeleting"
                        OnRowDataBound="gvGrupos_RowDataBound"
                        BorderStyle="None" ItemType="Grupo" AutoGenerateColumns="False" BorderWidth="0">
                        <Columns>
                            <asp:BoundField DataField="Codigo_Perfil" HeaderText="CODIGO" ReadOnly="true" SortExpression="id" ControlStyle-BorderStyle="None" />
                            <asp:BoundField DataField="descripcion" HeaderText="NOMBRE" ReadOnly="true" SortExpression="id" ControlStyle-BorderStyle="None" />
                            <asp:CommandField ShowDeleteButton="true" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="ELIMINAR" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12"><span class="lead"><%= Traducir("ASIGNAR_PERMISO") %></span></div>
                <div class="col-md-10">
                    <asp:DropDownList ID="ddPermisos" CssClass="form-control" runat="server" ValidationGroup="vgAgregarPermiso" />
                    <asp:RequiredFieldValidator ID="rfv_ddPermisos" runat="server" ControlToValidate="ddPermisos" ValidationGroup="vgAgregarPermiso"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                    <asp:Button ID="btnAgregarPermiso" runat="server" Text="+" OnClick="btnAgregarPermiso_Click" CssClass="btn btn-sm btn-warning" ValidationGroup="vgAgregarPermiso" />

                    <%--<input id="btnAgregarPermiso" type="button" class="btn btn-sm btn-warning" onclick="ShowCurrentTime()" value="+" />--%>
                </div>
                <div class="col-md-12">

                    <asp:GridView ID="gvPermisos" runat="server" CssClass="table table-hover table-bordered"
                        OnRowDeleting="gvPermisos_RowDeleting"
                        BorderStyle="None" ItemType="Grupo" AutoGenerateColumns="False" BorderWidth="0"
                        OnRowDataBound="gvPermisos_RowDataBound"
                        EmptyDataText="No se encontraron Permisos">
                        <Columns>
                            <asp:BoundField DataField="Codigo_Perfil" HeaderText="CODIGO" ReadOnly="true" SortExpression="id" ControlStyle-BorderStyle="None" />
                            <asp:BoundField DataField="descripcion" HeaderText="NOMBRE" ReadOnly="true" SortExpression="id" ControlStyle-BorderStyle="None" />
                            <%--<asp:BoundField DataField="permisoHeredado" HeaderText="Heredado" ReadOnly="true" SortExpression="id" ControlStyle-BorderStyle="None" />--%>
                            <asp:CommandField ShowDeleteButton="true" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="ELIMINAR" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>


    </asp:PlaceHolder>

    <div class="form-group clearfix">
        <a href="AdminUsuarios.aspx" class="btn btn-info pull-left"><%= Traducir("VOLVER") %></a>
        &nbsp;
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" OnClick="btnGuardar_Click" Text='Guardar' />
        <%--<button ID="btnGuardar" class="btn btn-success" onclick="ShowCurrentTime()">Guardar!</button>--%>
        <%--<input id="btnGetTime" type="button" class="btn btn-success" value="Show Current Time" onclick = "ShowCurrentTime()" />--%>
    </div>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">LicitAR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="body">
                    <asp:Label ID="lblMessage" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        function DesactivarRFV_Grupos2() {
            ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", false));
            ValidatorEnable(document.getElementById("<%=rfv_ddPermisos.ClientID %>", true));
            ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", false));
            return true;
        }

        function DesactivarRFV_Grupos() {

            if (document.getElementById('<%= ddGrupos.ClientID %>').value == null && document.getElementById('<%= ddPermisos.ClientID %>').value == null) {
                ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", true));
                ValidatorEnable(document.getElementById("<%=rfv_ddPermisos.ClientID %>", true));
                return false;
            }
            else {
                ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", false));
                ValidatorEnable(document.getElementById("<%=rfv_ddPermisos.ClientID %>", false));

                return true;
            }
        }


        function DesactivarRFV_Permiso2() {

            ValidatorEnable(document.getElementById("<%=rfv_ddPermisos.ClientID %>", false));
            ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", true));
            return true;
        }

        function DesactivarRFV_Permisos() {

            if (document.getElementById('<%= ddGrupos.ClientID %>').value == null && document.getElementById('<%= ddPermisos.ClientID %>').value == null) {
                ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", true));
                ValidatorEnable(document.getElementById("<%=rfv_ddPermisos.ClientID %>", true));
                return false;
            }
            else {
                ValidatorEnable(document.getElementById("<%=rfv_ddGrupos.ClientID %>", false));
                ValidatorEnable(document.getElementById("<%=rfv_ddPermisos.ClientID %>", false));
                return true;
            }


        }

        function ShowCurrentTime() {
            waitingDialog.show();
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("AdminUsuarioForm.aspx/HelloWorld") %>',
                data: '',
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    waitingDialog.hide();
                }
            });
        }
    </script>

</asp:Content>
