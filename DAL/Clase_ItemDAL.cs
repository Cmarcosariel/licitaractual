﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class Clase_ItemDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public Clase_ItemDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Clase_Item table.
        /// </summary>
        public void Insert(Clase_ItemBE clase_Item)
        {
            ValidationUtility.ValidateArgument("clase_Item", clase_Item);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(clase_Item.Propiedades);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", clase_Item.Descripcion)
            };

            SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "Clase_ItemInsert", parameters);
        }

        public void Insert(Clase_ItemBE objeto, RubroBE rubro)
        {
            ValidationUtility.ValidateArgument("clase_Item", objeto);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(objeto.Propiedades);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", objeto.Descripcion),
                new SqlParameter("@ID_Rubro", rubro.Identidad),
                new SqlParameter("@JsonPropiedades", jsonString)
            };

            SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "Clase_ItemInsert", parameters);
        }

        public void Update(Clase_ItemBE objeto, RubroBE rubro)
        {
            ValidationUtility.ValidateArgument("clase_Item", objeto);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(objeto.Propiedades);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", objeto.Identidad),
                new SqlParameter("@Descripcion", objeto.Descripcion),
                new SqlParameter("@ID_Rubro", rubro.Identidad),
                new SqlParameter("@JsonPropiedades", jsonString)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Clase_ItemUpdateConRubro", parameters);
        }

        public Dictionary<Clase_ItemBE, string> SP_reporteCatxCant()
        {

            Dictionary<Clase_ItemBE, string> midiccionario = new Dictionary<Clase_ItemBE, string>();

            try
            {


                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("SP_reporteCatxCant");
                if (dt.Rows.Count == 0)
                {
                    midiccionario = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        Clase_ItemBE rubroBE = new Clase_ItemBE();

                        rubroBE.Identidad = Convert.ToInt64(row["Identidad"]);

                        rubroBE = Select(rubroBE);

                        midiccionario.Add(rubroBE, row["Cantidad"].ToString());
                    }

                }

                return midiccionario;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Dictionary<Clase_ItemBE, string> SP_reporteCatxCantFiltrarRubro(RubroBE rubroBE_filtrar)
        {
            Dictionary<Clase_ItemBE, string> midiccionario = new Dictionary<Clase_ItemBE, string>();

            try
            {

                SqlParameter[] parameters = new SqlParameter[]
                {
                 new SqlParameter("@ID_Rubro", rubroBE_filtrar.Identidad)
                };

                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("SP_reporteCatxCantFiltrarRubro",parameters);

                if (dt.Rows.Count == 0)
                {
                    midiccionario = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        Clase_ItemBE rubroBE = new Clase_ItemBE();

                        rubroBE.Identidad = Convert.ToInt64(row["Identidad"]);

                        rubroBE = Select(rubroBE);

                        midiccionario.Add(rubroBE, row["Cantidad"].ToString());
                    }

                }

                return midiccionario;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Clase_ItemBE GetClaseforItem(ItemBE item)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", item.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetClaseforItem", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public Dictionary<string, string> GetRubrosStringForClases()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetRubrosStringForClases"))
            {
                Dictionary<string, string> eldiccionario = new Dictionary<string, string>();
                while (dataReader.Read())
                {

                    eldiccionario.Add(dataReader.GetString(0), dataReader.GetString(1));
                }

                return eldiccionario;
            }
        }

        public List<Clase_ItemBE> SelectAllByID_Rubro(RubroBE rubroBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", rubroBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelectAllByID_Rubro", parameters))
            {
                List<Clase_ItemBE> clase_ItemBEList = new List<Clase_ItemBE>();
                while (dataReader.Read())
                {
                    Clase_ItemBE clase_ItemBE = MapDataReader(dataReader);
                    clase_ItemBEList.Add(clase_ItemBE);
                }

                return clase_ItemBEList;
            }
        }

        public Clase_ItemBE Select(Clase_ItemBE org, RubroBE rub)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", org.Descripcion),
                new SqlParameter("@ID_Rubro", rub.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelectbyDescYRubro", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public List<Clase_ItemBE> Select(IDictionary<string, string> dictionary)
        {
            BE.Clase_ItemBE orgdb = null;
            List<BE.Clase_ItemBE> Empresas = new List<BE.Clase_ItemBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarClaseItemFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        Clase_ItemBE rubroBE = new Clase_ItemBE();
                        rubroBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        rubroBE.Descripcion = row["Descripcion"].ToString();

                        Empresas.Add(rubroBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        internal List<Clase_ItemBE> SelectAllByID_RubroConItems(RubroBE rubroBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
                  {
                new SqlParameter("@ID_Rubro", rubroBE.Identidad)
                  };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelectAllByID_Rubro", parameters))
            {
                List<Clase_ItemBE> clase_ItemBEList = new List<Clase_ItemBE>();
                while (dataReader.Read())
                {
                    Clase_ItemBE clase_ItemBE = MapDataReaderConItems(dataReader);
                    clase_ItemBEList.Add(clase_ItemBE);
                }

                return clase_ItemBEList;
            }
        }


        /// <summary>
        /// Updates a record in the Clase_Item table.
        /// </summary>
        public void Update(Clase_ItemBE clase_Item)
        {
            ValidationUtility.ValidateArgument("clase_Item", clase_Item);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(clase_Item.Propiedades);


            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", clase_Item.Identidad),
                new SqlParameter("@Descripcion", clase_Item.Descripcion),
                new SqlParameter("@JsonPropiedades", jsonString)

            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Clase_ItemUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Clase_Item table by its primary key.
        /// </summary>
        public void Delete(BE.Clase_ItemBE clase_ItemBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", clase_ItemBE.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Clase_ItemDeleteLogico", parameters);
        }

        /// <summary>
        /// Selects a single record from the Clase_Item table.
        /// </summary>
        public Clase_ItemBE Select(BE.Clase_ItemBE clase_ItemBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", clase_ItemBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Clase_Item table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Clase_Item table.
        /// </summary>
        public List<Clase_ItemBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelectAll"))
            {
                List<Clase_ItemBE> clase_ItemBEList = new List<Clase_ItemBE>();
                while (dataReader.Read())
                {
                    Clase_ItemBE clase_ItemBE = MapDataReader(dataReader);
                    clase_ItemBEList.Add(clase_ItemBE);
                }

                return clase_ItemBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Clase_Item table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Clase_ItemSelectAll");
        //}

        /// <summary>
        /// Creates a new instance of the Clase_ItemBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private Clase_ItemBE MapDataReader(SqlDataReader dataReader)
        {
            Clase_ItemBE clase_ItemBE = new Clase_ItemBE();
            clase_ItemBE.Identidad = dataReader.GetInt64("Identidad", 0);
            clase_ItemBE.Descripcion = dataReader.GetString("Descripcion", null);
            string mijsonprops = dataReader.GetString("Propiedades", null);

            if (mijsonprops != null)
            {
                clase_ItemBE.Propiedades = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BE.PropiedadItemBE>>(mijsonprops);
            }

            return clase_ItemBE;
        }


        private Clase_ItemBE MapDataReaderConItems(SqlDataReader dataReader)
        {
            Clase_ItemBE clase_ItemBE = new Clase_ItemBE();
            clase_ItemBE.Identidad = dataReader.GetInt64("Identidad", 0);
            clase_ItemBE.Descripcion = dataReader.GetString("Descripcion", null);
            string mijsonprops = dataReader.GetString("Propiedades", null);

            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                clase_ItemBE.Items = dao.SelectAllByID_Clase(clase_ItemBE);
            }


            if (mijsonprops != null)
            {
                clase_ItemBE.Propiedades = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BE.PropiedadItemBE>>(mijsonprops);
            }

            return clase_ItemBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
