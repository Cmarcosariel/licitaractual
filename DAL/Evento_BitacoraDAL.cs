﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class Evento_BitacoraDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public Evento_BitacoraDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Evento_Bitacora table.
        /// </summary>
        public void Insert(Evento_BitacoraBE evento_Bitacora)
        {
            ValidationUtility.ValidateArgument("evento_Bitacora", evento_Bitacora);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", evento_Bitacora.Descripcion),
                new SqlParameter("@Severidad", evento_Bitacora.Severidad)
            };

            evento_Bitacora.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the Evento_Bitacora table.
        /// </summary>
        public void Update(Evento_BitacoraBE evento_Bitacora)
        {
            ValidationUtility.ValidateArgument("evento_Bitacora", evento_Bitacora);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", evento_Bitacora.Identidad),
                new SqlParameter("@Descripcion", evento_Bitacora.Descripcion),
                new SqlParameter("@Severidad", evento_Bitacora.Severidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Evento_Bitacora table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraDelete", parameters);
        }

        public List<string> GetCategoriasEventoBitacora()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetCategoriasEventoBitacora"))
            {
                List<string> evento_BitacoraBEList = new List<string>();
                while (dataReader.Read())
                {
                    string evento_BitacoraBE = dataReader.GetString("Descripcion", null);
                    evento_BitacoraBEList.Add(evento_BitacoraBE);
                }

                return evento_BitacoraBEList;
            }
        }

        /// <summary>
        /// Selects a single record from the Evento_Bitacora table.
        /// </summary>
        public Evento_BitacoraBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public Evento_BitacoraBE GetSeveridadEvento(BE.Evento_BitacoraBE evento)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Desc", evento.Descripcion),
                new SqlParameter("@Severidad", evento.Severidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetSeveridadEvento", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Selects a single record from the Evento_Bitacora table.
        ///// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Evento_Bitacora table.
        /// </summary>
        public List<Evento_BitacoraBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraSelectAll"))
            {
                List<Evento_BitacoraBE> evento_BitacoraBEList = new List<Evento_BitacoraBE>();
                while (dataReader.Read())
                {
                    Evento_BitacoraBE evento_BitacoraBE = MapDataReader(dataReader);
                    evento_BitacoraBEList.Add(evento_BitacoraBE);
                }

                return evento_BitacoraBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Evento_Bitacora table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Evento_BitacoraSelectAll");
        //}

        /// <summary>
        /// Creates a new instance of the Evento_BitacoraBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private Evento_BitacoraBE MapDataReader(SqlDataReader dataReader)
        {
            Evento_BitacoraBE evento_BitacoraBE = new Evento_BitacoraBE();
            evento_BitacoraBE.Identidad = dataReader.GetInt64("Identidad", 0);
            evento_BitacoraBE.Descripcion = dataReader.GetString("Descripcion", null);
            evento_BitacoraBE.Severidad = dataReader.GetInt32("Severidad", 0);

            return evento_BitacoraBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
