﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
    public class ItemsLicitacionDAL
    {
        //#region Fields

        //private string connectionStringName;

        //#endregion

        //#region Constructors

        //public ItemsLicitacionDAL(string connectionStringName)
        //{
        //    ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

        //    this.connectionStringName = connectionStringName;
        //}

        //#endregion

        //#region Methods

        ///// <summary>
        ///// Saves a record to the ItemsLicitacion table.
        ///// </summary>
        //public void Insert(ItemsLicitacionBE itemsLicitacion, LicitacionBE objeto)
        //{
        //    ValidationUtility.ValidateArgument("itemsLicitacion", itemsLicitacion);

        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Licitacion", objeto.Identidad),
        //        new SqlParameter("@ID_Item", itemsLicitacion.Item.Identidad),
        //        //new SqlParameter("@PrecioUnitario", itemsLicitacion.PrecioUnitario),
        //        new SqlParameter("@Cantidad", itemsLicitacion.Cantidad)
        //    };

        //    SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionInsert", parameters);
        //}

        ///// <summary>
        ///// Updates a record in the ItemsLicitacion table.
        ///// </summary>
        //public void Update(ItemsLicitacionBE itemsLicitacion)
        //{
        //    ValidationUtility.ValidateArgument("itemsLicitacion", itemsLicitacion);

        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", itemsLicitacion.Identidad),
        //        new SqlParameter("@ID_Licitacion", itemsLicitacion.Licitacion.Identidad),
        //        new SqlParameter("@ID_Item", itemsLicitacion.Item.Identidad),
        //        //new SqlParameter("@PrecioUnitario", itemsLicitacion.PrecioUnitario),
        //        new SqlParameter("@Cantidad", itemsLicitacion.Cantidad)
        //    };

        //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionUpdate", parameters);
        //}

        ///// <summary>
        ///// Deletes a record from the ItemsLicitacion table by its primary key.
        ///// </summary>
        //public void Delete(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionDelete", parameters);
        //}

        ///// <summary>
        ///// Deletes a record from the ItemsLicitacion table by a foreign key.
        ///// </summary>
        //public void DeleteAllByID_Item(long iD_Item)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Item", iD_Item)
        //    };

        //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionDeleteAllByID_Item", parameters);
        //}

        ///// <summary>
        ///// Deletes a record from the ItemsLicitacion table by a foreign key.
        ///// </summary>
        //public void DeleteAllByID_Licitacion(long iD_Licitacion)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Licitacion", iD_Licitacion)
        //    };

        //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionDeleteAllByID_Licitacion", parameters);
        //}

        ///// <summary>
        ///// Selects a single record from the ItemsLicitacion table.
        ///// </summary>
        //public ItemsLicitacionBE Select(BE.ItemsLicitacionBE itemsLicitacionBE)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", itemsLicitacionBE.Identidad)
        //    };

        //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelect", parameters))
        //    {
        //        if (dataReader.Read())
        //        {
        //            return MapDataReader(dataReader);
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Selects a single record from the ItemsLicitacion table.
        ///// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelect", parameters);
        //}

        ///// <summary>
        ///// Selects all records from the ItemsLicitacion table.
        ///// </summary>
        //public List<ItemsLicitacionBE> SelectAll()
        //{
        //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelectAll"))
        //    {
        //        List<ItemsLicitacionBE> itemsLicitacionBEList = new List<ItemsLicitacionBE>();
        //        while (dataReader.Read())
        //        {
        //            ItemsLicitacionBE itemsLicitacionBE = MapDataReader(dataReader);
        //            itemsLicitacionBEList.Add(itemsLicitacionBE);
        //        }

        //        return itemsLicitacionBEList;
        //    }
        //}

        ///// <summary>
        ///// Selects all records from the ItemsLicitacion table.
        ///// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelectAll");
        //}

        ///// <summary>
        ///// Selects all records from the ItemsLicitacion table by a foreign key.
        ///// </summary>
        //public List<ItemsLicitacionBE> SelectAllByID_Item(long iD_Item)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Item", iD_Item)
        //    };

        //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelectAllByID_Item", parameters))
        //    {
        //        List<ItemsLicitacionBE> itemsLicitacionBEList = new List<ItemsLicitacionBE>();
        //        while (dataReader.Read())
        //        {
        //            ItemsLicitacionBE itemsLicitacionBE = MapDataReader(dataReader);
        //            itemsLicitacionBEList.Add(itemsLicitacionBE);
        //        }

        //        return itemsLicitacionBEList;
        //    }
        //}

        ///// <summary>
        ///// Selects all records from the ItemsLicitacion table by a foreign key.
        ///// </summary>
        //public List<ItemsLicitacionBE> SelectAllByID_Licitacion(long iD_Licitacion)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Licitacion", iD_Licitacion)
        //    };

        //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelectAllByID_Licitacion", parameters))
        //    {
        //        List<ItemsLicitacionBE> itemsLicitacionBEList = new List<ItemsLicitacionBE>();
        //        while (dataReader.Read())
        //        {
        //            ItemsLicitacionBE itemsLicitacionBE = MapDataReader(dataReader);
        //            itemsLicitacionBEList.Add(itemsLicitacionBE);
        //        }

        //        return itemsLicitacionBEList;
        //    }
        //}

        ///// <summary>
        ///// Selects all records from the ItemsLicitacion table by a foreign key.
        ///// </summary>
        //public string SelectAllByID_ItemJson(long iD_Item)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Item", iD_Item)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelectAllByID_Item", parameters);
        //}

        ///// <summary>
        ///// Selects all records from the ItemsLicitacion table by a foreign key.
        ///// </summary>
        //public string SelectAllByID_LicitacionJson(long iD_Licitacion)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Licitacion", iD_Licitacion)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsLicitacionSelectAllByID_Licitacion", parameters);
        //}

        ///// <summary>
        ///// Creates a new instance of the ItemsLicitacionBE class and populates it with data from the specified SqlDataReader.
        ///// </summary>
        //private ItemsLicitacionBE MapDataReader(SqlDataReader dataReader)
        //{
        //    ItemsLicitacionBE itemsLicitacionBE = new ItemsLicitacionBE();
        //    itemsLicitacionBE.Identidad = dataReader.GetInt64("Identidad", 0);

        //    using (LicitacionDAL dao = new LicitacionDAL("PRD"))
        //    {
        //        itemsLicitacionBE.Licitacion = dao.Select(new BE.LicitacionBE { Identidad = dataReader.GetInt64("ID_Licitacion", 0) });
        //    }

        //    using (ItemDAL dao = new ItemDAL("PRD"))
        //    {
        //        itemsLicitacionBE.Item = dao.Select(new BE.ItemBE { Identidad = dataReader.GetInt64("ID_Item", 0) });
        //    }

        //    //itemsLicitacionBE.PrecioUnitario = dataReader.GetDecimal("PrecioUnitario", Decimal.Zero);
        //    itemsLicitacionBE.Cantidad = dataReader.GetDecimal("Cantidad", Decimal.Zero);

        //    return itemsLicitacionBE;
        //}

        //public void Dispose()
        //{
        //}

        //#endregion
    }
}
