﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class LicitacionDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public LicitacionDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Licitacion table.
        /// </summary>
        public void Insert(LicitacionBE licitacion)
        {
            ValidationUtility.ValidateArgument("licitacion", licitacion);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(licitacion.Pliego);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Estado_Licitacion", licitacion.Estado_Licitacion),
                new SqlParameter("@Descripcion", licitacion.Descripcion),
                new SqlParameter("@ID_Rubro", licitacion.Rubro.Identidad),
                new SqlParameter("@FechaApertura", licitacion.FechaApertura),
                new SqlParameter("@FechaCierre", licitacion.FechaCierre),
                new SqlParameter("@Observaciones", licitacion.Observaciones),
                new SqlParameter("@Pliego", jsonString),
                new SqlParameter("@ID_Organizacion", licitacion.Organizacion.Identidad)
            };

            SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "LicitacionInsert", parameters);
        }

        public List<LicitacionBE> Select(IDictionary<string, string> dictionary)
        {
            BE.LicitacionBE orgdb = null;
            List<BE.LicitacionBE> Empresas = new List<BE.LicitacionBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarLicitacionFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        LicitacionBE licitacionBE = new LicitacionBE();
                        licitacionBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        licitacionBE.Descripcion = row["Descripcion"].ToString();

                        licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)Convert.ToInt64(row["ID_Estado_Licitacion"]);

                        licitacionBE.Descripcion = row["Descripcion"].ToString();

                        using (RubroDAL dao = new RubroDAL("PRD"))
                        {
                            licitacionBE.Rubro = dao.Select(new RubroBE { Identidad = Convert.ToInt64(row["ID_Rubro"]) });
                        }

                        licitacionBE.FechaApertura = Convert.ToDateTime(row["FechaApertura"]);
                        licitacionBE.FechaCierre = Convert.ToDateTime(row["FechaCierre"]);
                        licitacionBE.Observaciones = row["Observaciones"].ToString();
                        string jsonPliego = row["Pliego"].ToString();

                        if (jsonPliego != null)
                        {
                            licitacionBE.Pliego = Newtonsoft.Json.JsonConvert.DeserializeObject<BE.PliegoBE>(jsonPliego);
                        }


                        using (OrganizacionDAL dao = new OrganizacionDAL("PRD"))
                        {
                            licitacionBE.Organizacion = dao.Select(new OrganizacionBE { Identidad = Convert.ToInt64(row["ID_Organizacion"]) });
                        }

                        Empresas.Add(licitacionBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void AceptarOferta(LicitacionBE licitacionBE, OfertaBE mioferta)
        {
            ValidationUtility.ValidateArgument("licitacion", licitacionBE);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", licitacionBE.Identidad),
                new SqlParameter("@ID_OfertaAceptada", mioferta.Identidad)

            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionAceptarOferta", parameters);
        }

        public List<LicitacionBE> SelectAllVigenteParaSuscripciones(OrganizacionBE organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", organizacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "SelectAllVigenteParaSuscripciones", parameters))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        public void RechazarOferta(LicitacionBE licitacionBE, OfertaBE mioferta)
        {
            ValidationUtility.ValidateArgument("licitacion", licitacionBE);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", licitacionBE.Identidad),
                new SqlParameter("@ID_OfertaRechazada", mioferta.Identidad)

            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionRechazarOferta", parameters);
        }


        public List<LicitacionBE> SelectAllVigenteConOfertaDeMiOrg(OrganizacionBE organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", organizacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllVigenteConOfertaDeMiOrg", parameters))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        public List<LicitacionBE> SelectActivosParaUsuario(IDictionary<string, string> dictionary, OrganizacionBE organizacion)
        {
            BE.LicitacionBE orgdb = null;
            List<BE.LicitacionBE> Empresas = new List<BE.LicitacionBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarLicitacionActivoFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        LicitacionBE licitacionBE = new LicitacionBE();
                        licitacionBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        licitacionBE.Descripcion = row["Descripcion"].ToString();

                        licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)Convert.ToInt64(row["ID_Estado_Licitacion"]);

                        licitacionBE.Descripcion = row["Descripcion"].ToString();

                        using (RubroDAL dao = new RubroDAL("PRD"))
                        {
                            licitacionBE.Rubro = dao.Select(new RubroBE { Identidad = Convert.ToInt64(row["ID_Rubro"]) });
                        }

                        licitacionBE.FechaApertura = Convert.ToDateTime(row["FechaApertura"]);
                        licitacionBE.FechaCierre = Convert.ToDateTime(row["FechaCierre"]);
                        licitacionBE.Observaciones = row["Observaciones"].ToString();
                        string jsonPliego = row["Pliego"].ToString();

                        if (jsonPliego != null)
                        {
                            licitacionBE.Pliego = Newtonsoft.Json.JsonConvert.DeserializeObject<BE.PliegoBE>(jsonPliego);
                        }


                        using (OrganizacionDAL dao = new OrganizacionDAL("PRD"))
                        {
                            licitacionBE.Organizacion = dao.Select(new OrganizacionBE { Identidad = Convert.ToInt64(row["ID_Organizacion"]) });
                        }

                        if (licitacionBE.Organizacion.Razon_Social != organizacion.Razon_Social)
                        {
                            Empresas.Add(licitacionBE);
                        }

                        //Empresas.Add(licitacionBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<LicitacionBE> SelectActivos(IDictionary<string, string> dictionary)
        {
            BE.LicitacionBE orgdb = null;
            List<BE.LicitacionBE> Empresas = new List<BE.LicitacionBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarLicitacionActivoFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        LicitacionBE licitacionBE = new LicitacionBE();
                        licitacionBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        licitacionBE.Descripcion = row["Descripcion"].ToString();

                        licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)Convert.ToInt64(row["ID_Estado_Licitacion"]);

                        licitacionBE.Descripcion = row["Descripcion"].ToString();

                        using (RubroDAL dao = new RubroDAL("PRD"))
                        {
                            licitacionBE.Rubro = dao.Select(new RubroBE { Identidad = Convert.ToInt64(row["ID_Rubro"]) });
                        }

                        licitacionBE.FechaApertura = Convert.ToDateTime(row["FechaApertura"]);
                        licitacionBE.FechaCierre = Convert.ToDateTime(row["FechaCierre"]);
                        licitacionBE.Observaciones = row["Observaciones"].ToString();
                        string jsonPliego = row["Pliego"].ToString();

                        if (jsonPliego != null)
                        {
                            licitacionBE.Pliego = Newtonsoft.Json.JsonConvert.DeserializeObject<BE.PliegoBE>(jsonPliego);
                        }


                        using (OrganizacionDAL dao = new OrganizacionDAL("PRD"))
                        {
                            licitacionBE.Organizacion = dao.Select(new OrganizacionBE { Identidad = Convert.ToInt64(row["ID_Organizacion"]) });
                        }

                        Empresas.Add(licitacionBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<LicitacionBE> SelectAllVigente(OrganizacionBE organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", organizacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllVigente",parameters))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        public LicitacionBE LicitacionSelectLastInsertbyOrgyRub(LicitacionBE licitacionBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
            new SqlParameter("@ID_Organizacion", licitacionBE.Organizacion.Identidad),
            new SqlParameter("@ID_Rubro", licitacionBE.Rubro.Identidad)
            };
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectLastInsertbyOrgyRub", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public void ActualizarEstado(LicitacionBE licitacionBE)
        {
            ValidationUtility.ValidateArgument("licitacion", licitacionBE);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", licitacionBE.Identidad),
                new SqlParameter("@ID_Estado_Licitacion", licitacionBE.Estado_Licitacion)

            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionUpdateEstadoLicitacion", parameters);
        }

        /// <summary>
        /// Updates a record in the Licitacion table.
        /// </summary>
        public void Update(LicitacionBE licitacion)
        {
            ValidationUtility.ValidateArgument("licitacion", licitacion);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(licitacion.Pliego);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", licitacion.Identidad),
                new SqlParameter("@ID_Estado_Licitacion", licitacion.Estado_Licitacion),
                new SqlParameter("@Descripcion", licitacion.Descripcion),
                new SqlParameter("@ID_Rubro", licitacion.Rubro.Identidad),
                new SqlParameter("@FechaApertura", licitacion.FechaApertura),
                new SqlParameter("@FechaCierre", licitacion.FechaCierre),
                new SqlParameter("@Observaciones", licitacion.Observaciones),
                new SqlParameter("@Pliego", jsonString),
                new SqlParameter("@ID_Organizacion", licitacion.Organizacion.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Licitacion table by its primary key.
        /// </summary>
        public void Delete(LicitacionBE licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", licitacion.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionDeleteLogico", parameters);
        }

        /// <summary>
        /// Deletes a record from the Licitacion table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Estado_Licitacion(long iD_Estado_Licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Estado_Licitacion", iD_Estado_Licitacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionDeleteAllByID_Estado_Licitacion", parameters);
        }

        /// <summary>
        /// Deletes a record from the Licitacion table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Organizacion(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionDeleteAllByID_Organizacion", parameters);
        }

        /// <summary>
        /// Deletes a record from the Licitacion table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Rubro(long iD_Rubro)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", iD_Rubro)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "LicitacionDeleteAllByID_Rubro", parameters);
        }

        /// <summary>
        /// Selects a single record from the Licitacion table.
        /// </summary>
        public LicitacionBE Select(LicitacionBE licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", licitacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// Selects a single record from the Licitacion table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "LicitacionSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Licitacion table.
        /// </summary>
        public List<LicitacionBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAll"))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Licitacion table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAll");
        //}

        /// <summary>
        /// Selects all records from the Licitacion table by a foreign key.
        /// </summary>
        public List<LicitacionBE> SelectAllByID_Estado_Licitacion(long iD_Estado_Licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Estado_Licitacion", iD_Estado_Licitacion)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllByID_Estado_Licitacion", parameters))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Licitacion table by a foreign key.
        /// </summary>
        public List<LicitacionBE> SelectAllByID_Organizacion(OrganizacionBE organizacionBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", organizacionBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllByID_Organizacion", parameters))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Licitacion table by a foreign key.
        /// </summary>
        public List<LicitacionBE> SelectAllByID_Rubro(RubroBE rubroBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", rubroBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllByID_Rubro", parameters))
            {
                List<LicitacionBE> licitacionBEList = new List<LicitacionBE>();
                while (dataReader.Read())
                {
                    LicitacionBE licitacionBE = MapDataReader(dataReader);
                    licitacionBEList.Add(licitacionBE);
                }

                return licitacionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Licitacion table by a foreign key.
        /// </summary>
        //public string SelectAllByID_Estado_LicitacionJson(long iD_Estado_Licitacion)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Estado_Licitacion", iD_Estado_Licitacion)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllByID_Estado_Licitacion", parameters);
        //}

        /// <summary>
        /// Selects all records from the Licitacion table by a foreign key.
        /// </summary>
        //public string SelectAllByID_OrganizacionJson(long iD_Organizacion)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Organizacion", iD_Organizacion)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllByID_Organizacion", parameters);
        //}

        /// <summary>
        /// Selects all records from the Licitacion table by a foreign key.
        /// </summary>
        public string SelectAllByID_RubroJson(long iD_Rubro)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", iD_Rubro)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "LicitacionSelectAllByID_Rubro", parameters);
        }

        /// <summary>
        /// Creates a new instance of the LicitacionBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private LicitacionBE MapDataReader(SqlDataReader dataReader)
        {
            LicitacionBE licitacionBE = new LicitacionBE();
            licitacionBE.Identidad = dataReader.GetInt64("Identidad", 0);
            licitacionBE.Estado_Licitacion = (EstadoLicitacionBE)dataReader.GetInt64("ID_Estado_Licitacion", 0);


            licitacionBE.Descripcion = dataReader.GetString("Descripcion", null);

            using (RubroDAL dao = new RubroDAL("PRD"))
            {
                licitacionBE.Rubro = dao.Select(new RubroBE { Identidad = dataReader.GetInt64("ID_Rubro", 0) });
            }

            licitacionBE.FechaApertura = dataReader.GetDateTime("FechaApertura", new DateTime(0));
            licitacionBE.FechaCierre = dataReader.GetDateTime("FechaCierre", new DateTime(0));
            licitacionBE.Observaciones = dataReader.GetString("Observaciones", null);
            string jsonPliego = dataReader.GetString("Pliego", null);

            if (jsonPliego != null)
            {
                licitacionBE.Pliego = Newtonsoft.Json.JsonConvert.DeserializeObject<BE.PliegoBE>(jsonPliego);
            }


            using (OrganizacionDAL dao = new OrganizacionDAL("PRD"))
            {
                licitacionBE.Organizacion = dao.Select(new OrganizacionBE { Identidad = dataReader.GetInt64("ID_Organizacion", 0) });
            }

            return licitacionBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
