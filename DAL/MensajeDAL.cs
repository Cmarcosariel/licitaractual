﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class MensajeDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public MensajeDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Mensaje table.
        /// </summary>
        public void Insert(MensajeBE mensaje)
        {
            ValidationUtility.ValidateArgument("mensaje", mensaje);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", mensaje.Descripcion)
            };

            mensaje.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "MensajeInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the Mensaje table.
        /// </summary>
        public void Update(MensajeBE mensaje)
        {
            ValidationUtility.ValidateArgument("mensaje", mensaje);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", mensaje.Identidad),
                new SqlParameter("@Descripcion", mensaje.Descripcion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "MensajeUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Mensaje table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "MensajeDelete", parameters);
        }

        /// <summary>
        /// Selects a single record from the Mensaje table.
        /// </summary>
        public MensajeBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "MensajeSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Mensaje table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "MensajeSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Mensaje table.
        /// </summary>
        public List<MensajeBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "MensajeSelectAll"))
            {
                List<MensajeBE> mensajeBEList = new List<MensajeBE>();
                while (dataReader.Read())
                {
                    MensajeBE mensajeBE = MapDataReader(dataReader);
                    mensajeBEList.Add(mensajeBE);
                }

                return mensajeBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Mensaje table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "MensajeSelectAll");
        }

        /// <summary>
        /// Creates a new instance of the MensajeBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private MensajeBE MapDataReader(SqlDataReader dataReader)
        {
            MensajeBE mensajeBE = new MensajeBE();
            mensajeBE.Identidad = dataReader.GetInt64("Identidad", 0);
            mensajeBE.Descripcion = dataReader.GetString("Descripcion", null);

            return mensajeBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}

