﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class ItemDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public ItemDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Items table.
        /// </summary>
        //public void Insert(ItemBE item)
        //{
        //    ValidationUtility.ValidateArgument("item", item);

        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Descripcion", item.Descripcion),
        //        new SqlParameter("@Observaciones", item.Observaciones),
        //        new SqlParameter("@Imagen", item.Imagen)
        //    };

        //    SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "ItemsInsert", parameters);
        //}

        public Dictionary<string, string> GetClasesStringForItems()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetClasesStringForItems"))
            {
                Dictionary<string, string> eldiccionario = new Dictionary<string, string>();
                while (dataReader.Read())
                {

                    eldiccionario.Add(dataReader.GetString(0), dataReader.GetString(1));
                }

                return eldiccionario;
            }
        }

        public List<ItemBE> SelectAllByID_Clase(BE.Clase_ItemBE clase_ItemBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Clase", clase_ItemBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsSelectAllByID_Clase", parameters))
            {
                List<ItemBE> itemBEList = new List<ItemBE>();
                while (dataReader.Read())
                {
                    ItemBE itemBE = MapDataReader(dataReader);
                    itemBEList.Add(itemBE);
                }

                return itemBEList;
            }
        }

        public ItemBE SelectByDesc(ItemBE nuevoitem)
        {
            SqlParameter[] parameters = new SqlParameter[]
               {
                new SqlParameter("@Descripcion_Item", nuevoitem.Descripcion)
               };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemSelectByDesc", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public List<ItemBE> Select(IDictionary<string, string> dictionary)
        {
            BE.ItemBE orgdb = null;
            List<BE.ItemBE> Empresas = new List<BE.ItemBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarItemFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        ItemBE rubroBE = new ItemBE();
                        rubroBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        rubroBE.Descripcion = row["Descripcion"].ToString();

                        Empresas.Add(rubroBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ItemBE SelectbyDescyClase(ItemBE nuevoitem, Clase_ItemBE clase_Item)
        {
            SqlParameter[] parameters = new SqlParameter[]
                  {
                new SqlParameter("@Descripcion", nuevoitem.Descripcion),
                new SqlParameter("@ID_Clase", clase_Item.Identidad)
                  };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsSelectbyDescyClase", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public void Insert(ItemBE nuevoitem, Clase_ItemBE clase_Item)
        {

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", nuevoitem.Descripcion),
                new SqlParameter("@Observaciones", nuevoitem.Observaciones),
                //new SqlParameter("@Imagen", nuevoitem.Imagen),
                new SqlParameter("@ID_Clase", clase_Item.Identidad)
            };

            SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "ItemsInsert", parameters);
        }

        //public ItemBE Select(ItemBE nuevoitem)
        //{
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Updates a record in the Items table.
        /// </summary>
        public void Update(ItemBE item)
        {
            ValidationUtility.ValidateArgument("item", item);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", item.Identidad),
                new SqlParameter("@Descripcion", item.Descripcion),
                new SqlParameter("@Observaciones", item.Observaciones)
                //new SqlParameter("@Imagen", item.Imagen),

            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Items table by its primary key.
        /// </summary>
        public void Delete(ItemBE item)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", item.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsDeleteLogico", parameters);
        }

        /// <summary>
        /// Selects a single record from the Items table.
        /// </summary>
        public ItemBE Select(ItemBE item)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", item.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Items table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Items table.
        /// </summary>
        public List<ItemBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsSelectAll"))
            {
                List<ItemBE> itemBEList = new List<ItemBE>();
                while (dataReader.Read())
                {
                    ItemBE itemBE = MapDataReader(dataReader);
                    itemBEList.Add(itemBE);
                }

                return itemBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Items table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsSelectAll");
        //}

        /// <summary>
        /// Creates a new instance of the ItemBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private ItemBE MapDataReader(SqlDataReader dataReader)
        {
            ItemBE itemBE = new ItemBE();
            itemBE.Identidad = dataReader.GetInt64("Identidad", 0);
            itemBE.Descripcion = dataReader.GetString("Descripcion", null);
            itemBE.Observaciones = dataReader.GetString("Observaciones", null);
            //itemBE.Imagen = dataReader.GetString("Imagen", null);


            return itemBE;
        }

        public void Dispose()
        {
        }

        internal ItemBE SelectConDeleted(ItemBE itemBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", itemBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsSelectConDeleted", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion
    }
}
