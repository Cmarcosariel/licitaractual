﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class OfertaDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public OfertaDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Oferta table.
        /// </summary>
        public void Insert(OfertaBE oferta)
        {
            //ValidationUtility.ValidateArgument("oferta", oferta);

            //SqlParameter[] parameters = new SqlParameter[]
            //{
            //    new SqlParameter("@FechaPresentacion", oferta.FechaPresentacion),
            //    new SqlParameter("@ID_Estado_Oferta", oferta.Estado_Oferta.ToString()),
            //    new SqlParameter("@Total", oferta.Total),
            //    new SqlParameter("@ID_Organizacion", oferta.Organizacion.Identidad),
            //    new SqlParameter("@ID_Licitacion", oferta.Licitacion.Identidad)
            //};

            //oferta.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "OfertaInsert", parameters);
        }

        public void Insert(OfertaBE nueva, LicitacionBE licitacionBE)
        {
            ValidationUtility.ValidateArgument("oferta", nueva);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@FechaPresentacion", nueva.FechaPresentacion),
                new SqlParameter("@ID_Estado_Oferta", Enum.Parse(typeof(BE.Estado_OfertaBE), nueva.Estado_Oferta.ToString())),
                new SqlParameter("@Total", nueva.Total),
                new SqlParameter("@ID_Organizacion", nueva.Organizacion.Identidad),
                new SqlParameter("@ID_Licitacion", licitacionBE.Identidad)
            };

            DataTable dt = (DataTable)(SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "OfertaInsert", parameters));

            if (dt.Rows[0].ItemArray[0].GetType() == typeof(DBNull))
            {
                return;
            }

            nueva.Identidad = Convert.ToInt64(dt.Rows[0].ItemArray[0].ToString());
        }

        public OfertaBE SelectByLicitacionOrgBorrador(OfertaBE nueva, LicitacionBE licitacionBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
                      {
                new SqlParameter("@ID_Licitacion", licitacionBE.Identidad),
                new SqlParameter("@ID_Organizacion", nueva.Organizacion.Identidad)
                      };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectByLicitacionOrgBorrador", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReaderConItemsRecursivo(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public void ActualizarTotal(OfertaBE nueva)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", nueva.Identidad),
                new SqlParameter("@Total", nueva.Total)
                //new SqlParameter("@ID_Organizacion", oferta.Organizacion.Identidad)
                //new SqlParameter("@ID_Licitacion", oferta.Licitacion.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaUpdateActualizarTotal", parameters);
        }

        public int SelectCountAllNotBorradorByID_Licitacion(LicitacionBE licitacionBE)
        {

            int misofertas;

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Licitacion", licitacionBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "SelectCountAllNotBorradorByID_Licitacion", parameters))
            {
                if (dataReader.Read())
                {
                    misofertas = Convert.ToInt32(dataReader[0].ToString());

                    return misofertas;
                }
                else
                {
                    return 0;
                }

            }


        }

        public Dictionary<ItemBE, decimal> LlenarOfertaRecomendada(OfertaBE nueva, LicitacionBE licitacionBE)
        {
            Dictionary<ItemBE, decimal> midiccionario = new Dictionary<ItemBE, decimal>();

            try
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                new SqlParameter("@P_Licitacion", licitacionBE.Identidad)

                };

                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("sp_PrecioSugeridoxOrg", parameters);


                if (dt.Rows.Count == 0)
                {
                    midiccionario = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        ItemBE itemBE = new ItemBE();

                        itemBE.Identidad = Convert.ToInt64(row["ItemId"]);

                        using (DAL.ItemDAL daoitem = new DAL.ItemDAL("PRD"))
                        {
                            itemBE = daoitem.Select(itemBE);
                        }


                        midiccionario.Add(itemBE, Convert.ToDecimal(row["Indice"]));
                    }

                }

                return midiccionario;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<OfertaBE> SelectAllNotBorradorByID_Licitacion(LicitacionBE licitacionBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Licitacion", licitacionBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllNotBorradorByID_Licitacion", parameters))
            {
                List<OfertaBE> ofertaBEList = new List<OfertaBE>();
                while (dataReader.Read())
                {
                    OfertaBE ofertaBE = MapDataReaderConItemsRecursivo(dataReader);
                    ofertaBEList.Add(ofertaBE);
                }

                return ofertaBEList;
            }
        }

        public OfertaBE SelectByLicitacionOrg(OfertaBE nueva, LicitacionBE licitacionBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
                      {
                new SqlParameter("@ID_Licitacion", licitacionBE.Identidad),
                new SqlParameter("@ID_Organizacion", nueva.Organizacion.Identidad)
                      };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectByLicitacionOrg", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReaderConItemsRecursivo(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        private OfertaBE MapDataReaderConItemsRecursivo(SqlDataReader dataReader)
        {
            OfertaBE ofertaBE = new OfertaBE();
            ofertaBE.Identidad = dataReader.GetInt64("Identidad", 0);
            ofertaBE.FechaPresentacion = dataReader.GetDateTime("FechaPresentacion", new DateTime(0));
            ofertaBE.Estado_Oferta = (Estado_OfertaBE)dataReader.GetInt64("ID_Estado_Oferta", 0);


            ofertaBE.Total = dataReader.GetDecimal("Total", Decimal.Zero);

            using (OrganizacionDAL dao = new OrganizacionDAL("PRD"))
            {
                ofertaBE.Organizacion = dao.Select(new OrganizacionBE { Identidad = dataReader.GetInt64("ID_Organizacion", 0) });
            }


            using (ItemsOfertaDAL dao2 = new ItemsOfertaDAL("PRD"))
            {
                foreach (BE.ItemsOfertaBE item in dao2.SelectAllByID_Oferta(ofertaBE))
                {
                    ofertaBE.items.Add(item);
                }

            }


            return ofertaBE;
        }

        /// <summary>
        /// Updates a record in the Oferta table.
        /// </summary>
        public void Update(OfertaBE oferta)
        {
            ValidationUtility.ValidateArgument("oferta", oferta);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", oferta.Identidad),
                new SqlParameter("@FechaPresentacion", oferta.FechaPresentacion),
                new SqlParameter("@ID_Estado_Oferta", oferta.Estado_Oferta),
                new SqlParameter("@Total", oferta.Total),
                //new SqlParameter("@ID_Organizacion", oferta.Organizacion.Identidad)
                //new SqlParameter("@ID_Licitacion", oferta.Licitacion.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaUpdate", parameters);

        }


        public void UpdateEstadoOferta(OfertaBE oferta)
        {
            ValidationUtility.ValidateArgument("oferta", oferta);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", oferta.Identidad),
                new SqlParameter("@FechaPresentacion", oferta.FechaPresentacion),
                new SqlParameter("@ID_Estado_Oferta", oferta.Estado_Oferta)
                //new SqlParameter("@Total", oferta.Total),
                //new SqlParameter("@ID_Organizacion", oferta.Organizacion.Identidad)
                //new SqlParameter("@ID_Licitacion", oferta.Licitacion.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaUpdateEstado", parameters);

        }

        /// <summary>
        /// Deletes a record from the Oferta table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaDelete", parameters);
        }

        /// <summary>
        /// Deletes a record from the Oferta table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Estado_Oferta(long iD_Estado_Oferta)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Estado_Oferta", iD_Estado_Oferta)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaDeleteAllByID_Estado_Oferta", parameters);
        }

        /// <summary>
        /// Deletes a record from the Oferta table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Licitacion(long iD_Licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Licitacion", iD_Licitacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaDeleteAllByID_Licitacion", parameters);
        }

        /// <summary>
        /// Deletes a record from the Oferta table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Organizacion(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OfertaDeleteAllByID_Organizacion", parameters);
        }

        /// <summary>
        /// Selects a single record from the Oferta table.
        /// </summary>
        public OfertaBE Select(OfertaBE ofertaBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", ofertaBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReaderConItemsRecursivo(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Oferta table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OfertaSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Oferta table.
        /// </summary>
        public List<OfertaBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAll"))
            {
                List<OfertaBE> ofertaBEList = new List<OfertaBE>();
                while (dataReader.Read())
                {
                    OfertaBE ofertaBE = MapDataReader(dataReader);
                    ofertaBEList.Add(ofertaBE);
                }

                return ofertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Oferta table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAll");
        }

        /// <summary>
        /// Selects all records from the Oferta table by a foreign key.
        /// </summary>
        public List<OfertaBE> SelectAllByID_Estado_Oferta(long iD_Estado_Oferta)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Estado_Oferta", iD_Estado_Oferta)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllByID_Estado_Oferta", parameters))
            {
                List<OfertaBE> ofertaBEList = new List<OfertaBE>();
                while (dataReader.Read())
                {
                    OfertaBE ofertaBE = MapDataReader(dataReader);
                    ofertaBEList.Add(ofertaBE);
                }

                return ofertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Oferta table by a foreign key.
        /// </summary>
        public List<OfertaBE> SelectAllByID_Licitacion(LicitacionBE licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Licitacion", licitacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllByID_Licitacion", parameters))
            {
                List<OfertaBE> ofertaBEList = new List<OfertaBE>();
                while (dataReader.Read())
                {
                    OfertaBE ofertaBE = MapDataReaderConItemsRecursivo(dataReader);
                    ofertaBEList.Add(ofertaBE);
                }

                return ofertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Oferta table by a foreign key.
        /// </summary>
        public List<OfertaBE> SelectAllByID_Organizacion(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllByID_Organizacion", parameters))
            {
                List<OfertaBE> ofertaBEList = new List<OfertaBE>();
                while (dataReader.Read())
                {
                    OfertaBE ofertaBE = MapDataReader(dataReader);
                    ofertaBEList.Add(ofertaBE);
                }

                return ofertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Oferta table by a foreign key.
        /// </summary>
        public string SelectAllByID_Estado_OfertaJson(long iD_Estado_Oferta)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Estado_Oferta", iD_Estado_Oferta)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllByID_Estado_Oferta", parameters);
        }

        /// <summary>
        /// Selects all records from the Oferta table by a foreign key.
        /// </summary>
        public string SelectAllByID_LicitacionJson(long iD_Licitacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Licitacion", iD_Licitacion)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllByID_Licitacion", parameters);
        }

        /// <summary>
        /// Selects all records from the Oferta table by a foreign key.
        /// </summary>
        public string SelectAllByID_OrganizacionJson(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OfertaSelectAllByID_Organizacion", parameters);
        }

        /// <summary>
        /// Creates a new instance of the OfertaBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private OfertaBE MapDataReader(SqlDataReader dataReader)
        {
            OfertaBE ofertaBE = new OfertaBE();
            ofertaBE.Identidad = dataReader.GetInt64("Identidad", 0);
            ofertaBE.FechaPresentacion = dataReader.GetDateTime("FechaPresentacion", new DateTime(0));
            ofertaBE.Estado_Oferta = (Estado_OfertaBE)dataReader.GetInt64("ID_Estado_Oferta", 0);


            ofertaBE.Total = dataReader.GetDecimal("Total", Decimal.Zero);

            using (OrganizacionDAL dao = new OrganizacionDAL("PRD"))
            {
                ofertaBE.Organizacion = dao.Select(new OrganizacionBE { Identidad = dataReader.GetInt64("ID_Organizacion", 0) });
            }

            //using (LicitacionDAL dao = new LicitacionDAL("PRD"))
            //{
            //    ofertaBE.Licitacion = dao.Select(new BE.LicitacionBE { Identidad = dataReader.GetInt64("ID_Licitacion", 0) });
            //}

            return ofertaBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
