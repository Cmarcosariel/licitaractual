﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace DAL
{
    [Serializable]
    public class ExcepcionDB : Exception
    {
        public Exception test { get; set; }

        public ExcepcionDB(Exception excepcion)

        {
            test = excepcion;

            //GuardarExcepcion(test);
        }

        public ExcepcionDB()
        {

        }
        public ExcepcionDB(string message) : base(message) { }
        public ExcepcionDB(string message, Exception inner) : base(message, inner) { }
        protected ExcepcionDB(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }

        //public void GuardarExcepcion(Exception e)
        //{
        //    SaveFileDialog sfd = new SaveFileDialog();
        //    sfd.Filter = "Xml Files (*.xml)|*.XML|All files (*.*)|*.*";

        //    MessageBox.Show("Error, por favor seleccione ruta de archivo");



        //    if (sfd.ShowDialog() == DialogResult.OK)
        //    {
        //        string text = sfd.FileName;

        //        using (StreamWriter myWriter = new StreamWriter(text, true))
        //        {
        //            XmlSerializer mySerializer = new XmlSerializer(typeof(string));
        //            mySerializer.Serialize(myWriter, e.Message);
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show(e.Message);
        //    }

        //    Environment.Exit(4);
        //}
    }
}
