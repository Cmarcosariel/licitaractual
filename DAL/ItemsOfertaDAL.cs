﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class ItemsOfertaDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public ItemsOfertaDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the ItemsOferta table.
        /// </summary>
        public void Insert(ItemsOfertaBE itemsOferta, OfertaBE nueva)
        {
            ValidationUtility.ValidateArgument("itemsOferta", itemsOferta);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Item", itemsOferta.Item.Identidad),
                new SqlParameter("@ID_Oferta", nueva.Identidad),
                new SqlParameter("@PrecioUnitario", itemsOferta.PrecioUnitario)
            };

            DataTable dt = (DataTable)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaInsert", parameters);


            if (dt.Rows[0].ItemArray[0].GetType() == typeof(DBNull))
            {
                return;
            }

            itemsOferta.Identidad = Convert.ToInt64(dt.Rows[0].ItemArray[0].ToString());
        }

        public void Update(ItemsOfertaBE itemsOfertaBE, OfertaBE miofertaparaItem)
        {
            ValidationUtility.ValidateArgument("itemsOferta", itemsOfertaBE);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", itemsOfertaBE.Identidad),
                new SqlParameter("@ID_Item", itemsOfertaBE.Item.Identidad),
                new SqlParameter("@ID_Oferta", miofertaparaItem.Identidad),
                new SqlParameter("@PrecioUnitario", itemsOfertaBE.PrecioUnitario)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaUpdate", parameters);

        }

        /// <summary>
        /// Updates a record in the ItemsOferta table.
        /// </summary>
        public void Update(ItemsOfertaBE itemsOferta)
        {
            ValidationUtility.ValidateArgument("itemsOferta", itemsOferta);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", itemsOferta.Identidad),
                new SqlParameter("@ID_Item", itemsOferta.Item.Identidad),
                //new SqlParameter("@ID_Oferta", itemsOferta.Oferta.Identidad),
                new SqlParameter("@PrecioUnitario", itemsOferta.PrecioUnitario)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the ItemsOferta table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaDelete", parameters);
        }

        /// <summary>
        /// Deletes a record from the ItemsOferta table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Item(long iD_Item)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Item", iD_Item)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaDeleteAllByID_Item", parameters);
        }

        /// <summary>
        /// Deletes a record from the ItemsOferta table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Oferta(long iD_Oferta)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Oferta", iD_Oferta)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaDeleteAllByID_Oferta", parameters);
        }

        /// <summary>
        /// Selects a single record from the ItemsOferta table.
        /// </summary>
        public ItemsOfertaBE Select(BE.ItemsOfertaBE itemsOfertaBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", itemsOfertaBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the ItemsOferta table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the ItemsOferta table.
        /// </summary>
        public List<ItemsOfertaBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelectAll"))
            {
                List<ItemsOfertaBE> itemsOfertaBEList = new List<ItemsOfertaBE>();
                while (dataReader.Read())
                {
                    ItemsOfertaBE itemsOfertaBE = MapDataReader(dataReader);
                    itemsOfertaBEList.Add(itemsOfertaBE);
                }

                return itemsOfertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the ItemsOferta table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelectAll");
        //}

        /// <summary>
        /// Selects all records from the ItemsOferta table by a foreign key.
        /// </summary>
        public List<ItemsOfertaBE> SelectAllByID_Item(long iD_Item)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Item", iD_Item)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelectAllByID_Item", parameters))
            {
                List<ItemsOfertaBE> itemsOfertaBEList = new List<ItemsOfertaBE>();
                while (dataReader.Read())
                {
                    ItemsOfertaBE itemsOfertaBE = MapDataReader(dataReader);
                    itemsOfertaBEList.Add(itemsOfertaBE);
                }

                return itemsOfertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the ItemsOferta table by a foreign key.
        /// </summary>
        public List<ItemsOfertaBE> SelectAllByID_Oferta(OfertaBE ofertaBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Oferta", ofertaBE.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelectAllByID_Oferta", parameters))
            {
                List<ItemsOfertaBE> itemsOfertaBEList = new List<ItemsOfertaBE>();
                while (dataReader.Read())
                {
                    ItemsOfertaBE itemsOfertaBE = MapDataReader(dataReader);
                    itemsOfertaBEList.Add(itemsOfertaBE);
                }

                return itemsOfertaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the ItemsOferta table by a foreign key.
        ///// </summary>
        //public string SelectAllByID_ItemJson(long iD_Item)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Item", iD_Item)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelectAllByID_Item", parameters);
        //}

        /// <summary>
        /// Selects all records from the ItemsOferta table by a foreign key.
        ///// </summary>
        //public string SelectAllByID_OfertaJson(long iD_Oferta)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@ID_Oferta", iD_Oferta)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "ItemsOfertaSelectAllByID_Oferta", parameters);
        //}

        /// <summary>
        /// Creates a new instance of the ItemsOfertaBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private ItemsOfertaBE MapDataReader(SqlDataReader dataReader)
        {
            ItemsOfertaBE itemsOfertaBE = new ItemsOfertaBE();
            itemsOfertaBE.Identidad = dataReader.GetInt64("Identidad", 0);

            using (ItemDAL dao = new ItemDAL("PRD"))
            {
                itemsOfertaBE.Item = dao.SelectConDeleted(new BE.ItemBE { Identidad = dataReader.GetInt64("ID_Item", 0) });
            }

            itemsOfertaBE.PrecioUnitario = dataReader.GetDecimal("PrecioUnitario", Decimal.Zero);

            return itemsOfertaBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
