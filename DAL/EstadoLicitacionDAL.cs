﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    //public class EstadoLicitacionDAL
    //{
        //#region Fields

        //private string connectionStringName;

        //#endregion

        //#region Constructors

        //public EstadoLicitacionDAL(string connectionStringName)
        //{
        //    ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

        //    this.connectionStringName = connectionStringName;
        //}

        //#endregion

        //#region Methods

        ///// <summary>
        ///// Saves a record to the EstadoLicitacion table.
        ///// </summary>
        //public void Insert(EstadoLicitacionBE estadoLicitacion)
        //{
        //    ValidationUtility.ValidateArgument("estadoLicitacion", estadoLicitacion);

        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Descripcion", estadoLicitacion.Descripcion)
        //    };

        //    estadoLicitacion.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionInsert", parameters);
        //}

        ///// <summary>
        ///// Updates a record in the EstadoLicitacion table.
        ///// </summary>
        //public void Update(EstadoLicitacionBE estadoLicitacion)
        //{
        //    ValidationUtility.ValidateArgument("estadoLicitacion", estadoLicitacion);

        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", estadoLicitacion.Identidad),
        //        new SqlParameter("@Descripcion", estadoLicitacion.Descripcion)
        //    };

        //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionUpdate", parameters);
        //}

        ///// <summary>
        ///// Deletes a record from the EstadoLicitacion table by its primary key.
        ///// </summary>
        //public void Delete(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionDelete", parameters);
        //}

        ///// <summary>
        ///// Selects a single record from the EstadoLicitacion table.
        ///// </summary>
        //public EstadoLicitacionBE Select(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionSelect", parameters))
        //    {
        //        if (dataReader.Read())
        //        {
        //            return MapDataReader(dataReader);
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Selects a single record from the EstadoLicitacion table.
        ///// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionSelect", parameters);
        //}

        ///// <summary>
        ///// Selects all records from the EstadoLicitacion table.
        ///// </summary>
        //public List<EstadoLicitacionBE> SelectAll()
        //{
        //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionSelectAll"))
        //    {
        //        List<EstadoLicitacionBE> estadoLicitacionBEList = new List<EstadoLicitacionBE>();
        //        while (dataReader.Read())
        //        {
        //            EstadoLicitacionBE estadoLicitacionBE = MapDataReader(dataReader);
        //            estadoLicitacionBEList.Add(estadoLicitacionBE);
        //        }

        //        return estadoLicitacionBEList;
        //    }
        //}

        ///// <summary>
        ///// Selects all records from the EstadoLicitacion table.
        ///// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "EstadoLicitacionSelectAll");
        //}

        ///// <summary>
        ///// Creates a new instance of the EstadoLicitacionBE class and populates it with data from the specified SqlDataReader.
        ///// </summary>
        //private EstadoLicitacionBE MapDataReader(SqlDataReader dataReader)
        //{
        //    EstadoLicitacionBE estadoLicitacionBE = new EstadoLicitacionBE();
        //    estadoLicitacionBE.Identidad = dataReader.GetInt64("Identidad", 0);
        //    estadoLicitacionBE.Descripcion = dataReader.GetString("Descripcion", null);

        //    return estadoLicitacionBE;
        //}

        //public void Dispose()
        //{

        //}

        //#endregion
    //}
}
