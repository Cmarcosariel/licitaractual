﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;


namespace DAL
{
    public class BackupDAL : IDisposable
    {
        #region Fields

        private readonly string connectionStringName;

        #endregion

        #region Constructors

        public BackupDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Backup table.
        /// </summary>
        public void Insert(BackupBE backup)
        {
            ValidationUtility.ValidateArgument("backup", backup);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Filepath", backup.Filepath),
                new SqlParameter("@Timestamp", backup.Timestamp),
                new SqlParameter("@CreateDate", backup.CreateDate),
                new SqlParameter("@ChangeDate", backup.ChangeDate)
            };

            backup.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "BackupInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the Backup table.
        /// </summary>
        public void Update(BackupBE backup)
        {
            ValidationUtility.ValidateArgument("backup", backup);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", backup.Identidad),
                new SqlParameter("@Filepath", backup.Filepath),
                new SqlParameter("@Timestamp", backup.Timestamp),
                new SqlParameter("@CreateDate", backup.CreateDate),
                new SqlParameter("@ChangeDate", backup.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BackupUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Backup table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BackupDelete", parameters);
        }

        /// <summary>
        /// Selects a single record from the Backup table.
        /// </summary>
        public BackupBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "BackupSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Backup table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "BackupSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Backup table.
        /// </summary>
        public List<BackupBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "BackupSelectAll"))
            {
                List<BackupBE> backupBEList = new List<BackupBE>();
                while (dataReader.Read())
                {
                    BackupBE backupBE = MapDataReader(dataReader);
                    backupBEList.Add(backupBE);
                }

                return backupBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Backup table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "BackupSelectAll");
        //}

        /// <summary>
        /// Creates a new instance of the BackupBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private BackupBE MapDataReader(SqlDataReader dataReader)
        {
            BackupBE backupBE = new BackupBE();
            backupBE.Identidad = dataReader.GetInt64("Identidad", 0);
            backupBE.Filepath = dataReader.GetString("Filepath", null);
            backupBE.Timestamp = dataReader.GetInt64("Timestamp", 0);
            backupBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            backupBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return backupBE;
        }

        public void Dispose()
        {
        }

        public void RestaurarBackup(BackupBE backup)
        {
            SqlParameter[] parameters = new SqlParameter[]
                       {
                new SqlParameter("@filename", backup.Filepath)
                       };

            SqlClientUtility.ExecuteNonQueryMaster(connectionStringName, CommandType.StoredProcedure, "RESTORE_BACKUP_LICITAR", parameters);
        }
        public void TomarBackup(BackupBE backup)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]
           {
                new SqlParameter("@BackupPath", backup.Filepath),
                new SqlParameter("@timestamp2", backup.Timestamp)
           };

                SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "GET_BACKUP", parameters);
            }
            catch (Exception)
            {

                throw;
            }


        }

        //public int instalarBase()
        //{
        //    SqlConnection _objConexion;
        //    _objConexion = new SqlConnection(ConfigurationManager.ConnectionStrings["MASTER"].ToString());
        //    SqlCommand sqlCmd = new SqlCommand();
        //    sqlCmd.Connection = _objConexion;
        //    string strSQL = "";
        //    using (sqlCmd)
        //    {
        //        _objConexion.Open();

        //        string script = File.ReadAllText(@"C:\Program Files (x86)\Smart Resto\SmartRestoInstallLegacy\SqlServerSRScripts.sql");

        //        IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
        //                                 RegexOptions.Multiline | RegexOptions.IgnoreCase);

        //        foreach (string commandString in commandStrings)
        //        {
        //            if (commandString.Trim() != "")
        //            {
        //                using (var command = new SqlCommand(commandString, _objConexion))
        //                {
        //                    command.ExecuteNonQuery();
        //                }
        //            }
        //        }

        //        _objConexion.Close();

        //        strSQL = string.Format("SELECT database_id FROM sys.databases WHERE Name = 'PRD'");

        //        using (_objConexion)
        //        {
        //            using (SqlCommand sqlCmd2 = new SqlCommand(strSQL, _objConexion))
        //            {
        //                _objConexion.Open();

        //                object resultadoObj = sqlCmd2.ExecuteScalar();

        //                int databaseID = 0;

        //                if (resultadoObj != null)
        //                {
        //                    databaseID = Convert.ToInt32(resultadoObj);
        //                }
        //                _objConexion.Close();
        //                return databaseID;
        //            }
        //        }

        //    }

        //}



        //public bool chequearBase()
        //{

        //    SqlConnection _objConexion;
        //    string strSQL;
        //    bool result = false;

        //    try
        //    {
        //        try
        //        {
        //            IdentityReference everyoneIdentity = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
        //            DirectorySecurity dir_security = Directory.GetAccessControl(@"C:\Program Files (x86)\Smart Resto\SmartRestoInstallLegacy");

        //            FileSystemAccessRule full_access_rule = new FileSystemAccessRule(everyoneIdentity,
        //                            FileSystemRights.FullControl, InheritanceFlags.ContainerInherit |
        //                             InheritanceFlags.ObjectInherit, PropagationFlags.None,
        //                             AccessControlType.Allow);
        //            dir_security.AddAccessRule(full_access_rule);

        //            Directory.SetAccessControl(@"C:\Program Files (x86)\Smart Resto\SmartRestoInstallLegacy", dir_security);

        //        }
        //        catch (Exception ex)
        //        {
        //            result = false;
        //        }

        //        _objConexion = new SqlConnection(ConfigurationManager.ConnectionStrings["MASTER"].ToString());
        //        strSQL = string.Format("SELECT database_id FROM sys.databases WHERE Name = 'PRD'");

        //        using (_objConexion)
        //        {
        //            using (SqlCommand sqlCmd = new SqlCommand(strSQL, _objConexion))
        //            {
        //                _objConexion.Open();

        //                object resultadoObj = sqlCmd.ExecuteScalar();

        //                int databaseID = 0;

        //                if (resultadoObj != null)
        //                {
        //                    int.TryParse(resultadoObj.ToString(), out databaseID);
        //                }

        //                _objConexion.Close();

        //                result = (databaseID == 0);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //    }

        //    return result;
        //}
        #endregion
    }
}
