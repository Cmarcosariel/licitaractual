﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class DVVDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public DVVDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// 
        /// 
        /// Saves a record to the DVV table.
        /// </summary>
        public void Insert(DVVBE dVV)
        {
            ValidationUtility.ValidateArgument("dVV", dVV);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", dVV.Descripcion),
                new SqlParameter("@Digito_Verificador", dVV.Digito_Verificador)
            };

            /*dVV.Identidad = (long)*/SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "DVVInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the DVV table.
        /// </summary>
        public void Update(DVVBE dVV)
        {
            ValidationUtility.ValidateArgument("dVV", dVV);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", dVV.Identidad),
                new SqlParameter("@Descripcion", dVV.Descripcion),
                new SqlParameter("@Digito_Verificador", dVV.Digito_Verificador)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "DVVUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the DVV table by its primary key.
        /// </summary>
        public void Delete(DVVBE _dvv)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", _dvv.Descripcion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "DVVDelete", parameters);
        }

        /// <summary>
        /// Selects a single record from the DVV table.
        /// </summary>
        public DVVBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "DVVSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public void DeleteForRestore(string typeNameLimpio)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", typeNameLimpio)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "DVVDelete", parameters);
        }

        /// <summary>
        /// Selects a single record from the DVV table.
        ///// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "DVVSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the DVV table.
        /// </summary>
        public List<DVVBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "DVVSelectAll"))
            {
                List<DVVBE> dVVBEList = new List<DVVBE>();
                while (dataReader.Read())
                {
                    DVVBE dVVBE = MapDataReader(dataReader);
                    dVVBEList.Add(dVVBE);
                }

                return dVVBEList;
            }
        }

        /// <summary>
        /// Selects all records from the DVV table.
        ///// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "DVVSelectAll");
        //}

        ///// <summary>
        /// Creates a new instance of the DVVBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private DVVBE MapDataReader(SqlDataReader dataReader)
        {
            DVVBE dVVBE = new DVVBE();
            dVVBE.Identidad = dataReader.GetInt64("Identidad", 0);
            dVVBE.Descripcion = dataReader.GetString("Descripcion", null);
            dVVBE.Digito_Verificador = dataReader.GetString("Digito_Verificador", null);

            return dVVBE;
        }

        public void Dispose()
        {
        }

        public void CambioDia(DateTime asd)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@P_Fecha", asd)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Sp_InicioDia", parameters);
        }

        #endregion
    }
}
