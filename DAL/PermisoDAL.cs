﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class PermisoDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public PermisoDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Permiso table.
        /// </summary>
        public void Insert(PermisoBE permiso)
        {
            ValidationUtility.ValidateArgument("permiso", permiso);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@PermisoBase", permiso.PermisoBase),
                new SqlParameter("@Descripcion", permiso.Descripcion),
                new SqlParameter("@Codigo_Perfil", permiso.Codigo_Perfil),
                new SqlParameter("@DVH", permiso.DVH),
                new SqlParameter("@DELETED", permiso.DELETED),
                new SqlParameter("@CreateDate", permiso.CreateDate),
                new SqlParameter("@ChangeDate", permiso.ChangeDate)
            };

            /*permiso.Identidad = (long)*/SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "PermisoInsert", parameters);
        }

        public void DeleteByCodigo_Perfil(PermisoBE objeto)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Codigo_Perfil", objeto.Codigo_Perfil)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "PermisoDeleteByCodigo_Perfil", parameters);
        }

        public List<PermisoBE> GetAllRoles()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetAllRoles"))
            {
                List<PermisoBE> permisoBEList = new List<PermisoBE>();
                while (dataReader.Read())
                {
                    PermisoBE permisoBE = MapDataReaderTrunc(dataReader);
                    permisoBEList.Add(permisoBE);
                }

                return permisoBEList;
            }
        }

        /// <summary>
        /// Updates a record in the Permiso table.
        /// </summary>
        public void Update(PermisoBE permiso)
        {
            ValidationUtility.ValidateArgument("permiso", permiso);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", permiso.Identidad),
                new SqlParameter("@PermisoBase", permiso.PermisoBase),
                new SqlParameter("@Descripcion", permiso.Descripcion),
                new SqlParameter("@Codigo_Perfil", permiso.Codigo_Perfil),
                new SqlParameter("@DVH", permiso.DVH),
                new SqlParameter("@DELETED", permiso.DELETED),
                new SqlParameter("@CreateDate", permiso.CreateDate),
                new SqlParameter("@ChangeDate", permiso.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "PermisoUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Permiso table by its primary key.
        /// </summary>
        public void Delete(PermisoBE _Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", _Permiso.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "PermisoDelete", parameters);
        }

        /// <summary>
        /// Selects a single record from the Permiso table.
        /// </summary>
        public PermisoBE Select(PermisoBE _Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", _Permiso.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "PermisoSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public List<PermisoBE> GetRolesByUserID(UsuarioBE usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", usuario.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetRolesByUserID", parameters))
            {
                List<PermisoBE> permisoBEList = new List<PermisoBE>();
                while (dataReader.Read())
                {
                    PermisoBE permisoBE = MapDataReaderEspecial(dataReader);
                    permisoBEList.Add(permisoBE);
                }

                return permisoBEList;
            }
        }

        public void UpdateRolExist(PermisoBE permisoBE)
        {
            ValidationUtility.ValidateArgument("permiso", permisoBE);

            SqlParameter[] parameters = new SqlParameter[]
            {

                new SqlParameter("@Descripcion", permisoBE.Descripcion),
                new SqlParameter("@Codigo_Perfil", permisoBE.Codigo_Perfil),
                new SqlParameter("@DVH", permisoBE.DVH),
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "PermisoUpdateExist", parameters);
        }

        private PermisoBE MapDataReaderEspecial(SqlDataReader dataReader)
        {
            PermisoBE permisoBE = new PermisoBE();
            permisoBE.Identidad = dataReader.GetInt64("Identidad", 0);
            permisoBE.Descripcion = dataReader.GetString("Descripcion", null);
            permisoBE.Codigo_Perfil = dataReader.GetString("Codigo_Perfil", null);

            return permisoBE;
        }

        public List<PermisoBE> GetPermisoBaseByUserID(UsuarioBE usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", usuario.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetPermisoBaseByUserID", parameters))
            {
                List<PermisoBE> permisoBEList = new List<PermisoBE>();
                while (dataReader.Read())
                {
                    PermisoBE permisoBE = MapDataReaderTrunc(dataReader);
                    //PermisoBE permisoBE = MapDataReader(dataReader);
                    permisoBEList.Add(permisoBE);
                }

                return permisoBEList;
            }
        }

        public PermisoBE PermisoSelectbyCodigo(PermisoBE _Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Codigo", _Permiso.Codigo_Perfil)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "PermisoSelectbyCodigo", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public List<PermisoBE> GetAllPermisosNotAssignedToUser(UsuarioBE usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", usuario.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetAllPermisosNotAssignedToUser", parameters))
            {
                List<PermisoBE> permisoBEList = new List<PermisoBE>();
                while (dataReader.Read())
                {
                    PermisoBE permisoBE = MapDataReaderTrunc(dataReader);
                    permisoBEList.Add(permisoBE);
                }

                return permisoBEList;
            }
        }

        public List<PermisoBE> GetAllRolesNotAssignedToUser(UsuarioBE usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", usuario.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetAllRolesNotAssignedToUser", parameters))
            {
                List<PermisoBE> permisoBEList = new List<PermisoBE>();
                while (dataReader.Read())
                {
                    PermisoBE permisoBE = MapDataReaderTrunc(dataReader);
                    permisoBEList.Add(permisoBE);
                }

                return permisoBEList;
            }
        }

        /// <summary>
        /// Selects a single record from the Permiso table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "PermisoSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Permiso table.
        /// </summary>
        public List<PermisoBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "PermisoSelectAll"))
            {
                List<PermisoBE> permisoBEList = new List<PermisoBE>();
                while (dataReader.Read())
                {
                    PermisoBE permisoBE = MapDataReader(dataReader);
                    permisoBEList.Add(permisoBE);
                }

                return permisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Permiso table.
        ///// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "PermisoSelectAll");
        }

        /// <summary>
        /// Creates a new instance of the PermisoBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private PermisoBE MapDataReader(SqlDataReader dataReader)
        {   
            PermisoBE permisoBE = new PermisoBE();
            permisoBE.Identidad = dataReader.GetInt64("Identidad", 0);
            permisoBE.PermisoBase = dataReader.GetBoolean("PermisoBase", false);
            permisoBE.Descripcion = dataReader.GetString("Descripcion", null);
            permisoBE.Codigo_Perfil = dataReader.GetString("Codigo_Perfil", null);
            permisoBE.DVH = dataReader.GetString("DVH", null);
            permisoBE.DELETED = dataReader.GetBoolean("DELETED", false);
            permisoBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            permisoBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return permisoBE;
        }

        public void Dispose()
        {
        }


        private PermisoBE MapDataReaderTrunc(SqlDataReader dataReader)
        {
            PermisoBE permisoBE = new PermisoBE();
            permisoBE.Identidad = dataReader.GetInt64("Identidad", 0);
            permisoBE.Descripcion = dataReader.GetString("Descripcion", null);
            permisoBE.Codigo_Perfil = dataReader.GetString("Codigo_Perfil", null);
            return permisoBE;
        }

        #endregion
    }
}
