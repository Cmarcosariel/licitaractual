﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace DAL
{
    public static class SqlClientUtility
    {
        internal static object ExecuteScalar(string connectionStringName, CommandType storedProcedure, string v, SqlParameter[] parameters)
        {
            return SqlHelper.GetInstance(connectionStringName).ExecuteQuery(v, parameters.ToArray());
        }

        internal static SqlDataReader ExecuteReader(string connectionStringName, CommandType storedProcedure, string v, SqlParameter[] parameters)
        {
            return SqlHelper.GetInstance(connectionStringName).ExecuteReader(v, parameters.ToArray());
        }
        internal static SqlDataReader ExecuteReader(string connectionStringName, CommandType storedProcedure, string v)
        {
            return SqlHelper.GetInstance(connectionStringName).ExecuteReader(v, null);
        }
        internal static void ExecuteNonQuery(string connectionStringName, CommandType storedProcedure, string v, SqlParameter[] parameters)
        {
            SqlHelper.GetInstance(connectionStringName).ExecuteNonQuery(v, parameters.ToArray());
        }

        internal static string ExecuteJson(string connectionStringName, CommandType storedProcedure, string v, SqlParameter[] parameters)
        {
            throw new NotImplementedException();
        }

        internal static string ExecuteJson(string connectionStringName, CommandType storedProcedure, string v)
        {
            throw new NotImplementedException();
        }

        internal static void ExecuteNonQueryMaster(string connectionStringName, CommandType storedProcedure, string v, SqlParameter[] parameters)
        {
            SqlHelper.GetInstance(connectionStringName).ExecuteNonQueryMaster(v, parameters.ToArray());
        }
    }
}

