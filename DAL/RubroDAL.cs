﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class RubroDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public RubroDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Rubro table.
        /// </summary>
        public void Insert(RubroBE rubro)
        {
            ValidationUtility.ValidateArgument("rubro", rubro);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", rubro.Descripcion)
            };

  SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "RubroInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the Rubro table.
        /// </summary>
        public void Update(RubroBE rubro)
        {
            ValidationUtility.ValidateArgument("rubro", rubro);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", rubro.Identidad),
                new SqlParameter("@Descripcion", rubro.Descripcion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RubroUpdate", parameters);
        }

        public RubroBE GetRubrofromClaseItem(Clase_ItemBE organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", organizacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "GetRubrofromClaseItem", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public RubroBE SelectRubroCompleto(RubroBE rubroBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
                  {
                new SqlParameter("@Identidad", rubroBE.Identidad)
                  };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RubroSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReaderConClaseItem(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Deletes a record from the Rubro table by its primary key.
        /// </summary>
        public void Delete(RubroBE rubro)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", rubro.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RubroDeleteLogico", parameters);
        }

        /// <summary>
        /// Selects a single record from the Rubro table.
        /// </summary>
        public RubroBE Select(RubroBE rub)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", rub.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RubroSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Rubro table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "RubroSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Rubro table.
        /// </summary>
        public List<RubroBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RubroSelectAll"))
            {
                List<RubroBE> rubroBEList = new List<RubroBE>();
                while (dataReader.Read())
                {
                    RubroBE rubroBE = MapDataReader(dataReader);
                    rubroBEList.Add(rubroBE);
                }

                return rubroBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Rubro table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "RubroSelectAll");
        }

        /// <summary>
        /// Creates a new instance of the RubroBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private RubroBE MapDataReader(SqlDataReader dataReader)
        {
            RubroBE rubroBE = new RubroBE();
            rubroBE.Identidad = dataReader.GetInt64("Identidad", 0);
            rubroBE.Descripcion = dataReader.GetString("Descripcion", null);

            return rubroBE;
        }

        private RubroBE MapDataReaderConClaseItem(SqlDataReader dataReader)
        {
            RubroBE rubroBE = new RubroBE();
            rubroBE.Identidad = dataReader.GetInt64("Identidad", 0);
            rubroBE.Descripcion = dataReader.GetString("Descripcion", null);

            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                rubroBE.Clases = dal.SelectAllByID_RubroConItems(rubroBE);
            }

            return rubroBE;
        }

        public void Dispose()
        {
        }

        public List<BE.RubroBE> Select(IDictionary<string, string> dictionary)
        {
            BE.RubroBE orgdb = null;
            List<BE.RubroBE> Empresas = new List<BE.RubroBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarRubrosFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        RubroBE rubroBE = new RubroBE();
                        rubroBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        rubroBE.Descripcion = row["Descripcion"].ToString();

                        Empresas.Add(rubroBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        #endregion
    }
}
