﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class IdiomaDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public IdiomaDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Idioma table.
        /// </summary>
        public void Insert(IdiomaBE idioma)
        {
            ValidationUtility.ValidateArgument("idioma", idioma);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", idioma.Descripcion),
                new SqlParameter("@CreateDate", idioma.CreateDate),
                new SqlParameter("@ChangeDate", idioma.ChangeDate)
            };

            idioma.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "IdiomaInsert", parameters);
        }

        public void InsertIdiomaNuevo(IdiomaBE idioma, IdiomaBE idiomaComplemento)
        {
            ValidationUtility.ValidateArgument("idioma", idioma);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Complemento", idiomaComplemento.Identidad),
                new SqlParameter("@Descripcion", idioma.Descripcion),
                new SqlParameter("@CreateDate", idioma.CreateDate),
                new SqlParameter("@ChangeDate", idioma.ChangeDate)
            };

           /* idioma.Identidad = (long)*/SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "InsertIdiomaNuevoConComplemento", parameters);
        }

        public BE.IdiomaBE GetByDescripcion(IdiomaBE idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", idioma.Descripcion)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "IdiomaGetByDescripcion", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }

            }
        }

        public void UpdateTraduccion(TraduccionBE trad)
        {
            throw new NotImplementedException();
        }

        public IdiomaBE GuardarIdioma(UsuarioBE usuario, IdiomaBE idioma)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates a record in the Idioma table.
        /// </summary>
        public void Update(IdiomaBE idioma)
        {
            ValidationUtility.ValidateArgument("idioma", idioma);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", idioma.Identidad),
                new SqlParameter("@Descripcion", idioma.Descripcion),
                new SqlParameter("@CreateDate", idioma.CreateDate),
                new SqlParameter("@ChangeDate", idioma.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "IdiomaUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Idioma table by its primary key.
        /// </summary>
        public void Delete(IdiomaBE ojb)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", ojb.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "IdiomaDeleteLogico", parameters);
        }

        /// <summary>
        /// Selects a single record from the Idioma table.
        /// </summary>
        public IdiomaBE Select(IdiomaBE idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", idioma.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "IdiomaSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// Selects a single record from the Idioma table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "IdiomaSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Idioma table.
        /// </summary>
        public List<IdiomaBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "IdiomaSelectAll"))
            {
                List<IdiomaBE> idiomaBEList = new List<IdiomaBE>();
                while (dataReader.Read())
                {
                    IdiomaBE idiomaBE = MapDataReader(dataReader);
                    idiomaBEList.Add(idiomaBE);
                }
                return idiomaBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Idioma table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "IdiomaSelectAll");
        //}

        /// <summary>
        /// Creates a new instance of the IdiomaBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private IdiomaBE MapDataReader(SqlDataReader dataReader)
        {
            IdiomaBE idiomaBE = new IdiomaBE();
            idiomaBE.Identidad = dataReader.GetInt64("Identidad", 0);
            idiomaBE.Descripcion = dataReader.GetString("Descripcion", null);
            idiomaBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            idiomaBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return idiomaBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
