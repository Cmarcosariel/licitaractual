﻿using BE;
using SharpCore.Extensions;
using SharpCore.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class RolPermisoDAL : IDisposable
    {
        #region Fields

        private readonly string connectionStringName;

        #endregion

        #region Constructors

        public RolPermisoDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }



        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the RolPermiso table.
        /// </summary>
        public void Insert(RolPermisoBE rolPermiso)
        {
            ValidationUtility.ValidateArgument("rolPermiso", rolPermiso);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@DVH", rolPermiso.DVH),
                new SqlParameter("@Codigo_Perfil_Rol", rolPermiso.Rol.Codigo_Perfil),
                new SqlParameter("@Codigo_Perfil_Permiso", rolPermiso.Permiso.Codigo_Perfil)

            };

/*            rolPermiso.Identidad = (long)*/SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "RolPermisoInsertByCodigo_Perfil", parameters);
        }

        /// <summary>
        /// Updates a record in the RolPermiso table.
        /// </summary>
        public void Update(RolPermisoBE rolPermiso)
        {
            ValidationUtility.ValidateArgument("rolPermiso", rolPermiso);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", rolPermiso.Identidad),
                new SqlParameter("@DVH", rolPermiso.DVH),
                new SqlParameter("@ID_Rol", rolPermiso.Rol.Identidad),
                new SqlParameter("@ID_Permiso", rolPermiso.Permiso.Identidad),
                new SqlParameter("@CreateDate", rolPermiso.CreateDate),
                new SqlParameter("@ChangeDate", rolPermiso.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RolPermisoUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the RolPermiso table by its primary key.
        /// </summary>
        public void Delete(RolPermisoBE _Rolpermiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Codigo_Perfil_Rol", _Rolpermiso.Rol.Codigo_Perfil),
                new SqlParameter("@Codigo_Perfil_Permiso", _Rolpermiso.Permiso.Codigo_Perfil)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RolPermisoDeleteByCodigoPerfil", parameters);
        }

        /// <summary>
        /// Deletes a record from the RolPermiso table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Permiso(PermisoBE _Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Permiso", _Permiso.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RolPermisoDeleteAllByID_Permiso", parameters);
        }

        /// <summary>
        /// Deletes a record from the RolPermiso table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Rol(PermisoBE _Rol)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rol", _Rol.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RolPermisoDeleteAllByID_Rol", parameters);
        }

        /// <summary>
        /// Selects a single record from the RolPermiso table.
        /// </summary>
        public RolPermisoBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the RolPermiso table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //	SqlParameter[] parameters = new SqlParameter[]
        //	{
        //		new SqlParameter("@Identidad", identidad)
        //	};

        //	return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the RolPermiso table.
        /// </summary>
        public List<RolPermisoBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelectAll"))
            {
                List<RolPermisoBE> rolPermisoBEList = new List<RolPermisoBE>();
                while (dataReader.Read())
                {
                    RolPermisoBE rolPermisoBE = MapDataReader(dataReader);
                    rolPermisoBEList.Add(rolPermisoBE);
                }

                return rolPermisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the RolPermiso table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //	return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelectAll");
        //}

        /// <summary>
        /// Selects all records from the RolPermiso table by a foreign key.
        /// </summary>
        public List<RolPermisoBE> SelectAllByID_Permiso(long iD_Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Permiso", iD_Permiso)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelectAllByID_Permiso", parameters))
            {
                List<RolPermisoBE> rolPermisoBEList = new List<RolPermisoBE>();
                while (dataReader.Read())
                {
                    RolPermisoBE rolPermisoBE = MapDataReader(dataReader);
                    rolPermisoBEList.Add(rolPermisoBE);
                }

                return rolPermisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the RolPermiso table by a foreign key.
        /// </summary>
        public List<RolPermisoBE> SelectAllByID_Rol(PermisoBE _Rol)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rol", _Rol.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelectAllByID_Rol", parameters))
            {
                List<RolPermisoBE> rolPermisoBEList = new List<RolPermisoBE>();
                while (dataReader.Read())
                {
                    RolPermisoBE rolPermisoBE = MapDataReader(dataReader);
                    rolPermisoBEList.Add(rolPermisoBE);
                }

                return rolPermisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the RolPermiso table by a foreign key.
        /// </summary>
        public string SelectAllByID_PermisoJson(long iD_Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Permiso", iD_Permiso)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelectAllByID_Permiso", parameters);
        }

        /// <summary>
        /// Selects all records from the RolPermiso table by a foreign key.
        ///// </summary>
        public string SelectAllByID_RolJson(long iD_Rol)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rol", iD_Rol)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "RolPermisoSelectAllByID_Rol", parameters);
        }

        /// <summary>
        /// Creates a new instance of the RolPermisoBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private RolPermisoBE MapDataReader(SqlDataReader dataReader)
        {
            RolPermisoBE rolPermisoBE = new RolPermisoBE
            {
                Identidad = dataReader.GetInt64("Identidad", 0),
                DVH = dataReader.GetString("DVH", null)
            };

            using (DAL.PermisoDAL dalpermiso = new DAL.PermisoDAL("PRD"))
            {
                PermisoBE permisotmp = new PermisoBE() { Identidad = dataReader.GetInt64("ID_Rol", 0) };
                PermisoBE permisotmp2 = new PermisoBE() { Identidad = dataReader.GetInt64("ID_Permiso", 0) };

                rolPermisoBE.Rol = dalpermiso.Select(permisotmp);
                rolPermisoBE.Permiso = dalpermiso.Select(permisotmp2);
            }


            rolPermisoBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            rolPermisoBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return rolPermisoBE;
        }

        public void Dispose()
        {
        }


        public void DeleteAllByCodigo_Perfil(PermisoBE objeto)
        {

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Codigo_Perfil", objeto.Codigo_Perfil)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "RolPermisoDeleteAllByCodigo_Perfil", parameters);

        }

        #endregion
    }
}
