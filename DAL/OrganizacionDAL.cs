﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class OrganizacionDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public OrganizacionDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Organizacion table.
        /// </summary>
        public void Insert(OrganizacionBE organizacion)
        {
            ValidationUtility.ValidateArgument("organizacion", organizacion);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Descripcion", organizacion.Descripcion),
                new SqlParameter("@CUIT", organizacion.CUIT),
                new SqlParameter("@Razon_Social", organizacion.Razon_Social),
                new SqlParameter("@Domicilio", organizacion.Domicilio),
                new SqlParameter("@Email", organizacion.Email),
                new SqlParameter("@Telefono", organizacion.Telefono)
                //,
                //new SqlParameter("@Tipo_Organizacion", organizacion.Tipo_Organizacion)
            };
            try
            {
                SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "OrganizacionInsert", parameters);
            }
            catch (Exception e)
            {
                throw e;
            }


        }

        /// <summary>
        /// Updates a record in the Organizacion table.
        /// </summary>
        public void Update(OrganizacionBE organizacion)
        {
            ValidationUtility.ValidateArgument("organizacion", organizacion);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", organizacion.Identidad),
                new SqlParameter("@Descripcion", organizacion.Descripcion),
                new SqlParameter("@CUIT", organizacion.CUIT),
                new SqlParameter("@Razon_Social", organizacion.Razon_Social),
                new SqlParameter("@Domicilio", organizacion.Domicilio),
                new SqlParameter("@Email", organizacion.Email),
                new SqlParameter("@Telefono", organizacion.Telefono)
                //,
                //new SqlParameter("@Tipo_Organizacion", organizacion.Tipo_Organizacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OrganizacionUpdate", parameters);
        }

        public List<RubroBE> SelectRubros_Suscript_ByID_Organizacion(OrganizacionBE objeto)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", objeto.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "RubrosSuscript_SelectAllByID_Organizacion", parameters))
            {
                List<RubroBE> rubroOrganizacionBEList = new List<RubroBE>();

                while (dataReader.Read())
                {
                    BE.RubroBE rubroBE = MapDataReaderRubro(dataReader);
                    rubroOrganizacionBEList.Add(rubroBE);
                }

                return rubroOrganizacionBEList;
            }

        }

        public Dictionary<ItemBE, string> CalcularPerfilOferta(LicitacionBE licitacionBE, OfertaBE nueva)
        {
           Dictionary<ItemBE, string> asdasdas = new Dictionary<ItemBE, string>();

            return asdasdas;
        }

        public void BajaSuscripcion(OrganizacionBE org, RubroBE rubro)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", rubro.Identidad),
                new SqlParameter("@ID_Organizacion", org.Identidad)
            };

            SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "RubroOrganizacionDeleteLogico", parameters);
        }

        public void NuevaSuscripcion(OrganizacionBE org, RubroBE rubro)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", rubro.Identidad),
                new SqlParameter("@ID_Organizacion", org.Identidad)
            };

           SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "RubroOrganizacionInsert", parameters);
        }

        /// <summary>
        /// Deletes a record from the Organizacion table by its primary key.    
        /// </summary>
        public void Delete(BE.OrganizacionBE org)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", org.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OrganizacionDeleteLogico", parameters);
        }

        public List<OrganizacionBE> SelectOrgBy_RubroSuscript(LicitacionBE licitacionBE)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Rubro", licitacionBE.Rubro.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelectOrgBy_RubroSuscript", parameters))
            {
                List<OrganizacionBE> organizacionBEList = new List<OrganizacionBE>();
                while (dataReader.Read())
                {
                    OrganizacionBE organizacionBE = MapDataReader(dataReader);
                    organizacionBEList.Add(organizacionBE);
                }

                return organizacionBEList;
            }
        }

        /// <summary>
        /// Deletes a record from the Organizacion table by a foreign key.
        /// </summary>
        public void DeleteAllByTipo_Organizacion(long tipo_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Tipo_Organizacion", tipo_Organizacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "OrganizacionDeleteAllByTipo_Organizacion", parameters);
        }
        private RubroBE MapDataReaderRubro(SqlDataReader dataReader)
        {
            RubroBE rubroBE = new RubroBE();
            rubroBE.Identidad = dataReader.GetInt64("Identidad", 0);
            rubroBE.Descripcion = dataReader.GetString("Descripcion", null);

            return rubroBE;
        }
        /// <summary>
        /// Selects a single record from the Organizacion table.
        /// </summary>
        public OrganizacionBE Select(OrganizacionBE organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", organizacion.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Organizacion table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Organizacion table.
        /// </summary>
        public List<OrganizacionBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelectAll"))
            {
                List<OrganizacionBE> organizacionBEList = new List<OrganizacionBE>();
                while (dataReader.Read())
                {
                    OrganizacionBE organizacionBE = MapDataReader(dataReader);
                    organizacionBEList.Add(organizacionBE);
                }

                return organizacionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Organizacion table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelectAll");
        }

        /// <summary>
        /// Selects all records from the Organizacion table by a foreign key.
        /// </summary>
        public List<OrganizacionBE> SelectAllByTipo_Organizacion(long tipo_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Tipo_Organizacion", tipo_Organizacion)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelectAllByTipo_Organizacion", parameters))
            {
                List<OrganizacionBE> organizacionBEList = new List<OrganizacionBE>();
                while (dataReader.Read())
                {
                    OrganizacionBE organizacionBE = MapDataReader(dataReader);
                    organizacionBEList.Add(organizacionBE);
                }

                return organizacionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Organizacion table by a foreign key.
        /// </summary>
        public string SelectAllByTipo_OrganizacionJson(long tipo_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Tipo_Organizacion", tipo_Organizacion)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "OrganizacionSelectAllByTipo_Organizacion", parameters);
        }

        /// <summary>
        /// Creates a new instance of the OrganizacionBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private OrganizacionBE MapDataReader(SqlDataReader dataReader)
        {
            OrganizacionBE organizacionBE = new OrganizacionBE();
            organizacionBE.Identidad = dataReader.GetInt64("Identidad", 0);
            organizacionBE.Descripcion = dataReader.GetString("Descripcion", null);
            organizacionBE.CUIT = dataReader.GetString("CUIT", null);
            organizacionBE.Razon_Social = dataReader.GetString("Razon_Social", null);
            organizacionBE.Domicilio = dataReader.GetString("Domicilio", null);
            organizacionBE.Email = dataReader.GetString("Email", null);
            organizacionBE.Telefono = dataReader.GetString("Telefono", null);

            foreach (RubroBE item in SelectRubros_Suscript_ByID_Organizacion(organizacionBE))
            {
                organizacionBE.RubrosSuscriptos.Add(item);
            }


            return organizacionBE;
        }

        public void Dispose()
        {
        }


        public List<BE.OrganizacionBE> Select(IDictionary<string, string> dictionary)
        {
            BE.OrganizacionBE orgdb = null;
            List<BE.OrganizacionBE> Empresas = new List<BE.OrganizacionBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();

            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarOrgsFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    Empresas = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        OrganizacionBE organizacionBE = new OrganizacionBE();
                        organizacionBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        organizacionBE.Descripcion = row["Descripcion"].ToString();
                        organizacionBE.CUIT = row["CUIT"].ToString();
                        organizacionBE.Razon_Social = row["Razon_Social"].ToString();
                        organizacionBE.Domicilio = row["Domicilio"].ToString();
                        organizacionBE.Email = row["Email"].ToString();
                        organizacionBE.Telefono = row["Telefono"].ToString();

                        Empresas.Add(organizacionBE);
                    }

                }

                return Empresas;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        #endregion
    }
}
