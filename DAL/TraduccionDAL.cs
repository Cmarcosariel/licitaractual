﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class TraduccionDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public TraduccionDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Traduccion table.
        /// </summary>
        public void Insert(TraduccionBE traduccion)
        {
            ValidationUtility.ValidateArgument("traduccion", traduccion);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Traduccion", traduccion.Traduccion),
                new SqlParameter("@ID_Mensaje", traduccion.Mensaje.Identidad),
                new SqlParameter("@ID_Idioma", traduccion.Idioma.Identidad),
                new SqlParameter("@CreateDate", traduccion.CreateDate),
                new SqlParameter("@ChangeDate", traduccion.ChangeDate)
            };

            SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "TraduccionInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the Traduccion table.
        /// </summary>
        public void Update(TraduccionBE traduccion)
        {
            ValidationUtility.ValidateArgument("traduccion", traduccion);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", traduccion.Identidad),
                new SqlParameter("@Traduccion", traduccion.Traduccion),
                new SqlParameter("@ID_Mensaje", traduccion.Mensaje.Identidad),
                new SqlParameter("@ID_Idioma", traduccion.Idioma.Identidad),
                new SqlParameter("@CreateDate", traduccion.CreateDate),
                new SqlParameter("@ChangeDate", traduccion.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "TraduccionUpdate", parameters);
        }

        public void UpdateTraduccion(TraduccionBE trad)
        {
            ValidationUtility.ValidateArgument("traduccion", trad);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", trad.Identidad),
                new SqlParameter("@Traduccion", trad.Traduccion),
                new SqlParameter("@ChangeDate", trad.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "TraduccionUpdateMensaje", parameters);
        }

        /// <summary>
        /// Deletes a record from the Traduccion table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "TraduccionDelete", parameters);
        }

        /// <summary>
        /// Deletes a record from the Traduccion table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Idioma(long iD_Idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Idioma", iD_Idioma)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "TraduccionDeleteAllByID_Idioma", parameters);
        }

        /// <summary>
        /// Deletes a record from the Traduccion table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Mensaje(long iD_Mensaje)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Mensaje", iD_Mensaje)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "TraduccionDeleteAllByID_Mensaje", parameters);
        }

        /// <summary>
        /// Selects a single record from the Traduccion table.
        /// </summary>
        public TraduccionBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "TraduccionSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Traduccion table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "TraduccionSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Traduccion table.
        /// </summary>
        public List<TraduccionBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "TraduccionSelectAll"))
            {
                List<TraduccionBE> traduccionBEList = new List<TraduccionBE>();
                while (dataReader.Read())
                {
                    TraduccionBE traduccionBE = MapDataReader(dataReader);
                    traduccionBEList.Add(traduccionBE);
                }

                return traduccionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Traduccion table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "TraduccionSelectAll");
        }

        /// <summary>
        /// Selects all records from the Traduccion table by a foreign key.
        /// </summary>
        public List<TraduccionBE> SelectAllByID_Idioma(IdiomaBE idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Idioma", idioma.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "TraduccionSelectAllByID_Idioma", parameters))
            {
                List<TraduccionBE> traduccionBEList = new List<TraduccionBE>();
                while (dataReader.Read())
                {
                    TraduccionBE traduccionBE = MapDataReader(dataReader);
                    traduccionBEList.Add(traduccionBE);
                }

                return traduccionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Traduccion table by a foreign key.
        /// </summary>
        public List<TraduccionBE> SelectAllByID_Mensaje(long iD_Mensaje)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Mensaje", iD_Mensaje)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "TraduccionSelectAllByID_Mensaje", parameters))
            {
                List<TraduccionBE> traduccionBEList = new List<TraduccionBE>();
                while (dataReader.Read())
                {
                    TraduccionBE traduccionBE = MapDataReader(dataReader);
                    traduccionBEList.Add(traduccionBE);
                }

                return traduccionBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Traduccion table by a foreign key.
        /// </summary>
        //public string SelectAllByID_IdiomaJson(long iD_Idioma)
        //{
        //	SqlParameter[] parameters = new SqlParameter[]
        //	{
        //		new SqlParameter("@ID_Idioma", iD_Idioma)
        //	};

        //	return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "TraduccionSelectAllByID_Idioma", parameters);
        //}

        /// <summary>
        /// Selects all records from the Traduccion table by a foreign key.
        /// </summary>
        //public string SelectAllByID_MensajeJson(long iD_Mensaje)
        //{
        //	SqlParameter[] parameters = new SqlParameter[]
        //	{
        //		new SqlParameter("@ID_Mensaje", iD_Mensaje)
        //	};

        //	return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "TraduccionSelectAllByID_Mensaje", parameters);
        //}

        /// <summary>
        /// Creates a new instance of the TraduccionBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private TraduccionBE MapDataReader(SqlDataReader dataReader)
        {
            TraduccionBE traduccionBE = new TraduccionBE();
            traduccionBE.Identidad = dataReader.GetInt64("Identidad", 0);
            traduccionBE.Traduccion = dataReader.GetString("Traduccion", null);

            using (DAL.MensajeDAL dalmensaje = new DAL.MensajeDAL("PRD"))
            {
                traduccionBE.Mensaje = dalmensaje.Select(dataReader.GetInt64("ID_Mensaje", 0));
            }

            using (DAL.IdiomaDAL dalidioma = new DAL.IdiomaDAL("PRD"))
            {
                BE.IdiomaBE unidioma = new IdiomaBE();
                unidioma.Identidad = dataReader.GetInt64("ID_Idioma", 0);
                traduccionBE.Idioma = dalidioma.Select(unidioma);
            }




            traduccionBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            traduccionBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return traduccionBE;
        }

        public void Dispose()
        {

        }


        #endregion
    }
}
