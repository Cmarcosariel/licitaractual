﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class UsuarioPermisoDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public UsuarioPermisoDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }


        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the UsuarioPermiso table.
        /// </summary>
        public void Insert(UsuarioPermisoBE usuarioPermiso)
        {
            ValidationUtility.ValidateArgument("usuarioPermiso", usuarioPermiso);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", usuarioPermiso.Usuario.Identidad),
                new SqlParameter("@ID_Permiso", usuarioPermiso.Permiso.Identidad),
                new SqlParameter("@DELETED", usuarioPermiso.DELETED),
                new SqlParameter("@CreateDate", usuarioPermiso.CreateDate),
                new SqlParameter("@ChangeDate", usuarioPermiso.ChangeDate)
            };

            /*usuarioPermiso.Identidad = (long)*/SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the UsuarioPermiso table.
        /// </summary>
        public void Update(UsuarioPermisoBE usuarioPermiso)
        {
            ValidationUtility.ValidateArgument("usuarioPermiso", usuarioPermiso);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", usuarioPermiso.Identidad),
                new SqlParameter("@ID_Usuario", usuarioPermiso.Usuario.Identidad),
                new SqlParameter("@ID_Permiso", usuarioPermiso.Permiso.Identidad),
                new SqlParameter("@DELETED", usuarioPermiso.DELETED),
                new SqlParameter("@CreateDate", usuarioPermiso.CreateDate),
                new SqlParameter("@ChangeDate", usuarioPermiso.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the UsuarioPermiso table by its primary key.
        /// </summary>
        public void Delete(UsuarioPermisoBE _rolpermiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
            new SqlParameter("@ID_Usuario", _rolpermiso.Usuario.Identidad),
            new SqlParameter("@ID_Permiso", _rolpermiso.Permiso.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoDeleteLogico", parameters);
        }

        /// <summary>
        /// Deletes a record from the UsuarioPermiso table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Permiso(PermisoBE _Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Permiso", _Permiso.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoDeleteAllByID_Permiso", parameters);
        }


        public void DeleteAllByCodigo_Perfil(PermisoBE objeto)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Codigo_Perfil", objeto.Codigo_Perfil)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoDeleteAllByCodigo_Perfil", parameters);
        }

        /// <summary>
        /// Deletes a record from the UsuarioPermiso table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Usuario(long iD_Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", iD_Usuario)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoDeleteAllByID_Usuario", parameters);
        }

        /// <summary>
        /// Selects a single record from the UsuarioPermiso table.
        /// </summary>
        public UsuarioPermisoBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the UsuarioPermiso table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the UsuarioPermiso table.
        /// </summary>
        public List<UsuarioPermisoBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelectAll"))
            {
                List<UsuarioPermisoBE> usuarioPermisoBEList = new List<UsuarioPermisoBE>();
                while (dataReader.Read())
                {
                    UsuarioPermisoBE usuarioPermisoBE = MapDataReader(dataReader);
                    usuarioPermisoBEList.Add(usuarioPermisoBE);
                }

                return usuarioPermisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the UsuarioPermiso table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelectAll");
        }

        /// <summary>
        /// Selects all records from the UsuarioPermiso table by a foreign key.
        /// </summary>
        public List<UsuarioPermisoBE> SelectAllByID_Permiso(long iD_Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Permiso", iD_Permiso)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelectAllByID_Permiso", parameters))
            {
                List<UsuarioPermisoBE> usuarioPermisoBEList = new List<UsuarioPermisoBE>();
                while (dataReader.Read())
                {
                    UsuarioPermisoBE usuarioPermisoBE = MapDataReader(dataReader);
                    usuarioPermisoBEList.Add(usuarioPermisoBE);
                }

                return usuarioPermisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the UsuarioPermiso table by a foreign key.
        /// </summary>
        public List<UsuarioPermisoBE> SelectAllByID_Usuario(UsuarioBE _Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", _Usuario.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelectAllByID_Usuario", parameters))
            {
                List<UsuarioPermisoBE> usuarioPermisoBEList = new List<UsuarioPermisoBE>();
                while (dataReader.Read())
                {
                    UsuarioPermisoBE usuarioPermisoBE = MapDataReader(dataReader);
                    usuarioPermisoBEList.Add(usuarioPermisoBE);
                }

                return usuarioPermisoBEList;
            }
        }

        /// <summary>
        /// Selects all records from the UsuarioPermiso table by a foreign key.
        /// </summary>
        public string SelectAllByID_PermisoJson(long iD_Permiso)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Permiso", iD_Permiso)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelectAllByID_Permiso", parameters);
        }

        /// <summary>
        /// Selects all records from the UsuarioPermiso table by a foreign key.
        /// </summary>
        public string SelectAllByID_UsuarioJson(long iD_Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", iD_Usuario)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioPermisoSelectAllByID_Usuario", parameters);
        }

        /// <summary>
        /// Creates a new instance of the UsuarioPermisoBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private UsuarioPermisoBE MapDataReader(SqlDataReader dataReader)
        {
            UsuarioPermisoBE usuarioPermisoBE = new UsuarioPermisoBE();
            usuarioPermisoBE.Identidad = dataReader.GetInt64("Identidad", 0);

            using (DAL.UsuarioDAL dalusuario = new UsuarioDAL("PRD"))
            {
                UsuarioBE usuariotmp = new UsuarioBE() { Identidad = dataReader.GetInt64("ID_Usuario", 0) };

                usuarioPermisoBE.Usuario = dalusuario.Select(usuariotmp);
            }

            using (DAL.PermisoDAL dalpermiso = new PermisoDAL("PRD"))
            {

                PermisoBE permisotmp = new PermisoBE() { Identidad = dataReader.GetInt64("ID_Permiso", 0) };
                usuarioPermisoBE.Permiso = dalpermiso.Select(permisotmp);
            }





            usuarioPermisoBE.DELETED = dataReader.GetBoolean("DELETED", false);
            usuarioPermisoBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            usuarioPermisoBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return usuarioPermisoBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}

