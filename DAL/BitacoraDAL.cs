﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;

namespace DAL
{
    public class BitacoraDAL : IDisposable
    {
        #region Fields

        private readonly string connectionStringName;

        #endregion

        #region Constructors

        public BitacoraDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Bitacora table.
        /// </summary>
        public void Insert(BitacoraBE bitacora)
        {
            ValidationUtility.ValidateArgument("bitacora", bitacora);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Timestamp", bitacora.Timestamp),
                new SqlParameter("@Descripcion", bitacora.Descripcion),
                new SqlParameter("@ID_Evento_Bitacora", bitacora.Evento_Bitacora.Identidad),
                new SqlParameter("@DVH", bitacora.DVH),
                new SqlParameter("@ID_Usuario", bitacora.Usuario.Identidad),
                new SqlParameter("@CreateDate", bitacora.CreateDate),
                new SqlParameter("@ChangeDate", bitacora.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BitacoraInsert", parameters);
        }

        /// <summary>
        /// Updates a record in the Bitacora table.
        /// </summary>
        public void Update(BitacoraBE bitacora)
        {
            ValidationUtility.ValidateArgument("bitacora", bitacora);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", bitacora.Identidad),
                new SqlParameter("@Timestamp", bitacora.Timestamp),
                new SqlParameter("@Descripcion", bitacora.Descripcion),
                new SqlParameter("@ID_Evento_Bitacora", bitacora.Evento_Bitacora.Identidad),
                new SqlParameter("@DVH", bitacora.DVH),
                new SqlParameter("@ID_Usuario", bitacora.Usuario.Identidad),
                new SqlParameter("@CreateDate", bitacora.CreateDate),
                new SqlParameter("@ChangeDate", bitacora.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BitacoraUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Bitacora table by its primary key.
        /// </summary>
        public void Delete(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BitacoraDelete", parameters);
        }

        /// <summary>
        /// Deletes a record from the Bitacora table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Evento_Bitacora(long iD_Evento_Bitacora)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Evento_Bitacora", iD_Evento_Bitacora)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BitacoraDeleteAllByID_Evento_Bitacora", parameters);
        }

        /// <summary>
        /// Deletes a record from the Bitacora table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Usuario(long iD_Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", iD_Usuario)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "BitacoraDeleteAllByID_Usuario", parameters);
        }

        /// <summary>
        /// Selects a single record from the Bitacora table.
        /// </summary>
        public BitacoraBE Select(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "BitacoraSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Selects a single record from the Bitacora table.
        /// </summary>
        //public string SelectJson(long identidad)
        //{
        //    SqlParameter[] parameters = new SqlParameter[]
        //    {
        //        new SqlParameter("@Identidad", identidad)
        //    };

        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "BitacoraSelect", parameters);
        //}

        /// <summary>
        /// Selects all records from the Bitacora table.
        /// </summary>
        public List<BitacoraBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "BitacoraSelectAll"))
            {
                List<BitacoraBE> bitacoraBEList = new List<BitacoraBE>();
                while (dataReader.Read())
                {
                    BitacoraBE bitacoraBE = MapDataReader(dataReader);
                    bitacoraBEList.Add(bitacoraBE);
                }

                return bitacoraBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Bitacora table.
        /// </summary>
        //public string SelectAllJson()
        //{
        //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "BitacoraSelectAll");
        //}

        ///// <summary>
        /// Selects all records from the Bitacora table by a foreign key.
        /// </summary>
        public List<BitacoraBE> SelectAllByID_Evento_Bitacora(long iD_Evento_Bitacora)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Evento_Bitacora", iD_Evento_Bitacora)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "BitacoraSelectAllByID_Evento_Bitacora", parameters))
            {
                List<BitacoraBE> bitacoraBEList = new List<BitacoraBE>();
                while (dataReader.Read())
                {
                    BitacoraBE bitacoraBE = MapDataReader(dataReader);
                    bitacoraBEList.Add(bitacoraBE);
                }

                return bitacoraBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Bitacora table by a foreign key.
        /// </summary>
        public List<BitacoraBE> SelectAllByID_Usuario(long iD_Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Usuario", iD_Usuario)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "BitacoraSelectAllByID_Usuario", parameters))
            {
                List<BitacoraBE> bitacoraBEList = new List<BitacoraBE>();
                while (dataReader.Read())
                {
                    BitacoraBE bitacoraBE = MapDataReader(dataReader);
                    bitacoraBEList.Add(bitacoraBE);
                }

                return bitacoraBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Bitacora table by a foreign key.
        /// </summary>
        //public string SelectAllByID_Evento_BitacoraJson(long iD_Evento_Bitacora)
        //{
        //	SqlParameter[] parameters = new SqlParameter[]
        //	{
        //		new SqlParameter("@ID_Evento_Bitacora", iD_Evento_Bitacora)
        //	};

        //	return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "BitacoraSelectAllByID_Evento_Bitacora", parameters);
        //}

        /// <summary>
        /// Selects all records from the Bitacora table by a foreign key.
        /// </summary>
        //public string SelectAllByID_UsuarioJson(long iD_Usuario)
        //{
        //	SqlParameter[] parameters = new SqlParameter[]
        //	{
        //		new SqlParameter("@ID_Usuario", iD_Usuario)
        //	};

        //	return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "BitacoraSelectAllByID_Usuario", parameters);
        //}

        /// <summary>
        /// Creates a new instance of the BitacoraBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private BitacoraBE MapDataReader(SqlDataReader dataReader)
        {
            BitacoraBE bitacoraBE = new BitacoraBE
            {
                Identidad = dataReader.GetInt64("Identidad", 0),
                Timestamp = dataReader.GetInt64("Timestamp", 0),
                Descripcion = dataReader.GetString("Descripcion", null),
                DVH = dataReader.GetString("DVH", null)
            };
            using (Evento_BitacoraDAL dal = new Evento_BitacoraDAL("PRD"))
            {
                bitacoraBE.Evento_Bitacora = dal.Select(dataReader.GetInt64("ID_Evento_Bitacora", 0));
            }
            using (DAL.UsuarioDAL dalusuario = new DAL.UsuarioDAL("PRD"))
            {
                UsuarioBE usuariotmp = new UsuarioBE() { Identidad = dataReader.GetInt64("ID_Usuario", 0) };
                bitacoraBE.Usuario = dalusuario.Select(usuariotmp);
            }
            bitacoraBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            bitacoraBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return bitacoraBE;
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
