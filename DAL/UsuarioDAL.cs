﻿
using BE;

using SharpCore.Extensions;
using SharpCore.Utilities;
//using SharpCore.Data;
//using SharpCore.Extensions;
//using SharpCore.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
    public class UsuarioDAL : IDisposable
    {
        #region Fields

        private string connectionStringName;

        #endregion

        #region Constructors

        public UsuarioDAL()
        {

            this.connectionStringName = ConfigurationManager.ConnectionStrings["PRD"].ConnectionString;
        }

        public UsuarioDAL(string connectionStringName)
        {
            ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves a record to the Usuario table.
        /// </summary>
        public void Insert(UsuarioBE usuario)
        {
            ValidationUtility.ValidateArgument("usuario", usuario);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@NombreUsuario", usuario.NombreUsuario),
                new SqlParameter("@NombrePersona", usuario.NombrePersona),
                new SqlParameter("@ApellidoPersona", usuario.ApellidoPersona),
                new SqlParameter("@Clave", usuario.Clave),
                new SqlParameter("@LoginFails", usuario.LoginFails),
                new SqlParameter("@IsBlocked", usuario.IsBlocked),
                new SqlParameter("@DELETED", usuario.DELETED),
                new SqlParameter("@DVH", usuario.DVH),
                new SqlParameter("@ID_Idioma", usuario.Idioma.Identidad),
                new SqlParameter("@ID_Organizacion", usuario.Organizacion.Identidad),
                new SqlParameter("@CreateDate", usuario.CreateDate),
                new SqlParameter("@ChangeDate", usuario.ChangeDate)
            };

            ///*usuario.Identidad = (Int32)*/SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "UsuarioInsert", parameters);


            DataTable dt = (DataTable)(SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "UsuarioInsert", parameters));

            if (dt.Rows[0].ItemArray[0].GetType() == typeof(DBNull))
            {
                return;
            }

            usuario.Identidad = Convert.ToInt64(dt.Rows[0].ItemArray[0].ToString());


        }

        /// <summary>
        /// Updates a record in the Usuario table.
        /// </summary>
        public void Update(UsuarioBE usuario)
        {
            ValidationUtility.ValidateArgument("usuario", usuario);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", usuario.Identidad),
                new SqlParameter("@NombreUsuario", usuario.NombreUsuario),
                new SqlParameter("@NombrePersona", usuario.NombrePersona),
                new SqlParameter("@ApellidoPersona", usuario.ApellidoPersona),
                new SqlParameter("@Clave", usuario.Clave),
                new SqlParameter("@LoginFails", usuario.LoginFails),
                new SqlParameter("@IsBlocked", usuario.IsBlocked),
                new SqlParameter("@DELETED", usuario.DELETED),
                new SqlParameter("@DVH", usuario.DVH),
                new SqlParameter("@ID_Idioma", usuario.Idioma.Identidad),
                new SqlParameter("@ID_Organizacion", usuario.Organizacion.Identidad),
                new SqlParameter("@CreateDate", usuario.CreateDate),
                new SqlParameter("@ChangeDate", usuario.ChangeDate)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioUpdate", parameters);
        }

        /// <summary>
        /// Deletes a record from the Usuario table by its primary key.
        /// </summary>
        public void Delete(UsuarioBE usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", usuario.Identidad)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioDelete", parameters);
        }

        /// <summary>
        /// Deletes a record from the Usuario table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Idioma(long iD_Idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Idioma", iD_Idioma)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioDeleteAllByID_Idioma", parameters);
        }

        /// <summary>
        /// Deletes a record from the Usuario table by a foreign key.
        /// </summary>
        public void DeleteAllByID_Organizacion(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "UsuarioDeleteAllByID_Organizacion", parameters);
        }

        /// <summary>
        /// Selects a single record from the Usuario table.
        /// </summary>
        public UsuarioBE Select(UsuarioBE Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", Usuario.Identidad)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioSelect", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public List<BE.UsuarioBE> Select(IDictionary<string, string> dictionary)
        {
            BE.UsuarioBE usuarioDb = null;
            List<BE.UsuarioBE> usuarios = new List<BE.UsuarioBE>();
            IList<SqlParameter> parameters = new List<SqlParameter>();
            String id = null;
            String username = null;



            try
            {
                dictionary.TryGetValue("@Id", out id);
                dictionary.TryGetValue("@Username", out username);

                if (!String.IsNullOrEmpty(id))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Id", (id).ToString(), System.Data.DbType.Int32));
                }
                if (!String.IsNullOrEmpty(username))
                {
                    parameters.Add(DAL.SqlHelper.GetInstance().AddParameter("@Username", username, System.Data.DbType.String));
                }
                DataTable dt = DAL.SqlHelper.GetInstance().ExecuteQuery("ListarUsuariosFiltros", parameters);
                if (dt.Rows.Count == 0)
                {
                    usuarios = null;
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        UsuarioBE usuarioBE = new UsuarioBE();
                        usuarioBE.Identidad = Convert.ToInt64(row["Identidad"]);
                        usuarioBE.NombreUsuario = row["NombreUsuario"].ToString();
                        usuarioBE.NombrePersona = row["NombrePersona"].ToString();
                        usuarioBE.ApellidoPersona = row["ApellidoPersona"].ToString();
                        usuarioBE.Clave = row["Clave"].ToString();
                        usuarioBE.LoginFails = Convert.ToInt32(row["LoginFails"]);
                        usuarioBE.IsBlocked = (bool)row["IsBlocked"];
                        usuarioBE.DELETED = (bool)row["DELETED"];
                        usuarioBE.DVH = row["DVH"].ToString();

                        using (DAL.IdiomaDAL dalidioma = new DAL.IdiomaDAL("PRD"))
                        {
                            BE.IdiomaBE idiomaBE = new BE.IdiomaBE();
                            idiomaBE.Identidad = Convert.ToInt64(row["ID_Idioma"]);
                            usuarioBE.Idioma = dalidioma.Select(idiomaBE);
                        }

                        using (DAL.OrganizacionDAL dalorganizacion = new DAL.OrganizacionDAL("PRD"))
                        {
                            usuarioBE.Organizacion = dalorganizacion.Select(new OrganizacionBE {Identidad = Convert.ToInt64(row["ID_Organizacion"]) });
                        }




                        usuarioBE.CreateDate = Convert.ToInt64(row["CreateDate"]);
                        usuarioBE.ChangeDate = Convert.ToInt64(row["ChangeDate"]);

                        usuarios.Add(usuarioBE);
                    }

                }

                return usuarios;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Selects a single record from the Usuario table.
        /// </summary>
        public string SelectJson(long identidad)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Identidad", identidad)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioSelect", parameters);
        }

        /// <summary>
        /// Selects all records from the Usuario table.
        /// </summary>
        public List<UsuarioBE> SelectAll()
        {
            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectAll"))
            {
                List<UsuarioBE> usuarioBEList = new List<UsuarioBE>();
                while (dataReader.Read())
                {
                    UsuarioBE usuarioBE = MapDataReader(dataReader);
                    usuarioBEList.Add(usuarioBE);
                }
                return usuarioBEList;
            }

        }

        /// <summary>
        /// Selects all records from the Usuario table.
        /// </summary>
        public string SelectAllJson()
        {
            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectAll");
        }

        /// <summary>
        /// Selects all records from the Usuario table by a foreign key.
        /// </summary>
        public List<UsuarioBE> SelectAllByID_Idioma(long iD_Idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Idioma", iD_Idioma)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectAllByID_Idioma", parameters))
            {
                List<UsuarioBE> usuarioBEList = new List<UsuarioBE>();
                while (dataReader.Read())
                {
                    UsuarioBE usuarioBE = MapDataReader(dataReader);
                    usuarioBEList.Add(usuarioBE);
                }

                return usuarioBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Usuario table by a foreign key.
        /// </summary>
        public List<UsuarioBE> SelectAllByID_Organizacion(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectAllByID_Organizacion", parameters))
            {
                List<UsuarioBE> usuarioBEList = new List<UsuarioBE>();
                while (dataReader.Read())
                {
                    UsuarioBE usuarioBE = MapDataReader(dataReader);
                    usuarioBEList.Add(usuarioBE);
                }

                return usuarioBEList;
            }
        }

        /// <summary>
        /// Selects all records from the Usuario table by a foreign key.
        /// </summary>
        public string SelectAllByID_IdiomaJson(long iD_Idioma)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Idioma", iD_Idioma)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectAllByID_Idioma", parameters);
        }

        /// <summary>
        /// Selects all records from the Usuario table by a foreign key.
        /// </summary>
        public string SelectAllByID_OrganizacionJson(long iD_Organizacion)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@ID_Organizacion", iD_Organizacion)
            };

            return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectAllByID_Organizacion", parameters);
        }

        /// <summary>
        /// Creates a new instance of the UsuarioBE class and populates it with data from the specified SqlDataReader.
        /// </summary>
        private UsuarioBE MapDataReader(SqlDataReader dataReader)
        {
            UsuarioBE usuarioBE = new UsuarioBE();
            usuarioBE.Identidad = dataReader.GetInt64("Identidad", 0);
            usuarioBE.NombreUsuario = dataReader.GetString("NombreUsuario", null);
            usuarioBE.NombrePersona = dataReader.GetString("NombrePersona", null);
            usuarioBE.ApellidoPersona = dataReader.GetString("ApellidoPersona", null);
            usuarioBE.Clave = dataReader.GetString("Clave", null);
            usuarioBE.LoginFails = dataReader.GetInt32("LoginFails", 0);
            usuarioBE.IsBlocked = dataReader.GetBoolean("IsBlocked", false);
            usuarioBE.DELETED = dataReader.GetBoolean("DELETED", false);
            usuarioBE.DVH = dataReader.GetString("DVH", null);

            using (DAL.IdiomaDAL dalidioma = new DAL.IdiomaDAL("PRD"))
            {
                BE.IdiomaBE idiomaBE = new BE.IdiomaBE();
                idiomaBE.Identidad = dataReader.GetInt64("ID_Idioma", 0);
                usuarioBE.Idioma = dalidioma.Select(idiomaBE);
            }

            using (DAL.OrganizacionDAL dalorganizacion = new DAL.OrganizacionDAL("PRD"))
            {
                usuarioBE.Organizacion = dalorganizacion.Select(new OrganizacionBE { Identidad = dataReader.GetInt64("ID_Organizacion", 0) });
            }




            usuarioBE.CreateDate = dataReader.GetInt64("CreateDate", 0);
            usuarioBE.ChangeDate = dataReader.GetInt64("ChangeDate", 0);

            return usuarioBE;
        }

        public void Dispose()
        {
        }

        public UsuarioBE SelectByNombre(UsuarioBE _Usuario)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@NombreUsuario", _Usuario.NombreUsuario)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioSelectByNombre", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }

        public UsuarioBE Select(string nombre, string password)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Nombre", nombre),
                 new SqlParameter("@Clave", password)
            };

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "UsuarioLogin", parameters))
            {
                if (dataReader.Read())
                {
                    return MapDataReader(dataReader);
                }
                else
                {
                    return null;
                }
            }
        }


        public void CambiarIdioma(BE.UsuarioBE usuario, BE.IdiomaBE idioma)
        {
            try
            {
                IList<SqlParameter> parameters = new List<SqlParameter>()
                {

                    DAL.SqlHelper.GetInstance().AddParameter("@IdIdioma", idioma.Identidad.ToString(), System.Data.DbType.Int32),
                    DAL.SqlHelper.GetInstance().AddParameter("@IdUsuario", usuario.Identidad.ToString(), System.Data.DbType.Int32)

                };

                DAL.SqlHelper.GetInstance().ExecuteNonQuery("CambiarIdioma", parameters);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}

