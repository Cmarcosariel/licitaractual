﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;


namespace DAL
{
    //public class Estado_OfertaDAL
    //{
    //    //#region Fields

    //    //private string connectionStringName;

    //    //#endregion

    //    //#region Constructors

    //    //public Estado_OfertaDAL(string connectionStringName)
    //    //{
    //    //    ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

    //    //    this.connectionStringName = connectionStringName;
    //    //}

    //    //#endregion

    //    //#region Methods

    //    ///// <summary>
    //    ///// Saves a record to the Estado_Oferta table.
    //    ///// </summary>
    //    //public void Insert(Estado_OfertaBE estado_Oferta)
    //    //{
    //    //    ValidationUtility.ValidateArgument("estado_Oferta", estado_Oferta);

    //    //    SqlParameter[] parameters = new SqlParameter[]
    //    //    {
    //    //        new SqlParameter("@Descripcion", estado_Oferta.Descripcion)
    //    //    };

    //    //    estado_Oferta.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaInsert", parameters);
    //    //}

    //    ///// <summary>
    //    ///// Updates a record in the Estado_Oferta table.
    //    ///// </summary>
    //    //public void Update(Estado_OfertaBE estado_Oferta)
    //    //{
    //    //    ValidationUtility.ValidateArgument("estado_Oferta", estado_Oferta);

    //    //    SqlParameter[] parameters = new SqlParameter[]
    //    //    {
    //    //        new SqlParameter("@Identidad", estado_Oferta.Identidad),
    //    //        new SqlParameter("@Descripcion", estado_Oferta.Descripcion)
    //    //    };

    //    //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaUpdate", parameters);
    //    //}

    //    ///// <summary>
    //    ///// Deletes a record from the Estado_Oferta table by its primary key.
    //    ///// </summary>
    //    //public void Delete(long identidad)
    //    //{
    //    //    SqlParameter[] parameters = new SqlParameter[]
    //    //    {
    //    //        new SqlParameter("@Identidad", identidad)
    //    //    };

    //    //    SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaDelete", parameters);
    //    //}

    //    ///// <summary>
    //    ///// Selects a single record from the Estado_Oferta table.
    //    ///// </summary>
    //    //public Estado_OfertaBE Select(long identidad)
    //    //{
    //    //    SqlParameter[] parameters = new SqlParameter[]
    //    //    {
    //    //        new SqlParameter("@Identidad", identidad)
    //    //    };

    //    //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaSelect", parameters))
    //    //    {
    //    //        if (dataReader.Read())
    //    //        {
    //    //            return MapDataReader(dataReader);
    //    //        }
    //    //        else
    //    //        {
    //    //            return null;
    //    //        }
    //    //    }
    //    //}

    //    ///// <summary>
    //    ///// Selects a single record from the Estado_Oferta table.
    //    ///// </summary>
    //    //public string SelectJson(long identidad)
    //    //{
    //    //    SqlParameter[] parameters = new SqlParameter[]
    //    //    {
    //    //        new SqlParameter("@Identidad", identidad)
    //    //    };

    //    //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaSelect", parameters);
    //    //}

    //    ///// <summary>
    //    ///// Selects all records from the Estado_Oferta table.
    //    ///// </summary>
    //    //public List<Estado_OfertaBE> SelectAll()
    //    //{
    //    //    using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaSelectAll"))
    //    //    {
    //    //        List<Estado_OfertaBE> estado_OfertaBEList = new List<Estado_OfertaBE>();
    //    //        while (dataReader.Read())
    //    //        {
    //    //            Estado_OfertaBE estado_OfertaBE = MapDataReader(dataReader);
    //    //            estado_OfertaBEList.Add(estado_OfertaBE);
    //    //        }

    //    //        return estado_OfertaBEList;
    //    //    }
    //    //}

    //    ///// <summary>
    //    ///// Selects all records from the Estado_Oferta table.
    //    ///// </summary>
    //    //public string SelectAllJson()
    //    //{
    //    //    return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Estado_OfertaSelectAll");
    //    //}

    //    ///// <summary>
    //    ///// Creates a new instance of the Estado_OfertaBE class and populates it with data from the specified SqlDataReader.
    //    ///// </summary>
    //    //private Estado_OfertaBE MapDataReader(SqlDataReader dataReader)
    //    //{
    //    //    Estado_OfertaBE estado_OfertaBE = new Estado_OfertaBE();
    //    //    estado_OfertaBE.Identidad = dataReader.GetInt64("Identidad", 0);
    //    //    estado_OfertaBE.Descripcion = dataReader.GetString("Descripcion", null);

    //    //    return estado_OfertaBE;
    //    //}

    //    //public void Dispose()
    //    //{
    //    //}

    //    //#endregion
    //}
}
