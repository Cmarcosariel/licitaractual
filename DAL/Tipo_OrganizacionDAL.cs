﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpCore.Extensions;
using SharpCore.Utilities;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    //public class Tipo_OrganizacionDAL
    //{
    //    #region Fields

    //    private string connectionStringName;

    //    #endregion

    //    #region Constructors

    //    public Tipo_OrganizacionDAL(string connectionStringName)
    //    {
    //        ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

    //        this.connectionStringName = connectionStringName;
    //    }

    //    #endregion

    //    #region Methods

    //    /// <summary>
    //    /// Saves a record to the Tipo_Organizacion table.
    //    /// </summary>
    //    public void Insert(Tipo_OrganizacionBE tipo_Organizacion)
    //    {
    //        ValidationUtility.ValidateArgument("tipo_Organizacion", tipo_Organizacion);

    //        SqlParameter[] parameters = new SqlParameter[]
    //        {
    //            new SqlParameter("@Descripcion", tipo_Organizacion.Descripcion)
    //        };

    //        tipo_Organizacion.Identidad = (long)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionInsert", parameters);
    //    }

    //    /// <summary>
    //    /// Updates a record in the Tipo_Organizacion table.
    //    /// </summary>
    //    public void Update(Tipo_OrganizacionBE tipo_Organizacion)
    //    {
    //        ValidationUtility.ValidateArgument("tipo_Organizacion", tipo_Organizacion);

    //        SqlParameter[] parameters = new SqlParameter[]
    //        {
    //            new SqlParameter("@Identidad", tipo_Organizacion.Identidad),
    //            new SqlParameter("@Descripcion", tipo_Organizacion.Descripcion)
    //        };

    //        SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionUpdate", parameters);
    //    }

    //    /// <summary>
    //    /// Deletes a record from the Tipo_Organizacion table by its primary key.
    //    /// </summary>
    //    public void Delete(long identidad)
    //    {
    //        SqlParameter[] parameters = new SqlParameter[]
    //        {
    //            new SqlParameter("@Identidad", identidad)
    //        };

    //        SqlClientUtility.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionDelete", parameters);
    //    }

    //    /// <summary>
    //    /// Selects a single record from the Tipo_Organizacion table.
    //    /// </summary>
    //    public Tipo_OrganizacionBE Select(long identidad)
    //    {
    //        SqlParameter[] parameters = new SqlParameter[]
    //        {
    //            new SqlParameter("@Identidad", identidad)
    //        };

    //        using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionSelect", parameters))
    //        {
    //            if (dataReader.Read())
    //            {
    //                return MapDataReader(dataReader);
    //            }
    //            else
    //            {
    //                return null;
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Selects a single record from the Tipo_Organizacion table.
    //    /// </summary>
    //    public string SelectJson(long identidad)
    //    {
    //        SqlParameter[] parameters = new SqlParameter[]
    //        {
    //            new SqlParameter("@Identidad", identidad)
    //        };

    //        return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionSelect", parameters);
    //    }

    //    /// <summary>
    //    /// Selects all records from the Tipo_Organizacion table.
    //    /// </summary>
    //    public List<Tipo_OrganizacionBE> SelectAll()
    //    {
    //        using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionSelectAll"))
    //        {
    //            List<Tipo_OrganizacionBE> tipo_OrganizacionBEList = new List<Tipo_OrganizacionBE>();
    //            while (dataReader.Read())
    //            {
    //                Tipo_OrganizacionBE tipo_OrganizacionBE = MapDataReader(dataReader);
    //                tipo_OrganizacionBEList.Add(tipo_OrganizacionBE);
    //            }

    //            return tipo_OrganizacionBEList;
    //        }
    //    }

    //    /// <summary>
    //    /// Selects all records from the Tipo_Organizacion table.
    //    /// </summary>
    //    public string SelectAllJson()
    //    {
    //        return SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, "Tipo_OrganizacionSelectAll");
    //    }

    //    /// <summary>
    //    /// Creates a new instance of the Tipo_OrganizacionBE class and populates it with data from the specified SqlDataReader.
    //    /// </summary>
    //    private Tipo_OrganizacionBE MapDataReader(SqlDataReader dataReader)
    //    {
    //        Tipo_OrganizacionBE tipo_OrganizacionBE = new Tipo_OrganizacionBE();
    //        tipo_OrganizacionBE.Identidad = dataReader.GetInt64("Identidad", 0);
    //        tipo_OrganizacionBE.Descripcion = dataReader.GetString("Descripcion", null);

    //        return tipo_OrganizacionBE;
    //    }

    //    #endregion
    //}
}
