﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DAL
{
    public class SqlHelper
    {
        private static SqlHelper _dbHelper;
        private String _connString = ConfigurationManager.ConnectionStrings["PRD"].ConnectionString;
        private String _connStringMaster = ConfigurationManager.ConnectionStrings["MASTER"].ConnectionString;
        private SqlConnection _sqlConnection = null;
        private SqlCommand _sqlCommand = null;

        private SqlHelper(string connString)
        {
        }
        public static SqlHelper GetInstance()
        {
            return GetInstance("");
        }
        public static SqlHelper GetInstance(string connString)
        {
            if (_dbHelper == null)
            {
                _dbHelper = new SqlHelper(connString);
            }

            return _dbHelper;
        }


        public int ExecuteNonQueryMaster(string query, IList<SqlParameter> parametros)
        {
            _sqlCommand = new SqlCommand(query);
            int _result = 0;

            try
            {
                _sqlConnection = new SqlConnection(_connStringMaster);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();
                _result = _sqlCommand.ExecuteNonQuery();
                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return _result;
        }



        public int ExecuteNonQuery(string query, IList<SqlParameter> parametros)
        {
            _sqlCommand = new SqlCommand(query);
            int _result = 0;

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();
                _result = _sqlCommand.ExecuteNonQuery();
                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return _result;
        }

        public int ExecuteNonQueryText(string query, IList<SqlParameter> parametros)
        {
            _sqlCommand = new SqlCommand(query);
            int _result = 0;

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.Text;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();
                _result = _sqlCommand.ExecuteNonQuery();
                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return _result;
        }

        public int ExecuteNonQueryText(string query)
        {
            return ExecuteNonQueryText(query, new List<SqlParameter>());
        }

        public int ExecuteNonQuery(string query)
        {
            return ExecuteNonQuery(query, new List<SqlParameter>());
        }

        public DataTable ExecuteQuery(string query, IList<SqlParameter> parametros)
        {
            SqlCommand _sqlCommand = new SqlCommand(query);
            var dt = new DataTable();

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();

                using (SqlDataReader dr = _sqlCommand.ExecuteReader())
                {
                    dt.Load(dr);
                }

                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return dt;
        }

        public SqlDataReader ExecuteReader(string query, IList<SqlParameter> parametros)
        {
            SqlCommand _sqlCommand = new SqlCommand(query);
            //DataTable dt = new DataTable();

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.StoredProcedure;
              

                if (parametros != null)
                {
                    foreach (SqlParameter param in parametros)
                    {
                        _sqlCommand.Parameters.Add(param);
                    }
                }

                _sqlConnection.Open();

                return _sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //dt.Load(_sqlCommand.ExecuteReader());

                //_sqlConnection.Close();




            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                _sqlConnection.Close();
                throw e;
            }
            finally
            {

            }

            //return dr;
        }

        public DataTable ExecuteQueryText(string query, IList<SqlParameter> parametros)
        {
            SqlCommand _sqlCommand = new SqlCommand(query);
            var dt = new DataTable();

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.Text;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();

                using (SqlDataReader dr = _sqlCommand.ExecuteReader())
                {
                    dt.Load(dr);
                }

                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return dt;
        }

        public DataTable ExecuteQuery(string query)
        {
            return ExecuteQuery(query, new List<SqlParameter>());
        }

        public DataTable ExecuteQueryText(string query)
        {
            return ExecuteQueryText(query, new List<SqlParameter>());
        }

        public SqlParameter AddParameter(string name, string value, DbType type)
        {
            SqlParameter _parameter = new SqlParameter();
            _parameter.Value = value;
            _parameter.DbType = type;
            _parameter.ParameterName = name;

            return _parameter;
        }

        private void GuardarMensajeXML(Exception e)
        {
            //TODO

            string asd = e.GetType().ToString() + " " + e.Message + " " + DateTime.Now; 

            using (StreamWriter myWriter = new StreamWriter(@"C:\Users\Public\Prueba.xml", true))
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(string));
                mySerializer.Serialize(myWriter, asd);
            }
        }
    }
}
