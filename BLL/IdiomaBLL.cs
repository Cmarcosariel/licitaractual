﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;

namespace BLL
{
    public class IdiomaBLL : IDisposable
    {
        private IdiomaDAL _idiomaDAL;

        public IdiomaBLL()
        {
            _idiomaDAL = new IdiomaDAL("PRD");
        }

        public void Insert(IdiomaBE obj)
        {
            _idiomaDAL.Insert(obj);
        }

        public void Delete(IdiomaBE obj)
        {
            _idiomaDAL.Delete(obj);
        }

        public List<IdiomaBE> SelectAll()
        {
            return _idiomaDAL.SelectAll();
        }

        public void Update(IdiomaBE obj)
        {
            _idiomaDAL.Update(obj);
        }

        public BE.IdiomaBE GetByDescripcion(IdiomaBE idioma)
        {
            return _idiomaDAL.GetByDescripcion(idioma);
        }

        public void InsertIdiomaNuevo(IdiomaBE idioma, IdiomaBE idiomaComplemento)
        {

            _idiomaDAL.InsertIdiomaNuevo(idioma, idiomaComplemento);

        }

        //public IdiomaBE ObtenerPorCodigo(string codigo)
        //{
        //    return _idiomaDAL.SelectByDescripcion(codigo);
        //}

        public IdiomaBE idiomaBE(int id)
        {
            BE.IdiomaBE idiomaBE = new BE.IdiomaBE();
            idiomaBE.Identidad = id;
            return _idiomaDAL.Select(idiomaBE);
        }

        public IdiomaBE GuardarIdioma(UsuarioBE usuario, IdiomaBE idioma)
        {
            return _idiomaDAL.GuardarIdioma(usuario, idioma);
        }

        public List<TraduccionBE> ObtenerMensajesPorIdioma(IdiomaBE idioma)
        {
            return new TraduccionDAL("PRD").SelectAllByID_Idioma(idioma);
        }

        //public IdiomaBE ObtenerIdiomaPorUsuario(UsuarioBE usuario)
        //{
        //    return _idiomaDAL.Obtener(usuario.idioma.id);
        //}

        public IdiomaBE Select(BE.IdiomaBE idioma)
        {
            return _idiomaDAL.Select(idioma);
        }

        public void Dispose()
        {
        }

        public void UpdateTraduccion(TraduccionBE trad)
        {
            using (DAL.TraduccionDAL dal = new DAL.TraduccionDAL("PRD"))
            {
                dal.UpdateTraduccion(trad);
            }


        }
    }
}
