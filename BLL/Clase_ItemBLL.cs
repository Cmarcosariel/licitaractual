﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class Clase_ItemBLL : IDisposable, INTF.ICRUD<BE.Clase_ItemBE>
    {
        public void Delete(Clase_ItemBE objeto)
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
                dao.Delete(objeto);
            }
        }

        public void Dispose()
        {
        }

        public void Insert(Clase_ItemBE objeto)
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
                dao.Insert(objeto);
            }
        }

        public void Insert(Clase_ItemBE objeto,RubroBE rubro)
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
                dao.Insert(objeto,rubro);
            }
        }

        public Clase_ItemBE Select(Clase_ItemBE objeto)
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
                return dao.Select(objeto);
            }
        }

        public List<Clase_ItemBE> SelectAll()
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
               return dao.SelectAll();
            }
        }

        public void Update(Clase_ItemBE objeto)
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
                dao.Update(objeto);
            }
        }
        public void Update(Clase_ItemBE objeto, RubroBE rubro)
        {
            using (DAL.Clase_ItemDAL dao = new DAL.Clase_ItemDAL("PRD"))
            {
                dao.Update(objeto,rubro);
            }
        }

        public Dictionary<Clase_ItemBE, string> SP_reporteCatxCant()
        {
            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.SP_reporteCatxCant();
            }
        }

        public List<Clase_ItemBE> Select(IDictionary<string, string> dictionary)
        {

            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.Select(dictionary);
            }
        }

        public Clase_ItemBE GetClaseforItem(ItemBE item)
        {
            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.GetClaseforItem(item);
            }
        }

        public Dictionary<string, string> GetRubrosStringForClases()
        {
            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.GetRubrosStringForClases();
            }
        }

        public Clase_ItemBE Select(Clase_ItemBE org, RubroBE rub)
        {
            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.Select(org, rub);
            }
        }

        public List<Clase_ItemBE> SelectAllByID_Rubro(RubroBE rubroBE)
        {
            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.SelectAllByID_Rubro(rubroBE);
            }
        }

        public Dictionary<Clase_ItemBE, string> SP_reporteCatxCantFiltrarRubro(RubroBE rubroBE)
        {
            using (DAL.Clase_ItemDAL dal = new DAL.Clase_ItemDAL("PRD"))
            {
                return dal.SP_reporteCatxCantFiltrarRubro(rubroBE);
            }
        }
    }
}
