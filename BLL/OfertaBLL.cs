﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class OfertaBLL : IDisposable
    {
        //public void Delete(OfertaBE objeto)
        //{
        //    throw new NotImplementedException();
        //}

        public void Dispose()
        {

        }

        public void Insert(OfertaBE objeto)
        {
            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
                DAL.Insert(objeto);
            }
        }

        public OfertaBE Select(OfertaBE objeto)
        {
            using (DAL.OfertaDAL dao = new DAL.OfertaDAL("PRD"))
            {
                return dao.Select(objeto);
            }
        }

        //public List<OfertaBE> SelectAll()
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(OfertaBE objeto)
        {
            using (DAL.OfertaDAL dal = new DAL.OfertaDAL("PRD"))
            {
                dal.Update(objeto);
            }
        }

        public void Insert(OfertaBE nueva, LicitacionBE licitacionBE)
        {

            nueva.FechaPresentacion = DateTime.Now.Date;
            nueva.Estado_Oferta = Estado_OfertaBE.Borrador;

            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
                DAL.Insert(nueva,licitacionBE);
            }



            using (DAL.ItemsOfertaDAL dalitem = new DAL.ItemsOfertaDAL("PRD"))
            {
                foreach (ItemsOfertaBE item in nueva.items)
                {
                    dalitem.Insert(item,nueva);
                }


            }

        }

        internal void CalcularTarget(OfertaBE nueva, LicitacionBE licitacionBE)
        {
            nueva.Total = 0;

            foreach (ItemsOfertaBE item in nueva.items)
            {
                nueva.Total += (item.PrecioUnitario * (licitacionBE.Pliego.ItemsLicitacion.First(x => x.Item == item.Item)).Cantidad);
            }
        }


        public void CalcularTarget(DataTable dataTable, OfertaBE nueva)
        {
            nueva.Total = 0;

            foreach (DataRow item in dataTable.Rows)
            {
                nueva.Total += Convert.ToDecimal(item["Cantidad"]) * Convert.ToDecimal(item["PrecioUnitario"]);
            }

            ActualizarTotal(nueva);


        }

        private void ActualizarTotal(OfertaBE nueva)
        {
            using (DAL.OfertaDAL DAO = new DAL.OfertaDAL("PRD"))
            {
                DAO.ActualizarTotal(nueva);
            }
        }

        public OfertaBE SelectByLicitacionOrg(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
               return  DAL.SelectByLicitacionOrg(nueva, licitacionBE);
            }
        }

        public OfertaBE SelectByLicitacionOrgBorrador(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
                return DAL.SelectByLicitacionOrgBorrador(nueva, licitacionBE);
            }
        }

        public void UpdateEstadoOferta(OfertaBE miofertaparaItem)
        {
            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
                DAL.UpdateEstadoOferta(miofertaparaItem);
            }
        }

        public Dictionary<ItemBE, decimal> LlenarOfertaRecomendada(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
                return DAL.LlenarOfertaRecomendada(nueva, licitacionBE);
            }
        }

        public int SelectCountOfertasXlicitacion(LicitacionBE licitacionBE)
        {
            using (DAL.OfertaDAL DAL = new DAL.OfertaDAL("PRD"))
            {
                return DAL.SelectCountAllNotBorradorByID_Licitacion(licitacionBE);
            }
        }
    }
}
