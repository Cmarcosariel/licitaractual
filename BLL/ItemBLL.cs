﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class ItemBLL : IDisposable, INTF.ICRUD<BE.ItemBE>
    {
        public void Delete(ItemBE objeto)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                dao.Delete(objeto);
            }
        }

        public void Dispose()
        {

        }

        public void Insert(ItemBE objeto)
        {
            //using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            //{
            //    dao.Insert(objeto);
            //}
        }

        public ItemBE Select(ItemBE objeto)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                return dao.Select(objeto);
            }
        }

        public List<ItemBE> SelectAll()
        {
            using (DAL.ItemDAL dal = new DAL.ItemDAL("PRD"))
            {
                return dal.SelectAll();
            }
        }

        public void Update(ItemBE objeto)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                dao.Update(objeto);
            }
        }

        public List<ItemBE> Select(IDictionary<string, string> dictionary)
        {
            using (DAL.ItemDAL dal = new DAL.ItemDAL("PRD"))
            {
                return dal.Select(dictionary);
            }
        }

        public Dictionary<string, string> GetClasesStringForItems()
        {
            using (DAL.ItemDAL dal = new DAL.ItemDAL("PRD"))
            {
                return dal.GetClasesStringForItems();
            }
        }

        public void Insert(ItemBE nuevoitem, Clase_ItemBE clase_Item)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                dao.Insert(nuevoitem, clase_Item);
            }
        }

        public ItemBE SelectbyDescyClase(ItemBE nuevoitem, Clase_ItemBE clase_Item)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                return dao.SelectbyDescyClase(nuevoitem,clase_Item);
            }
        }

        public List<ItemBE> SelectAllByID_Clase(Clase_ItemBE clase_ItemBE)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                return dao.SelectAllByID_Clase(clase_ItemBE);
            }
        }

        public ItemBE SelectByDesc(ItemBE nuevoitem)
        {
            using (DAL.ItemDAL dao = new DAL.ItemDAL("PRD"))
            {
                return dao.SelectByDesc(nuevoitem);
            }
        }
    }
}
