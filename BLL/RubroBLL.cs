﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class RubroBLL : IDisposable, INTF.ICRUD<BE.RubroBE>
    {
        public void Delete(RubroBE objeto)
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                dao.Delete(objeto);
            }
        }

        public void Dispose()
        {
        }

        public void Insert(RubroBE objeto)
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                dao.Insert(objeto);
            }
        }

        public RubroBE Select(RubroBE objeto)
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                return dao.Select(objeto);
            }
        }

        public List<RubroBE> SelectAll()
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                return dao.SelectAll();
            }
        }

        public void Update(RubroBE objeto)
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                dao.Update(objeto);
            }
        }

        public List<BE.RubroBE> Select(IDictionary<string, string> dictionary)
        {

            using (DAL.RubroDAL dal = new DAL.RubroDAL("PRD"))
            {
                return dal.Select(dictionary);
            }

        }

        public RubroBE GetRubrofromClaseItem(Clase_ItemBE organizacion)
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                return dao.GetRubrofromClaseItem(organizacion);
            }
        }

        public RubroBE SelectRubroCompleto(RubroBE rubroBE)
        {
            using (DAL.RubroDAL dao = new DAL.RubroDAL("PRD"))
            {
                return dao.SelectRubroCompleto(rubroBE);
            }
        }
    }
}
