﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Reflection;
using INTF;

namespace BLL
{
    public abstract class VerificarBLL
    {

        public void RecalcularDvhTotalBase(Type t)
        {
            try
            {
                string typeName = t.Name;
                string typeNameLimpio = typeName.Replace("BE", "");
                Type objectType = Type.GetType("BLL." + typeNameLimpio + "BLL");

                IList listaObjetos = ObtenerListaPorEntidad(objectType);

                foreach (INTF.IVerificable item in ObtenerListaPorEntidad(objectType))
                {
                    item.DVH = CalcularVerificacionHorizontal(item);

                    MethodInfo listarMethod = objectType.GetMethod("UpdateSinDVH");
                    object listarResultObj = listarMethod.Invoke(this, BindingFlags.Public, null, new object[] { item }, null);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList ObtenerListaPorEntidad(Type entidadType)
        {
            IList listarResult;
            try
            {
                MethodInfo listarMethod = entidadType.GetMethod("SelectAll");
                object listarResultObj = listarMethod.Invoke(this, BindingFlags.Public, null, null, null);

                listarResult = (IList)listarResultObj;

            }
            catch (Exception e)
            {
                throw e;
            }

            return listarResult;
        }

        public void Dispose()
        {
        }

        private string ObtenerDvvPorEntidad(Type entidadType)
        {
            string dvvTotal = string.Empty;
            try
            {
                foreach (IEntidad item in ObtenerListaPorEntidad(entidadType))
                {
                    Type fieldsType = item.GetType();
                    IEnumerable<PropertyInfo> fields = fieldsType.GetRuntimeProperties();
                    PropertyInfo field = fields.First(x => x.Name == "DVH");
                    object valor = field.GetValue(item);
                    string valorStr = valor.ToString();
                    dvvTotal += fields.First(x => x.Name == "DVH").GetValue(item).ToString();
                }
                dvvTotal = SEC.Encriptacion.EncriptarHash(dvvTotal);
            }
            catch (Exception)
            {
                throw;
            }
            return dvvTotal;
        }

        public void CalcularDvvBase(Type t)
        {
            string typeName = t.Name;
            string typeNameLimpio = typeName.Replace("BE", "");
            Type objectType = Type.GetType("BLL." + typeNameLimpio + "BLL");

            string dvvEntidad = ObtenerDvvPorEntidad(objectType);

            BE.DVVBE dvv = new BE.DVVBE();
            dvv.Descripcion = typeNameLimpio;
            dvv.Digito_Verificador = dvvEntidad;

            using (DAL.DVVDAL dal = new DAL.DVVDAL("PRD"))
            {
                dal.DeleteForRestore(typeNameLimpio);
                dal.Insert(dvv);

            }

        }
        public string CalcularVerificacionHorizontal(IVerificable entidad)
        {
            string dvh = string.Empty;

            try
            {
                Type fieldsType = entidad.GetType();
                IEnumerable<PropertyInfo> fields = fieldsType.GetRuntimeProperties();

                foreach (PropertyInfo field in fields)
                {
                    dvh += CalcularPropiedad(entidad, field);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return SEC.Encriptacion.EncriptarHash(dvh);
        }
        private string CalcularPropiedad(IVerificable entidad, PropertyInfo field)
        {
            int total = 0;
            string valor = string.Empty;
            try
            {
                var customAttributes = field.GetCustomAttributes();
                bool isNoVerificable = customAttributes.Any(x => x.GetType().Name == "NoVerificable");
                if (!isNoVerificable)
                {
                    //La propiedad es de tipo Entidad
                    if (typeof(IEntidad).IsAssignableFrom(field.PropertyType))
                    {
                        var _entidad = (field.GetValue(entidad) as IEntidad);
                        if (_entidad != null)
                        {
                            valor = _entidad.Identidad.ToString();
                        }
                    }
                    else
                    {
                        if (field.GetValue(entidad) == null)
                        {
                            valor = "0";
                        }
                        else
                        {
                            valor = field.GetValue(entidad).ToString();
                        }

                    }

                    if (!string.IsNullOrEmpty(valor))
                    {
                        total += valor.GetHashCode();
                        valor = SEC.Encriptacion.EncriptarHash(total.ToString());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return valor;
        }
        public string ObtenerDvv(IList<INTF.IVerificable> objetos)
        {
            string DVVsinhash = string.Empty;

            foreach (IVerificable item in objetos)
            {
                DVVsinhash += CalcularVerificacionHorizontal(item);
            }

            return SEC.Encriptacion.EncriptarHash(DVVsinhash).ToString();

        }
    }
}
