﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class ItemsOfertaBLL : IDisposable
    {
        //public void Delete(ItemsOfertaBE objeto)
        //{
        //    throw new NotImplementedException();
        //}

        public void Dispose()
        {
        }

        //public void Insert(ItemsOfertaBE objeto)
        //{
        //    throw new NotImplementedException();
        //}

        public ItemsOfertaBE Select(ItemsOfertaBE objeto)
        {
            using (DAL.ItemsOfertaDAL dal = new DAL.ItemsOfertaDAL("PRD"))
            {
                return dal.Select(objeto);
            }
        }

        //public List<ItemsOfertaBE> SelectAll()
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(ItemsOfertaBE objeto)
        {
            using (DAL.ItemsOfertaDAL dao = new DAL.ItemsOfertaDAL("PRD"))
            {
                dao.Update(objeto);
            }
        }

        public void Update(ItemsOfertaBE itemsOfertaBE, OfertaBE miofertaparaItem)
        {
            using (DAL.ItemsOfertaDAL dao = new DAL.ItemsOfertaDAL("PRD"))
            {
                dao.Update(itemsOfertaBE, miofertaparaItem);
            }
        }
    }
}
