﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class OrganizacionBLL : IDisposable, INTF.ICRUD<BE.OrganizacionBE>
    {
        public void Delete(OrganizacionBE objeto)
        {
            using (DAL.OrganizacionDAL dal = new DAL.OrganizacionDAL("PRD"))
            {
                dal.Delete(objeto);
            }
        }

        public void Dispose()
        {

        }

        public void Insert(OrganizacionBE objeto)
        {
            using (DAL.OrganizacionDAL dal = new DAL.OrganizacionDAL("PRD"))
            {
                dal.Insert(objeto);
            }
        }

        public OrganizacionBE Select(OrganizacionBE objeto)
        {
            using (DAL.OrganizacionDAL dal = new DAL.OrganizacionDAL("PRD"))
            {
               return dal.Select(objeto);
            }
        }

        public List<OrganizacionBE> SelectAll()
        {
            using (DAL.OrganizacionDAL dao = new DAL.OrganizacionDAL("PRD"))
            {
                return dao.SelectAll();
            }
        }


        public List<OrganizacionBE> SelectAllbyOrganizacion(BE.UsuarioBE usuario)
        {
            using (DAL.OrganizacionDAL dao = new DAL.OrganizacionDAL("PRD"))
            {

                if (usuario.Organizacion.CUIT == "9999999999" || usuario.Organizacion.Razon_Social == "LicitAR S.A." )
                {
                    return dao.SelectAll();
                }
                else
                {
                    List<OrganizacionBE> lista = new List<OrganizacionBE>();
                    lista.Add(dao.Select(usuario.Organizacion));
                    return lista;

                }

            }

        }

        public List<BE.OrganizacionBE> Select(IDictionary<string, string> dictionary)
        {

            using (DAL.OrganizacionDAL dal = new DAL.OrganizacionDAL("PRD"))
            {
                return dal.Select(dictionary);
            }

        }


        public void Update(OrganizacionBE objeto)
        {
            using (DAL.OrganizacionDAL dal = new DAL.OrganizacionDAL("PRD"))
            {
                dal.Update(objeto);
            }
        }


        public List<RubroBE> SelectRubros_Suscript_ByID_Organizacion(OrganizacionBE objeto)
        {
            using (DAL.OrganizacionDAL dao = new DAL.OrganizacionDAL("PRD"))
            {
                return dao.SelectRubros_Suscript_ByID_Organizacion(objeto);
            }
        }

        public void NuevaSuscripcion(OrganizacionBE org, RubroBE rubro)
        {
            using (DAL.OrganizacionDAL dao = new DAL.OrganizacionDAL("PRD"))
            {
                dao.NuevaSuscripcion(org,rubro);
            }
        }

        public void BajaSuscripcion(OrganizacionBE organizacion, RubroBE rubroBE)
        {
            using (DAL.OrganizacionDAL dao = new DAL.OrganizacionDAL("PRD"))
            {
                dao.BajaSuscripcion(organizacion, rubroBE);
            }
        }

        internal Dictionary<ItemBE, string> CalcularPerfilOferta(LicitacionBE licitacionBE, OfertaBE nueva)
        {
            using (DAL.OrganizacionDAL dal = new DAL.OrganizacionDAL("PRD"))
            {
                return dal.CalcularPerfilOferta(licitacionBE,nueva);
            }
        }
    }
}
