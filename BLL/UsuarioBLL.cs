﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class UsuarioBLL : VerificarBLL, IDisposable
    {
        #region singleton

        private static UsuarioBLL _instance;
        public UsuarioBLL() { }

        public static UsuarioBLL GetInstance()
        {
            if (_instance == null)
            {
                _instance = new UsuarioBLL();
                return _instance;
            }
            else return _instance;

        }

        public void Delete(UsuarioBE objeto)
        {

            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                dal.Delete(objeto);
            }

            RegenerarDVV();

        }



        public void Insert(UsuarioBE objeto)
        {

            objeto.DVH = CalcularVerificacionHorizontal(objeto);
            objeto.IsBlocked = false;
            objeto.LoginFails = 0;

            objeto.Clave = SEC.Encriptacion.EncriptarHash(objeto.Clave);


            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                dal.Insert(objeto);
            }

            RegenerarDVV();

        }

        #endregion

        public BE.UsuarioBE IntentarLogin(BE.UsuarioBE usuario)
        {
            string user = usuario.NombreUsuario;
            string pass = usuario.Clave;

            BE.UsuarioBE _usuario = null;

            try
            {
                using (DAL.UsuarioDAL dao = new DAL.UsuarioDAL("PRD"))
                {
                    _usuario = dao.SelectByNombre(usuario);

                    if (_usuario == null)
                    {
                        throw new Exception("ExcUsuarioNoExiste");
                    }

                    if (_usuario.IsBlocked == true)
                    {
                        NuevaBitacoraDeLogin(string.Format("El usuario {0} esta bloqueado e intento ingresar", _usuario.NombreUsuario), _usuario, 1);
                        throw new Exception("ExcUsuarioBloqueado");
                    }

                    if (!verificarIgualdadClaves(pass, _usuario.Clave))
                    {
                        IncrementarContadorClave(_usuario);

                        if (_usuario.LoginFails >= 3 && _usuario.NombreUsuario != "Administrador")
                        {
                            throw new Exception("ExcUsuarioBloqueado");
                        }
                        else
                        {
                            throw new Exception("ExcClaveMala");
                        }

                    }
                }

                //cargo el idioma
                CargarIdioma(_usuario);

                //limpio el contador de ingresos incorrectos
                limpiarLoginFails(_usuario);

                //cargo los permisos del usuario
                CargarPerfil(_usuario);



                NuevaBitacoraDeLogin(string.Format("Usuario {0} Ingreso al sistema", _usuario.NombreUsuario), _usuario, 1);

                return _usuario;

            }
            catch (Exception)
            {
                throw;
            }


        }

        public UsuarioBE Select(UsuarioBE Objeto)
        {
            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                return dal.Select(Objeto);
            }
        }


        public UsuarioBE SelectUsuarioyPermisos(UsuarioBE Objeto)
        {
            BE.UsuarioBE usuario; 

            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                usuario = dal.Select(Objeto);

                return CargarPerfil(usuario);
            }
        }

        public List<BE.UsuarioBE> Select(IDictionary<string, string> dictionary)
        {

            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                return dal.Select(dictionary);
            }

        }

        public List<UsuarioBE> SelectAll()
        {
            using (DAL.UsuarioDAL usuario = new DAL.UsuarioDAL("PRD"))
            {
                return usuario.SelectAll();
            }
        }

        public void Update(UsuarioBE Objeto)
        {
            Objeto.DVH = CalcularVerificacionHorizontal(Objeto);

            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                dal.Update(Objeto);
            }

            RegenerarDVV();
        }


        public UsuarioBE CargarPerfil(UsuarioBE Objeto)
        {

            Objeto.permisos.Clear();

            using (SEC.PermisoSEC permiso123 = new SEC.PermisoSEC())
            {

                List<PermisoBE> listadepermisosdeusuario = permiso123.SelectAllByID_Usuario(Objeto);
                
                foreach (PermisoBE item in listadepermisosdeusuario)
                {
                    PermisoBE permisotmp;
                    if (item.PermisoBase == false)
                    {
                        permisotmp = permiso123.SelectIndexado(item);

                    }
                    else
                    {
                         permisotmp = permiso123.Select(item);
                    }
                    Objeto.permisos.Add(permisotmp);
                }


            }
            return Objeto;

        }


        public void AssemblePermissions(List<BE.PermisoBE> listaasignar, List<BE.PermisoBE> listaasignar2)
        {
            List<RolPermisoBE> listarelacion = new List<RolPermisoBE>();

            PermisoBE permisohijo;
            PermisoBE permisopadre;

            foreach (PermisoBE item in listaasignar)
            {
                listaasignar2.Add(item);
            }

            foreach (PermisoBE padre in listaasignar)
            {
                using (DAL.RolPermisoDAL familiapatente = new DAL.RolPermisoDAL("PRD"))
                {
                    listarelacion.AddRange(familiapatente.SelectAllByID_Rol(padre));
                }
            }

            if (listarelacion.Count >= 1)
            {
                foreach (RolPermisoBE relacion in listarelacion)
                {
                    using (DAL.PermisoDAL buscador = new DAL.PermisoDAL("PRD"))
                    {
                        if (listaasignar.Count(p1 => p1.Identidad == relacion.Permiso.Identidad) > 0)
                        {
                            permisohijo = listaasignar.First(p => relacion.Permiso.Identidad == p.Identidad);
                            permisopadre = listaasignar.First(e => relacion.Rol.Identidad == e.Identidad);

                            permisopadre.Components.Add(permisohijo);
                            listaasignar2.RemoveAll(r => r.Identidad == permisohijo.Identidad);
                        }
                    }

                }
            }
        }


        public void RegenerarDVV()
        {
            using (DAL.UsuarioDAL dao = new DAL.UsuarioDAL("PRD"))
            {
                List<BE.UsuarioBE> clientList = new List<BE.UsuarioBE>();

                using (DAL.UsuarioDAL daousuario = new DAL.UsuarioDAL("PRD"))
                {
                    clientList = daousuario.SelectAll();
                }
                List<INTF.IVerificable> list = clientList.Select(c => (INTF.IVerificable)c).ToList();



                using (DAL.DVVDAL BE = new DAL.DVVDAL("PRD"))
                {
                    BE.DVVBE nuevodvv = new BE.DVVBE();

                    List<BE.DVVBE> listadvvs = BE.SelectAll();

                    listadvvs.First(x => x.Descripcion == "Usuario").Digito_Verificador = ObtenerDvv(list);

                    BE.Update(listadvvs.First(x => x.Descripcion == "Usuario"));
                }

            }
        }

        public UsuarioBE SelectByNombre(UsuarioBE usuario)
        {
            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
               return dal.SelectByNombre(usuario);
            }
        }

        public void NuevaBitacoraDeLogin(string registro, BE.UsuarioBE _usuario, int Severidad)
        {
            Evento_BitacoraBE eventonuevo = new Evento_BitacoraBE();
            eventonuevo.Descripcion = "LOGIN";
            eventonuevo.Severidad = Severidad;

            using (SEC.BitacoraSEC sec = new SEC.BitacoraSEC())
            {
                BE.BitacoraBE nuevoregistro = new BE.BitacoraBE();
                nuevoregistro.Descripcion = registro;
                using (DAL.Evento_BitacoraDAL dal = new DAL.Evento_BitacoraDAL("PRD"))
                {
                    nuevoregistro.Evento_Bitacora = dal.GetSeveridadEvento(eventonuevo);
                }
                nuevoregistro.Usuario = _usuario;
                nuevoregistro.Timestamp = DateTime.Now.Ticks;

                sec.GrabarBitacora(nuevoregistro);
            }

        }


        public bool verificarIgualdadClaves(String clave1, String clave2)
        {
            if (SEC.Encriptacion.EncriptarHash(clave1).Equals(clave2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void IncrementarContadorClave(BE.UsuarioBE Usuario)
        {
            using (DAL.UsuarioDAL dao3 = new DAL.UsuarioDAL("PRD"))
            {
                Usuario.LoginFails++;

                NuevaBitacoraDeLogin((string.Format("El usuario {0} ha ingresado una clave erronea", Usuario.NombreUsuario)), Usuario, 2);


                if (Usuario.LoginFails == 3 && Usuario.NombreUsuario != "Administrador")
                {
                    Usuario.IsBlocked = true;
                    NuevaBitacoraDeLogin((string.Format("El usuario {0} ha sido bloqueado", Usuario.NombreUsuario)), Usuario, 3);
                }

                Usuario.DVH = CalcularVerificacionHorizontal(Usuario);

                dao3.Update(Usuario);

                RegenerarDVV();

            }
        }

        public void limpiarLoginFails(BE.UsuarioBE Usuario)
        {
            using (DAL.UsuarioDAL dao3 = new DAL.UsuarioDAL("PRD"))
            {
                Usuario.LoginFails = 0;

                Usuario.DVH = CalcularVerificacionHorizontal(Usuario);

                dao3.Update(Usuario);

                RegenerarDVV();

            }
        }

        public void CargarIdioma(UsuarioBE usuario)
        {
            using (DAL.IdiomaDAL dal = new DAL.IdiomaDAL("PRD"))
            {
                usuario.Idioma = dal.Select(usuario.Idioma);
            }
        }


        public void cambiarIdioma(BE.UsuarioBE usuario, BE.IdiomaBE idioma)
        {
            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                dal.CambiarIdioma(usuario, idioma);
            }


        }

        public void RecalcularDvhTotal()
        {
            try
            {
                //IList<UsuarioBE> ususarios = new UsuarioBLL().SelectAllIList();

                Type t = typeof(UsuarioBE);

                RecalcularDvhTotalBase(t);
            }
            catch (Exception e)
            {
                //log.mensaje = e.Message.ToString();
                //log.evento = TipoEvento.Error;
                //gestorBitacora.Crear(log);
                throw e;
            }


        }

        public void GenerarDvv()
        {
            try
            {
                Type t = typeof(UsuarioBE);

                CalcularDvvBase(t);
            }
            catch (Exception e)
            {
                //log.mensaje = e.Message.ToString();
                //log.evento = TipoEvento.Error;
                //gestorBitacora.Crear(log);
                throw e;
            }
        }

        public void UpdateSinDVH(UsuarioBE Objeto)
        {
            //Objeto.DVH = CalcularVerificacionHorizontal(Objeto);

            using (DAL.UsuarioDAL dal = new DAL.UsuarioDAL("PRD"))
            {
                dal.Update(Objeto);
            }

            //RegenerarDVV();
        }

    }
}
