﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class LicitacionBLL : IDisposable, INTF.ICRUD<BE.LicitacionBE>
    {
        public void Delete(LicitacionBE objeto)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                dal.Delete(objeto);
            }
        }

        public void Dispose()
        {

        }

        public void Insert(LicitacionBE objeto)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                dal.Insert(objeto);
            }
        }

        public LicitacionBE Select(LicitacionBE objeto)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.Select(objeto);
            }
        }

        public List<LicitacionBE> SelectAll()
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectAll();
            }
        }

        public void Update(LicitacionBE objeto)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                dal.Update(objeto);
            }

        }

        public List<LicitacionBE> Select(IDictionary<string, string> dictionary)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.Select(dictionary);
            }
        }

        public LicitacionBE LicitacionSelectLastInsertbyOrgyRub(LicitacionBE licitacionBE)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.LicitacionSelectLastInsertbyOrgyRub(licitacionBE);
            }
        }

        public List<LicitacionBE> SelectAllByID_Organizacion(OrganizacionBE organizacion)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectAllByID_Organizacion(organizacion);
            }
        }

        public List<LicitacionBE> SelectAllVigente(OrganizacionBE organizacion)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectAllVigente(organizacion);
            }
        }

        public void CancelarLicitacion(LicitacionBE licitacionBE)
        {
            licitacionBE.Estado_Licitacion = EstadoLicitacionBE.Cancelada;

            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                dal.ActualizarEstado(licitacionBE);
            }
        }

        public List<LicitacionBE> SelectAllByID_Rubro(RubroBE rubroitem)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectAllByID_Rubro(rubroitem);
            }
        }

        public List<LicitacionBE> SelectActivos(IDictionary<string, string> dictionary)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectActivos(dictionary);
            }
        }

        public void CalcularPreciosTarget(LicitacionBE licitacionBE, OfertaBE nueva)
        {




            using (BLL.OfertaBLL dal = new BLL.OfertaBLL())
            {
                using (BLL.OrganizacionBLL org = new BLL.OrganizacionBLL())
                {

                    Dictionary<ItemBE, string> asd = org.CalcularPerfilOferta(licitacionBE, nueva);

                    dal.CalcularTarget(nueva, licitacionBE);
                }
            }


        }

        public List<LicitacionBE> SelectAllVigenteConOfertaDeMiOrg(OrganizacionBE organizacion)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectAllVigenteConOfertaDeMiOrg(organizacion);
            }
        }

        public List<OfertaBE> GetOfertasParaLicitacion(BE.LicitacionBE licitacionBE)
        {
            using (DAL.OfertaDAL dal = new DAL.OfertaDAL("PRD"))
            {
                return dal.SelectAllNotBorradorByID_Licitacion(licitacionBE);
            }
        }

        public void AceptarOferta(LicitacionBE licitacionBE, OfertaBE mioferta)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                dal.AceptarOferta(licitacionBE,mioferta);
            }
        }

        public void RechazarOferta(LicitacionBE licitacionBE, OfertaBE mioferta)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                dal.RechazarOferta(licitacionBE, mioferta);
            }
        }

        public List<LicitacionBE> SelectAllVigenteParaSuscripciones(OrganizacionBE organizacion)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectAllVigenteParaSuscripciones(organizacion);
            }
        }

        public void EnviarAlerta(LicitacionBE licitacionBE, OrganizacionBE organizacion)
        {

            using (DAL.OrganizacionDAL dao = new DAL.OrganizacionDAL("PRD"))
            {
                foreach (OrganizacionBE item in dao.SelectOrgBy_RubroSuscript(licitacionBE))
                {
                    if (item.Identidad != organizacion.Identidad)
                    {
                        SendEmail(item.Email, licitacionBE);
                    }



                }
            }


        }


        public void SendEmail(string correo, LicitacionBE licitacionBE)
        {

            string smtpAddress = "smtp.gmail.com";
            int portNumber = 587;
            bool enableSSL = true;

            using (System.Net.Mail.MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("licitAR.TFI@gmail.com");
                mail.To.Add(correo);
                mail.Subject = String.Format("Nueva Licitacion: {0}", licitacionBE.Descripcion);
                mail.Body = String.Format("Hay una nueva licitacion con fecha de apertura {0} cargada en el sistema", licitacionBE.FechaApertura.Date);
                mail.IsBodyHtml = true;
                //mail.Attachments.Add(new Attachment("D:\\TestFile.txt"));//--Uncomment this to send any attachment  
                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new System.Net.NetworkCredential("licitAR.TFI@gmail.com", "qweasd45!");
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        public List<LicitacionBE> SelectActivosParaUsuario(IDictionary<string, string> dictionary, OrganizacionBE organizacion)
        {
            using (DAL.LicitacionDAL dal = new DAL.LicitacionDAL("PRD"))
            {
                return dal.SelectActivosParaUsuario(dictionary, organizacion);
            }
        }
    }
}

