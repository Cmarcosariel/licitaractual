﻿using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC
{
    public class PermisoSEC : VerificarSEC, IDisposable
    {
        public void AddComponent(PermisoBE padre, PermisoBE hijo)
        {
            padre.Components.Add(hijo);
        }

        public void Delete(PermisoBE objeto)
        {

            if (objeto.Identidad == 10051 || objeto.Identidad == 1 || objeto.Identidad == 10044  || objeto.Codigo_Perfil == "ADM001" || objeto.Codigo_Perfil == "NEG001" || objeto.Codigo_Perfil == "NEG008")
            {
                return;
            }

            using (DAL.UsuarioPermisoDAL dao1 = new DAL.UsuarioPermisoDAL("PRD"))
            {
                dao1.DeleteAllByCodigo_Perfil(objeto);
            }


            using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
            {
                dao.DeleteAllByCodigo_Perfil(objeto);
                //dao.DeleteAllByID_Rol(objeto);
            }

            RegenerarDVVFamiliaPatente();

            using (DAL.PermisoDAL dao2 = new DAL.PermisoDAL("PRD"))
            {
                dao2.DeleteByCodigo_Perfil(objeto);
            }

            RegenerarDVV();
        }

        public PermisoBE SelectPlanoByCodigo_Perfil(PermisoBE permisoBE)
        {
            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return (dao.PermisoSelectbyCodigo(permisoBE));
            }
        }

        public PermisoBE SelectIndexadoByCodigo_Perfil(PermisoBE permisoBE)
        {
            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return SelectIndexado(dao.PermisoSelectbyCodigo(permisoBE));
            }
        }

        public List<PermisoBE> GetAllRoles()
        {
            using (DAL.PermisoDAL dao2 = new DAL.PermisoDAL("PRD"))
            {
                return dao2.GetAllRoles();
            }
        }

        public void Insert(PermisoBE objeto)
        {
            objeto.DVH = CalcularVerificacionHorizontal(objeto);

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                dao.Insert(objeto);
            }

            RegenerarDVV();
        }

        //public void RemoveComponent(PermisoBE padre, PermisoBE hijo)
        //{
        //    throw new NotImplementedException();
        //}

        public PermisoBE Select(PermisoBE Objeto)
        {
            using (DAL.PermisoDAL dal = new DAL.PermisoDAL("PRD"))
            {
               return  dal.Select(Objeto);
            }
        }

        public List<PermisoBE> SelectAllIndexado()
        {

            List<PermisoBE> lista;
            List<PermisoBE> lista2 = new List<PermisoBE>();

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                lista = this.SelectAll();
            }

            foreach (BE.PermisoBE item in lista)
            {
                if (item.PermisoBase == false)
                {
                    lista2.Add(this.SelectIndexado(item));
                }

            }

            return lista2;

        }
        public List<PermisoBE> SelectAllByID_Usuario(UsuarioBE _Usuario)
        {
            using (DAL.UsuarioPermisoDAL usuariopermiso = new DAL.UsuarioPermisoDAL("PRD"))
            {
                List<BE.UsuarioPermisoBE> listausuario = usuariopermiso.SelectAllByID_Usuario(_Usuario);

                List<PermisoBE> listadevolver = new List<PermisoBE>();

                foreach (BE.UsuarioPermisoBE item in listausuario)
                {
                    PermisoBE permisoabuscar = new PermisoBE();

                    permisoabuscar.Identidad = item.Permiso.Identidad;

                    using (DAL.PermisoDAL buscador = new DAL.PermisoDAL("PRD"))
                    {
                        listadevolver.Add(buscador.Select(permisoabuscar));
                    }

                }

                return listadevolver;

            }

        }
        public void Update(PermisoBE Objeto)
        {

            Objeto.DVH = CalcularVerificacionHorizontal(Objeto);

            using (DAL.PermisoDAL dal = new DAL.PermisoDAL("PRD"))
            {
                dal.Update(Objeto);

            }

            RegenerarDVV();
        }

        public List<PermisoBE> GenerarListaPlanaRol(PermisoBE permisoBE)
        {
            List<PermisoBE> unalista = new List<PermisoBE>();

            using (SEC.PermisoSEC sec = new PermisoSEC())
            {

            }

            foreach (PermisoBE item in permisoBE.Components)
            {
                AddNodeAndChildNodesToList(item, unalista);
            }

            return unalista;

        }

        public List<PermisoBE> SelectAll()
        {
            List<PermisoBE> lista;

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return lista = dao.SelectAll();
            }
        }

        public List<PermisoBE> GetRolesByUserID(UsuarioBE usuario)
        {
            List<PermisoBE> lista;

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return lista = dao.GetRolesByUserID(usuario);
            }
        }

        public void InsertRolNuevo(PermisoBE objeto)
        {
            objeto.DVH = CalcularVerificacionHorizontal(objeto);
            objeto.CreateDate = 0;
            objeto.ChangeDate = 0; 

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                dao.Insert(objeto);
            }

            RegenerarDVV();
        }

        public void UpdateRolExist(PermisoBE permisoBE)
        {
            permisoBE.DVH = CalcularVerificacionHorizontal(permisoBE);

            using (DAL.PermisoDAL dal = new DAL.PermisoDAL("PRD"))
            {
                dal.UpdateRolExist(permisoBE);

            }

            RegenerarDVV();
        }

        public List<PermisoBE> GetRolesByUserIDIndexado(UsuarioBE usuario)
        {
            List<PermisoBE> lista;
            List<PermisoBE> listadevolver = new List<PermisoBE>();

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                 lista = dao.GetRolesByUserID(usuario);

                foreach (PermisoBE item in lista)
                {
                    listadevolver.Add(SelectIndexado(item));
                }

            }
            return listadevolver;
        }


        public List<PermisoBE> GetPermisoBaseByUserID(UsuarioBE usuario)
        {
            List<PermisoBE> lista;

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return lista = dao.GetPermisoBaseByUserID(usuario);
            }
        }

        public void AgregarPermisoAUsuario(BE.PermisoBE permiso, BE.UsuarioBE usuario)
        {
            using (DAL.UsuarioPermisoDAL dao = new DAL.UsuarioPermisoDAL("PRD"))
            {

                BE.UsuarioPermisoBE nuevarelacion = new BE.UsuarioPermisoBE();
                nuevarelacion.Permiso.Identidad = permiso.Identidad;
                nuevarelacion.Usuario.Identidad = usuario.Identidad;

                dao.Insert(nuevarelacion);


            }

            if (permiso.Components.Count < 1)
            {

            }
            else
            {
                AddRecursiveGrant(permiso, usuario);
            }
        }

        public object obtenerSoloGruposSinUsuario(UsuarioBE usuario)
        {
            List<PermisoBE> lista;

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return lista = dao.GetAllRolesNotAssignedToUser(usuario);
            }
        }

        public object obtenerSoloPermisosSinUsuario(UsuarioBE usuario)
        {
            List<PermisoBE> lista;

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                return lista = dao.GetAllPermisosNotAssignedToUser(usuario);
            }
        }

        public void QuitarPermisoAUsuario(BE.PermisoBE permiso, BE.UsuarioBE usuario)
        {

            PermisoBE permisodelete;

            Int64.TryParse(permiso.Identidad.ToString(), out long asd);
            if (asd == 0)
            {
                using (DAL.PermisoDAL dal = new DAL.PermisoDAL("PRD"))
                {
                    permisodelete = dal.PermisoSelectbyCodigo(permiso);

                    permisodelete = SelectIndexado(permisodelete);

                    using (DAL.UsuarioPermisoDAL dao = new DAL.UsuarioPermisoDAL("PRD"))
                    {

                        BE.UsuarioPermisoBE nuevarelacion = new BE.UsuarioPermisoBE();
                        nuevarelacion.Permiso.Identidad = permisodelete.Identidad;
                        nuevarelacion.Usuario.Identidad = usuario.Identidad;
                        dao.Delete(nuevarelacion);
                    }

                    if (permisodelete.Components.Count < 1)
                    {

                    }
                    else
                    {
                        DeleteRecursiveGrant(permiso, usuario);
                    }

                }
            }


        }

        public void AddRecursiveGrant(BE.PermisoBE permiso, BE.UsuarioBE usuario)
        {

            using (DAL.UsuarioPermisoDAL dao = new DAL.UsuarioPermisoDAL("PRD"))
            {
                BE.UsuarioPermisoBE nuevarelacion = new BE.UsuarioPermisoBE();
                nuevarelacion.Permiso.Identidad = permiso.Identidad;
                nuevarelacion.Usuario.Identidad = usuario.Identidad;

                dao.Insert(nuevarelacion);
            }


            foreach (BE.PermisoBE item in permiso.Components)
            {
                AddRecursiveGrant(item, usuario);
            }
        }

        public void DeleteRecursiveGrant(BE.PermisoBE permiso, BE.UsuarioBE usuario)
        {
            using (DAL.PermisoDAL dal = new DAL.PermisoDAL("PRD"))
            {
                permiso = dal.PermisoSelectbyCodigo(permiso);
            }


            using (DAL.UsuarioPermisoDAL dao = new DAL.UsuarioPermisoDAL("PRD"))
            {
                BE.UsuarioPermisoBE nuevarelacion = new BE.UsuarioPermisoBE();
                nuevarelacion.Permiso.Identidad = permiso.Identidad;
                nuevarelacion.Usuario.Identidad = usuario.Identidad;

                dao.Delete(nuevarelacion);
            }

            foreach (BE.PermisoBE item in permiso.Components)
            {
                DeleteRecursiveGrant(item, usuario);
            }
        }


        public PermisoBE SelectIndexado(PermisoBE Objeto)
        {
            BE.PermisoBE PermisoRoot;
            BE.PermisoBE permisotmp;

            using (DAL.PermisoDAL permiso = new DAL.PermisoDAL("PRD"))
            {
                PermisoRoot = permiso.Select(Objeto);

                using (DAL.RolPermisoDAL familiapatente = new DAL.RolPermisoDAL("PRD"))
                {
                    List<RolPermisoBE> listarelacion = familiapatente.SelectAllByID_Rol(Objeto);

                    foreach (RolPermisoBE relacion in listarelacion)
                    {

                        permisotmp = new PermisoBE();
                        permisotmp.Identidad = relacion.Permiso.Identidad;

                        BE.PermisoBE permisotmp2 = permiso.Select(permisotmp);

                        if (permisotmp2.PermisoBase == true)
                        {
                            Objeto.Components.Add(permisotmp2);
                        }
                        else
                        {
                            Objeto.Components.Add(SelectIndexado(permisotmp2));
                        }

                    }
                }

            }

            return Objeto;
        }

        public void GuardarPermiso(PermisoBE objeto)
        {
            List<RolPermisoBE> relaciones = new List<RolPermisoBE>();

            if (objeto.Codigo_Perfil.First().ToString() == "Z")
            {
                this.Insert(objeto);
            }



            if (objeto.Components.Count < 0)
            {

            }
            else
            {


                foreach (BE.PermisoBE item in objeto.Components)
                {

                    BE.RolPermisoBE relacion = new RolPermisoBE();
                    relacion.Rol.Identidad = objeto.Identidad;
                    relacion.Permiso.Identidad = item.Identidad;

                    relaciones.Add(relacion);
                }
            }

            if (relaciones.Count > 0)
            {
                foreach (RolPermisoBE item2 in relaciones)
                {

                    using (SEC.RolPermisoSEC rolpermisotmp = new SEC.RolPermisoSEC())
                    {
                        item2.DVH = rolpermisotmp.CalcularVerificacionHorizontal(item2);
                    }


                    using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
                    {
                        dao.Insert(item2);
                    }
                }

                RegenerarDVVFamiliaPatente();
            }
        }



        public PermisoBE SelectIndexadoHijos(PermisoBE Padre, PermisoBE Hijo)
        {
            BE.PermisoBE PermisoRoot;
            BE.PermisoBE permisotmp;

            using (DAL.PermisoDAL permiso = new DAL.PermisoDAL("PRD"))
            {
                PermisoRoot = permiso.Select(Hijo);

                using (DAL.RolPermisoDAL familiapatente = new DAL.RolPermisoDAL("PRD"))
                {
                    if (PermisoRoot.PermisoBase == false)
                    {
                        List<RolPermisoBE> listarelacion = familiapatente.SelectAllByID_Rol(PermisoRoot);

                        foreach (RolPermisoBE relacion in listarelacion)
                        {

                            permisotmp = new PermisoBE();
                            permisotmp.Identidad = relacion.Permiso.Identidad;

                            PermisoRoot.Components.Add(SelectIndexado(permisotmp));
                        }
                    }
                    else
                    {
                        Padre.Components.Add(SelectIndexado(PermisoRoot));
                    }
                }
            }
            return Padre;
        }

        public void GuardarRelacion(BE.RolPermisoBE relacion)
        {

            using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
            {

                using (SEC.RolPermisoSEC rolpermisotmp = new SEC.RolPermisoSEC())
                {
                    relacion.DVH = rolpermisotmp.CalcularVerificacionHorizontal(relacion);
                }

                dao.Insert(relacion);
            }

            RegenerarDVVFamiliaPatente();

        }

        public void BorrarRelacion(BE.RolPermisoBE relacion)
        {

            using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
            {

                dao.Delete(relacion);
            }

            RegenerarDVVFamiliaPatente();

        }

        public void RegenerarDVV()
        {
            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                List<BE.PermisoBE> clientList = new List<BE.PermisoBE>();

                using (DAL.PermisoDAL daousuario = new DAL.PermisoDAL("PRD"))
                {
                    clientList = daousuario.SelectAll();
                }
                List<INTF.IVerificable> list = clientList.Select(c => (INTF.IVerificable)c).ToList();



                using (DAL.DVVDAL BE = new DAL.DVVDAL("PRD"))
                {
                    BE.DVVBE nuevodvv = new BE.DVVBE();

                    List<BE.DVVBE> listadvvs = BE.SelectAll();

                    listadvvs.First(x => x.Descripcion == "Permiso").Digito_Verificador = ObtenerDvv(list);

                    BE.Update(listadvvs.First(x => x.Descripcion == "Permiso"));
                }

            }
        }

        public void RegenerarDVVFamiliaPatente()
        {
            using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
            {
                List<BE.RolPermisoBE> clientList = new List<BE.RolPermisoBE>();

                using (DAL.RolPermisoDAL daousuario = new DAL.RolPermisoDAL("PRD"))
                {
                    clientList = daousuario.SelectAll();
                }
                List<INTF.IVerificable> list = clientList.Select(c => (INTF.IVerificable)c).ToList();



            }
        }


        public List<PermisoBE> GenerarListaPlana(UsuarioBE usuario)
        {
            List<PermisoBE> unalista = new List<PermisoBE>();

            using (SEC.PermisoSEC sec = new PermisoSEC())
            {
                
            }

            foreach (PermisoBE item in usuario.permisos)
            {
                AddNodeAndChildNodesToList(item, unalista);
            }

            return unalista;

        }


        public void AddNodeAndChildNodesToList(PermisoBE node, List<PermisoBE> lista)
        {

            lista.Add(node);    // Adding current nodename to ListBox     

            foreach (PermisoBE actualNode in node.Components)
            {
                AddNodeAndChildNodesToList(actualNode, lista); // recursive call
            }
        }

        public void Dispose()
        { 
        }

        public void RecalcularDvhTotal()
        {
            try
            {
                //IList<UsuarioBE> ususarios = new UsuarioBLL().SelectAllIList();

                Type t = typeof(PermisoBE);

                RecalcularDvhTotalBase(t);
            }
            catch (Exception e)
            {

            }
           

        }
        public void GenerarDvv()
        {
            try
            {
                Type t = typeof(BE.PermisoBE);

                CalcularDvvBase(t);
            }
            catch (Exception e)
            {
              
            }
        }

        public void UpdateSinDVH(PermisoBE Objeto)
        {
            using (DAL.PermisoDAL dal = new DAL.PermisoDAL("PRD"))
            {
                dal.Update(Objeto);

            }
        }

        public List<PermisoBE> CalcularPermisosQuePermitenDelete(UsuarioBE usuario)
        {
            List<PermisoBE> lista;
           
            List<PermisoBE> listadevolver = new List<PermisoBE>();

            using (DAL.PermisoDAL dao = new DAL.PermisoDAL("PRD"))
            {
                lista = dao.GetRolesByUserID(usuario);
                

            }
            return listadevolver;
        }

        public void AssingPermisoARol(BE.PermisoBE padre, BE.PermisoBE hijo)
        {

            BE.RolPermisoBE nuevogrant = new BE.RolPermisoBE() { Rol = padre, Permiso = hijo };

            using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
            {

                using (SEC.RolPermisoSEC rolpermisotmp = new SEC.RolPermisoSEC())
                {
                    nuevogrant.DVH = rolpermisotmp.CalcularVerificacionHorizontal(nuevogrant);
                }

                dao.Insert(nuevogrant);
            }

            RegenerarDVVFamiliaPatente();

        }

        public void RevokePermisoARol(BE.PermisoBE padre, BE.PermisoBE hijo)
        {

            BE.RolPermisoBE nuevogrant = new BE.RolPermisoBE() { Rol = padre, Permiso = hijo };

            using (DAL.RolPermisoDAL dao = new DAL.RolPermisoDAL("PRD"))
            {


                dao.Delete(nuevogrant);
            }

            RegenerarDVVFamiliaPatente();

        }

    }
}
