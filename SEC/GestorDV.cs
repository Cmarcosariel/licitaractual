﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace SEC
{
    public class GestorDV : VerificarSEC, IDisposable
    {
        private static GestorDV _instance;
#pragma warning disable IDE0052 // Remove unread private members
        //private bool todook;
#pragma warning restore IDE0052 // Remove unread private members

        public GestorDV() { }

        public static GestorDV GetInstance()
        {
            if (_instance == null)
            {
                _instance = new GestorDV();
                return _instance;
            }
            else return _instance;

        }

        public void Dispose()
        {
        }

        public bool CalcularIntegridadDB()
        {
            List<string> ListaErrores = new List<string>();

            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly assembly in assemblies)
            {

                foreach (Type t in assembly.GetTypes())
                {
                    if (t.IsSubclassOf(typeof(VerificarSEC)))
                    {
                        string typeNameLimpio = "";

                        Type objectType = t;
                        object testInstance = Activator.CreateInstance(t);
                        MethodInfo listarMethod = objectType.GetMethod("SelectAll");
                        List<BE.DVVBE> listadvv;

                        using (DAL.DVVDAL BE = new DAL.DVVDAL("PRD"))
                        {
                            listadvv = BE.SelectAll();
                        }

                        if (listarMethod != null)
                        {

                            var lista = (IEnumerable<object>)listarMethod.Invoke(testInstance, null);
                            List<INTF.IVerificable> Listaitemsmal = new List<INTF.IVerificable>();

                            foreach (INTF.IVerificable item in lista)
                            {


                                string typeName = t.Name;

                                if (typeName.Contains("BLL") || typeName.Contains("SEC"))
                                {
                                    typeNameLimpio = typeName.Replace("BLL", "");
                                    typeNameLimpio = typeNameLimpio.Replace("SEC", "");
                                }
                                else
                                {
                                    typeNameLimpio = typeName;
                                }



                                if (item.DVH == SEC.GestorDV.GetInstance().CalcularVerificacionHorizontal(item))
                                {

                                }
                                else
                                {

                                    ListaErrores.Add(string.Format("Error DVH en Tabla {0} , Registro {1} - Valor Esperado: {2} - Valor Encontrado {3}", typeNameLimpio.ToString(), item.GetType().GetProperty("Identidad").GetValue(item), SEC.GestorDV.GetInstance().CalcularVerificacionHorizontal(item), item.DVH));

                                }
                            }



                            List<INTF.IVerificable> verificables = new List<INTF.IVerificable>();

                            foreach (INTF.IVerificable item in lista)
                            {
                                verificables.Add(item);
                            }

                            string typeName2 = t.Name;
                            string typeNameLimpio2 = "";

                            if (typeName2.Contains("BLL") || typeName2.Contains("SEC"))
                            {
                                typeNameLimpio2 = typeName2.Replace("BLL", "");
                                typeNameLimpio2 = typeNameLimpio2.Replace("SEC", "");
                            }
                            else
                            {
                                typeNameLimpio2 = typeName2;
                            }


                            if (ObtenerDvv(verificables) != listadvv.First(x => x.Descripcion == typeNameLimpio2.ToString()).Digito_Verificador)
                            {
                                ListaErrores.Add(string.Format("Error DVV en Tabla {0} - Valor Esperado: {1} - Valor Encontrado {2}", typeNameLimpio2.ToString(), ObtenerDvv(verificables), listadvv.First(x => x.Descripcion == typeNameLimpio2.ToString()).Digito_Verificador));
                            }

                            //ListaErrores.Add(string.Format("Error DVV en Tabla {0} - Valor Esperado: {1} - Valor Encontrado {2}", typeNameLimpio2.ToString(), ObtenerDvv(verificables), listadvv.First(x => x.Descripcion == typeNameLimpio2.ToString()).Digito_Verificador));

                        }
                        else
                        {

                        }

                    }
                }
            }


            if (ListaErrores.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void CambioDia()
        {
            DateTime asd = DateTime.Now.Date;

            using (DAL.DVVDAL dVVDAL = new DAL.DVVDAL("PRD"))
            {
                dVVDAL.CambioDia(asd);
            }
        }


    }
}

