﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace SEC
{
    public class BitacoraSEC : IDisposable
    {


        public List<BE.BitacoraBE> SelectAll()
        {
            using (DAL.BitacoraDAL bitacora = new DAL.BitacoraDAL("PRD"))
            {
                return bitacora.SelectAll();
            }
        }


        //public DateTime FechaHora { get; set; }
        //public string Mensaje { get; set; }
        //public int TipoEvento { get; set; }

        public void GrabarBitacora(BE.BitacoraBE objeto)
        {
            using (DAL.BitacoraDAL dao = new DAL.BitacoraDAL("PRD"))
            {

                dao.Insert(objeto);
            }

        }

        public void Dispose()
        {
        }

        //public void RegenerarDVV()
        //{
        //    using (DAL.BitacoraDAL dao = new DAL.BitacoraDAL("PRD"))
        //    {
        //        List<BE.BitacoraBE> clientList = new List<BE.BitacoraBE>();

        //        using (DAL.BitacoraDAL daousuario = new DAL.BitacoraDAL("PRD"))
        //        {
        //            clientList = daousuario.SelectAll();
        //        }
        //        List<INTF.IVerificable> list = clientList.Select(c => (INTF.IVerificable)c).ToList();



        //        using (DAL.DVVDAL dto = new DAL.DVVDAL("PRD"))
        //        {
        //            BE.DVVBE nuevodvv = new BE.DVVBE();

        //            List<BE.DVVBE> listadvvs = dto.SelectAll();

        //            listadvvs.First(x => x.Descripcion == "Bitacora").Digito_Verificador = this.ObtenerDvv(list);

        //            dto.Update(listadvvs.First(x => x.Descripcion == "Bitacora"));
        //        }

        //    }
        //}

        public List<string> GetCategoriasEventoBitacora()
        {
            using (DAL.Evento_BitacoraDAL dal = new DAL.Evento_BitacoraDAL("PRD"))
            {
                return dal.GetCategoriasEventoBitacora();
            }
        }

        //public int Insert(BE.BitacoraBE objeto)
        //{
        //    throw new NotImplementedException();
        //}

        //public int Delete(BE.BitacoraBE objeto)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(BE.BitacoraBE objeto)
        {
            //objeto.DVH = CalcularVerificacionHorizontal(objeto);

            using (DAL.BitacoraDAL dal = new DAL.BitacoraDAL("PRD"))
            {
                dal.Update(objeto);
            }

            //RegenerarDVV();
        }

        //public void RecalcularDvhTotal()
        //{
        //    try
        //    {

        //        Type t = typeof(BE.BitacoraBE);

        //        RecalcularDvhTotalBase(t);
        //    }
        //    catch (Exception e)
        //    {

        //    }


        //}
        //public void GenerarDvv()
        //{
        //    try
        //    {
        //        Type t = typeof(BE.BitacoraBE);

        //        CalcularDvvBase(t);
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //}

        public void UpdateSinDVH(BE.BitacoraBE objeto)
        {
            using (DAL.BitacoraDAL dal = new DAL.BitacoraDAL("PRD"))
            {
                dal.Update(objeto);
            }
        }

        public Evento_BitacoraBE GetSeveridadEvento(Evento_BitacoraBE eventonuevo)
        {
            using (DAL.Evento_BitacoraDAL dal = new DAL.Evento_BitacoraDAL("PRD"))
            {
                return dal.GetSeveridadEvento(eventonuevo);
            }
        }
    }
}
