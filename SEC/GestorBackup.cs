﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC
{
    public class GestorBackup : IDisposable
    {
        public void Dispose()
        {
        }

        public void TomarBackup(BE.BackupBE backup)
        {

            using (SEC.GestorDV secgestor = GestorDV.GetInstance())
            {
                if (secgestor.CalcularIntegridadDB() == true)
                {
                    using (DAL.BackupDAL gestor = new DAL.BackupDAL("PRD"))
                    {

                        try
                        {
                            gestor.TomarBackup(backup);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
            }
                else
            {
                //COMO AVISO DEL ERROR ???? Messagebox.Show pero en webform
            }

        }




        }

        public void RestaurarBackup(BE.BackupBE backup)
        {
            using (DAL.BackupDAL gestor = new DAL.BackupDAL("MASTER"))
            {
                gestor.RestaurarBackup(backup);
            }
        }


        public List<BE.BackupBE> SelectAll()
        {
            using (DAL.BackupDAL gestor = new DAL.BackupDAL("PRD"))
            {
                return gestor.SelectAll();
            }
        }

        //public bool chequearBase()
        //{
        //    DAL.BackupDAL _unBackUpDAL = new DAL.BackupDAL("PRD");
        //    return _unBackUpDAL.chequearBase(); ;
        //}

        //public int instalarBase()
        //{
        //    DAL.BackupDAL _unBackUpDAL = new DAL.BackupDAL("PRD");
        //    return _unBackUpDAL.instalarBase(); ;
        //}

    }
}
