﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;

namespace SEC
{
    public class Encriptacion
    {

        private static byte[] _key { get { return Convert.FromBase64String(ConfigurationManager.AppSettings["Key1"]); } }
        private static byte[] _IV { get { return Convert.FromBase64String(ConfigurationManager.AppSettings["Key2"]); } }


        private static Encriptacion _encriptacion { get; set; }

        public static Encriptacion GetInstance()
        {
            if (_encriptacion == null)
            {
                _encriptacion = new Encriptacion();
            }

            return _encriptacion;
        }

        public static string EncriptarHash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static string Encriptar(string str)
        {
            string result = string.Empty;
            using (Aes aes = Aes.Create())
            {
                // Set custom symmetric algorithm
                aes.Key = _key;
                aes.IV = _IV;
                // Encrypt the string to an array of bytes.
                result = GetAesEncrypt(str, aes.Key, aes.IV);
            }
            return result;
        }
        private static string GetAesEncrypt(string str, byte[] key, byte[] IV)
        {
            string result;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = IV;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(str);
                        }
                        result = Convert.ToBase64String(msEncrypt.ToArray());
                    }
                }
            }
            return result;
        }
    }
}
