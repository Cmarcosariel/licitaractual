﻿using INTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SEC
{
    public abstract class VerificarSEC
    {

        //Este metodo sirve para GENERAR el DVH
        public string CalcularVerificacionHorizontal(IVerificable entidad)
        {
            string dvh = string.Empty;

            try
            {
                Type fieldsType = entidad.GetType();
                IEnumerable<PropertyInfo> fields = fieldsType.GetRuntimeProperties();

                foreach (PropertyInfo field in fields)
                {
                    dvh += CalcularPropiedad(entidad, field);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Encriptacion.EncriptarHash(dvh);
        }

        //Este metodo sirve para COMPARAR el DVH con el de la BD
        public bool VerificarHorizontal(IVerificable entidad, string dvh)
        {
            return CalcularVerificacionHorizontal(entidad) == dvh;
        }

        //Este metodo sirve para calcular el DVH propio de cada propiedad
        private string CalcularPropiedad(IVerificable entidad, PropertyInfo field)
        {
            int total = 0;
            string valor = string.Empty;
            try
            {
                var customAttributes = field.GetCustomAttributes();
                bool isNoVerificable = customAttributes.Any(x => x.GetType().Name == "NoVerificable");
                if (!isNoVerificable)
                {
                    //La propiedad es de tipo Entidad
                    if (typeof(IEntidad).IsAssignableFrom(field.PropertyType))
                    {
                        var _entidad = (field.GetValue(entidad) as IEntidad);
                        if (_entidad != null)
                        {
                            valor = _entidad.Identidad.ToString();
                        }
                    }
                    else
                    {
                        if (field.GetValue(entidad)  == null)
                        {
                            valor = "0";
                        }
                        else
                        {
                            valor = field.GetValue(entidad).ToString();
                        }

                    }

                    if (!string.IsNullOrEmpty(valor))
                    {
                        total += valor.GetHashCode();
                        valor = Encriptacion.EncriptarHash(total.ToString());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return valor;
        }

        //Este metodo sirve para Obtener el DVV de una lista de objetos pero no o guarda ni nada
        public string ObtenerDvv(IList<INTF.IVerificable> objetos)
        {
            string DVVsinhash = string.Empty;

            foreach (IVerificable item in objetos)
            {
                DVVsinhash += CalcularVerificacionHorizontal(item);
            }

            return Encriptacion.EncriptarHash(DVVsinhash).ToString();

        }



        DAL.DVVDAL digitoVerificadorDal = new DAL.DVVDAL("PRD");

        //Este metodo sirve para GENERAR el DVV de una lista de objetos y GUARDARLO EN LA DB
        public void CalcularDvvBase(Type t)
        {
            string typeName = t.Name;
            string typeNameLimpio = typeName.Replace("BE", "");
            Type objectType = Type.GetType("SEC." + typeNameLimpio + "SEC");

            MethodInfo listarMethod = objectType.GetMethod("SelectAll");
            object listarResultObj = listarMethod.Invoke(this, BindingFlags.Public, null, null, null);

            IList listarResult = (IList)listarResultObj;

            string dvvTotal = string.Empty;
            foreach (IVerificable item in listarResult)
            {
                dvvTotal += CalcularVerificacionHorizontal(item);
            }

            BE.DVVBE dvv = new BE.DVVBE();
            dvv.Descripcion = typeNameLimpio;
            dvv.Digito_Verificador = SEC.Encriptacion.EncriptarHash(dvvTotal);

            digitoVerificadorDal.Delete(dvv);
            digitoVerificadorDal.Insert(dvv);
        }

        public string ObtenerDvv(List<INTF.IVerificable> objetos)
        {
            string DVVsinhash = string.Empty;

            foreach (IVerificable item in objetos)
            {
                DVVsinhash += CalcularVerificacionHorizontal(item);
            }

            return Encriptacion.EncriptarHash(DVVsinhash).ToString();

        }

        public void RecalcularDvhTotalBase(Type t)
        {
            try
            {
                string typeName = t.Name;
                string typeNameLimpio = typeName.Replace("BE", "");
                Type objectType = Type.GetType("SEC." + typeNameLimpio + "SEC");

                IList listaObjetos = ObtenerListaPorEntidad(objectType);

                foreach (INTF.IVerificable item in ObtenerListaPorEntidad(objectType))
                {
                    item.DVH = CalcularVerificacionHorizontal(item);

                    MethodInfo listarMethod = objectType.GetMethod("UpdateSinDVH");
                    object listarResultObj = listarMethod.Invoke(this, BindingFlags.Public, null, new object[] { item }, null);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList ObtenerListaPorEntidad(Type entidadType)
        {
            IList listarResult;
            try
            {
                MethodInfo listarMethod = entidadType.GetMethod("SelectAll");
                object listarResultObj = listarMethod.Invoke(this, BindingFlags.Public, null, null, null);

                listarResult = (IList)listarResultObj;

            }
            catch (Exception e)
            {
                throw e;
            }

            return listarResult;
        }
    }
}
