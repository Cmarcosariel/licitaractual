﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class RolPermisoBE : INTF.IVerificable, INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the RolPermisoBE class.
        ///// </summary>
        //public RolPermisoBE()
        //{
        //}

        /// <summary>
        /// Initializes a new instance of the RolPermisoBE class.
        /// </summary>


        /// <summary>
        /// Initializes a new instance of the RolPermisoBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the DVH value.
        /// </summary>
        public string DVH { get; set; }

        /// <summary>
        /// Gets or sets the ID_Rol value.
        /// </summary>
        public PermisoBE Rol { get; set; }

        /// <summary>
        /// Gets or sets the ID_Permiso value.
        /// </summary>
        public PermisoBE Permiso { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>
        public long ChangeDate { get; set; }

        #endregion
    }
}
