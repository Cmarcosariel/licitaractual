﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class DVVBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DVVBE class.
        /// </summary>
        public DVVBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the DVVBE class.
        /// </summary>
        public DVVBE(string descripcion, string digito_Verificador)
        {
            this.Descripcion = descripcion;
            this.Digito_Verificador = digito_Verificador;
        }

        /// <summary>
        /// Initializes a new instance of the DVVBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the Digito_Verificador value.
        /// </summary>
        public string Digito_Verificador { get; set; }

        #endregion
    }
}
