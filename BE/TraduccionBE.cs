﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class TraduccionBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the TraduccionBE class.
        /// </summary>
        public TraduccionBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the TraduccionBE class.
        /// </summary>


        /// <summary>
        /// Initializes a new instance of the TraduccionBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Traduccion value.
        /// </summary>
        public string Traduccion { get; set; }

        /// <summary>
        /// Gets or sets the ID_Mensaje value.
        /// </summary>
        public MensajeBE Mensaje { get; set; }

        /// <summary>
        /// Gets or sets the ID_Idioma value.
        /// </summary>
        public IdiomaBE Idioma { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>
        public long ChangeDate { get; set; }

        #endregion
    }
}
