﻿using System;
using System.Collections.Generic;

namespace BE
{
    public class UsuarioBE : INTF.IVerificable, INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the UsuarioBE class.
        /// </summary>
        public UsuarioBE()
        {
            permisos = new List<PermisoBE>();

        }

        public UsuarioBE(string usuario,string pass)
        {
            Clave = pass;
            NombreUsuario = usuario;

        }

        /// <summary>
        /// Initializes a new instance of the UsuarioBE class.
        /// </summary>


        /// <summary>
        /// Initializes a new instance of the UsuarioBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        [NoVerificable()]
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the NombreUsuario value.
        /// </summary>
        public string NombreUsuario { get; set; }

        /// <summary>
        /// Gets or sets the NombrePersona value.
        /// </summary>
        public string NombrePersona { get; set; }

        /// <summary>
        /// Gets or sets the ApellidoPersona value.
        /// </summary>
        public string ApellidoPersona { get; set; }

        /// <summary>
        /// Gets or sets the Clave value.
        /// </summary>
        public string Clave { get; set; }

        /// <summary>
        /// Gets or sets the LoginFails value.
        /// </summary>
        public int LoginFails { get; set; }

        /// <summary>
        /// Gets or sets the IsBlocked value.
        /// </summary>
        public bool IsBlocked { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>

        public bool DELETED { get; set; }

        /// <summary>
        /// Gets or sets the DVH value.
        /// </summary>
        [NoVerificable()]
        public string DVH { get; set; }

        /// <summary>
        /// Gets or sets the ID_Idioma value.
        /// </summary>
        [NoVerificable()]
        public IdiomaBE Idioma { get; set; }

        /// <summary>
        /// Gets or sets the ID_Organizacion value.
        /// </summary>
        public OrganizacionBE Organizacion { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        /// 
        [NoVerificable()]
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>

        [NoVerificable()]
        public long ChangeDate { get; set; }

        public List<BE.PermisoBE> permisos;


        #endregion
    }
}
