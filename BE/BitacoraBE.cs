﻿using INTF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BitacoraBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the BitacoraBE class.
        /// </summary>
        public BitacoraBE()
        {
            Evento_Bitacora = new Evento_BitacoraBE();
        }

        /// <summary>
        /// Initializes a new instance of the BitacoraBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>

        [NoVerificable()]
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Timestamp value.
        /// </summary>

        public long Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the ID_Evento_Bitacora value.
        /// </summary>
        public Evento_BitacoraBE Evento_Bitacora { get; set; }

        /// <summary>
        /// Gets or sets the DVH value.
        /// </summary>
        [NoVerificable()]
        public string DVH { get; set; }

        /// <summary>
        /// Gets or sets the ID_Usuario value.
        /// </summary>


        public UsuarioBE Usuario { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>

        [NoVerificable()]
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>

        [NoVerificable()]
        public long ChangeDate { get; set; }

        #endregion
    }
}
