﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class ItemsOfertaBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ItemsOfertaBE class.
        /// </summary>
        public ItemsOfertaBE()
        {
            Item = new ItemBE();

        }

        public ItemsOfertaBE(ItemBE miItem, OfertaBE miOferta)
        {
            Item = miItem;

        }

        /// <summary>
        /// Initializes a new instance of the ItemsOfertaBE class.
        /// </summary>
        //public ItemsOfertaBE(long iD_Item, long iD_Oferta, decimal precioUnitario)
        //{
        //    this.ID_Item = iD_Item;
        //    this.ID_Oferta = iD_Oferta;
        //    this.PrecioUnitario = precioUnitario;
        //}

        ///// <summary>
        ///// Initializes a new instance of the ItemsOfertaBE class.
        ///// </summary>
        //public ItemsOfertaBE(long identidad, long iD_Item, long iD_Oferta, decimal precioUnitario)
        //{
        //    this.Identidad = identidad;
        //    this.Item = iD_Item;
        //    this.ID_Oferta = iD_Oferta;
        //    this.PrecioUnitario = precioUnitario;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the ID_Item value.
        /// </summary>
        public ItemBE Item { get; set; }

        /// <summary>
        /// Gets or sets the ID_Oferta value.
        /// </summary>

        /// <summary>
        /// Gets or sets the PrecioUnitario value.
        /// </summary>
        public decimal PrecioUnitario { get; set; }

        #endregion
    }
}
