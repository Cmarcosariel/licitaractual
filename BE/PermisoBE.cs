﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class PermisoBE : INTF.IVerificable, INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the PermisoBE class.
        /// </summary>
        public PermisoBE()
        {
            Components = new List<PermisoBE>();
        }

        /// <summary>
        /// Initializes a new instance of the PermisoBE class.
        /// </summary>




        /// <summary>
        /// Initializes a new instance of the PermisoBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        [NoVerificable()]
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the PermisoBase value.
        /// </summary>
        public bool PermisoBase { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the Codigo_Perfil value.
        /// </summary>
        public string Codigo_Perfil { get; set; }

        /// <summary>
        /// Gets or sets the DVH value.
        /// </summary>
        [NoVerificable()]
        public string DVH { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>
        public bool DELETED { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        [NoVerificable()]
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>
        [NoVerificable()]
        public long ChangeDate { get; set; }


        public List<PermisoBE> Components;
        #endregion
    }
}
