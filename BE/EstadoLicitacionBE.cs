﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public enum EstadoLicitacionBE
    {

        Borrador = 1,
        Programada = 2,
        Abierta = 3,
        Cerrada = 4,
        Cancelada = 5

    }
}
