﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Clase_ItemBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Clase_ItemBE class.
        /// </summary>
        public Clase_ItemBE()
        {
            Items = new List<ItemBE>();
            Propiedades = new List<PropiedadItemBE>();
        }

        /// <summary>
        /// Initializes a new instance of the Clase_ItemBE class.
        /// </summary>
        //public Clase_ItemBE(string descripcion, bool dELETED)
        //{
        //    this.Descripcion = descripcion;
        //    this.DELETED = dELETED;
        //}

        ///// <summary>
        ///// Initializes a new instance of the Clase_ItemBE class.
        ///// </summary>
        //public Clase_ItemBE(long identidad, string descripcion, bool dELETED)
        //{
        //    this.identidad = identidad;
        //    this.descripcion = descripcion;
        //    this.dELETED = dELETED;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>
        /// 

        public List<BE.ItemBE> Items { get; set; }

        public List<PropiedadItemBE> Propiedades { get; set; }

        #endregion
    }
}
