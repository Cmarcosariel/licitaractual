﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class UsuarioPermisoBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the UsuarioPermisoBE class.
        /// </summary>
        public UsuarioPermisoBE()
        {
            Permiso = new PermisoBE();
            Usuario = new UsuarioBE();
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the ID_Usuario value.
        /// </summary>
        public UsuarioBE Usuario { get; set; }

        /// <summary>
        /// Gets or sets the ID_Permiso value.
        /// </summary>
        public PermisoBE Permiso { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>
        public bool DELETED { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>
        public long ChangeDate { get; set; }

        #endregion
    }
}
