﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class IdiomaBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the IdiomaBE class.
        /// </summary>
        public IdiomaBE()
        {
            mensajes = new List<TraduccionBE>();
        }

        /// <summary>
        /// Initializes a new instance of the IdiomaBE class.
        /// </summary>
        public IdiomaBE(string descripcion, long createDate, long changeDate)
        {
            this.Descripcion = descripcion;
            this.CreateDate = createDate;
            this.ChangeDate = changeDate;
        }

        /// <summary>
        /// Initializes a new instance of the IdiomaBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>
        public long ChangeDate { get; set; }

        public List<TraduccionBE> mensajes;
        #endregion
    }
}
