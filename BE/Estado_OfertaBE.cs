﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public enum Estado_OfertaBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Estado_OfertaBE class.
        /// </summary>
        /// 
        Borrador = 1,
        Presentada = 2,
        Aceptada = 3,
        Rechazada = 4,
        Vencida = 5,
        Cancelada = 6
        #endregion
    }
}
