﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class OrganizacionBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the OrganizacionBE class.
        /// </summary>
        public OrganizacionBE()
        {
            RubrosSuscriptos = new List<RubroBE>();
        }

        /// <summary>
        /// Initializes a new instance of the OrganizacionBE class.
        /// </summary>
        //public OrganizacionBE(long createDate, long changeDate)
        //{
        //    this.CreateDate = createDate;
        //    this.ChangeDate = changeDate;
        //}

        /// <summary>
        /// Initializes a new instance of the OrganizacionBE class.
        /// </summary>


        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the CUIT value.
        /// </summary>
        public string CUIT { get; set; }

        /// <summary>
        /// Gets or sets the Razon_Social value.
        /// </summary>
        public string Razon_Social { get; set; }

        /// <summary>
        /// Gets or sets the Domicilio value.
        /// </summary>
        public string Domicilio { get; set; }

        /// <summary>
        /// Gets or sets the Email value.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Telefono value.
        /// </summary>
        public string Telefono { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>

        /// <summary>
        /// Gets or sets the Tipo_Organizacion value.
        /// </summary>
        //public long Tipo_Organizacion { get; set; }

        public List<BE.RubroBE> RubrosSuscriptos { get; set; }

        #endregion
    }

}
