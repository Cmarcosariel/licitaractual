﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{

    public class LicitacionBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the LicitacionBE class.
        /// </summary>
        public LicitacionBE()
        {
            Organizacion = new OrganizacionBE();
            Rubro = new RubroBE();
            Pliego = new PliegoBE();
            Estado_Licitacion = new EstadoLicitacionBE();
        }

        public LicitacionBE(OrganizacionBE org, RubroBE rub, EstadoLicitacionBE estado)
        {
            Organizacion = org;
            Rubro = rub;
            Estado_Licitacion = estado;
        }

        /// <summary>
        /// Initializes a new instance of the LicitacionBE class.
        /// </summary>
        //public LicitacionBE(long iD_Estado_Licitacion, string descripcion, long iD_Rubro, DateTime fechaApertura, DateTime fechaCierre, string observaciones, string pliego, long iD_Organizacion)
        //{
        //    this.ID_Estado_Licitacion = iD_Estado_Licitacion;
        //    this.Descripcion = descripcion;
        //    this.ID_Rubro = iD_Rubro;
        //    this.FechaApertura = fechaApertura;
        //    this.FechaCierre = fechaCierre;
        //    this.Observaciones = observaciones;
        //    this.Pliego = pliego;
        //    this.ID_Organizacion = iD_Organizacion;
        //}

        ///// <summary>
        ///// Initializes a new instance of the LicitacionBE class.
        ///// </summary>
        //public LicitacionBE(long identidad, long iD_Estado_Licitacion, string descripcion, long iD_Rubro, DateTime fechaApertura, DateTime fechaCierre, string observaciones, string pliego, long iD_Organizacion)
        //{
        //    this.Identidad = identidad;
        //    this.Estado_Licitacion = iD_Estado_Licitacion;
        //    this.Descripcion = descripcion;
        //    this.ID_Rubro = iD_Rubro;
        //    this.FechaApertura = fechaApertura;
        //    this.FechaCierre = fechaCierre;
        //    this.Observaciones = observaciones;
        //    this.Pliego = pliego;
        //    this.Organizacion = iD_Organizacion;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the ID_Estado_Licitacion value.
        /// </summary>
        public EstadoLicitacionBE Estado_Licitacion { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the ID_Rubro value.
        /// </summary>
        public RubroBE Rubro { get; set; }

        /// <summary>
        /// Gets or sets the FechaApertura value.
        /// </summary>
        public DateTime FechaApertura { get; set; }

        /// <summary>
        /// Gets or sets the FechaCierre value.
        /// </summary>
        public DateTime FechaCierre { get; set; }

        /// <summary>
        /// Gets or sets the Observaciones value.
        /// </summary>
        public string Observaciones { get; set; }

        /// <summary>
        /// Gets or sets the Pliego value.
        /// </summary>
        public PliegoBE Pliego { get; set; }

        /// <summary>
        /// Gets or sets the ID_Organizacion value.
        /// </summary>
        public OrganizacionBE Organizacion { get; set; }

        #endregion
    }
}
