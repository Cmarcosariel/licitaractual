﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class ItemBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ItemBE class.
        /// </summary>
        public ItemBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ItemBE class.
        /// </summary>
        public ItemBE(string descripcion, string observaciones)
        {
            this.Descripcion = descripcion;
            this.Observaciones = observaciones;
        }

        /// <summary>
        /// Initializes a new instance of the ItemBE class.
        /// </summary>
        public ItemBE(long identidad, string descripcion, string observaciones)
        {
            this.Identidad = identidad;
            this.Descripcion = descripcion;
            this.Observaciones = observaciones;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the Observaciones value.
        /// </summary>
        public string Observaciones { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>

        //public string Imagen { get; set; }

        #endregion
    }
}
