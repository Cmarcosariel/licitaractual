﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Evento_BitacoraBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Evento_BitacoraBE class.
        /// </summary>
        public Evento_BitacoraBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the Evento_BitacoraBE class.
        /// </summary>
        public Evento_BitacoraBE(string descripcion, int severidad)
        {
            this.Descripcion = descripcion;
            this.Severidad = severidad;
        }

        /// <summary>
        /// Initializes a new instance of the Evento_BitacoraBE class.
        /// </summary>

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Gets or sets the Severidad value.
        /// </summary>
        public int Severidad { get; set; }

        #endregion
    }
}
