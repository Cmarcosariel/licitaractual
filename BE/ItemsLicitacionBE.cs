﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class ItemsLicitacionBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ItemsLicitacionBE class.
        /// </summary>
        public ItemsLicitacionBE()
        {
            Item = new ItemBE();
            //Licitacion = new LicitacionBE();

        }


        public ItemsLicitacionBE(ItemBE miItem, LicitacionBE miLicitacion)
        {
            Item = miItem;
            //Licitacion = miLicitacion;

        }

        /// <summary>
        /// Initializes a new instance of the ItemsLicitacionBE class.
        /// </summary>
        //public ItemsLicitacionBE(long iD_Licitacion, long iD_Item, decimal precioUnitario, float cantidad)
        //{
        //    this.ID_Licitacion = iD_Licitacion;
        //    this.ID_Item = iD_Item;
        //    this.PrecioUnitario = precioUnitario;
        //    this.Cantidad = cantidad;
        //}

        ///// <summary>
        ///// Initializes a new instance of the ItemsLicitacionBE class.
        ///// </summary>
        //public ItemsLicitacionBE(long identidad, long iD_Licitacion, long iD_Item, decimal precioUnitario, float cantidad)
        //{
        //    this.Identidad = identidad;
        //    this.ID_Licitacion = iD_Licitacion;
        //    this.Item = iD_Item;
        //    this.PrecioUnitario = precioUnitario;
        //    this.Cantidad = cantidad;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        //public long Identidad { get; set; }

        ///// <summary>
        ///// Gets or sets the ID_Licitacion value.
        ///// </summary>
        //public LicitacionBE Licitacion { get; set; }

        /// <summary>
        /// Gets or sets the ID_Item value.
        /// </summary>
        public ItemBE Item { get; set; }

        /// <summary>
        /// Gets or sets the PrecioUnitario value.
        /// </summary>
        //public decimal PrecioUnitario { get; set; }

        /// <summary>
        /// Gets or sets the Cantidad value.
        /// </summary>
        public decimal Cantidad { get; set; }

        #endregion
    }
}
