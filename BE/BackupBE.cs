﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BackupBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the BackupBE class.
        /// </summary>
        public BackupBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the BackupBE class.
        /// </summary>
        public BackupBE(string filepath, long timestamp, long createDate, long changeDate)
        {
            this.Filepath = filepath;
            this.Timestamp = timestamp;
            this.CreateDate = createDate;
            this.ChangeDate = changeDate;
        }

        /// <summary>
        /// Initializes a new instance of the BackupBE class.
        /// </summary>

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Filepath value.
        /// </summary>
        public string Filepath { get; set; }

        /// <summary>
        /// Gets or sets the Timestamp value.
        /// </summary>
        public long Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the CreateDate value.
        /// </summary>
        public long CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ChangeDate value.
        /// </summary>
        public long ChangeDate { get; set; }

        #endregion
    }
}
