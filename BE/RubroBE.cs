﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class RubroBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the RubroBE class.
        /// </summary>
        public RubroBE()
        {
            Clases = new List<Clase_ItemBE>();
        }

        /// <summary>
        /// Initializes a new instance of the RubroBE class.
        /// </summary>
        //public RubroBE(bool dELETED, string descripcion)
        //{
        //    this.DELETED = dELETED;
        //    this.Descripcion = descripcion;
        //}

        ///// <summary>
        ///// Initializes a new instance of the RubroBE class.
        ///// </summary>
        //public RubroBE(long identidad, bool dELETED, string descripcion)
        //{
        //    this.identidad = identidad;
        //    this.dELETED = dELETED;
        //    this.descripcion = descripcion;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the DELETED value.
        /// </summary>

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        public List<BE.Clase_ItemBE> Clases { get; set; }  

        #endregion
    }

}
