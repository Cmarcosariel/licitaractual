﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{

    public class OfertaBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the OfertaBE class.
        /// </summary>
        public OfertaBE()
        {
            Organizacion = new OrganizacionBE();
            Estado_Oferta = Estado_OfertaBE.Borrador;
            items = new List<ItemsOfertaBE>();
        }
        public OfertaBE(OrganizacionBE org, LicitacionBE li, Estado_OfertaBE estado)
        {
            Organizacion = org;
            Estado_Oferta = estado;
        }


        /// <summary>
        /// Initializes a new instance of the OfertaBE class.
        /// </summary>
        //public OfertaBE(DateTime fechaPresentacion, long iD_Estado_Oferta, decimal total, long iD_Organizacion, long iD_Licitacion)
        //{
        //    this.FechaPresentacion = fechaPresentacion;
        //    this.ID_Estado_Oferta = iD_Estado_Oferta;
        //    this.Total = total;
        //    this.ID_Organizacion = iD_Organizacion;
        //    this.ID_Licitacion = iD_Licitacion;
        //}

        ///// <summary>
        ///// Initializes a new instance of the OfertaBE class.
        ///// </summary>
        //public OfertaBE(long identidad, DateTime fechaPresentacion, long iD_Estado_Oferta, decimal total, long iD_Organizacion, long iD_Licitacion)
        //{
        //    this.Identidad = identidad;
        //    this.fechaPresentacion = fechaPresentacion;
        //    this.iD_Estado_Oferta = iD_Estado_Oferta;
        //    this.total = total;
        //    this.iD_Organizacion = iD_Organizacion;
        //    this.iD_Licitacion = iD_Licitacion;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the FechaPresentacion value.
        /// </summary>
        public DateTime FechaPresentacion { get; set; }

        /// <summary>
        /// Gets or sets the ID_Estado_Oferta value.
        /// </summary>
        public Estado_OfertaBE Estado_Oferta { get; set; }

        /// <summary>
        /// Gets or sets the Total value.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Gets or sets the ID_Organizacion value.
        /// </summary>
        public OrganizacionBE Organizacion { get; set; }

        /// <summary>
        /// Gets or sets the ID_Licitacion value.
        /// </summary>
        /// 

        public List<ItemsOfertaBE> items { get; set; }

        #endregion
    }
}
