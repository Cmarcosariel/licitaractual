﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class PliegoBE
    {
        public PliegoBE()
        {
            ItemsLicitacion = new List<ItemsLicitacionBE>();
        }



        public List<BE.ItemsLicitacionBE> ItemsLicitacion { get; set; }


        public string DetallesEntrega { get; set; }

    }
}
