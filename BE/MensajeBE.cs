﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class MensajeBE : INTF.IEntidad
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MensajeBE class.
        /// </summary>
        public MensajeBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the MensajeBE class.
        /// </summary>
        public MensajeBE(string descripcion)
        {
            this.Descripcion = descripcion;
        }

        /// <summary>
        /// Initializes a new instance of the MensajeBE class.
        /// </summary>

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        #endregion
    }
}
