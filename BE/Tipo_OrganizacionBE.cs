﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Tipo_OrganizacionBE
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Tipo_OrganizacionBE class.
        /// </summary>
        public Tipo_OrganizacionBE()
        {
        }

        /// <summary>
        /// Initializes a new instance of the Tipo_OrganizacionBE class.
        ///// </summary>
        //public Tipo_OrganizacionBE(string descripcion)
        //{
        //    this.Descripcion = descripcion;
        //}

        ///// <summary>
        ///// Initializes a new instance of the Tipo_OrganizacionBE class.
        ///// </summary>
        //public Tipo_OrganizacionBE(long identidad, string descripcion)
        //{
        //    this.identidad = identidad;
        //    this.descripcion = descripcion;
        //}

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Identidad value.
        /// </summary>
        public long Identidad { get; set; }

        /// <summary>
        /// Gets or sets the Descripcion value.
        /// </summary>
        public string Descripcion { get; set; }

        #endregion
    }
}
