﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTF
{
    public interface ICRUD<T>
    {
        void Insert(T objeto);
        void Delete(T objeto);
        T Select(T objeto);
        List<T> SelectAll();
        void Update(T objeto);

    }
}
